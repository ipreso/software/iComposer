<?php

// Show errors
error_reporting (E_ALL|E_STRICT);
ini_set ('display_errors', 1);

// Define Timezone
date_default_timezone_set ('Europe/Paris');

// Set paths of the framework
set_include_path ('.'
    . PATH_SEPARATOR . '../library'
    . PATH_SEPARATOR . '../application/models/'
    . PATH_SEPARATOR . 'uploader/'
    . PATH_SEPARATOR . get_include_path ());

//=============================================================================
// INCLUDE OPERATIONS
require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';
require_once 'Zend/Auth/Storage/Session.php';
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Controller/Front.php';
require_once 'Zend/Controller/Response/Http.php';
require_once 'Zend/Db.php';
require_once 'Zend/Db/Table.php';
require_once 'Zend/Form.php';
require_once 'Zend/Json.php';
require_once 'Zend/Layout.php';
require_once 'Zend/Locale.php';
require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Db.php';
require_once 'Zend/Registry.php';
require_once 'Zend/Translate.php';
require_once 'Zend/Session.php';

require_once 'Zend/Amf/Server.php';

require_once 'Actions/Command.php';
require_once 'Actions/Commands.php';
require_once 'Actions/Property.php';
require_once 'Actions/Properties.php';

require_once 'Admin/Group.php';
require_once 'Admin/Groups.php';
require_once 'Admin/Logs.php';
require_once 'Admin/Permissions.php';
require_once 'Admin/Resources.php';
require_once 'Admin/User.php';
require_once 'Admin/Users.php';
require_once 'Admin/Agreement.php';
require_once 'Admin/Agreements.php';

require_once 'Form/Login.php';

require_once 'Media/Plugin.php';
require_once 'Media/Plugins.php';
require_once 'Media/Properties.php';
require_once 'Media/Property.php';

require_once 'Players/Box.php';
require_once 'Players/Boxes.php';
require_once 'Players/Criticities.php';
require_once 'Players/Downloads.php';
require_once 'Players/Group.php';
require_once 'Players/Groups.php';
require_once 'Players/License.php';
require_once 'Players/Licenses.php';
require_once 'Players/Logs.php';
require_once 'Players/Programs.php';

require_once 'Progs/Channel.php';
require_once 'Progs/Channels.php';
require_once 'Progs/Event.php';
require_once 'Progs/Events.php';
require_once 'Progs/Item.php';
require_once 'Progs/Layout.php';
require_once 'Progs/Layouts.php';
require_once 'Progs/Manifest.php';
require_once 'Progs/Manifests.php';
require_once 'Progs/Playlist.php';
require_once 'Progs/Playlists.php';
require_once 'Progs/Program.php';
require_once 'Progs/Programs.php';
require_once 'Progs/Schedules.php';
require_once 'Progs/Schedule.php';
require_once 'Progs/Zone.php';

// Get the Registry
$registry = Zend_Registry::getInstance ();



//=============================================================================
// DATABASE
$config = new Zend_Config_Ini ('../application/config.ini');
$registry->set ('Config_Ini_Db', $config->database);
$registry->set ('Config_Ini_Upload', $config->upload);
$registry->set ('Config_Ini_Licenses', $config->licenses);
$registry->set ('Config_Ini_Plugins', $config->plugins);

// If website is disabled, return HTTP 503 !
if ($config->global->enable == 0)
{
    header ("HTTP/1.0 503 ");
    return;
}

$db = Zend_Db::factory ($config->database);
Zend_Db_Table::setDefaultAdapter ($db);
$registry->set ('Zend_Db', $db);



//=============================================================================
// CRITICITIES
$criticities = new Players_Criticities ();
$registry->set ('Players_Criticities', $criticities);


//=============================================================================
// MVC ARCHITECTURE
$frontController = Zend_Controller_Front::getInstance ();
$frontController->throwExceptions (true);
$frontController->setControllerDirectory ('../application/controllers');

$response = new Zend_Controller_Response_Http;
$response->setHeader ('Content-Type', 'text/html; charset=utf-8');
$frontController->setResponse ($response);

// Define paths of layout files
Zend_Layout::startMvc (array ('layoutPath' => '../application/layouts'));



//=============================================================================
// TRACES
// - Write to DB
$logsColumns = array ('timestamp' => 'timestamp',
                      'lvl' => 'priority', 
                      'msg' => 'message',
                      'who' => 'user');
$writerDB = new Zend_Log_Writer_Db ($db, 'Admin_logs', $logsColumns);

// - Log adapter
$logger = new Zend_Log ();
$logger->addWriter ($writerDB);
// - Setting priority
$filter = new Zend_Log_Filter_Priority (Zend_Log::DEBUG);
$logger->addFilter ($filter);
// - Saving it in the registry
$registry->set('Zend_Log', $logger);


//=============================================================================
// START USER SESSION
Zend_Session::start();

//=============================================================================
// AUTHENTICATION
$users = new Admin_Users ();
if (Zend_Auth::getInstance ()->hasIdentity ())
{
    // Get the connected user
    $user = $users->findLogin (Zend_Auth::getInstance ()->getIdentity ());
}
else
    $user = NULL;

$registry->set ('App_User', $user);

if ($user == NULL)
    $user_ID = 0;
else
{
    $user_ID = $user->getId ();
    $permissions = new Admin_Permissions ();
    $permissions->filterGroup ($user->getGroup ()->getId ());
    $registry->set ('Group_permissions', $permissions);
}

$logger->setEventItem ('user', $user_ID);

//=============================================================================
// TRANSLATIONS
$translate = new Zend_Translate ('gettext', '../application/languages/french.mo', 'fr');
$translate->addTranslation ('../application/languages/english.mo', 'en');
$translate->setOptions (array ('disableNotices' => TRUE));

try
{
    $locale = new Zend_Locale (Zend_Locale::BROWSER);
}
catch (Zend_Locale_Exception $e)
{
    $locale = new Zend_Locale ('en_US');
}
$defaultLang = $locale->getLanguage ();
if ($user)
{
    $lang = $user->getLanguage ();
    if ($lang == NULL || strcmp ($lang, "auto") == 0)
        $lang = $defaultLang;
}
else
    $lang = $defaultLang;

$translate->setLocale ($lang);
$registry->set ('Zend_Translate', $translate);


//=============================================================================
// Go go go !
try
{
    $frontController->dispatch ();
}
catch (Zend_Controller_Dispatcher_Exception $e)
{
    header ("HTTP/1.0 404 Not Found");
}
catch (Zend_Controller_Action_Exception $e)
{
    header ("HTTP/1.0 404 Not Found");
}
