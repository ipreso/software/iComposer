window.addEvent('domready', function() {

    var updatePluginsList = new Request.JSON ({
        url: '/Administration/getplugins/format/json',
        method: 'post',
        onComplete: function (jsonObj) {
            doUpdateTree (jsonObj);
        }
    });

    var doUpdateTree = function (jsonObj) {

        if (jsonObj == null || jsonObj.count < 1)
            return;

        $('pluginsList').empty ();
        $('cmdsList').empty ();

        if (jsonObj.Cmd)
            addInCmds (jsonObj.Cmd);
        if (jsonObj.Media)
            addInPlugins (jsonObj.Media);
    };

    var addInPlugins = function (plugins) {

        for (var i = 0 ; i < plugins.length ; i++)
        {
            var plugName = plugins[i].name;
            var plugLabel = plugins[i].label;
            if (plugins [i].enable)
                liClass = 'enabled';
            else
                liClass = 'disabled';

            var liElement = new Element ('li', {id: 'p-'+plugName});
            var handle = new Element ('div',
                    {
                        id: 'hp-'+plugName,
                        'class': 'status ' + liClass
                    });

            liElement.innerHTML = plugLabel;
            liElement.innerHTML += ' ('+plugins[i].version+')';

            handle.inject (liElement, 'top');
            $('pluginsList').adopt (liElement);

            $('hp-'+plugName).addEvent ('click', function (e) {
                e.stop ();

                var request = new Request.JSON ({

                    url: '/Administration/swap/format/json',
                    method: 'post',
                    data: {'id': this.id},
                    onComplete: function (jsonObj) {
                        doUpdateTree (jsonObj);
                    }
                }).send ();
            });
        }
    };

    var addInCmds = function (cmds) {

        for (var i = 0 ; i < cmds.length ; i++)
        {
            var cmdName = cmds[i].name;
            var cmdLabel = cmds[i].label;
            if (cmds [i].enable)
                liClass = 'enabled';
            else
                liClass = 'disabled';

            var handle = new Element ('div',
                    {
                        id: 'hc-'+cmdName,
                        'class': 'status ' + liClass
                    });

            var liElement = new Element ('li', {id: 'c-'+cmdName});

            liElement.innerHTML = cmdLabel;
            liElement.innerHTML += ' ('+cmds[i].version+')';

            handle.inject (liElement, 'top');
            $('cmdsList').adopt (liElement);

            $('hc-'+cmdName).addEvent ('click', function (e) {
                e.stop ();

                var request = new Request.JSON ({

                    url: '/Administration/swap/format/json',
                    method: 'post',
                    data: {'id': this.id},
                    onComplete: function (jsonObj) {
                        doUpdateTree (jsonObj);
                    }
                }).send ();
            });
        }
    };

    updatePage = function () {
        updatePluginsList.send ();
    };

    updatePage ();
});
