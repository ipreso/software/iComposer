/*
 * Mootools Tree Component - MIT-style License
 * Author: christoph.pojer@gmail.com
 *
 * Original idea by http://blog.inquirylabs.com/
 */

Selectors.Pseudo.uid = function(uid){
	return $uid(this)==uid;
};

var Tree = new Class({
	
	droppables: [],
	draggables: [],
	padding: 0,
	
	initialize: function(el){
		this.el = $(el);
		
		this.padding = this.el.getStyle('padding-left').toInt();
		
		this.draggables = this.el.getElements('li');
		this.droppables = this.el.getElements('li span').addEvent('click', function(e){
			e.stop();
		});
		
		this.mouseup = (function(){
			if(this.clone) this.clone.destroy();
		}).bind(this);
		
		document.addEvent('mouseup', this.mouseup);
		
		var self = this;
		$$(this.draggables).addEvent('mousedown', function(e){
			e.stop();
			var elem = this;
			
			self.clone = elem.clone().store('parent', $uid(elem)).setStyles({
				position: 'absolute',
				opacity: 0.8,
				zIndex: 50
			}).addClass('drag').setStyles(Hash.getValues(e.page).map(function(v){ return v+16; }).associate(['left', 'top'])).inject(document.body);
			
			self.clone.makeDraggable({
				droppables: this.droppables,
				
				onLeave: self.hideIndicator.bind(self),
				
				onDrag: function(el, e){
					if(!e) return;
					
					e.target = $(e.target);
					if(!e.target) return;
					
					var droppable = e.target.get('tag')=='li' ? e.target : e.target.getParent('li');
					if(!droppable || !droppable.getParent('ul.tree')) return;
					
					var c = droppable.getCoordinates(),
						elementCenter = c.top+(c.height/2);
						isSubnode = e.page.x>c.left+self.padding,
						pos = {
							x: c.left+(isSubnode ? self.padding : 0),
							y: c.top+c.height
						};
					
					if($uid(droppable)==el.retrieve('parent') || droppable.getParent('li:uid('+el.retrieve('parent')+')'))
						self.dropTarget = null;
					else if(e.page.y>=elementCenter)
						self.setDropTarget(droppable, 'after', isSubnode);
					else if(e.page.y<elementCenter){
						pos.y -= c.height;
						pos.x = c.left;
						self.setDropTarget(droppable, 'before');
					}
					
					if(self.dropTarget) self.showIndicator(pos);
					else self.hideIndicator();
				},
				
				onDrop: function(el, droppable){
					el.destroy();
					self.hideIndicator();
					
					if(!self.dropTarget) return;
					
					if(self.dropSubnode) elem.inject(self.dropTarget.getElement('ul') || new Element('ul').inject(self.dropTarget), 'bottom');
					else elem.inject(self.dropTarget, self.dropWhere || 'after');

                    /**
                     * iPreso added the following line
                     */
                    self.el.fireEvent ('change');
				}
			}).start(e);
		});
		
	},
	
	setDropTarget: function(el, where, subnode){
		this.dropTarget = el;
		this.dropWhere = where;
		this.dropSubnode = subnode;
	},
	
	createIndicator: function(){
		this.indicator = new Element('div', {
			'class': 'treeIndicator',
			styles: {
				position: 'absolute',
				opacity: 0
			}
		}).inject(document.body);
	},
	
	showIndicator: function(pos){
		if(!this.indicator) this.createIndicator();
		
		this.indicator.setStyles({
			opacity: 1,
			left: pos.x,
			top: (pos.y-this.indicator.getSize().y/2)
		});
	},
	
	hideIndicator: function(){
		if(!this.indicator) return;
		
		this.indicator.set('opacity', 0);
	},
	
	serialize: function(fn, base){
		if(!fn) fn = function(el){ return el.get('id'); };
		if(!base) base = this.el;
		
		var result = {};
		
		Array.each(base.getChildren('li'), function(el){
			var ul = el.getElement('ul');
			result[fn(el)] = ul ? this.serialize(fn, ul) : 1;
		}, this);
		
		return result;
	}
	
});
