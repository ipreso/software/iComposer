window.addEvent('domready', function() {
    const units = ['B', 'KiB', 'MiB', 'GiB', 'TiB'];
    var uploadStartedAt = 0;
    var refresh = 2000;
    var count = 0;
    var currentUpload;
    var pendingUpload = null;
    var uploadMaxSize = 0;

    try {
      uploadMaxSize = parseInt($("uploadForm").dataset.maxSize || 0, 10);
    } catch(e) {
      console.error("Invalid upload max size");
    }

    var delRow = function (id)
    {
        if ($('input-'+id) && $('input-'+id).getParent ())
            $('input-'+id).getParent ().destroy ();
        if ($('row-'+id))
            $('row-'+id).destroy ();
    }

    var addNewRow = function (fileId, fileName)
    {
        var newRow = new Element ('div', {'class':'fileRow',
                                          'id':'row-'+fileId});

        var divIco      = new Element ('div', {'class':'rowLeft'});
        var divFile     = new Element ('div', {'class':'rowMiddle'});
        var divLinks    = new Element ('div', {'class':'rowRight'});

        var spanIco     = new Element ('span', {'class':'upThumb'});
        divIco.grab (spanIco);

        var spanFile    = new Element ('span', {'class':'upFile',
                                                'text': fileName});
        divFile.grab (spanFile);
        var divProgress= new Element ('div', {'class':'rowProgress'});
        divFile.grab (divProgress);
        var spanStatus  = new Element ('span', {'class':'rowStatus'});
        divFile.grab (spanStatus);

        var divProgTotal = new Element ('div', {'class':'rowProgressBar'});
        var divProgDone  = new Element ('div', {'class':'rowProgressBarDone'});
        divProgress.grab (divProgTotal);
        divProgTotal.grab (divProgDone);
        divProgTotal.hide ();

        var spanDel     = new Element ('span', {'class':'upLinkDel'});
        var hrefDel     = new Element ('a', {'text':deleteString});
        spanDel.grab (hrefDel);
        spanDel.addEvent ('click', function (e) {
            e.stop ();
            delRow (this.getParent ('.fileRow').id.substr (4));
        });

        var spanCancel  = new Element ('span', {'class':'upLinkCancel'});
        var hrefCancel  = new Element ('a', {'text':cancelString});
        spanCancel.grab (hrefCancel);
        spanCancel.hide ();
        spanCancel.addEvent ('click', function (e) {
            e.stop ();
            if (pendingUpload != null)
            {
                pendingUpload.abort();
                pendingUpload = null;
                updateStatusColor (currentUpload, '#aaaa00');
                updateStatus (currentUpload, abortedString);
            }
        });
        divLinks.adopt (spanDel, spanCancel);

        newRow.adopt (divIco, divLinks, divFile);

        $('uploadList').grab (newRow);
    };

    var addNewFile = function (e) {
        e.stop ();

        var fileId = randomString ();

        this.set ('id', 'input-'+fileId);
        this.set ('name', 'input-'+fileId);

        var newInputSpan = new Element ('span', {'class':'fileInput'});
        var newInputFile = new Element ('input',{'type':'file',
                                                 'name':'newfile',
                                                 'id':'newfile'});
        newInputSpan.grab (newInputFile);
        $('fileAdd').grab (newInputSpan);
        $('newfile').addEvent ('change', addNewFile);

        addNewRow (fileId, this.value);

        if (uploadMaxSize > 0 && this.files[0].size > uploadMaxSize) {
          const maxSizeUnits = getUnitsInfo(uploadMaxSize);
          const uploadMaxSizeInUnits = convertToUnits(maxSizeUnits, uploadMaxSize);
          updateStatusColor (fileId, '#cc0000');
          updateStatus (fileId, [
              getLabel("file-too-big", "File is too big, it should be less than: "),
              uploadMaxSizeInUnits,
              " ",
              maxSizeUnits.units
          ].join(""));
          if (this.getParent ('span.fileInput')) {
             this.getParent ('span.fileInput').destroy ();
          }
        }
    };

    var getUnitsInfo = function (value) {
      const factor = Math.floor(Math.log2(value) / 10);
      return {
        factor: factor,
        units: units[factor]
      };
    }

    var convertToUnits = function (unitsInfo, value) {
        return (value / Math.pow(2, unitsInfo.factor * 10))
            .toFixed(unitsInfo.factor > 0 ? 2 : 0);
    }

    var convertFromSeconds = function (seconds) {
        let hours = Math.floor(seconds / 3600);
        let min = Math.floor((seconds % 3600) / 60);
        let sec = seconds - (hours * 3600) - (min * 60);

        if (hours < 10)
            hours = "0" + hours;
        if (min < 10)
            min = "0" + min;
        if (sec < 10)
            sec = "0" + sec;

        return [hours, min, sec].join(":");
    }

    var updateProgressBar = function (id, loaded, total) {
        var bar = $('row-'+id);
        if (!bar)
            return (false);

        const unitsInfo = getUnitsInfo(total);
        const totalInUnits = convertToUnits(unitsInfo, total);
        const loadedInUnits = convertToUnits(unitsInfo, loaded);
        const speed = Math.floor(1000 * loaded / (Date.now() - uploadStartedAt));
        const speedUnitsInfo = getUnitsInfo(speed);
        const speedInUnits = convertToUnits(speedUnitsInfo, speed);
        const percentage = Math.floor(100 * loaded / total);
        const eta = convertFromSeconds(Math.floor((total - loaded) / speed));

        const progressString = [
            loadedInUnits,
            " / ",
            totalInUnits,
            " ",
            unitsInfo.units,
            " (",
            percentage,
            "%) - ",
            getLabel("average-speed", "Average speed: "),
            speedInUnits,
            " ",
            speedUnitsInfo.units,
            "/s - ",
            getLabel("eta", "ETA: "),
            eta
        ].join("");

        updateStatus (id, progressString);
        var done = bar.getElement ('.rowProgressBarDone');
        done.setStyle ('width', percentage+'%');
    };

    var updateStatus = function (id, statusStr) {
        var str = $('row-'+id).getElement ('.rowStatus');
        str.set ('text', statusStr);
    };

    var updateStatusColor = function (id, color) {
        var str = $('row-'+id).getElement ('.rowStatus');
        str.setStyle ('color', color);

        var done = $('row-'+id).getElement ('.rowProgressBarDone');
        done.setStyle ('background-color', color);
    };

    var getLabel = function (name, defaultValue) {
      const e = $("label-" + name);

      if (e && e.innerText) {
        return e.innerText;
      }

      return defaultValue;
    }

    var uploadFiles = function () {
      if (pendingUpload != null) {
        console.error("An upload is already pending");
        return;
      }

      var inputs = $('fileAdd').getElements ('input');
      var i = 0;
      while (i < inputs.length && inputs [i].id == 'newfile')
          i++;

      if (i < inputs.length)
      {
          var postId = randomString();
          var formData = new FormData();
          var inputSpan = inputs [i].getParent ('span.fileInput');
          var input = inputs [i].dispose ();
          var fileId = input.id.substr (6);
          currentUpload = fileId;
          if (inputSpan)
              inputSpan.destroy ();

          $('row-'+fileId).getElement ('.upLinkDel').hide ();
          $('row-'+fileId).getElement ('.upLinkCancel').show ();
          $('row-'+fileId).getElement ('.rowProgressBar').show ();

          pendingUpload = new XMLHttpRequest();
          pendingUpload.open('POST', '/Catalog/handlefileupload', true);
          pendingUpload.upload.addEventListener("progress", (e) => {
              updateProgressBar(currentUpload, e.loaded, e.total);
          });

          const onFinish = (e) => {
            try {
              // Try to parse the result if it has been correctly sent by backend
              const result = JSON.parse(pendingUpload.responseText);
              updateStatus(fileId, result.status);
              updateStatusColor(fileId, result.color);

              $('row-'+fileId).getElement ('.rowProgressBarDone').setStyle ('width', '100%');
            } catch (error) {
              updateStatusColor (currentUpload, '#cc0000');
              updateStatus (currentUpload, "Error:" + pendingUpload.responseText);
            }

            $('row-'+fileId).getElement ('.upLinkCancel').hide ();
            $('row-'+fileId).getElement ('.upLinkDel').show ();

            pendingUpload = null;
            currentUpload = "";

            // Start uploading the next file
            uploadFiles();
          };

          pendingUpload.addEventListener("load", onFinish);
          pendingUpload.addEventListener("error", onFinish);


          formData.append("UPLOAD_IDENTIFIER", postId);
          formData.append(input.id, input.files[0]);

          uploadStartedAt = Date.now();
          pendingUpload.send(formData);
      }
    }

    $('uploadBClean').addEvent ('click', function (e) {

        var rows = $('uploadList').getChildren ('.fileRow');
        for (var i = 0 ; i < rows.length ; i++)
        {
            rows [i].destroy ();
        }

        var inputs = $('fileAdd').getChildren ('.fileInput');
        for (var i = 0 ; i < inputs.length ; i++)
        {
            if (!inputs [i].getElement ('#newfile'))
                inputs [i].destroy ();
        }
    });

    $('newfile').addEvent ('change', addNewFile);
    $('uploadBUp').addEvent ('click', function (e) {
        e.stop ();
        uploadFiles ()
    });

    $('uploadBClose').addEvent ('click', function (e) {

        e.stop ();
        self.close ();

    });

    function randomString() {
        var chars = "0123456789ABCDEF";
        var string_length = 10;
        var randomstring = '';
        for (var i=0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
        }
        return (randomstring);
    }
});
