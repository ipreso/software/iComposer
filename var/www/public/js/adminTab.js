window.addEvent('domready', function() {

    var sortables;
    var usersList = new Array;
    var permsAccordion;

    if ($('editGroup'))
        $('editGroup').hide ();
    if ($('addUser'))
        $('addUser').hide ();
    if ($('editUser'))
        $('editUser').hide ();
    if ($('addGroup'))
        $('addGroup').show ();

    var updateRequest = function ()
    {
        // Update group list
        if ($('groups'))
        {
            var request = new Request.JSON ({
                    url: '/Administration/listgroups/format/json',
                    method: 'post',
                    onComplete: function (jsonObj) {
                        updateGroups (jsonObj);
                    }
            }).send ();
        }
    }

    if ($('groupsHierarchy'))
    {
        var request = new Request.JSON ({
                url: '/Administration/gethierarchy/format/json',
                method: 'post',
                onComplete: function (jsonObj) {
                    updateHierarchy (jsonObj);
                }
        }).send ();
    }

    var updateNewOrder = function () {

        var getUserId = function (el) {
            return (el.id.substring (2));
        };

        // Get groups
        var groups = $('groups').getChildren ();

        var resultArray = new Array ();

        var sortableIndex = 0;
        for (var i = 0 ; i < groups.length ; i++)
        {
            if (!groups [i].hasClass ('droppable'))
                continue;

            var newGroup = new Object ();
            newGroup.id = groups[i].id.substring (3);
            newGroup.users = sortables.serialize (sortableIndex++, getUserId);
            resultArray.push (newGroup);
        }

        var sendChange = new Request.JSON ({
            url : '/Administration/updateusersgroups/format/json',
            method: 'post',
            data: { 'users' : JSON.encode (resultArray) },
            onComplete: function (jsonObj) {
                if (jsonObj.status == 1)
                    updateGroups (jsonObj.groups);
                else
                {
                    alert (jsonObj.msg);
                    updateRequest ();
                }
            }
        }).send ();
    };

    var updateHierarchy = function (groupsObj) {

        if (groupsObj == null || groupsObj.count < 1)
            return;

        var parentNode = $('groupsHierarchy');
        if (!parentNode)
            return;

        parentNode.empty ();
        parentNode.removeEvent ('change', updateHierarchyEvent);

        for (var i = 0 ; i < groupsObj.length ; i++)
            addChildToNode (parentNode, groupsObj [i]);

        /* http://cpojer.net/blog/MooTools_Tree_Components */
        sortables = new Tree ('groupsHierarchy', 
                            {
                                checkDrag: function (element) {
                                   return (!element.hasClass ('nodrag'));
                                },
                                checkDrop: function (element, options){
                                    return (!element.hasClass ('nodrop'));
                                }
                            });

        parentNode.addEvent ('change', updateHierarchyEvent);
    };

    var updateHierarchyEvent = function ()
    {
        var sendChange = new Request.JSON ({
            url : '/Administration/updatehierarchy/format/json',
            method: 'post',
            data: { 'perms' : JSON.encode (sortables.serialize ()) },
            onSuccess: function (jsonObj) {
                updateHierarchy (jsonObj);
                var groupId = $('groupIdPerms').get ('value');
                if (groupId)
                    getGroupPermissions (groupId);
            }
        }).send ();
    };

    var addChildToNode = function (parentNode, groupObj)
    {
        var li  = new Element ('li',    {'id'   : 'gr-' + groupObj.group.id,
                                         'class': 'groupSortable'});

        var span = new Element ('span', {'text' : groupObj.group.groupname});
        li.adopt (span);
        parentNode.adopt (li);

        if (groupObj.children.length > 0)
        {
            var ul = new Element ('ul');
            li.adopt (ul);
            for (var i = 0 ; i < groupObj.children.length ; i++)
                addChildToNode (ul, groupObj.children [i]);
        }

        span.addEvent ('click', function (e) {

            e.stop ();
            var groupId = this.getParent ().get ('id').substr (3);
            getGroupPermissions (groupId);

        });
    };

    var getGroupPermissions = function (groupId) {
        new Request.JSON ({
            url : '/Administration/getperms/format/json',
            method: 'post',
            data: { 'grId' : groupId },
            onSuccess: function (jsonObj) {
                showPermissions (jsonObj);
            }
        }).send ();
    }

    var showPermissions = function (jsonObj)
    {
        if (!$('editPerms'))
            return;

        if (!jsonObj)
            return;

        var node = $('editPerms');
        node.hide ();
        node.empty ();

        var title = new Element ('div',
                        {'text': jsonObj.groupname});
        title.setStyle ('font-weight', 'bold');
        title.setStyle ('margin-top', '10px');
        title.setStyle ('margin-bottom', '10px');
        title.setStyle ('width', '100%');
        title.setStyle ('text-align', 'center');

        var groupIdElt = new Element ('input', {'id':       'groupIdPerms',
                                                'name':     'groupIdPerms',
                                                'type':     'hidden',
                                                'value':    jsonObj.groupid});

        node.adopt (title);
        node.adopt (groupIdElt);

        for (var type in jsonObj.permissions)
            addNewPermSlide (type, jsonObj.permissions [type], jsonObj.groupadmin);

        var buttons = new Element ('div');
        buttons.setStyle ('font-weight', 'bold');
        buttons.setStyle ('margin-top', '10px');
        buttons.setStyle ('margin-bottom', '10px');
        buttons.setStyle ('width', '100%');
        buttons.setStyle ('text-align', 'center');
        node.adopt (buttons);

        if (jsonObj.groupadmin)
        {
            var bOk = new Element ('input', {
                                        'id':       'bOk',
                                        'name':     'bOk',
                                        'type':     'submit',
                                        'value':    strSave
                                            });
            var bMaxPerms = new Element ('input', {
                                        'id':       'bMax',
                                        'name':     'bMax',
                                        'type':     'submit',
                                        'value':    strMaxPerms
                                            });
            buttons.adopt (bMaxPerms, bOk);
        }
        var bCancel = new Element ('input', {
                                    'id':       'bCancel',
                                    'name':     'bCancel',
                                    'type':     'submit',
                                    'value':    strCancel
                                        });
        buttons.adopt (bCancel);

        bCancel.addEvent ('click', function (e) {
            e.stop ();
            $('editPerms').hide ();
        });

        if ($('bMax'))
        {
            bMaxPerms.addEvent ('click', function (e) {
                e.stop ();
                
                new Request.JSON ({
                    url : '/Administration/setmaxperms/format/json',
                    method: 'post',
                    data: { 'group': $('groupIdPerms').get ('value')},
                    onSuccess: function (jsonObj) {
                        if (!jsonObj || jsonObj.status != '1')
                            alert (jsonObj.msg);

                        var groupId = $('groupIdPerms').get ('value');
                        getGroupPermissions (groupId);
                    }
                }).send ();
            });
        }

        if ($('bOk'))
        {
            bOk.addEvent ('click', function (e) {
                e.stop ();

                var formProperties = new Array ();
                var chkList = $$('.permChk');
                for (var i = 0 ; i < chkList.length ; i++)
                {
                    var prop = new Array ();
                    prop [0] = chkList [i].get ('name');
                    prop [1] = chkList [i].get ('checked');
                    formProperties [i] = prop;
                }
                new Request.JSON ({
                    url : '/Administration/saveperms/format/json',
                    method: 'post',
                    data: { 'group':    $('groupIdPerms').get ('value'),
                            'permissions' : JSON.encode (formProperties)},
                    onSuccess: function (jsonObj) {
                        if (!jsonObj || jsonObj.status != '1')
                            alert (jsonObj.msg);

                        var groupId = $('groupIdPerms').get ('value');
                        getGroupPermissions (groupId);
                    }
                }).send ();
            });
        }

        permsAccordion = new Fx.Accordion ($$('h3.typeToggler'),
                                            $$('div.typeElements'),
                    {
                        display: -1,
                        opacity: false,
                        onActive: function (toggler, elt) {
                            toggler.setStyle('color', '#333333');
                        },
                        onBackground: function (toggler, elt) {
                            toggler.setStyle('color', '#888888');
                        }
                    });

        node.show ();
    };

    var addNewPermSlide = function (title, permObj, edit) {

        var permissions = $('editPerms');

        var toggler = new Element ('h3', { 'id':    'toggler_'+title,
                                           'class': 'typeToggler',
                                           'text':  title });

        var content = new Element ('div', { 'id':   'elt_'+title,
                                            'class':'typeElements' });

        permissions.adopt (toggler, content);

        for (var resId in permObj)
        {
            var olTitle = new Element ('div');
            olTitle.setStyle ('font-weight', 'bold');
            olTitle.setStyle ('margin-top', '5px');
            olTitle.setStyle ('margin-bottom', '5px');
            if (resId.length > 0)
                olTitle.set ('text', '- '+resId);
            else
                olTitle.set ('text', title);
            content.adopt (olTitle);

            var ol = new Element ('ol');
            ol.setStyle ('margin-bottom', '15px');
            content.adopt (ol);
            
            for (var resIdAction in permObj [resId])
            {
                //var ourId = 'chk-'+resId.replace (/\s+/g, '')+'-'+resIdAction.replace (/\s+/g, '');
                var ourId       = (permObj [resId])[resIdAction]['id'];
                var li          = new Element ('li');
                var label       = new Element ('label', {'for': ourId});
                var checkbox = new Element ('input', {
                                        'id':       ourId,
                                        'name':     ourId,
                                        'class':    'permChk',
                                        'type':     'checkbox'
                                                     });
            
                if (!edit)
                    checkbox.set ('disabled', true);

                label.adopt (checkbox);
                label.appendText (' '+resIdAction);
                li.adopt (label);
                ol.adopt (li);

                if ((permObj [resId])[resIdAction]['allowed'])
                    checkbox.set ('checked', true);
                else
                    checkbox.set ('checked', false);
            }
        }
    };

    var addUserToNode = function (parentNode, user) {

        var li      = new Element ('li',    {'id': 'u-'+user.id,
                                             'class': 'user'});
        var font    = new Element ('font',  {'id': 'ULabel'+user.id,
                                             'class': 'clickable',
                                             'text': user.lastname + ', '+user.firstname});
        var div     = new Element ('div',   {'id': 'handle-'+user.id,
                                             'class': 'drag-handle UIcon'});

        li.adopt (div);
        li.adopt (font);
        parentNode.adopt (li);
    };

    var updateGroups = function (groupsObj) {

        if (groupsObj == null || groupsObj.count < 1)
            return;

        var children = $('groups').getChildren ();

        // Remove or modify old groups
        for (var i = 0 ; i < children.length ; i++)
        {
            found = false;
            for (var j = 0 ; !found && j < groupsObj.length ; j++)
            {
                if (children[i].id == 'gr-'+groupsObj[j].group.id)
                {
                    found = true;
                    if ($('GrLabel'+groupsObj[j].group.id).get('text') != groupsObj[j].group.groupname)
                        $('GrLabel'+groupsObj[j].group.id).set('text', groupsObj[j].group.groupname);

                    // Check users...
                    for (var k = 0 ; k < groupsObj[j].users.length ; k++)
                    {
                        var liElement = $('u-'+groupsObj[j].users[k].id);
                        if (!liElement)
                        {
                            // Add a new user
                            var user = groupsObj[j].users[k];
                            var liElement = new Element ('li', {id: 'u-'+user.id});
                            liElement.innerHTML = '';
                            liElement.innerHTML += '<a id=\'delUser'+user.id+'\' href=\'#\' class=\'del\'></a>';        
                            liElement.innerHTML += '<font id=\'ULabel'+user.id+'\' class=\'clickable\'>'
                                                   + user.lastname + ', ' + user.firstname + '</font>';
                            var handle = new Element ('div', 
                                                        {id : 'handle-'+user.id, 
                                                         'class' : 'drag-handle UIcon'});
                            handle.inject (liElement, 'top');
                            $('users-'+groupsObj[j].group.id+'-ol').adopt (liElement);
                            liElement.highlight ();

                            $('ULabel'+user.id).addEvent ('click', function (e) {
                                e.stop ();

                                if ($('editGroup'))
                                    $('editGroup').hide ();
                                if ($('addUser'))
                                    $('addUser').hide ();
                                if ($('addGroup'))
                                    $('addGroup').hide ();
                                if ($('createUser'))
                                    $('createUser').set ('disabled', true);
                                if ($('applyUser'))
                                    $('applyUser').set ('disabled', false);
                                if ($('editUser'))
                                    $('editUser').show ();

                                var request = new Request.JSON ({
                                
                                    url: '/Administration/getuser/format/json',
                                    method: 'post',
                                    onComplete: function (jsonObj) {

                                        if ($('userGroup'))
                                        {
                                            $('userGroup').set ('value', jsonObj.groupName);
                                            $('userName').set ('value', jsonObj.login);
                                            $('userPass').set ('value', '');
                                            $('userFirstname').set ('value', jsonObj.firstname);
                                            $('userLastname').set ('value', jsonObj.lastname);
                                            $('userEmail').set ('value', jsonObj.email);
                                            $('userId').set ('value', jsonObj.id);
                                        }
                                    }
                                }).send ('id='+user.id);
                            });
                        }
                        else if (liElement.getParent().id != 'users-'+groupsObj[j].group.id+'-ol')
                            $('users-'+groupsObj[j].group.id+'-ol').adopt (liElement);
                    }

                    // Check for ghost-users
                    var usersChildren = $('users-'+groupsObj[j].group.id+'-ol').getChildren ();
                    for (var k = 0 ; k < usersChildren.length ; k++)
                    {
                        userFounded = false;
                        for (var m = 0 ; !userFounded && m < groupsObj[j].users.length ; m++)
                        {
                            if (usersChildren[k].id == 'u-'+groupsObj[j].users[m].id)
                                userFounded = true;
                        }
                        if (!userFounded)
                            $('users-'+groupsObj[j].group.id+'-ol').removeChild (usersChildren[k]);
                    }
                }
            }

            if (!found)
            {
                if (sortables != undefined)
                    sortables.removeLists ($('users-'+children[i].id.substring(3)+'-ol'));
                $('groups').removeChild (children[i]);
            }
        }

        // Add new groups
        groupsObj.each (function(item) {

            if (document.getElementById ('gr-'+item.group.id))
                return;

            var li = new Element ('li', {id: 'gr-'+item.group.id});
            li.innerHTML = '<div class=\'GrIcon\'></div>';
            if (item.group.admin)
            {
                li.addClass ('droppable');
                li.innerHTML += '<a id=\'delGr'+item.group.id+'\' href=\'#\' class=\'del\'></a>';
                li.innerHTML += '<font id=\'GrLabel'+item.group.id+'\' class=\'clickable\'>'
                                + item.group.groupname + '</font>';
            }
            else
                li.innerHTML += '<font id=\'GrLabel'+item.group.id+'\'>'
                                + item.group.groupname + '</font>';

            $('groups').adopt (li);
            li.highlight();

            var div = new Element ('div', {id: 'users-'+item.group.id,
                                           'class': 'users'});
            li.adopt (div);
            var ol = new Element ('ol', {id: 'users-'+item.group.id+'-ol',
                                             'class': 'users'});
            div.adopt (ol);

            // Make list draggable
            item.users.each (function (user) {

                var handle;
                var liUser = new Element ('li', {id: 'u-'+user.id});
                liUser.innerHTML = '';
                if (item.group.admin)
                {
                    liUser.innerHTML += '<a id=\'delUser'+user.id+'\' href=\'#\' class=\'del\'></a>';        
                    liUser.innerHTML += '<font id=\'ULabel'+user.id+'\' class=\'clickable\'>'
                                        + user.lastname + ', ' + user.firstname + '</font>';
                    handle = new Element ('div', 
                                            {id : 'handle-'+user.id, 
                                             'class' : 'drag-handle UIcon'});
                }
                else
                {
                    liUser.innerHTML += '<font id=\'ULabel'+user.id+'\'>'
                                        + user.lastname + ', ' + user.firstname + '</font>';
                    handle = new Element ('div', 
                                            {id : 'handle-'+user.id, 
                                             'class' : 'UIcon'});
                }

                handle.inject (liUser, 'top');

                ol.adopt (liUser);
                liUser.highlight ();

                if ($('delUser'+user.id))
                {
                    $('delUser'+user.id).addEvent ('click', function (e) {
                        e.stop ();

                        // Delete the user from the server
                        var request = new Request.JSON ({

                            url: '/Administration/deluser/format/json',
                            method: 'post',
                            onComplete: function (jsonObj) {
                                if (jsonObj.status == 1)
                                    updateGroups (jsonObj.groups);
                                else
                                {
                                    alert (jsonObj.msg);
                                    updateRequest ();
                                }
                            }
                        }).send ('id='+user.id);
                    });

                    $('ULabel'+user.id).addEvent ('click', function (e) {
                        e.stop ();

                        if ($('editGroup'))
                            $('editGroup').hide ();
                        if ($('addUser'))
                            $('addUser').hide ();
                        if ($('addGroup'))
                            $('addGroup').hide ();
                        if ($('createUser'))
                            $('createUser').set ('disabled', true);

                        if ($('applyUser'))
                            $('applyUser').set ('disabled', false);
                        if ($('editUser'))
                            $('editUser').show ();

                        var request = new Request.JSON ({
                        
                            url: '/Administration/getuser/format/json',
                            method: 'post',
                            onComplete: function (jsonObj) {

                                if ($('userGroup'))
                                {
                                    $('userGroup').set ('value', jsonObj.groupName);
                                    $('userName').set ('value', jsonObj.login);
                                    $('userPass').set ('value', '');
                                    $('userFirstname').set ('value', jsonObj.firstname);
                                    $('userLastname').set ('value', jsonObj.lastname);
                                    $('userEmail').set ('value', jsonObj.email);
                                    $('userId').set ('value', jsonObj.id);
                                }
                            }
                        }).send ('id='+user.id);
                    });
                }
            });

            if (item.group.admin)
            {
                if (sortables == undefined)
                {
                    sortables = new Sortables 
                                    ('users-'+item.group.id+'-ol',
                                            {
                                             handle: '.drag-handle',
                                             constrain: false,
                                             clone: true,
                                             onComplete: updateNewOrder
                                            });
                }
                else
                {
                    sortables.addLists ($('users-'+item.group.id+'-ol'));
                }
            }

            // Delete Group
            if ($('delGr'+item.group.id))
            {
                $('delGr'+item.group.id).addEvent('click', function (e) {
                    e.stop ();

                    // Delete the group from the server
                    var request = new Request.JSON ({

                        url: '/Administration/delgroup/format/json',
                        method: 'post',
                        onComplete: function (jsonObj) {
                            if (jsonObj.status == 1)
                                updateGroups (jsonObj.groups);
                            else
                            {
                                alert (jsonObj.msg);
                                updateRequest ();
                            }
                        }

                    }).send ('id='+this.id);
                });
            }

            // Update group
            if (item.group.admin)
            {
                $('GrLabel'+item.group.id).addEvent('click', function (e) {
                    e.stop ();

                    var groupname = $('GrLabel'+item.group.id).get('text');

                    $('groupName').set ('value', groupname);
                    $('groupId').set ('value', item.group.id);

                    $('userGroup').set ('value', groupname);

                    if ($('addGroup'))
                        $('addGroup').hide ();
                    if ($('editGroup'))
                        $('editGroup').show ();
                    if ($('addUser'))
                        $('addUser').show ();
                    if ($('editUser'))
                        $('editUser').hide ();
                });
            }
        });
    };

    if ($('addGroup'))
    {
        $('addGroup').addEvent('submit', function(e) {
            e.stop();
            var val = $('newGroup').get('value');
            if (!val) {
                $('newGroup').highlight('#f00').focus();
                return;
            }
            $('newGroup').set('value', '');

            // Add the new group on the server
            var request = new Request.JSON ({

                url: '/Administration/addgroup/format/json',
                method: 'post',
                onComplete: function (jsonObj) {

                    if (jsonObj.status == 1)
                        updateGroups (jsonObj.groups);
                    else
                    {
                        alert (jsonObj.msg);
                        updateRequest ();
                    }
                }

            }).send ('groupname='+val);
        });
    }

    if ($('editGroup'))
    {
        $('editGroup').addEvent ('submit', function (e) {
            e.stop ();

            var newName = $('groupName').get('value');
            var groupId = $('groupId').get('value');
            var request = new Request.JSON ({

                url: '/Administration/modifygroup/format/json',
                method: 'post',
                onComplete: function (jsonObj) {
                    if (jsonObj.status == 1)
                        updateGroups (jsonObj.groups);
                    else
                    {
                        alert (jsonObj.msg);
                        updateRequest ();
                    }
                }

            }).send ('groupId='+groupId+'&groupname='+newName);

            $('editGroup').hide ();
            $('addUser').hide ();
            $('editUser').hide ();
            $('addGroup').show ();
        });

        $('cancelGroup').addEvent ('click', function (e) {
            e.stop ();

            $('editGroup').hide ();
            $('addUser').hide ();
            $('editUser').hide ();
            $('addGroup').show ();
        });
        $('cancelUser').addEvent ('click', function (e) {
            e.stop ();

            $('editGroup').hide ();
            $('addUser').hide ();
            $('editUser').hide ();
            $('addGroup').show ();
        });
    };

    if ($('applyUser'))
    {
        $('applyUser').addEvent ('click', function (e) {
            e.stop ();

            var user        = new Object ();
            user.id         = $('userId').get ('value');
            user.groupId    = $('groupId').get ('value');
            user.login      = $('userName').get ('value');
            user.pass       = $('userPass').get ('value');
            user.firstname  = $('userFirstname').get ('value');
            user.lastname   = $('userLastname').get ('value');
            user.email      = $('userEmail').get ('value');

            // Add the new user on the Server
            var request = new Request.JSON ({

                url: '/Administration/commituser/format/json',
                method: 'post',
                data: { 'user' : JSON.encode (user) },
                onComplete: function (jsonObj) {
                    if (jsonObj.status == 1)
                        updateGroups (jsonObj.groups);
                    else
                    {
                        alert (jsonObj.msg);
                        updateRequest ();
                    }
                }

            }).send ();

            $('editGroup').hide ();
            $('addUser').hide ();
            $('editUser').hide ();
            $('addGroup').show ();
            

        });
    }

    if ($('addUser'))
    {
        $('addUser').addEvent ('submit', function (e) {
            e.stop ();

            // Clean fields
            $('userName').set ('value', '');
            $('userPass').set ('value', '');
            $('userFirstname').set ('value', '');
            $('userLastname').set ('value', '');
            $('userEmail').set ('value', '');
            $('userId').set ('value', '-1');


            $('editGroup').hide ();
            $('addUser').hide ();
            $('addGroup').hide ();
            $('editUser').show ();
            $('createUser').set ('disabled', false);
            $('applyUser').set ('disabled', true);
        });

        $('createUser').addEvent ('click', function (e) {
            e.stop ();

            var user        = new Object ();
            user.id         = $('userId').get ('value');
            user.groupId    = $('groupId').get ('value');
            user.login      = $('userName').get ('value');
            user.pass       = $('userPass').get ('value');
            user.firstname  = $('userFirstname').get ('value');
            user.lastname   = $('userLastname').get ('value');
            user.email      = $('userEmail').get ('value');

            // Add the new user on the Server
            var request = new Request.JSON ({

                url: '/Administration/commituser/format/json',
                method: 'post',
                data: { 'user' : JSON.encode (user) },
                onComplete: function (jsonObj) {
                    if (jsonObj.status == 1)
                        updateGroups (jsonObj.groups);
                    else
                    {
                        alert (jsonObj.msg);
                        updateRequest ();
                    }
                }

            }).send ();

            $('editGroup').hide ();
            $('addUser').hide ();
            $('editUser').hide ();
            $('addGroup').show ();
        });
    }
	
	if ($('validateAgreement'))
    {
        $('validateAgreement').addEvent ('click', function (e) {
            e.stop ();
            
			var agreement = $('net-conf').get('value');
			var request = new Request.JSON ({
				url: '/Administration/editagreement/format/json',
				method: 'post',
				data: { 'agreement': agreement},
				onSuccess: function (jsonObj) {
					if (!jsonObj || jsonObj.status != '1')
						alert (jsonObj.msg);
				}

			}).send ();
        });
    }

    if ($('editPerms'))
        $('editPerms').hide ();

    if ($('groups'))
        updateRequest ();
});
