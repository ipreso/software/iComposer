window.addEvent('domready', function() {

    var sortables;
    var editBoxAcc;
    var editGroupAcc;

    var refreshPage;
    var refreshChange = false;

    var currentBox;
    var currentSlide;

    if ($('editGroup'))
        $('editGroup').hide ();
    if ($('editBox'))
        $('editBox').hide ();
    if ($('programDetails'))
        $('programDetails').hide ();

    var updateGroups = function () {

        var getHostHash = function (el) {
            return (el.id.substring (3));
        };

        // Get groups
        var groups = $('groupsList').getChildren ();

        var resultArray = new Array ();

        var sortableIndex = 0;
        for (var i = 0 ; i < groups.length ; i++)
        {
            if (!groups [i].hasClass ('droppable'))
                continue;

            var newGroup = new Object ();
            newGroup.id = groups[i].id.substring (3);
            newGroup.hosts = sortables.serialize (sortableIndex++, getHostHash);
            resultArray.push (newGroup);
        }

        var sendChange = new Request.JSON ({
            url : location.href + '/updgroups/format/json',
            method: 'post',
            data: { 'hosts' : JSON.encode (resultArray) },
            onComplete: function (jsonObj) {
                doUpdateTree (jsonObj);
            }
        }).send ();
    };


    var updateBoxesList = new Request.JSON ({
        url: location.href + '/listgroups/format/json',
        method: 'post',
        onComplete: function (jsonObj) {
            doUpdateTree (jsonObj);
        }
    });

    var doUpdateTree = function (jsonObj) {

        if (jsonObj == null || jsonObj.count < 1)
            return;

        var groupsList = $('groupsList').getChildren ();

        refreshPage = 30000;

        addNewGroups (jsonObj, groupsList);
        updateOldGroups (jsonObj, groupsList);
        
        setTimeout (updatePage, refreshPage);
    };

    var cleanCmdsSlides = function () {

        var myCmdsSlides = $$('.cmd');
        for (var i = 0 ; i < myCmdsSlides.length ; i++)
            myCmdsSlides[i].destroy ();
    };

    var addEventsOnButtons = function () {

        buttons = $$('.doButton');
        for (var i = 0 ; i < buttons.length ; i++)
        {
            buttons [i].removeEvent ('click', cmdButtonClick);
            buttons [i].addEvent ('click', cmdButtonClick);
        }
    };

    var cmdButtonClick = function (e) {

        e.stop ();
        var request = new Request.JSON ({
            url: location.href+'/doaction/format/json',
            method: 'post',
            data: { 'id': this.id,
                    'cmd': this.getProperty ('cmd'),
                    'fct': this.getProperty ('fct') },
            onComplete: function (jsonObj) {
                if (jsonObj != null && jsonObj.msg)
                    alert (jsonObj.msg);

                if (jsonObj != null && jsonObj.html && currentSlide)
                {
                    currentSlide.innerHTML = jsonObj.html;
                    addEventsOnButtons ();
                }
            }
        }).send ();
    }

    var addGrNewCmdSlide = function (id, html) {

        groupEditZone = $('formGroupProperties');
        if (!groupEditZone)
            return;

        var container = new Element ('div', {'class': 'cmd'});

        var toggler = new Element ('h3', {  'id': 'toggler'+id,
                                            'class': 'togglerGr',
                                            'text': id
                                         });

        var content = new Element ('div', { 'id': 'elt'+id,
                                            'class': 'elementGr'});
        content.innerHTML = html;

        container.adopt (toggler, content);
        groupEditZone.grab (container, 'bottom');
    }

    var addBoxNewCmdSlide = function (id, html) {

        boxEditZone = $('formBoxProperties');
        if (!boxEditZone)
            return;

        var container = new Element ('div', {'class': 'cmd'});

        var toggler = new Element ('h3', {  'id': 'toggler'+id,
                                            'class': 'togglerBox',
                                            'text': id
                                         });

        var content = new Element ('div', { 'id': 'elt'+id,
                                            'class': 'elementBox'});
        content.innerHTML = html;

        container.adopt (toggler, content);
        boxEditZone.grab (container, 'bottom');
    }

    var showGroupCmds = function (commands) {

        cleanCmdsSlides ();

        for (var i = 0 ; i < commands.length ; i++)
        {
            if (commands[i].html.length > 0)
                addGrNewCmdSlide (commands[i].cmd, commands[i].html);
        }

        editGroupAcc = new Accordion ($('editGroup'),
                                      'h3.togglerGr',
                                      'div.elementGr', {
            display: -1,
            opacity: false,
            onActive: function (toggler, element) {
                currentSlide = element;
                toggler.setStyle('color', '#333333');
            },
            onBackground: function (toggler, element) {
                toggler.setStyle('color', '#888888');
            }
        });
        editGroupAcc.display (0);
        addEventsOnButtons ();
    };

    var showBoxCmds = function (commands) {

        cleanCmdsSlides ();

        for (var i = 0 ; i < commands.length ; i++)
        {
            if (commands[i].html.length > 0)
                addBoxNewCmdSlide (commands[i].cmd, commands[i].html);
        }

        var editBoxAcc = new Accordion ($('editBox'),
                                        'h3.togglerBox',
                                        'div.elementBox', {
            display: -1,
            opacity: false,
            onActive: function (toggler, element) {
                currentSlide = element;
                toggler.setStyle('color', '#333333');
            },
            onBackground: function (toggler, element) {
                toggler.setStyle('color', '#888888');
            }
        });
        editBoxAcc.display (0);
        addEventsOnButtons ();
    };

    var showGroup = function (jsonObj) {

        if (!$('editGroup'))
            return;

        if ($('editGroup'))
        {
            $('editGroup').show ();
            $('groupId').set ('value', jsonObj.id);
            $('groupName').set ('value', jsonObj.name);
            $('groupDesc').set ('value', jsonObj.description);
            if (jsonObj.edit)
            {
                $('groupName').removeProperties ('readonly');
                $('groupDesc').removeProperties ('readonly');
                $('applyGroup').show ().setStyle ('display', 'inline');
                $('cancelGroup').show ().setStyle ('display', 'inline');
            }
            else
            {
                $('groupName').set ('readonly', 'true')
                $('groupDesc').set ('readonly', 'true');
                $('applyGroup').hide ();
                $('cancelGroup').hide ();
            }

            showGroupCmds (jsonObj.cmds);
        }
        if ($('programDetails'))
        {
            $('programDetails').show ();
            $('referer').set ('value', 'gr-'+jsonObj.id);
            $('progId').set ('value', jsonObj.programId);
            if (jsonObj.emiId && $('emissionId'))
                $('emissionId').set ('value', jsonObj.emiId);
            if (jsonObj.chgProg)
            {
                if (jsonObj.programId.length < 1 ||
                    !$('channel-'+jsonObj.programId))
                {
                    if (!$('channel-blank'))
                    {
                        var blankOption = new Element ('option',
                            {   'id':       'channel-blank',
                                'value':    '',
                                'name':     'channel-blank',
                                'text':     '' });
                        $('currentProgSelect').adopt (blankOption);
                    }
                    $('channel-blank').set ('selected', true);
                }
                else
                {
                    if ($('channel-blank'))
                        $('channel-blank').destroy ();
                    if ($('channel-'+jsonObj.programId))
                        $('channel-'+jsonObj.programId).set ('selected', true);
                }
                $('currentProgSelect').show ();
                $('currentProgSelect').setStyle ('display', 'inline');
                $('currentProgSpan').hide ();
            }
            else
            {
                $('currentProgSelect').hide ();
                $('currentProgSpan').set ('text', jsonObj.program);
                $('currentProgSpan').show ();
                $('currentProgSpan').setStyle ('display', 'inline');
            }
        }
        if ($('editProg'))
        {
            if (jsonObj.viewProg != undefined &&
                jsonObj.viewProg)
            {
                $('editProg').show ();
                $('editProg').setStyle ('display', 'inline');

                if (jsonObj.editProg)
                    $('editProg').set ('value', strEditProgram);
                else
                    $('editProg').set ('value', strViewProgram);
            }
            else
                $('editProg').hide ();
        }
        if ($('editEmi'))
        {
            if (jsonObj.viewEmi != undefined &&
                jsonObj.viewEmi)
            {
                $('editEmi').show ();
                $('editEmi').setStyle ('display', 'inline');

                if (jsonObj.editEmi)
                    $('editEmi').set ('value', strEditProgram);
                else
                    $('editEmi').set ('value', strViewProgram);
            }
            else
                $('editEmi').hide ();
        }

        if ($('editBox'))
            $('editBox').hide ();
    };

    var showBox = function (jsonObj) {

        if (!$('editBox'))
            return;

        if ($('editGroup'))
            $('editGroup').hide ();
        if ($('programDetails'))
        {
            $('programDetails').hide ();
            $('referer').set ('value', 'b-'+jsonObj.hash);
            $('progId').set ('value', jsonObj.programId);
            if ($('channel-'+jsonObj.programId))
                $('channel-'+jsonObj.programId).set ('selected', true);
        }
        if ($('editBox'))
        {
            $('editBox').show ();
            $('boxStatus').set          ('text', jsonObj.status);
            $('boxSync').set            ('text', jsonObj.sync);
            $('boxProgram').set         ('text', jsonObj.program);
            $('boxEmission').set        ('text', jsonObj.emission);
            //$('boxLogs').set            ('href', '/Administration/getboxlogs/hash/'+jsonObj.hash+'/format/txt');
            $('boxHash').set            ('text', jsonObj.hash);
            $('boxName').set            ('value', jsonObj.label);
            $('boxCoord').set           ('value', jsonObj.coordinates);
            $('boxMac').set             ('text', jsonObj.mac);
            $('boxLastIp').set          ('text', jsonObj.lastIp);
            $('boxIp').set              ('value', jsonObj.ip);
            $('boxGw').set              ('value', jsonObj.gw);
            $('boxDns').set             ('value', jsonObj.dns);
            $('boxComposerHost').set    ('text', jsonObj.composer);
            $('boxComposerIp').set      ('value', jsonObj.composer_ip);
            $('boxPkgver').set          ('text', jsonObj.pkg_ver);
            $('boxLicstatus').set       ('text', jsonObj.lic_status);
            $('boxLicexpiration').set   ('text', jsonObj.lic_exp);

            if (jsonObj.editable)
            {
                $('boxName').removeProperties ('readonly');
                $('boxCoord').removeProperties ('readonly');
                $('boxIp').removeProperties ('readonly');
                $('boxGw').removeProperties ('readonly');
                $('boxDns').removeProperties ('readonly');
                $('boxComposerIp').removeProperties ('readonly');
                $('applyBox').show ().setStyle ('display', 'inline');
                $('cancelBox').show ().setStyle ('display', 'inline');
            }
            else
            {
                $('boxName').set            ('readonly', 'true');
                $('boxCoord').set           ('readonly', 'true');
                $('boxIp').set              ('readonly', 'true');
                $('boxGw').set              ('readonly', 'true');
                $('boxDns').set             ('readonly', 'true');
                $('boxComposerIp').set      ('readonly', 'true');
                $('applyBox').hide ();
                $('cancelBox').hide ();
            }

            showBoxCmds (jsonObj.cmds);
        }
    };

    var addNewGroups = function (newList, currentNodes) {

        newList.each (function (groupInNewList) {
            
            var gId = groupInNewList.group.id;

            if (document.getElementById ('gr-'+gId))
                return;

            var liGroup = new Element ('li', { id: 'gr-'+gId });
            if (groupInNewList.group.associable)
                liGroup.addClass ('droppable');

            var groupStatus = groupInNewList.group.status;
            if ((groupStatus == 'ERROR' || groupStatus == 'DESYNC') &&
                refreshPage == 30000)
                refreshPage = 10000;

            liGroup.innerHTML = '<div id=\'status'+gId+'\' '
                                    +'class=\'status status'
                                    + groupStatus
                                    +'\'></div>';

            if (groupInNewList.group.deletable)
                liGroup.innerHTML += '<a id=\'delGr'+gId+'\' href=\'#\' class=\'del\'></a>';

            liGroup.innerHTML += '<font id=\'GrLabel'+gId+'\' class=\'clickable\'>'
                                 + groupInNewList.group.groupname + ' '
                                 + '('+groupInNewList.children.length+')</font>';
            $('groupsList').adopt (liGroup);
            liGroup.highlight ();

            var olHosts = new Element ('ol', { 'id': 'gr-'+gId+'-ol',
                                               'class': 'hosts'});
            liGroup.adopt (olHosts);

            if (groupStatus == 'OK' || groupStatus == 'OFF')
                $('gr-'+gId+'-ol').hide ();
            else
                $('gr-'+gId+'-ol').show ();

            addNewBoxes (groupInNewList.children, $('gr-'+gId+'-ol'));

            if ($('delGr'+gId))
            {
                $('delGr'+gId).addEvent ('click', function (e) {
                    e.stop ();
                    var request = new Request.JSON ({
                        url: location.href+'/delgroup/format/json',
                        method: 'post',
                        data: { 'id': gId },
                        onComplete: function (jsonObj) {
                            doUpdateTree (jsonObj);
                        }
                    }).send ();
                });
            }

            $('GrLabel'+gId).addEvent ('click', updateGroupDetails);

            $('status'+gId).addEvent ('click', function (e) {
                e.stop ();

                // Toggle visibility of hosts
                $('gr-'+gId+'-ol').toggle ();
            });

            if (groupInNewList.group.associable)
            {
                if (sortables == undefined)
                {
                    sortables = new Sortables
                                        ('gr-'+gId+'-ol',
                                            {
                                            handle: '.drag-handle',
                                            constrain: false,
                                            clone: true,
                                            onComplete: updateGroups
                                            });
                }
                else
                    sortables.addLists ($('gr-'+gId+'-ol'));
            }
        });

    };

    var updateGroupDetails = function (groupId) {

        var gId;
        if (groupId === undefined || typeof (groupId) == "object")
            gId = this.id.substr (7);
        else
            gId = groupId;

        var request = new Request.JSON ({
            url: location.href + '/getgroup/format/json',
            method: 'post',
            data: { 'id': gId },
            onComplete: function (jsonObj) {
                showGroup (jsonObj);
            }
        }).send ();
        $('gr-'+gId+'-ol').show ();
    };


    var addNewBoxes = function (hostsList, groupNode) {

        hostsList.each (function (newHost) { addNewBox (newHost, groupNode); });

    };

    var addNewBox = function (boxObj, groupNode) {

        var hHash = boxObj.hash;
        var liHost = new Element ('li', { id: 'li-'+hHash });
        var hostStatus = boxObj.status;

        if (boxObj.deletable)
            liHost.innerHTML += '<a id=\'del'+hHash+'\' href=\'#\' class=\'del\'></a>';

        liHost.innerHTML += '<font id=\'h'+hHash+'\' class=\'clickable\'>'
                             + boxObj.label + '</font>';

        var handle = new Element ('div',
                             {
                              id: 'handle-'+hHash,
                              'class': 'drag-handle movable status status'+hostStatus
                             });
        handle.inject (liHost, 'top');

        groupNode.adopt (liHost);
        liHost.highlight ();

        if ($('del'+hHash))
        {
            $('del'+hHash).addEvent ('click', function (e) {
                e.stop ();
                var request = new Request.JSON ({
                    url: location.href+'/delbox/format/json',
                    method: 'post',
                    data: { 'hash': hHash },
                    onComplete: function (jsonObj) {
                        doUpdateTree (jsonObj);
                    }
                }).send ();
            });
        }

        if (boxObj.viewable)
        {
            $('h'+hHash).addEvent ('click', function (e) {
                e.stop ();
                var request = new Request.JSON ({
                    url: location.href + '/getbox/format/json',
                    method: 'post',
                    data: { 'hash': hHash },
                    onComplete: function (jsonObj) {
                        currentBox = jsonObj.hash;
                        showBox (jsonObj);
                    }
                }).send ();
            });
        }
        else
        {
            $('h'+hHash).addEvent ('click', function (e) {
                e.stop ();
                if ($('editBox'))
                    $('editBox').hide ();
                if ($('editGroup'))
                    $('editGroup').hide ();
                if ($('programDetails'))
                    $('programDetails').hide ();
            });
        }
    }

    var updateOldGroups = function (newList, liGrList) {

        for (var i = 0 ; i < liGrList.length ; i++)
        {
            found = false;
            for (var j = 0 ; !found && j < newList.length ; j++)
            {
                if (liGrList [i].id == 'gr-'+newList [j].group.id)
                {
                    found = true;
                    updateBoxesListInGroup (liGrList [i], newList [j]);
                }
            }
            if (!found)
            {
                if (sortables != undefined)
                    sortables.removeLists ($('gr-'+liGrList[i].id+'-ol'));
                $('groupsList').removeChild (liGrList [i]);
            }
        }
    };

    var updateBoxesListInGroup = function (nodeGroup, jsonGroup) {

        var gId = nodeGroup.id.substring (3);

        var groupStatus = jsonGroup.group.status;
        if ((groupStatus == 'ERROR' || groupStatus == 'DESYNC') &&
            refreshPage == 30000)
            refreshPage = 5000;

        $('status'+gId).set ('class', 'status status'+groupStatus);
        $('GrLabel'+gId).set ('text', jsonGroup.group.groupname +
                                      ' (' + jsonGroup.children.length + ')');

        olGroup = $(nodeGroup.id+'-ol');
        olGroupChildren = olGroup.getChildren ();
        for (var i = 0 ; i < olGroupChildren.length ; i++)
        {
            var founded = false;
            for (var j = 0 ; !founded && j < jsonGroup.children.length ; j++)
            {
                if (olGroupChildren[i].id == 'li-'+jsonGroup.children[j].hash)
                    founded = true;
            }
            if (!founded)
                $(olGroup.id).removeChild (olGroupChildren[i]);
        }

        for (var i = 0 ; i < jsonGroup.children.length ; i++)
        {
            olNode = $(nodeGroup.get('id')+'-ol');
            if (!olNode.getElementById('li-'+jsonGroup.children[i].hash))
            {
                addNewBox (jsonGroup.children [i], olNode);
                if (sortables == undefined)
                {
                    sortables = new Sortables
                                        ('gr-'+gId+'-ol',
                                            {
                                            handle: '.drag-handle',
                                            constrain: false,
                                            clone: true,
                                            onComplete: updateGroups
                                            });
                }
                else
                    sortables.addLists ($('gr-'+gId+'-ol'));
            }
            else
            {
                if ($('h'+jsonGroup.children[i].hash) &&
                    $('h'+jsonGroup.children[i].hash).get ('text') != jsonGroup.children[i].label)
                    $('h'+jsonGroup.children[i].hash).set ('text', jsonGroup.children[i].label);

                var boxStatus = jsonGroup.children [i].status;

                $('handle-'+jsonGroup.children[i].hash).set ('class', 'movable status status'+boxStatus);
            }
        }

    };

    if ($('addGroup'))
    {
        $('addGroup').addEvent ('submit', function (e) {
            e.stop ();
            var groupname = $('newGroup').get ('value');
            if (!groupname) {
                $('newGroup').highlight ('#f00').focus ();
                return;
            }
            $('newGroup').set ('value', '');

            var request = new Request.JSON ({
                url: location.href + '/addgroup/format/json',
                method: 'post',
                data: {name: groupname},
                onComplete: function (jsonObj) {
                    doUpdateTree (jsonObj);
                }
            }).send ();
        });
    }

    if ($('applyGroup'))
    {
        $('applyGroup').set ('disabled', false);
        $('applyGroup').addEvent ('click', function (e) {
            e.stop ();

            var group           = new Object ();
            group.id            = $('groupId').get ('value');
            group.name          = $('groupName').get ('value');
            group.description   = $('groupDesc').get ('value');

            var formProperties = new Array ();
            if ($('formGroupProperties'))
            {
                var formElement = document.forms.formGroupProperties;

                var i;
                for (i = 0 ; i < formElement.elements.length ; i++)
                {
                    var prop = new Array ();
                    prop [0] = formElement.elements[i].name;
                    prop [1] = formElement.elements[i].value;
                    formProperties [i] = prop;
                }

                var colorProperties = $$('div.colorInput');
                for (var j = 0 ; j < colorProperties.length; j++, i++)
                {
                    var prop = new Array ();
                    prop [0] = colorProperties [j].get ('name');
                    prop [1] = colorProperties [j].get ('color');
                    formProperties [i] = prop;
                }
            }

            var request = new Request.JSON ({
                url: location.href + '/editgroup/format/json',
                method: 'post',
                data: { 'group' : JSON.encode (group),
                        'plugins' : JSON.encode (formProperties) },
                onComplete: function (jsonObj) {
                    doUpdateTree (jsonObj);
                    updatePage ();
                }
            }).send ();

            if ($('editGroup'))
                $('editGroup').hide ();
            if ($('editBox'))
                $('editBox').hide ();
            if ($('programDetails'))
                $('programDetails').hide ();
        });
    }

    if ($('editBox'))
    {
        var editBoxAcc = new Accordion ($('editBox'),
                                        'h3.togglerBox',
                                        'div.elementBox', {
            display: -1,
            opacity: false,
            onActive: function (toggler, element) {
                currentSlide = element;
                toggler.setStyle('color', '#333333');
            },
            onBackground: function (toggler, element) {
                toggler.setStyle('color', '#888888');
            }
        });
    }

    if ($('editGroup'))
    {
        var editGroupAcc = new Accordion ($('editGroup'),
                                          'h3.togglerGr',
                                          'div.elementGr', {
            display: -1,
            opacity: false,
            onActive: function (toggler, element) {
                currentSlide = element;
                toggler.setStyle('color', '#333333');
            },
            onBackground: function (toggler, element) {
                toggler.setStyle('color', '#888888');
            }
        });
    }

    if ($('applyBox'))
    {
        $('applyBox').set ('disabled', false);
        $('applyBox').addEvent ('click', function (e) {
            e.stop ();

            var box         = new Object ();
            box.hash        = $('boxHash').get ('text');
            box.label       = $('boxName').get ('value');
            box.coordinates = $('boxCoord').get ('value');
            box.ip          = $('boxIp').get ('value');
            box.gw          = $('boxGw').get ('value');
            box.dns         = $('boxDns').get ('value');
            box.composer_ip = $('boxComposerIp').get ('value');

            var formProperties = new Array ();
            if ($('formBoxProperties'))
            {
                var formElement = document.forms.formBoxProperties;

                var i;
                for (i = 0 ; i < formElement.elements.length ; i++)
                {
                    var prop = new Array ();
                    prop [0] = formElement.elements[i].name;
                    prop [1] = formElement.elements[i].value;
                    formProperties [i] = prop;
                }

                var colorProperties = $$('div.colorInput');
                for (var j = 0 ; j < colorProperties.length; j++, i++)
                {
                    var prop = new Array ();
                    prop [0] = colorProperties [j].get ('name');
                    prop [1] = colorProperties [j].get ('color');
                    formProperties [i] = prop;
                }
            }

            var request = new Request.JSON ({
                url: location.href + '/editbox/format/json',
                method: 'post',
                data: { 'box' : JSON.encode (box),
                        'plugins' : JSON.encode (formProperties) },
                onComplete: function (jsonObj) {
                    doUpdateTree (jsonObj);
                    updatePage ();
                }
            }).send ();

            if ($('editBox'))
                $('editBox').hide ();
            if ($('editGroup'))
                $('editGroup').hide ();
            if ($('programDetails'))
                $('programDetails').hide ();
        });
    }

    if ($('cancelGroup'))
    {
        $('cancelGroup').addEvent ('click', function (e) {
            e.stop ();
            if ($('editGroup'))
                $('editGroup').hide ();
            if ($('editBox'))
                $('editBox').hide ();
            if ($('programDetails'))
                $('programDetails').hide ();
        });
    }
    if ($('cancelBox'))
    {
        $('cancelBox').addEvent ('click', function (e) {
            e.stop ();
            if ($('editGroup'))
                $('editGroup').hide ();
            if ($('editBox'))
                $('editBox').hide ();
            if ($('programDetails'))
                $('programDetails').hide ();
        });
    }

    if ($('editProg'))
    {
        $('editProg').addEvent ('click', function (e) {
            e.stop ();

            $('toProgramPage').set ('action', '/Programmation');
            $('toProgramPage').submit ();
        });
    }

    if ($('editEmi'))
    {
        $('editEmi').addEvent ('click', function (e) {
            e.stop ();

            $('toProgramPage').set ('action', '/Catalog');
            $('toProgramPage').submit ();
        });
    }

    if ($('currentProgSelect'))
    {
        $('currentProgSelect').addEvent ('change', function (e) {

            $('progId').set ('value', $('currentProgSelect').get ('value'));

            var request = new Request.JSON ({
                url: '/Programmation/broadcast/format/json',
                data: {'group': $('referer').get ('value'),
                       'prog':  $('progId').get ('value')},
                method: 'post',
                onSuccess: function (jsonObj) {

                    updatePage ();
                    if (jsonObj.status != 1)
                        alert (jsonObj.msg);
                    updateGroupDetails ($('referer').get ('value').substr (3));
                },
                onFailure: function (xhr) {
                    alert ('Error '+xhr.status);
                }
            }).send ();
        });
        $('currentProgSelect').hide ();
    }
    if ($('currentProgSpan'))
    {
        $('currentProgSpan');
    }

    updatePage = function () {
        updateBoxesList.send ();
    }

    updatePage ();
});
