ipresoLoading = new Class ({

    message: "Loading...",

    createLoadBox: function (msg)
    {
        if (msg && msg.length > 0)
            this.setMessage (msg);

        // Create background
        var background = new Element ('div',
                {
                    'id':       'dialogBackground',
                    'class':    'dialogBackground'
                });

        var screenHeight = Math.max(
            Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
            Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
            Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        );
        background.setStyle ('height', screenHeight);

        // Create message box
        var dialog = new Element ('div', { 'id': 'loadBox' });
        dialog.set ('text', this.message);
        dialog.hide ();

        background.inject (document.body);

        var screenDimensions = background.getDimensions ();
        var dialogDimensions = dialog.getDimensions ();

        dialog.setStyle ('top', (screenDimensions.height - dialogDimensions.height) / 2);
        dialog.setStyle ('left', (screenDimensions.width - dialogDimensions.width) / 2);
        dialog.inject (document.body);
        dialog.show ();
    },

    setMessage: function (msg)
    {
        this.message = msg;
        if ($('loadBox'))
            $('loadBox').set ('text', this.message);
    },

    destroyLoadBox: function ()
    {
        if ($('dialogBackground'))
            $('dialogBackground').destroy ();
        if ($('loadBox'))
            $('loadBox').destroy ();
    }
});

function createPopupLegalAgreement()
{
	var width = 980;
	var height = 480;
	// center the popup
	var left = (screen.width/2)-(width/2);
	var top = (screen.height/2)-(height/2);
	window.open("/supervision/agreement","popupAgreement","center=yes, menubar=no, status=no, scrollbars=no, menubar=no, width="+width+", height="+height+",top="+top+",left="+left);
}


