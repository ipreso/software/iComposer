Drag.Mymove = new Class ({

    Extends: Drag.Move,

    checkAgainst: function(el, i){

        var scroll = el.getScrolls();
        el = (this.positions) ? this.positions[i] : el.getCoordinates();

        el.top      = el.top + scroll.y;
        el.bottom   = el.bottom + scroll.y;
        el.left     = el.left + scroll.x;
        el.right    = el.right + scroll.x;

        var now = this.mouse.now;
        return (now.x > el.left && now.x < el.right && now.y < el.bottom && now.y > el.top);
    }
});

window.addEvent('domready', function() {

    var catAccordion;
    var zonesAccordion;
    var zonesSortables;
    var zonesPlaylists  = new Array ();
    var dragObjects     = new Array ();
    var zoneCount       = 0;
    var CatalogLoaded   = false;
    var EmissionsLoaded = false;
    var EmissionLoaded  = true;

    var loadingObject = new ipresoLoading ();
    loadingObject.createLoadBox (strLoading);

    var updateEmissionRequest = function () {

        new Request.JSON ({
            url: '/Catalog/getprog/format/json',
            method: 'post',
            data: { 'id': $('emissionId').get ('value') },
            onComplete: function (jsonObj) { doUpdateEmission (jsonObj); }
        }).send ();
    };

    var doUpdateEmission = function (jsonObj) {

        if (jsonObj == null || jsonObj.length < 1)
            return;

        if ($('zonesList'))
            $('zonesList').empty ();
        if ($('layout'))
            $('layout').empty ();

        setFormDetails (jsonObj);
        if (typeof initOrientation == 'function')
            initOrientation ();

        zoneCount = 0;
        showZones (jsonObj.zones);

        if ($('emiDelete'))
        {
            if (jsonObj.emiDel)
            {
                $('emissionDel').set ('value', '1');
                //$('emiDelete').show ();
                $('emiDelete').setStyle ('display', 'inline');
            }
            else
            {
                $('emiDelete').hide ();
                $('emissionDel').set ('value', '0');
            }
        }

        if ($('bSave'))
        {
            if (jsonObj.emiEdit)
            {
                $('bSave').show ();
                $('bSave').setStyle ('display', 'inline');
                if ($('bSaveNProg'))
                {
                    //$('bSaveNProg').show ();
                    $('bSaveNProg').setStyle ('display', 'inline');
                    $('bOnlyProg').hide ();
                }
                if ($('newZone'))
                {
                    //$('newZone').show ();
                    $('newZone').setStyle ('display', 'inline');
                }
            }
            else
            {
                $('bSave').hide ();
                if ($('bSaveNProg'))
                {
                    $('bSaveNProg').hide ();
                    //$('bOnlyProg').show ();
                    $('bOnlyProg').setStyle ('display', 'inline');
                }
//                if ($('newZone'))
//                    $('newZone').hide ();
            }
        }
    };

    var updateCatalog = function (index) {
        var request = new Request.JSON ({
            url: '/Catalog/getcatalog/format/json',
            method: 'post',
            onComplete: function (jsonObj) {
                doUpdateCatalog (jsonObj, index);
            }
        }).send ();
    };

    var doUpdateCatalog = function (jsonObj, index) {

        if (jsonObj == null || jsonObj.length < 1)
            return;

        if ($('catalogBox'))
            $('catalogBox').empty ();

        for (var i = 0 ; i < jsonObj.length ; i++)
            addNewCategory (jsonObj [i].title, jsonObj [i].elements);

        delLinks = $$('a.delLibItem');
        for (var i = 0 ; i < delLinks.length ; i++)
        {
            delLinks [i].addEvent ('click', function (e) {
                e.stop ();
                delFromLib (this.getParent ().id);
              });
        }

        catAccordion = new Fx.Accordion ($$('h3.catToggler'),
                                     $$('div.catElements'),
                    {
                        display: -1,
                        opacity: false,
                        onActive: function (toggler, elt) {
                            toggler.setStyle('color', '#333333');
                        },
                        onBackground: function (toggler, elt) {
                            toggler.setStyle('color', '#888888');
                        }
                    });

        if (index != null)
            catAccordion.display (index);

        $$('.draggable').each (function (drag) {
            makeDraggable (drag);
        });

        resizeLibraryContent ();

        CatalogLoaded   = true;
        if (CatalogLoaded && EmissionLoaded && EmissionsLoaded)
            loadingObject.destroyLoadBox ();
    };

    var makeDraggable = function (element) {

        var dragObj = new Drag.Mymove (element,
                    {
                        snap: 1,
                        droppables: '.itemDrop',
                        style: false,
                        onStart: startToDragFromCatalog,
                        onDrag: dragFromCatalog,
                        onEnter: enterDropZone,
                        onLeave: leaveDropZone,
                        onDrop: dropFromCatalog
                    });

        dragObjects.push (dragObj);
    };

    var startToDragFromCatalog = function (element) {

        var scroll = {x:0, y: 0};
        element.getParents ().each (function (el)
        {
            if (['auto','scroll'].contains (el.getStyle ('overflow')))
            {
                scroll = {
                    x: scroll.x + el.getScroll().x,
                    y: scroll.y + el.getScroll().y
                }
            }
        });
        var position = element.getPosition();
        var newElement = element.clone ();
        element.getParent ().grab (newElement);
        element.setStyles ({'position': 'absolute',
                            'padding': '5px',
                            'border': '1px solid #a7a7a7',
                            'background-color': '#ffffff',
                            'top': position.y + scroll.y,
                            'left': position.x + scroll.x});
        element.inject (document.body);
        newElement.set ('id', element.get ('id'));
        element.set ('plugid', element.get ('id'));
        element.set ('id', element.get ('id')+'_drag');
        element.set ('opacity', '.5');
        var delLink = element.getElement ('.delLink');
        if (delLink)
        {
            var delLinkId = delLink.get ('id');
            delLink.destroy ();
            delLink = newElement.getElement ('.delLink');
            delLink.set ('id', delLinkId);
            delLink.addEvent ('click', function (e) {
                e.stop ();
                delFromLib (this.id);
            });
        }
        makeDraggable (newElement);
    };

    var dragFromCatalog = function (element, evt) {
        element.setStyles ({'top': evt.page.y,
                            'left': evt.page.x});
    };

    var dropFromCatalog = function (elt, drop, evt) {

        if (!drop)
        {
            elt.destroy ();
            return;
        }

        var eltNode;
        leaveDropZone (elt, drop);
        if (drop.id.match ("^ZiL"))
        {
            eltNode = $('elt'+drop.id.substring (3));
/*
            if (eltNode)
                zonesAccordion.display (zonesAccordion.elements.indexOf (eltNode));
*/
        }
        else if (drop.id.match ("^seq"))
        {
            eltNode = $('elt'+drop.id.substring (3));
        }
        else
        {
            elt.destroy ();
            return;
        }

        if (!eltNode)
        {
            elt.destroy ();
            return;
        }

        var olNode = eltNode.getElement ('ol');
        if (!olNode)
        {
            elt.destroy ();
            return;
        }

        var newItem = new Object ();
        newItem.id      = elt.get ('plugid');
        newItem.plugin  = elt.get ('plugin');
        newItem.length  = elt.get ('length');
        newItem.prop    = elt.get ('prop');
        newItem.name    = elt.get ('label');

        var itemLi = getItemLi (newItem);

        var oldHeight = olNode.getStyle ('height').substr (0, olNode.getStyle ('height').lastIndexOf ('px')).toInt ();
        olNode.adopt (itemLi);
        var newHeight = olNode.getStyle ('height').substr (0, olNode.getStyle ('height').lastIndexOf ('px')).toInt ();
        var totalPlaylistHeight = olNode.getParent ().getStyle ('height').substr (0, olNode.getParent ().getStyle ('height').lastIndexOf ('px')).toInt ();
        olNode.getParent ().setStyle ('height', totalPlaylistHeight - (oldHeight - newHeight));

        if (drop.id.match ("^ZiL"))
            zonesAccordion.display (zonesAccordion.elements.indexOf (eltNode));

        deleteItemEvent (itemLi);
        editItemEvent (itemLi);

        zonesPlaylists [eltNode.id.substring (3)] = 
                new Sortables ($('seq'+eltNode.id.substring (3)),
                                { 'constrain': false,
                                  'clone': cloneSortable 
                                });

        updateTotalLength (olNode);
        elt.destroy ();
        resizePlaylistContent ();
    };

    var enterDropZone = function (elt, drop) {
        elt.setStyles ({'border': '1px solid #000000'});
        elt.set ('opacity', '1');
        drop.addClass ('drop_over');
    };

    var leaveDropZone = function (elt, drop) {
        elt.setStyles ({'border': '1px solid #a7a7a7'});
        elt.set ('opacity', '.5');
        drop.removeClass ('drop_over');
    };

    var delFromLib = function (item) {

        var request = new Request.JSON ({
            url: '/Catalog/delfromlibrary/format/json',
            method: 'post',
            data: { 'item': item.substr (3) },
            onSuccess: function (jsonObj) { 

                if (jsonObj == null || jsonObj == undefined)
                    return;

                if (jsonObj.code != 1)
                {
                    if (jsonObj.details != undefined)
                        alert (jsonObj.details);
                    else
                        alert ('Error');
                    return;
                }
                updateCatalog (catAccordion.previous);
            }
        }).send ();
    }

    var updateEmissionsList = function () {
        var request = new Request.JSON ({
            url: '/Quickupdate/getemissions/format/json',
            method: 'post',
            onComplete: function (jsonObj) {
                doUpdateEmissionsList (jsonObj);
            }
        }).send ();
    };

    var emiSelectChange = function () {

        if (!$('emissionId'))
            return;
        $('emiSaved').hide ();
        $('emissionId').set ('value', this.get ('value'));
        displayCurrentLayout ();
    }

    var doUpdateEmissionsList = function (jsonObj) {

        if (jsonObj == null)
            return;

        var select = $('emiSelect');
        if (!select)
            return;

        select.removeEvent ('change', emiSelectChange);
        select.addEvent ('change', emiSelectChange);

        select.empty ();
        for (var i = 0 ; i < jsonObj.emissions.length ; i++)
        {
            var option = new Element ('option', {
                                        'value':    jsonObj.emissions [i].value,
                                        'id':       'option-'+jsonObj.emissions [i].value,
                                        'text':     jsonObj.emissions [i].name
                                                });
            select.grab (option);
        }

        var option = new Element ('option', {
            'value':    '-1',
            'id':       'option-new',
            'text':     strSelectEmission
                    });
        select.grab (option);
//        if (jsonObj.newEmission)
//        {
//            var option = new Element ('option', {
//                                        'value':    '-1',
//                                        'id':       'option-new',
//                                        'text':     strSelectEmission
//                                                });
//            select.grab (option);
//            if ($('emiRename'))
//            {
//                //$('emiRename').show ();
//                $('emiRename').setStyle ('display', 'inline');
//            }
//        }
//        else
//        {
//            if ($('emiRename'))
//                $('emiRename').hide ();
//        }

        displayCurrentLayout ();

        EmissionsLoaded   = true;
        if (CatalogLoaded && EmissionLoaded && EmissionsLoaded)
            loadingObject.destroyLoadBox ();
    }

    var displayCurrentLayout = function () {

        if (!$('emissionId'))
            return;

        var emissionId = $('emissionId').get ('value');
        if (emissionId.length < 1)
            emissionId = -1;
        else
            emissionId = emissionId.toInt ();
        
//        // Show status
//        var request = new Request.JSON ({
//            url: location.href + '/getbox/format/json',
//            method: 'post',
//            data: { 'hash': hHash },
//            onComplete: function (jsonObj, emissionId) {
//                showEmissionStatus (jsonObj, emissionId);
//            }
//        }).send ();

        showEmission (emissionId);
    };
    
//    var showEmissionStatus = function (jsonObj, emissionId) {
//    	alert('showBox');
//    	if (jsonObj.emission == emissionId )
//    	{
//    		alert('EGAL !');
//    	}
//    }

    var showEmission = function (id) {

        if (id < 0 || !($('option-'+id)))
        {
            if ($('option-new'))
            {
                $('option-new').set ('selected', true);
                id = -1;
            }
            else
            {
                var id = $('emiSelect').get ('value');
                if ($('option-'+id))
                    $('option-'+id).set ('selected', true);
            }
        }
        else
        {
            if ($('option-'+id))
                $('option-'+id).set ('selected', true);
        }

        $('emissionId').set ('value', id);

//        if (id < 0 && $('option-new'))
//        {
//            if ($('newEmiName'))
//            {
//                $('newEmiName').set ('value', '');
//                //$('newEmiName').show ();
//                $('newEmiName').setStyle ('display', 'inline');
//            }
//            if ($('emiLinks'))
//                $('emiLinks').hide ();
//        }
//        else if (id >= 0)
//        {
//            if ($('newEmiName'))
//            {
//                $('newEmiName').set ('value', '');
//                $('newEmiName').hide ();
//            }
//            if ($('emiLinks'))
//            {
//                //$('emiLinks').show ();
//                $('emiLinks').setStyle ('display', 'inline');
//            }
//        }
//        else
//        {
//            if ($('newEmiName'))
//            {
//                $('newEmiName').hide ();
//                $('newEmiName').set ('value', '');
//            }
//            if ($('emiLinks'))
//                $('emiLinks').hide ();
//        }

        updateEmissionRequest ();
    };

    var addNewCategory = function (title, elements) {

        var catalog = $('catalogBox');

        var toggler = new Element ('h3', { 'id':    'toggler_'+title,
                                           'class': 'catToggler',
                                           'text':  title });

        var content = new Element ('div', { 'id':   'elt_'+title,
                                            'class':'catElements' });

        var mediaList = new Element ('ol', { 'id':      'list_'+title,
                                             'class':   'mediaList' });
        for (var i = 0 ; i < elements.length ; i++)
        {
            var item = new Element ('li', { 'id': elements [i].plugin+'_'+i});
            var itemDiv = new Element ('div', { 'id':       elements [i].id,
                                                'plugin':   elements [i].plugin,
                                                'prop':     elements [i].prop,
                                                'length':   elements [i].length,
                                                'label':    elements [i].name,
                                                'title':    elements [i].name,
                                                'del':      elements [i].del,
                                                'class':    'draggable' });
            if (itemDiv.get ('length').indexOf (':') >= 0)
                itemDiv.set ('length', getTimeInSeconds (itemDiv.get ('length')));

            var itemPix = new Element ('img', { 'src':  elements [i].thumbLink,
                                                'class': 'thumb',
                                                'title': elements [i].name,
                                                'alt':  elements [i].name });
            var itemDel = new Element ('span', {'id': 'del'+elements [i].plugin
                                                           +'-'+elements [i].id,
                                                'class': 'delLink'});

            var itemText = new Element ('span', {'class': 'catalogLabel',
                                                 'text': elements [i].name});

            itemDiv.grab (itemPix, 'top');
            itemDiv.adopt (itemText);

            if (elements [i].del)
            {
                itemDel.setStyle ('float', 'right');
                itemDel.setStyle ('padding-right', '5px');
                itemDel.innerHTML = '<a class="delLibItem" href="#"><img alt="del" src="/images/Del.png"/></a>';
                itemDiv.grab (itemDel);
            }
            item.grab (itemDiv);
            mediaList.grab (item);
        }

        content.adopt (mediaList);
        catalog.adopt (toggler, content);
    };

    var adaptTVSize = function () {

        var tv = $('layout');
        if (!tv)
            return;

        var boxMiddle = $('boxM');
        if (!boxMiddle)
            return;

        // Landscape or Portrait ?
        var initialWidth    = tv.getStyle ('width').substr (0, tv.getStyle ('width').lastIndexOf ('px'));
        var initialHeight   = tv.getStyle ('height').substr (0, tv.getStyle ('height').lastIndexOf ('px'));
        if (initialWidth > initialHeight)
        {
            // Lanscape
            var boxSize1 = boxMiddle.getSize ();
            tv.setStyle ('width', boxSize1.x - 10);
            tv.setStyle ('height', Math.round (((boxSize1.x-10) * 9) / 16));
            var boxSize2 = boxMiddle.getSize ();
            if (boxSize1.x != boxSize2.x)
            {
                tv.setStyle ('width', boxSize2.x - 10);
                tv.setStyle ('height', Math.round (((boxSize2.x-10) * 9) / 16));
            }
            $('tv').setStyle ('width', boxSize2.x - 10);
        }
        else
        {
            // Portrait
            var boxSize1 = boxMiddle.getSize ();
            tv.setStyle ('height', boxSize1.x - 10);
            tv.setStyle ('width', Math.round (((boxSize1.x-10) * 9) / 16));
            var boxSize2 = boxMiddle.getSize ();
            if (boxSize1.x != boxSize2.x)
            {
                tv.setStyle ('height', boxSize2.x - 10);
                tv.setStyle ('width', Math.round (((boxSize2.x-10) * 9) / 16));
            }
            $('tv').setStyle ('width', Math.round ((boxSize2.x-10) * 9) / 16);
        }

        var zones = tv.getChildren ();
        var boxTV = tv.getSize ();
        for (var i = 0 ; i < zones.length ; i++)
        {
            var zoneWidth   = zones [i].get ('pwidth');
            var zoneHeight  = zones [i].get ('pheight');

            var widthPixel  = zoneWidth * boxTV.x / 100;
            var heightPixel = zoneHeight * boxTV.y / 100;
            widthPixel      = widthPixel - 2;
            heightPixel     = heightPixel - 2;
            zones [i].setStyle ('width', widthPixel+'px');
            zones [i].setStyle ('height', heightPixel+'px');
        }

        // Resize all media in the left column
        resizeLibraryContent ();
        // Rezize all items in the right column
        resizePlaylistContent ();
    };

    var resizeLibraryContent = function () {

        var container = $('catalogBox');
        if (!container)
            return;

        var containerSize = container.getSize ();

        // Remove padding, delete link and thumbnail from the width
        var boxWidth = containerSize.x - (2 * 10) - 35;

        var items = $$('.catalogLabel');
        for (var i = 0 ; i < items.length ; i++)
        {
            var item = items [i];
            var label = item.getParent().get ('label');
            var maxWidth = boxWidth;
            if (item.getParent().get ('del') == "true")
                maxWidth = maxWidth - 50;

            var divX = item.getSize ().x;
            var divY = item.getSize ().y;
            var length = label.length;
            length = length - 3;

            while (length > 3 && (divX > maxWidth || divY > 20))
            {
                item.set ('text', label.substr (0, length) + '...');
                length = length.toInt () - 1;
                divX = item.getSize ().x;
                divY = item.getSize ().y;
            }
        }
    };

    var resizePlaylistContent = function () {

        var container = $('contextualBox');
        if (!container)
            return;

        var containerSize = container.getSize ();

        // Remove padding of boxes
        var boxWidth = containerSize.x - 210;

        var items = $$('.label');
        for (var i = 0 ; i < items.length ; i++)
        {
            var item = items [i];
            var label = item.getParent().get ('itemname');

            var divX = item.getSize ().x;
            var length = label.length;
            length = length - 3;

            while (length > 3 && divX > boxWidth)
            {
                item.set ('text', label.substr (0, length) + '...');
                length = length.toInt () - 1;
                divX = item.getSize ().x;
            }
        }
    };

    var showZones = function (zonesList) {
    	$('emiSaved').hide ();

        for (var i = 0 ; i < zonesList.length ; i++)
            addNewZone (zonesList [i]);

        zonesAccordion = new Fx.Accordion ($$('h3.zoneToggler'), 
                                       $$('div.zoneElement'),
                            {
                                display: -1,
                                opacity: false,
                                onActive: function (toggler, elt) {
                                    toggler.setStyle('color', '#333333');
                                    enableSortable (elt);
                                },
                                onBackground: function (toggler, elt) {
                                    toggler.setStyle('color', '#888888');
                                    disableSortable (elt);
                                }
                            });

        zonesSortables = new Sortables ('zonesList',
                                {
                                    handle:     'h3',
                                    constrain:   true,
                                    clone:      false,
                                    onComplete: reorderZones
                                });

        if (zonesList.length > 0)
            zonesAccordion.display (0);

        resizePlaylistContent ();
    };

    var addNewZone = function (zone) {

        var name    = zone.name;
        var x       = zone.x;
        var y       = zone.y;
        var width   = zone.width;
        var height  = zone.height;
        var depth   = zone.layer;
        var zoneId  = depth;
        var screen  = $('layout');

        var widthLayout = $('layout').getStyle ('width').toInt ();
        var heightLayout = $('layout').getStyle ('height').toInt ();
        var widthPixel = width * widthLayout / 100;
        var heightPixel = height * heightLayout / 100;
        widthPixel = widthPixel - 2;
        heightPixel = heightPixel - 2;

        var newZone = new Element ('div',
            {
                'id':       'ZiL'+zoneId,
                'class':    'dynDiv_moveDiv dynDiv_dropArea dynDiv_dropLimit zone itemDrop',
                'styles': {
                    'top': y+'%',
                    'left': x+'%',
                    'width': widthPixel+'px',
                    'height': heightPixel+'px',
                    'z-index': zoneId
                },
                'pLeft': x,
                'pTop': y,
                'pWidth': width,
                'pHeight': height
            });
        var zoneName = new Element ('span', {'id': 'ZiLName'+zoneId,
                                             'text': name});

//        var tlCorner = new Element ('div', {'class': 'deleteDiv'});
        var tlCorner = new Element ('div', {'class': ''});
        tlCorner.innerHTML = '<a href="#" id="del'+zoneId+'">';
//                             + '<img alt="delete" src="/images/Del.png" />';
//        tlCorner.innerHTML = '<a href="#" id="del'+zoneId+'">'
//        					+ '<img alt="delete" src="/images/Del.png" />';

//        var blCorner = new Element ('div', {'class': 'dynDiv_resizeDiv_bl'});
//        var brCorner = new Element ('div', {'class': 'dynDiv_resizeDiv_br'});
        var blCorner = new Element ('div', {'class': ''});
        var brCorner = new Element ('div', {'class': ''});
        newZone.adopt (zoneName, tlCorner, blCorner, brCorner);
        screen.adopt (newZone);

        $('del'+zoneId).addEvent ('click', function (e) {
            e.stop ();

            if (confirm (strConfirmDelZone))
            {
                if ($('ZiL'+zoneId))
                    $('layout').removeChild ($('ZiL'+zoneId));
                if ($('container'+zoneId))
                    $('zonesList').removeChild ($('container'+zoneId));
            }
        });

        ByRei_dynDiv.add (newZone, zoneId);
        ByRei_dynDiv.add (blCorner, 'bl'+zoneId);
        ByRei_dynDiv.add (brCorner, 'br'+zoneId);

        addNewSlide (zoneId, zone);
        zoneCount++;
        return (zoneId);
    };

    var addNewSlide = function (id, zone) {

        var name = zone.name;

        zoneList = $('zonesList');
        if (!zoneList)
            return;

        var container = new Element ('li', 
                                    {'id': 'container'+id});
        var toggler = new Element ('h3', {  'id': 'toggler'+id,
                                            'class': 'drag-handler zoneToggler',
                                            'text': name
                                         });
        var content = new Element ('div', { 'id': 'elt'+id,
                                            'class': 'zoneElement'});
        content.innerHTML = 
            '<table style="width: 100%">'
            +'  <tr>'
            +'    <td class="tdRight">' + strZoneName + '</td>'
            +'    <td class="tdLeft"><input type="text" '
                                         + 'id="zoneName'+id+'" '
                                         + 'name="zoneName'+id+'" '
                                         + 'value="' + name + '" />'
            +    '</td>'
            +'  </tr>'
            +'  <tr>'
            +'    <td class="tdRight">' + strZoneDimension + '</td>'
            +'    <td class="tdLeft" id="zoneDim'+id+'">'
                    + zone.x + 'x' + zone.y
                    + ' (' + zone.width + 'x' + zone.height + ')'
            +    '</td>'
            +'  </tr>'
            +'</table>';

        content.innerHTML += '<span>' + strSequence + ':</span>';

        zoneSeq = '<ol class="sequences" id="seq'+id+'">';
        for (var i = 0 ; i < zone.playlist.length ; i++)
        {
            zoneSeq += getItemLiHtml (zone.playlist [i]);
        }
        zoneSeq += '</ol>';

        content.innerHTML += zoneSeq;
        content.innerHTML += '<span id="totalLength' + id + '" '
                          + 'style="float: right; font-weight: bold;"></span>';
        container.adopt (toggler, content);
        zoneList.grab (container, 'top');

        zonesPlaylists [id] = new Sortables ($('seq'+id),
                                { 'constrain': false,
                                  'clone': cloneSortable 
                                });
        zonesPlaylists [id].detach ();
        updateTotalLength ($('seq'+id));

        var liItems = $('seq'+id).getChildren ();
        for (var i = 0 ; i < liItems.length ; i++)
        {
            deleteItemEvent (liItems [i]);
            editItemEvent (liItems [i]);
        }

        if ($('boxR'))
            $('boxR').show ();
    };

    var cloneSortable = function (event, element, list)
    {
        var position = element.getPosition();
        return element.clone ().setStyles ({
            margin: '0px',
            position: 'absolute',
            visibility: 'hidden',
            'width': element.getStyle ('width'),
            'z-index': 2000,
            top: position.y,
            left: position.x
        }).inject (this.list);
    };

    var deleteItemEvent = function (item) {

        delLink = item.getElement ('.delItem');
        if (!delLink)
            return;

        delLink.addEvent ('click', function (e) {
            e.stop ();
            var eltParent = item.getParent ();
            var oldHeight = eltParent.getStyle ('height').substr (0, eltParent.getStyle ('height').lastIndexOf ('px')).toInt ();
            item.destroy ();
            var newHeight = eltParent.getStyle ('height').substr (0, eltParent.getStyle ('height').lastIndexOf ('px')).toInt ();
            var totalPlaylistHeight = eltParent.getParent ().getStyle ('height').substr (0, eltParent.getParent ().getStyle ('height').lastIndexOf ('px')).toInt ();
            eltParent.getParent ().setStyle ('height', totalPlaylistHeight - (oldHeight - newHeight));
            updateTotalLength (eltParent);
        });
    };

    var editItemEvent = function (item) {

        editLink = item.getElement ('.editItem');
        if (!editLink)
            return;

        editLink.addEvent ('click', function (e) {
            e.stop ();
            openProperties (item);
        });
    };

    var updateTotalLength = function (olElt) {

        var zoneId = olElt.id.substr (3);
        var spanTotal = $('totalLength'+zoneId);
        if (spanTotal == undefined || !spanTotal)
            return;

        liElts = olElt.getChildren ('li');
        var timeLength = 0;
        for (var i = 0 ; i < liElts.length ; i++)
        {
            var itemlength = liElts[i].get ('itemlength');
            if (itemlength == 0)
            {
                timeLength = 0;
                break;
            }
            else
                timeLength += itemlength.toInt ();
        }

        spanTotal.set ('text', getFormattedTime (timeLength));
    };

    var getItemLiHtml = function (item) {

        var html = '<li id="' + item.id + '" class="libMedia" '
                    + 'itemplugin="' + item.plugin + '" '
                    + 'itemlength="' + item.length + '" '
                    + 'itemprop="' + item.prop + '" '
                    + 'itemname="' + item.name + '">\n';

        html += '<span style="float: right;">'
                + '<a href="#" class="delItem">'
                +   '<img src="/images/Del.png" alt="del" />'
                + '</a>'
                + '</span>\n';
        html += '<span style="float: right;">'
                + '<a href="#" class="editItem">'
                +   '<img src="/images/Edit.png" alt="edit" />'
                + '</a>'
                + '</span>\n';

        html += '<img alt="'+item.name+'" class="preview" '
                    + 'title="'+item.name+'" '
                    + 'src="/Catalog/getfilepreview/type/' + item.plugin
                            + '/id/' + item.id + '/preview.png" />\n';

        html += '<span class="spanLength">'
                    + getFormattedTime (item.length) + '</span>\n';

        html += '<span class="label" title="'+item.name+'">' + item.name + '</span>\n';
        html += '</li>\n';
        return (html);
    };

    var getItemLi = function (item) {

        var li = new Element ('li', { 'id':         item.id,
                                      'class':      'libMedia',
                                      'itemplugin': item.plugin,
                                      'itemlength': item.length,
                                      'itemprop':   item.prop,
                                      'itemname':   item.name });

        var html = '';
        html += '<span style="float: right;">'
                + '<a href="#" class="delItem">'
                +   '<img src="/images/Del.png" alt="del" />'
                + '</a>'
                + '</span>\n';
        html += '<span style="float: right;">'
                + '<a href="#" class="editItem">'
                +   '<img src="/images/Edit.png" alt="edit" />'
                + '</a>'
                + '</span>\n';

        html += '<img alt="'+item.name+'" class="preview" '
                    + 'title="'+item.name+'" '
                    + 'src="/Catalog/getfilepreview/type/' + item.plugin
                            + '/id/' + item.id + '/preview.png" />\n';

        html += '<span class="spanLength">'
                    + getFormattedTime (item.length) + '</span>\n';

        var displayedName = item.name;
        if (item.name.length > 30)
            displayedName = item.name.substr (0, 27) + '...';
        html += '<span class="label" title="'+item.name+'">' + displayedName + '</span>\n';
        html += '</li>\n';

        li.innerHTML = html;
        return (li);
    };

    var getFormattedTime = function (seconds) {

        if (seconds == 0)
            return (strNoEnd);

        var hours   = (seconds / 3600).toInt ();
        var minutes = ((seconds - (hours * 3600)) / 60).toInt ();
        var seconds = seconds - ((hours * 3600) + (minutes * 60));

        if (hours < 10)
            hours = '0' + hours;
        if (minutes < 10)
            minutes = '0' + minutes;
        if (seconds < 10)
            seconds = '0' + seconds;
        return (hours+':'+minutes+':'+seconds);
    };

    var getTimeInSeconds = function (lengthString) {

        var lengthInt = lengthString.split (':');
        if (lengthInt.length != 3)
            return (0);

        var hour    = lengthInt [0].toInt ();
        var min     = lengthInt [1].toInt ();
        var sec     = lengthInt [2].toInt ();
        if (hour < 0 || hour > 23)
            hour = 0;
        if (min < 0 || min > 59)
            min = 0;
        if (sec < 0 || sec > 59)
            sec = 0;

        return (sec + (min * 60) + (hour * 3600));
    };

    var enableSortable = function (elt) {
        var id = elt.id.substr (3);
        zonesPlaylists [id].attach ();
        currentZone = elt;
        if ($('ZiL'+id))
            $('ZiL'+id).addClass ('ZiL_selected');
        var olNode = elt.getElement ('.sequences');
        if (olNode)
            olNode.addClass ('itemDrop');

        while (dragObjects.length > 0)
        {
            var dragObj = dragObjects.pop ();
            dragObj.detach ();
        }
        $$('.draggable').each (function (drag) {
            makeDraggable (drag);
        });
    };

    var disableSortable = function (elt) {
        var id = elt.id.substr (3);
        zonesPlaylists [id].detach ();
        if ($('ZiL'+id))
            $('ZiL'+id).removeClass ('ZiL_selected');
        var olNode = elt.getElement ('.sequences');
        if (olNode)
            olNode.removeClass ('itemDrop');
    };

    var reorderZones = function () {
        var elements = $$('div.zoneElement');
        zindexMax = elements.length + 1;
        for (var i = 0 ; i < elements.length ; i++)
        {
            var id = elements[i].id.substr (3);
            $('ZiL'+id).setStyle ('z-index', zindexMax--);
        }
    };

    ByRei_dynDiv.api.drop = function () {
        if (ByRei_dynDiv.api.obj)
        {
            var div         = ByRei_dynDiv.db (0);

            if (div.id.substring (0, 3) != 'ZiL')
                return;

            var divLeft     = ByRei_dynDiv.db (4)-1;
            var divTop      = ByRei_dynDiv.db (5)-1;
            var divWidth    = ByRei_dynDiv.db (13)+2;
            var divHeight   = ByRei_dynDiv.db (14)+2;
            var widthLayout = $('layout').getStyle ('width').toInt ();
            var heightLayout = $('layout').getStyle ('height').toInt ();

            if (divLeft + divWidth >= widthLayout-4)
                divWidth = widthLayout - divLeft;
            if (divTop + divHeight >= heightLayout-4)
                divHeight = heightLayout - divTop;

            div.set ('pLeft', ((divLeft*100) / widthLayout).round ());
            div.set ('pWidth', ((divWidth*100) / widthLayout).round ());
            div.set ('pTop', ((divTop*100) / heightLayout).round ());
            div.set ('pHeight', ((divHeight*100) / heightLayout).round ());

            div.setStyle ('top', ((divTop*100) / heightLayout).round () + '%');
            div.setStyle ('left', ((divLeft*100) / widthLayout).round () + '%');
            div.setStyle ('width', (((divWidth*100) / widthLayout).round () * widthLayout / 100).round () - 2 + 'px');
            div.setStyle ('height', (((divHeight*100) / heightLayout).round () * heightLayout / 100).round () - 2 + 'px');

            var zoneId = div.id.substring (3);
            var dimensionNode = $('zoneDim'+zoneId);
            if (dimensionNode)
            {
                dimensionNode.set ('text', 
                    $('ZiL'+zoneId).get ('pLeft') + 'x' +
                    $('ZiL'+zoneId).get ('pTop') +
                    ' (' + $('ZiL'+zoneId).get ('pWidth') + 'x' +
                           $('ZiL'+zoneId).get ('pHeight') + ')');
            }

            eltNode = $('elt'+div.id.substring (3));
            if (eltNode)
                zonesAccordion.display (zonesAccordion.elements.indexOf (eltNode));
        }
    };

//    $('newZone').addEvent ('click', function (e) {
//        e.stop ();
//
//        var zonesH3 = $$('h3.zoneToggler');
//        do
//        {
//            searchName = false;
//            for (var i = 0 ; !searchName && i < zonesH3.length ; i++)
//            {
//                if (zonesH3[i].innerHTML == strDefaultZoneName + (zoneCount+1))
//                    searchName = true;
//            }
//            if (searchName)
//                zoneCount++; 
//        } 
//        while (searchName);
//
//        var zone = new Object ();
//        zone.name   = strDefaultZoneName + (zoneCount+1);
//        zone.x      = 0;
//        zone.y      = 0;
//        zone.width  = 20;
//        zone.height = 20;
//        zone.layer  = zoneCount + 1;
//        zone.playlist = new Array ();
//        addNewZone (zone);
//
//        zonesAccordion = new Fx.Accordion ($$('h3.zoneToggler'), 
//                                       $$('div.zoneElement'),
//                            {
//                                display: -1,
//                                opacity: false,
//                                onActive: function (toggler, elt) {
//                                    toggler.setStyle('color', '#333333');
//                                    enableSortable (elt);
//                                },
//                                onBackground: function (toggler, elt) {
//                                    toggler.setStyle('color', '#888888');
//                                    disableSortable (elt);
//                                }
//                            });
//
//        zonesSortables = new Sortables ('zonesList',
//                                {
//                                    handle:     'h3',
//                                    constrain:   true,
//                                    clone:      false,
//                                    onComplete: reorderZones
//                                });
//
//        zonesAccordion.display (0);
//    });

    var openProperties = function (item) {
    	
        var request = new Request.HTML ({
            url: '/Catalog/getproperties',
            evalScripts: false,
            data: { 'plugin':       item.get ('itemplugin'),
                    'properties':   item.get ('itemprop'),
                    'item':         item.get ('id')},
            onSuccess: function (tree, elts, html, script) 
                       { showProperties (item, html, script); }
        });
        request.send ();
    };

    var showProperties = function (liItem, html, script) {
        
        if (!$('propWindow'))
        {
            var propWin = new Element ('div',
                {
                    'id':       'propWindow',
                    'class':    'propWin',
                    'styles': {
                        'top': 80,
                        'left': 200
                    }
                });

            var propWinBar = new Element ('div',
                {
                    'id':       'propWindowBar',
                    'class':    'dynDiv_moveParentDiv propWinBar',
                    'text':     strItemProperties
                });
            var propWinContent = new Element ('div',
                {
                    'id':       'propWindowContent',
                    'class':    'propWinContent'
                });

            var propWinFoot = new Element ('div',
                {
                    'id':       'propWindowFooter',
                    'class':    'propWinFooter'
                });

            var bOk = new Element ('input',
                {
                    'type':     'submit',
                    'value':    strOk
                });
            var bCancel = new Element ('input',
                {
                    'type':     'submit',
                    'value':    strCancel
                });

            propWinFoot.adopt (bOk, bCancel);
            propWin.adopt (propWinBar, propWinContent, propWinFoot);
            propWin.inject (document.body);
            ByRei_dynDiv.add (propWinBar, 60);

            bCancel.addEvent ('click', function (e) {
                e.stop ();
                var colorBoxes = $$('div.moor-box');
                for (var i = 0 ; i < colorBoxes.length ; i++)
                    colorBoxes [i].getParent ().destroy ();

                if ($('propWindow'))
                    $('propWindow').destroy ();
            });

            bOk.addEvent ('click', function (e) {
                e.stop ();

                var colorBoxes = $$('div.moor-box');
                for (var i = 0 ; i < colorBoxes.length ; i++)
                    colorBoxes [i].getParent ().destroy ();

                if ($('formProperties'))
                {
                    var formElement = document.forms.formProperties;
                    
                    var formProperties = new Array ();
                    var i;
                    for (i = 0 ; i < formElement.elements.length ; i++)
                    {
                        var prop = new Array ();
                        prop [0] = formElement.elements[i].name;
                        prop [1] = formElement.elements[i].value;
                        formProperties [i] = prop;
                    }

                    var colorProperties = $$('div.colorInput');
                    for (var j = 0 ; j < colorProperties.length; j++, i++)
                    {
                        var prop = new Array ();
                        prop [0] = colorProperties [j].get ('name');
                        prop [1] = colorProperties [j].get ('color');
                        formProperties [i] = prop;
                    }

                    var updateProgRequest = new Request.JSON ({
                        url: '/Catalog/commitproperties/format/json',
                        method: 'post',
                        data: { 'properties': JSON.encode (formProperties),
                                'plugin': liItem.get ('itemplugin'),
                                'propId': liItem.get ('itemprop'),
                                'item': liItem.get ('id') },
                        onComplete: function (jsonReply) { 
                            liItem.set ('itemprop', jsonReply.propId); 
                            liItem.set ('itemlength', jsonReply.duration);
                            liItem.getElement ('.spanLength').set ('text',
                                getFormattedTime (jsonReply.duration));
                            updateTotalLength (liItem.getParent ());
                            if ($('propWindow'))
                                $('propWindow').destroy ();
                        }
                    }).send ();
                }
            });
        }
        if ($('propWindowContent'))
        {
            $('propWindowContent').empty ();
            $('propWindowContent').innerHTML = 
                '<form id="formProperties" method="post">' + html + '</form>\n';
        }
        if (script.length > 0)
            eval (script);
    };

    if ($('bSaveNProg'))
    {
        $('bSaveNProg').addEvent ('click', function (e) {
            e.stop ();

            var emission = getEmission ();
            if (emission != false)
                trySendQuickProgram (emission);
        });
    }
    
    if ($('bOnlyProg'))
    {
        $('bOnlyProg').addEvent ('click', function (e) {
            e.stop ();

            var emission = getEmission ();
            if (emission != false)
                createQuickProgrammation ($('emissionId').get ('value'));
        });
    }

    if ($('bCancel'))
    {
        $('bCancel').addEvent ('click', function (e) {
            e.stop ();

            var page = $('from').value;
            if (!page.length)
                page = "/Supervision";
            jumpToPage (page);
        });
    }

    var jumpToPage = function (page) {
        $('progEdit').set ('action', page);
        $('progEdit').submit ();
    }

    var getEmission = function () {

        var program = new Object ();
        program.id = $('emissionId').get ('value');
        var progName = $('newEmiName').get ('value');
        if (progName.length < 1)
            progName = $('origEmiName').get ('value');
        program.name = progName;
        if (!program.name)
        {
//        	$('newEmiName').highlight('#f00').focus();
        	$('emiSelect').highlight('#f00').focus();
            return (false);
        }
        program.layout = $('emiLayout').get ('value');

        // Get zones and return object
        var zones = getZones ();
        if (zones.length < 1)
        {
            alert (strAtLeastOneZone);
            return (false);
        }

        program.zones = zones;
        return (program);
    };

    var getZones = function () {

        var zones = new Array ();
        var zonesElt = $$('div.zoneElement');
        for (var i = 0 ; i < zonesElt.length ; i++)
        {
            var id = zonesElt[i].id.substr (3);
            var zoneName = $('zoneName'+id).get ('value');
            var zone = new Object ();
            if (!zoneName)
            {
                alert (zoneNameIncorrect+i);
                $('zoneName'+id).highlight('#f00').focus();
                continue;
            }
            zone.id         = id;
            zone.name       = zoneName;
            zone.x          = $('ZiL'+id).get ('pLeft');
            zone.y          = $('ZiL'+id).get ('pTop');
            zone.width      = $('ZiL'+id).get ('pWidth');
            zone.height     = $('ZiL'+id).get ('pHeight');
            zone.playlist   = getZonePlaylist (id);
            zones[i] = zone;
        }

        return (zones);
    };
    
    if ($('bSave'))
    {
        $('bSave').addEvent ('click', function (e) {
            e.stop ();

            var emission = getEmission ();
            if (emission != false)
                trySendProgram (emission);
        });
    }

    var getZonePlaylist = function (id) {

        var playlist = new Array ();
        var items = $('seq'+id).getChildren ();
        for (var i = 0 ; i < items.length ; i++)
        {
            var item = new Object;
            item.id     = items[i].id;
            item.name   = items[i].get ('itemname');
            item.prop   = items[i].get ('itemprop');
            item.plugin = items[i].get ('itemplugin');
            item.length = items[i].get ('itemlength');
            playlist [i] = item;
        }

        return (playlist);
    };

    var trySendProgram = function (emission) {
    	
        if ($('emissionDel').get ('value') == '0')
        {
            // Deletion of the emission if forbidden, only
            // can create a new one
            emission.id = '';
        }
        if (emission.id.length >= 1 && emission.id.toInt () >= 1)
        {
            // Override or not ?
//            var msgBox = strEmiBasedOn
//                            + '"' + $('origEmiName').get ('value') + '"<br />'
//                            + strReplaceOldEmi;
        	// Save ?
            var msgBox = strSaveEmission;

            var dialog = new ModalBox ();
            dialog.setTitle ('iPreso');
            dialog.setText  (msgBox);
            dialog.addButton ('yes', strYes);
            //dialog.addButton ('no', strNo);
            dialog.addButton ('cancel', strCancel);
            dialog.show (function (result) {

                if (result == "no")
                {
                    emission.id = "";
                    sendProgram (emission);
                }
                else if (result == "yes")
                {
                    sendProgram (emission);
                }
            });
        }
        else
        {
            sendProgram (emission);
        }
    };

    var sendProgram = function (postData) {

        var request = new Request.JSON ({
            url: '/Catalog/commitprog/format/json',
            method: 'post',
            data: { 'program': postData },
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    $('emissionId').set ('value', jsonObj.id);
                    updateEmissionsList ();
                    $('emiSaved').show();
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert (strErrorSaveCalendar + '('+xhr.status+')');
            }

        }).send ();
    };

    var trySendQuickProgram = function (emission) {

        if ($('emissionDel').get ('value') == '0')
        {
            // Deletion of the emission if forbidden, only
            // can create a new one
            emission.id = '';
        }
        if (emission.id.length >= 1 && emission.id.toInt () >= 1)
        {
            // Override or not ?
//            var msgBox = strEmiBasedOn
//                            + '"' + $('origEmiName').get ('value') + '"<br />'
//                            + strReplaceOldEmi;
        	// Save ?
            var msgBox = strSaveEmission;
            
            var dialog = new ModalBox ();
            dialog.setTitle ('iPreso');
            dialog.setText  (msgBox);
            dialog.addButton ('yes', strYes);
            //dialog.addButton ('no', strNo);
            dialog.addButton ('cancel', strCancel);
            dialog.show (function (result) {

                if (result == "no")
                {
                    emission.id = "";
                    sendProgram (emission);
                }
                else if (result == "yes")
                {
                    sendQuickProgram (emission);
                }
            });
        }
        else
        {
            sendQuickProgram (emission);
        }
    };

    var sendQuickProgram = function (postData) {

        var request = new Request.JSON ({
            url: '/Catalog/commitprog/format/json',
            method: 'post',
            data: { 'program': postData },
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    $('emissionId').set ('value', jsonObj.id);
                    createQuickProgrammation (jsonObj.id);
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert (strErrorSaveCalendar + '('+xhr.status+')');
            }

        }).send ();
    };

    var quickProgWindow = function (result, values)
    {
        if (result == "cancel")
            return;
        
        var emiId   = $('emissionId').get ('value');
        var emiName = $('newEmiName').get ('value');
        if (emiName.length < 1 && $('emiSelect'))
        {
            var optionNode = $('option-'+$('emiSelect').get ('value'));
            if (optionNode)
                emiName = optionNode.get ('text');
            else
            {
                $('newEmiName').highlight('#f00').focus();
                return;
            }
        }

        commitQuickProgrammation (emiId, emiName, values);
    }

    var commitQuickProgrammation = function (id, name, values)
    {
        var request = new Request.JSON ({
            url: '/Catalog/quickprog/format/json',
            method: 'post',
            data: { 'emission': id,
                    'name': name,
                    'programmation': values },
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    $('progId').set ('value', jsonObj.progId);
                    alert (strProgCreated + ' "' + jsonObj.progName+'"');
                    jumpToPage (jsonObj.nextPage);
                }
                else if (jsonObj.status == 2)
                {
                    var result = prompt (jsonObj.msg, name);
                    if (result)
                        commitQuickProgrammation (id, result, values);
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert (strErrorSaveCalendar + '('+xhr.status+')');
            }

        }).send ();
    };

    var createQuickProgrammation = function (emissionId) {

        var dialog = new ModalBox ();
        dialog.setTitle (strQuickProg);
        dialog.setHTMLContent ('<table cellspacing="0" cellpadding="2px">'+
                                '<tr>'+
                                 '<td>'+strStart+'</td>'+
                                 '<td><input id="evtStart" type="text" style="width: 80px;" name="evtStart" value="07:30" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td>'+strEnd+'</td>'+
                                 '<td><input id="evtEnd" type="text" style="width: 80px;" name="evtEnd" value="19:30" /></td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td colspan="2">&nbsp;</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td colspan="2">'+strRecurrence+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day0" style="width: 80px;" value="64" checked="" /></td>'+
                                 '<td>'+strMon+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day1" style="width: 80px;" value="32" checked="" /></td>'+
                                 '<td>'+strTue+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day2" style="width: 80px;" value="16" checked="" /></td>'+
                                 '<td>'+strWed+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day3" style="width: 80px;" value="8" checked="" /></td>'+
                                 '<td>'+strThu+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day4" style="width: 80px;" value="4" checked="" /></td>'+
                                 '<td>'+strFri+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day5" style="width: 80px;" value="2" /></td>'+
                                 '<td>'+strSat+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                 '<td><input type="checkbox" name="day6" style="width: 80px;" value="1" /></td>'+
                                 '<td>'+strSun+'</td>'+
                                '</tr>'+
                                '</table>');
        dialog.getValues (true);
        dialog.addButton ('ok', strOk);
        dialog.addButton ('cancel', strCancel);
        dialog.show (quickProgWindow);
    };

    var setFormDetails = function (jsonObj) {

        $('emissionId').set ('value', jsonObj.id);
        $('origEmiName').set ('value', jsonObj.name);
        $('newEmiName').set ('value', jsonObj.name);
        $('emiLayout').set ('value', jsonObj.layout);
    };

    window.addEvent ('resize', adaptTVSize);

    if ($('emiRename'))
    {
        $('emiRename').addEvent ('click', function () {

            if (!$('newEmiName'))
                return;

            var selectedNode = $('option-' + $('emiSelect').get ('value'));
            if (!selectedNode)
                return;
            $('newEmiName').set ('value', selectedNode.get ('text'));
            //$('newEmiName').show ();
            $('newEmiName').setStyle ('display', 'inline');

            if ($('bSave'))
            {
                $('bSave').show ();
                $('bSave').setStyle ('display', 'inline');
                if ($('bSaveNProg'))
                {
                    //$('bSaveNProg').show ();
                    $('bSaveNProg').setStyle ('display', 'inline');
                    $('bOnlyProg').hide ();
                }
            }
        });
    }

    if ($('emiDelete'))
    {
        $('emiDelete').addEvent ('click', function () {

            var emiName = $('option-' + $('emiSelect').get ('value')).get ('text');
            if (confirm (strConfirmDelEmission+' "'+emiName+'" ?'))
            {
                var request = new Request.JSON ({
                    url: '/Catalog/delprog/format/json',
                    method: 'post',
                    data: { 'id': $('emiSelect').get ('value') },
                    onComplete: function () {
                        updateEmissionsList ();
                    }
                }).send ();
            }
        });
    }

    if ($('myUploadBox'))
    {
        $('myUploadBox').addEvent ('click', function (e) {
            e.stop ();
            var displayed = catAccordion.previous;
            window.open('/Catalog/upload', '_blank', "width=1024px, height=600px")
        });
    }

    if ($('boxR'))
        $('boxR').hide ();
    if ($('newEmiName'))
        $('newEmiName').hide ();
    if ($('emiSaved'))
        $('emiSaved').hide ();

    adaptTVSize ();
    updateCatalog ();
    updateEmissionsList ();
});
