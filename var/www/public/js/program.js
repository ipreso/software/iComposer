Drag.Mymove = new Class ({

    Extends: Drag.Move,

    checkAgainst: function(el, i){

        var scroll = el.getScrolls();
        el = (this.positions) ? this.positions[i] : el.getCoordinates();

        el.top      = el.top + scroll.y;
        el.bottom   = el.bottom + scroll.y;
        el.left     = el.left + scroll.x;
        el.right    = el.right + scroll.x;

        var now = this.mouse.now;
        return (now.x > el.left && now.x < el.right && now.y < el.bottom && now.y > el.top);
    }
});

window.addEvent('domready', function() {

    var catAccordion;
    var secondsPerPixelBase = (60*60*24);
    var calendarObj;
    var dragObjects = new Array ();
    var poolList;

    var jumpToPage = function (page) {
        $('progEdit').set ('action', page);
        $('progEdit').submit ();
    };

    var updateCalendar = function () {
        var request = new Request.JSON ({
            url: '/Programmation/getcalendar/format/json',
            method: 'post',
            data: { 'week': $('weekId').get ('text'),
                    'prog': $('progId').get ('value')
                  },
            onComplete: function (jsonObj) {
                doUpdateCalendar (jsonObj);
            }
        }).send ();
    };

    var updateCatalog = function () {
        var request = new Request.JSON ({
            url: '/Programmation/getcatalog/format/json',
            method: 'post',
            onComplete: function (jsonObj) {
                doUpdateCatalog (jsonObj);
            }
        }).send ();
    };

    var doUpdateCalendar = function (jsonObj) {

        if (jsonObj == null || jsonObj.length < 1)
            return;

        if (jsonObj.status != 1)
        {
            if (jsonObj.msg.length > 0)
                alert (jsonObj.msg);
            return;
        }

        calendarObj = jsonObj.events;
        loadCalendar (calendarObj);

        $('origProgName').set ('value', jsonObj.progName);
        $('groups').set ('value', jsonObj.usedBy);
        if (jsonObj.progDel)
            $('permProgDel').set ('value', '1');
        else
            $('permProgDel').set ('value', '');
        if (jsonObj.progEdit)
            $('permProgEdit').set ('value', '1');
        else
            $('permProgEdit').set ('value', '');

        if ($('progDelete'))
        {
            if (jsonObj.progDel)
            {
                $('progDelete').show ();
                $('progDelete').setStyle ('display', 'inline');
            }
            else
                $('progDelete').hide ();
        }

        if ($('bSave'))
        {
            if (jsonObj.progEdit)
            {
                $('bSave').show ();
                $('bSave').setStyle ('display', 'inline');
                if ($('bBroadcast'))
                {
                    $('bBroadcast').show ();
                    $('bBroadcast').setStyle ('display', 'inline');
                }
            }
            else
            {
                $('bSave').hide ();
                if ($('bBroadcast'))
                    $('bBroadcast').hide ();
            }
        }
    };

    var loadCalendar = function (events) {

        for (var i = 0 ; i < events.length ; i++)
        {
            if (events [i].uid.match ("@ipreso.com-d"))
            {
                var drecur = 0;
                var exdate = "";
                for (var j = 0 ; j < events.length ; j++)
                {
                    var dayIndex = events [j].uid.lastIndexOf ('-d');
                    if (dayIndex < 0)
                        continue;
                    if (events [i].uid.match ("^"+events [j].uid.substr (0, dayIndex)))
                    {
                        var dayValue = events [j].uid.substr (dayIndex+2);
                        switch (dayValue)
                        {
                            case "0":
                                drecur = drecur.toInt () + 64;
                                break;
                            case "1":
                                drecur = drecur.toInt () + 32;
                                break;
                            case "2":
                                drecur = drecur.toInt () + 16;
                                break;
                            case "3":
                                drecur = drecur.toInt () + 8;
                                break;
                            case "4":
                                drecur = drecur.toInt () + 4;
                                break;
                            case "5":
                                drecur = drecur.toInt () + 2;
                                break;
                            case "6":
                                drecur = drecur.toInt () + 1;
                                break;
                        }
                        if (events [j].exdate.length > 0 && !exdate.match (events [j].exdate))
                            exdate += events [j].exdate + ',';
                    }
                }
                events [i].exdate = exdate;
                if (events [i].period != 0)
                {
                    events [i].drecur = drecur;
                }
            }
            createPeriodicEvents (events [i].uid,
                                  events [i].label,
                                  events [i].type,
                                  events [i].start,
                                  events [i].end,
                                  events [i].delay,
                                  events [i].period,
                                  events [i].drecur,
                                  events [i].exdate,
                                  events [i].pool);
        }

        if ($('calendarTable'))
        {
            var element = $('calendarTable');
            var calendarHeight = element.getStyle ('height').substr (0, element.getStyle ('height').lastIndexOf ('px'));
            if ($('calendarSizeTable'))
                $('calendarSizeTable').scrollTo (0, calendarHeight / 4);
        }
    };

    var doUpdateCatalog = function (jsonObj) {

        if (jsonObj == null || jsonObj.length < 1)
            return;

        for (var i = 0 ; i < jsonObj.length ; i++)
            addNewCategory (jsonObj [i].title, jsonObj [i].elements);

        if (catAccordion.togglers.length > 0)
            catAccordion.display (0);

        $$('.draggable').each (function (drag) {
            makeDraggable (drag);
        });

        resizeLibraryContent ();
    };

    var resizeLibraryContent = function () {

        var container = $('catalogBox');
        if (!container)
            return;

        var containerSize = container.getSize ();

        // Remove padding and thumbnail from the width
        var boxWidth = containerSize.x - (2 * 10) - 60;

        var items = $$('.catalogLabel');
        for (var i = 0 ; i < items.length ; i++)
        {
            var item = items [i];
            var label = item.getParent().get ('label');

            var divX = item.getSize ().x;
            var divY = item.getSize ().y;
            var length = label.length;
            length = length - 3;

            while (length > 3 && (divX > boxWidth || divY > 20))
            {
                item.set ('text', label.substr (0, length) + '...');
                length = length.toInt () - 1;
                divX = item.getSize ().x;
                divY = item.getSize ().y;
            }
        }

        // Resize pool if exists
        resizePoolContent ();
    };

    var resizePoolContent = function () {

        var container = $('contextualBox');
        if (!container)
            return;

        var containerSize = container.getSize ();

        // Remove padding and thumbnail from the width
        var boxWidth = containerSize.x - (2 * 10) - 60;
        // Remove del link
        boxWidth = boxWidth - 17;

        var items = $$('.poolLabel');
        for (var i = 0 ; i < items.length ; i++)
        {
            var item = items [i];
            var label = item.get ('title');

            var divX = item.getSize ().x;
            var divY = item.getSize ().y;
            var length = label.length;
            length = length - 3;

            while (length > 3 && (divX > boxWidth || divY > 20))
            {
                item.set ('text', label.substr (0, length) + '...');
                length = length.toInt () - 1;
                divX = item.getSize ().x;
                divY = item.getSize ().y;
            }
        }
    }

    var dragFromCatalog = function (element, evt) {
        element.setStyles ({'top': evt.page.y,
                            'left': evt.page.x});
    };

    var startToDragFromCatalog = function (element) {

        var scroll = {x:0, y: 0};
        element.getParents ().each (function (el)
        {
            if (['auto','scroll'].contains (el.getStyle ('overflow')))
            {
                scroll = {
                    x: scroll.x + el.getScroll().x,
                    y: scroll.y + el.getScroll().y
                }
            }
        });
        var position = element.getPosition();
        var evType = element.getParent ().id.substring (0, element.getParent ().id.indexOf ('_'));
        if (!evType || evType.length < 1)
            evType = 'Unknown';
        if (evType.match ("^Media -"))
            evType = 'Media';
        else if (evType.match ("^Actions -"))
            evType = 'Action';
        else if (evType.match ("^Emissions"))
            evType = 'Emission';
        else
            evType = 'Unknown';

        var newElement = element.clone ();
        element.getParent ().grab (newElement);
        element.setStyles ({'position': 'absolute',
                            'padding': '5px',
                            'border': '1px solid #a7a7a7',
                            'top': position.y + scroll.y,
                            'left': position.x + scroll.x});
        element.inject (document.body);
        newElement.set ('id', element.get ('id'));
        element.set ('plugid', element.get ('id'));
        element.set ('id', element.get ('id')+'_drag');
        element.set ('evType', evType);

        makeDraggable (newElement);
    };

    var enterDropZone = function (elt, drop) {
        elt.setStyles ({'border': '1px solid #000000'});
        drop.setStyles ({'border': '1px solid #000000'});
    };

    var leaveDropZone = function (elt, drop) {
        elt.setStyles ({'border': '1px solid #a7a7a7'});
        drop.setStyles ({'border': '0px'});
    };

    var getNewUID = function () {

        var length  = 10;
        var sUID    = "";

        for (var i = 0 ; i < length ; i++)
        {
            var numI = getRandomNum();
            while (checkPunc (numI))
            {
                numI = getRandomNum();
            }

            sUID = sUID + String.fromCharCode (numI);
        }

        return (sUID + '@ipreso.com');
    };

    var getRandomNum = function () {

        var rndNum = Math.random()
        rndNum = parseInt (rndNum * 1000);
        rndNum = (rndNum % 74) + 48;
        return (rndNum);
    };

    var checkPunc = function (num) {

        if (num < 48) { return true; }
        if ((num >=58) && (num <=64)) { return true; }
        if ((num >=91) && (num <=96)) { return true; }
        if (num >=123) { return true; }

        return false;
    };

    var dropFromCatalog = function (elt, drop, evt) {

        if (drop)
        {
            drop.setStyles ({'border': '0px'});

            var startPixel  = evt.page.y - drop.getCoordinates().top;
            var nbSeconds   = Math.round (startPixel * secondsPerPixel);
            var dayPart = drop.get ('id').substr (3, 1);

            var newEvent = new Object ();
            newEvent.uid    = getNewUID () + '-d' + dayPart;
            newEvent.label  = elt.get ('label');
            newEvent.type   = elt.get ('evType');
            newEvent.plugin = elt.get ('plugin');
            newEvent.pool   = new Object();
            newEvent.plugid = elt.get ('plugid');
            newEvent.start  = drop.get ('start').toInt () + nbSeconds;
            switch (newEvent.type)
            {
                case "Action":
                    newEvent.end = newEvent.start;
                    break;
                case "Media":
                case "Emission":
                default:
                    newEvent.end = newEvent.start + 3600;
            }
            newEvent.delay  = $('day0').get ('delay').toInt ();
            newEvent.period = 0;

            newEvent.exdate = "";
            newEvent.drecur = 0;

            var result = true;
            if (newEvent.type == "Emission" ||
                newEvent.type == "Media")
            {
                result = isNewEvent (newEvent);
            }

            if (result)
            {
                calendarObj.push (newEvent);
                createPeriodicEvents (newEvent.uid,
                                      newEvent.label,
                                      newEvent.type,
                                      newEvent.start,
                                      newEvent.end,
                                      newEvent.delay,
                                      newEvent.period,
                                      newEvent.drecur,
                                      newEvent.exdate,
                                      newEvent.pool);

                switch (newEvent.type)
                {
                    case "Action":
                        showActionInfo (newEvent.uid + '-r0');
                        break;
                    case "Emission":
                    case "Media":
                    default:
                        showEventsInfo (newEvent.uid + '-r0');
                }
            }
        }

        elt.destroy ();
    };

    var makeDraggable = function (element) {

        var dragObj = new Drag.Mymove (element,
                    {
                        snap: 1,
                        droppables: '.dayDrop',
                        style: false,
                        onStart: startToDragFromCatalog,
                        onDrag: dragFromCatalog,
                        onEnter: enterDropZone,
                        onLeave: leaveDropZone,
                        onDrop: dropFromCatalog
                    });

        dragObjects.push (dragObj);
    };

    var createPeriodicEvents = function (uid, label, type, start, end, delay, period, drecur, exdate, pool) {

        var divDays = $$('.eventCol');

        var recurId         = 0;
        var currentStart    = start;
        var currentEnd      = end;
        var calendarDelay   = $('day0').get ('delay').toInt ();
        var currentDelay    = delay.toInt ();
        var dayStart;
        var dayEnd;
        var startPixel;
        var endPixel;
        var node;
        var done = false;

        period = period.toInt ();
        if (period)
        {
            currentStart    = currentStart.toInt ();
            currentEnd      = currentEnd.toInt ();

            // Manage daylight saving time
            currentStart    = currentStart + (currentDelay - calendarDelay);
            currentEnd      = currentEnd + (currentDelay - calendarDelay);

            var startCalendar   = divDays [0].get ('start').toInt ();
            var endCalendar     = divDays [divDays.length-1].get ('end').toInt ();
            // If the calendar is showing something BEFORE
            // the first occurence, forget this task
            if (currentStart > endCalendar)
                return;

            while (currentStart < startCalendar)
            {
                currentStart    = currentStart + period;
                currentEnd      = currentEnd + period;
            }
            var firstStart = currentStart - period;
            while (firstStart >= startCalendar)
            {
                currentStart    = firstStart;
                currentEnd      = currentEnd - period;
                firstStart      = firstStart - period;
            }
        }

        for (var i = 0 ; !done && i < divDays.length ; i++)
        {
            dayStart    = divDays [i].get ('start').toInt ();
            dayEnd      = divDays [i].get ('end').toInt ();
            while (!done && currentStart >= dayStart && currentStart < dayEnd)
            {
                if (currentEnd > dayEnd)
                    currentEnd = dayEnd;

                if (!exdate.match (currentStart))
                {
                    startPixel  = (currentStart - dayStart) / secondsPerPixel;
                    endPixel    = (currentEnd - dayStart) / secondsPerPixel - startPixel;
                    startPixel  = Math.round (startPixel);
                    endPixel    = Math.round (endPixel)

                    switch (type)
                    {
                        case "Action":
                            node = createAction (divDays [i],
                                                 uid + '-r' + recurId++,
                                                 label, type, startPixel);
                            break;
                        case "Pool":
                            if (pool == undefined)
                            {
                                alert ('Empty pool !');
                                pool = new Object();
                            }
                            node = createEvent (divDays [i],
                                                uid + '-r' + recurId++,
                                                label, type, startPixel,
                                                endPixel, pool);
                            break;
                        case "Media":
                        case "Emission":
                        default:
                            node = createEvent (divDays [i],
                                                uid + '-r' + recurId++,
                                                label, type, startPixel,
                                                endPixel);
                    }

                    node.set ('label',  label);
                    node.set ('evtype', type);
                    node.set ('start',  currentStart);
                    node.set ('end',    currentEnd);
                    node.set ('period', period);
                    node.set ('uid',    uid);
                    node.set ('drecur', drecur);
                    node.set ('exdate', exdate);
                    node.set ('delay',  currentDelay);
                }

                if (period)
                {
                    currentStart    += period;
                    currentEnd      += period;
                }
                else
                    done = true;
            }
        }
    };

    var createEvent = function (tdDay, id, label, type,
                                topPixel, heightPixel, pool) {

        if (pool == undefined)
            pool = new Object();

        if ($(id))
            $(id).destroy ();

        var eventDiv = new Element ('div', { 'class': 'evt' });
        var eventDl = new Element ('dl', {'class': 'evt-'+type});
        var eventDt = new Element ('dt', {'text': label});
        var eventDd = new Element ('dd', {'class': 'no-margin'});
        var eventResizer = new Element ('div', {'class':'resizer'});
        var eventResizerIco = new Element ('div', {'class':'resizer-ico'});

        eventDiv.set('title', label);
        eventDiv.setStyle ('top', topPixel);
        eventDl.setStyle ('height', heightPixel);
        eventDiv.setStyle ('z-index', 10);

        eventResizer.adopt (eventResizerIco);

        eventDd.innerHTML = '';
        if (type == "Pool")
        {
            if (pool.size.toInt () > 0)
            {
                for (key in pool)
                {
                    if (key == "size")
                        continue;
                    eventDd.innerHTML += '- '+pool [key]+'<br />';
                }
            }
        }

        eventDl.adopt (eventDt, eventDd, eventResizer);
        eventDiv.adopt (eventDl);

        tdDay.adopt (eventDiv);

        eventDiv.set ('id', id);

        eventDiv.addEvent ('click', function (e) {
            e.stop ();
            showEventsInfo (this.id);
        });

        var eventResize = new Drag (eventDl, {
                                    handle: eventResizer,
                                    snap: 1,
                                    modifiers: {x: '', y: 'height'},
                                    onComplete: updateEndOfEvt
                                            });

        var eventMove = new Drag (eventDiv, {
                                    handle: eventDt,
                                    snap: 1,
                                    modifiers: {x: '', y: 'top'},
                                    onComplete: updateStartOfEvt
                                            });

        return (eventDiv);
    };

    var createAction = function (tdDay, id, label, type, topPixel) {

        if ($(id))
            $(id).destroy ();

        var eventDiv = new Element ('div', { 'class': 'evt' });
        var eventDl = new Element ('dl', {'class': 'evt-'+type});
        var eventDt = new Element ('dt', {'text': label});
        var eventDd = new Element ('dd');

        eventDiv.set('title', label);
        eventDiv.setStyle ('top', topPixel);
        eventDiv.setStyle ('z-index', 20);

        eventDl.adopt (eventDt, eventDd);
        eventDiv.adopt (eventDl);

        tdDay.adopt (eventDiv);

        eventDiv.set ('id', id);

        eventDiv.addEvent ('click', function (e) {
            e.stop ();
            showActionInfo (this.id);
        });

        var eventMove = new Drag (eventDiv, {
                                    handle: eventDt,
                                    snap: 1,
                                    modifiers: {x: '', y: 'top'},
                                    onComplete: updateStartOfAction
                                            });

        return (eventDiv);
    };

    var updateEndOfEvt = function (element) {

        var parentNode  = element.getParent ();
        var newHeight   = element.getStyle ('height').substr (0, element.getStyle ('height').lastIndexOf ('px'));
        var newEndDate  = parentNode.get ('start').toInt () + (newHeight*secondsPerPixel);
        newEndDate = (Math.floor (newEndDate / 60)) * 60;
        parentNode.set ('end', newEndDate);
        showEventsInfo (parentNode.get ('id'));
        updateEvent ();
    };

    var updateStartOfEvt = function (element) {

        var dayNode  = element.getParent ();
        var newTop = element.getStyle ('top').substr (0, element.getStyle ('top').lastIndexOf ('px'));
        if (newTop.toInt () < 0)
            newTop = 0;
        var newStartDate = newTop * secondsPerPixel + dayNode.get ('start').toInt ();
        newStartDate = (Math.floor (newStartDate / 60)) * 60;
        element.set ('start', newStartDate);
        updateEndOfEvt (element.getElement ('dl'));
    };

    var updateStartOfAction = function (element) {

        var dayNode  = element.getParent ();
        var newTop = element.getStyle ('top').substr (0, element.getStyle ('top').lastIndexOf ('px'));
        if (newTop.toInt () < 0)
            newTop = 0;
        var newStartDate = newTop * secondsPerPixel + dayNode.get ('start').toInt ();
        newStartDate = (Math.floor (newStartDate / 60)) * 60;
        element.set ('start', newStartDate);
        showActionInfo (element.get ('id'));
        updateAction ();
    };

    var getTitleStr = function (str) {
        return ('<p style="text-align: center; margin-bottom: 10px;">'
                    + str + '</p>');
    };

    var getTableHeader = function () {
        return ('<table cellspacing="0" cellpadding="2px">');
    }
    var getTableFooter = function () {
        return ('</table>');
    }

    var getInputTextLine = function (name, label, value) {
        return ('<tr>' +
                   '<td>'+label+'</td>' +
                   '<td><input type="text" name="'+name+'" id="'+name+'" ' +
                               'style="width: 80px;" ' +
                               'value="'+value+'" /></td>' +
                '</tr>');
    }

    var getTextLine = function (line) {
        return ('<tr><td colspan="2">'+line+'</td></tr>');
    }

    var getInputCBLine = function (name, label, value, checked) {
        if (checked > 0)
            ckbStr = " checked";
        else
            ckbStr = "";

        return ('<tr>' +
                   '<td><input type="checkbox" name="'+name+'" id="'+name+'" ' +
                               'style="width: 80px;" ' +
                               'value="'+value+'"'+ckbStr+'/>' +
                   '<td>'+label+'</td>' +
                '</tr>');
    }

    var showActionInfo = function (actionId) {
        if (!$('boxR') || !$('contextualBox'))
            return;

        var actionNode = $(actionId);
        if (!actionNode)
        {
            $('boxR').hide ();
            return;
        }

        selectDiv (actionNode);

        $('contextualBox').empty ();
        $('boxR').show ();

        var evExdate = actionNode.get ('exdate');
        if (!evExdate)
            evExdate = "";
        html = '<input type="hidden" id="evId" name="evId" value="'+actionNode.get ('id')+'" />';
        html += '<input type="hidden" id="evUid" name="evUid" value="'+actionNode.get ('uid')+'" />';
        html += '<input type="hidden" id="evExdate" name="evExdate" value="'+evExdate+'" />';
        html += '<input type="hidden" id="evDelay" name="evDelay" value="'
                 +$('day0').get ('delay')+'" />';
        html += getTitleStr (actionNode.get ('evtype')+': '+
                             actionNode.get ('label'));

        html += getTableHeader ();
        html += getInputTextLine ('evtTime', strTime, 
                                  convertToTime (actionNode.get ('start').toInt (),
                                                 actionNode.getParent ().get ('delay').toInt ()));

        html += getTextLine ('&nbsp;');
        html += getTextLine (strRecurrence);

        var nodeDrecur = actionNode.get ('drecur');
        var period;
        if (!nodeDrecur)
            period = 0;
        else
            period = nodeDrecur.toInt ();
        html += getInputCBLine ('recMon', strMon,       64, (period & 64));
        html += getInputCBLine ('recTue', strTue,       32, (period & 32));
        html += getInputCBLine ('recWed', strWed,       16, (period & 16));
        html += getInputCBLine ('recThu', strThu,       8,  (period & 8));
        html += getInputCBLine ('recFri', strFri,       4,  (period & 4));
        html += getInputCBLine ('recSat', strSat,       2,  (period & 2));
        html += getInputCBLine ('recSun', strSun,       1,  (period & 1));
        html += getTableFooter ();

        html += '<p style="text-align: center; margin-top: 10px;">' +
                    '<input type="submit" name="okEvent" id="okEvent" ' +
                        'value="'+strApply+'" />' +
                    '<input type="submit" name="delEvent" id="delEvent" ' +
                        'value="'+strDelete+'" />' +
                '<p>';

        $('contextualBox').innerHTML = html;

        $('delEvent').addEvent ('click', function (e) {
            e.stop ();

            var evId = $('evId').get ('value');
            var uid = $('evUid').get ('value');
            var evts = $$('.evt');

            if (evId && $(evId).get ('period').toInt () > 0)
            {
                var dialog = new ModalBox ();
                dialog.setTitle ('iPreso');
                dialog.setText  (strDeleteAllOccur);
                dialog.addButton ('yes', strYesAll);
                dialog.addButton ('weekly', strDeleteWeeklyOccur);
                dialog.addButton ('no', strOnlyThisOne);
                dialog.addButton ('cancel', strCancel);
                dialog.show (function (result) {

                    if (result == "no")
                    {
                        var timestamp = "";
                        for (i = 0 ; i < evts.length ; i++)
                        {
                            if (evts [i].get ('id').match ("^"+evId))
                                timestamp = evts [i].get ('start');
                        }
                        if (timestamp.length)
                        {
                            $('evExdate').set ('value',
                                timestamp + ',' + $('evExdate').get ('value'));
                        }
                        updateAction ();
                        $('boxR').hide ();
                    }
                    else if (result == "weekly")
                    {
                        calendarIndex = getCalendarEventIndex (uid);
                        if ($(evId))
                            $(evId).destroy ();
                        else
                            alert (uid);
                        if (calendarIndex >= 0)
                            calendarObj.splice (calendarIndex, 1);
                        $('boxR').hide ();
                    }
                    else if (result == "yes")
                    {
                        var dayPos = uid.lastIndexOf ('-d');
                        if (dayPos >= 0)
                        {
                            var baseUID = uid.substr (0, dayPos);
                            for (d = 0 ; d < 7 ; d++)
                            {
                                var fullUID = baseUID+'-d'+d+'-r0'
                                if ($(fullUID))
                                {
                                    evIndex = getCalendarEventIndex (baseUID+'-d'+d);
                                    if (evIndex >= 0)
                                    {
                                        calendarObj.splice (evIndex, 1);
                                        $(fullUID).destroy ();
                                    }
                                }
                            }
                        }
                        $('boxR').hide ();
                    }
                });
                return;
            }
            else
            {
                calendarIndex = getCalendarEventIndex (uid);
                if ($(evId))
                    $(evId).destroy ();
                else
                    alert (uid);
                if (calendarIndex >= 0)
                    calendarObj.splice (calendarIndex, 1);
                $('boxR').hide ();
            }
            var dayPos = uid.lastIndexOf ('-d');
            if (dayPos >= 0 && $(evId).get ('period').toInt () > 0)
            {
                var day = uid.substr (uid.lastIndexOf ('-d'));
                switch (day)
                {
                    case "-d0":
                        $('recMon').set ('checked', false);
                        break;
                    case "-d1":
                        $('recTue').set ('checked', false);
                        break;
                    case "-d2":
                        $('recWed').set ('checked', false);
                        break;
                    case "-d3":
                        $('recThu').set ('checked', false);
                        break;
                    case "-d4":
                        $('recFri').set ('checked', false);
                        break;
                    case "-d5":
                        $('recSat').set ('checked', false);
                        break;
                    case "-d6":
                        $('recSun').set ('checked', false);
                        break;
                }
                updateAction ();
                $('boxR').hide ();
            }
            else
            {
                calendarIndex = getCalendarEventIndex (uid);
                if ($(evId))
                    $(evId).destroy ();
                else
                    alert (uid);
                if (calendarIndex >= 0)
                    calendarObj.splice (calendarIndex, 1);
                $('boxR').hide ();
            }
        });

        $('okEvent').addEvent ('click', updateAction);
    };

    var selectDiv = function (node) {

        unselectDivs ();

        var dlNode = node.getElement ('dl');
        if (!dlNode)
            return;
        dlNode.addClass ('selected');

        var dtNode = node.getElement ('dt');
        if (!dtNode)
            return;
        dtNode.addClass ('selected');
    };

    var unselectDivs = function () {

        var nodes = $$('.selected');
        for (var i = 0 ; i < nodes.length ; i++)
            nodes.removeClass ('selected');
    };

    var getPoolItemHtml = function (id, item) {

        var keyId = id.split ('-');
        var index = keyId [0];
        var emiId = keyId [1];

        var html = '<li id="' + index + '-' + emiId + '" '
                + 'class="poolItem" '
                + 'index="'+index+'" '
                + 'emiid="'+emiId+'">\n';

        html += '<span style="float: right;">'
                + '<a href="#" class="delItem">'
                +   '<img src="/images/Del.png" alt="del" />'
                + '</a>'
                + '</span>\n';

        html += '<img alt="'+item+'" class="preview" '
                    + 'title="'+item+'" '
                    + 'src="/Catalog/getemilayout/id/' + emiId + '/layout.png" />\n';

        html += '<span class="poolLabel" title="'+item+'">' + item + '</span>\n';

/*
        html += '<span class="spanLength">'
                    + getFormattedTime (item.length) + '</span>\n';
*/

        html += '</li>\n';
        return (html);
    };

    var deleteItemEvent = function (item) {

        delLink = item.getElement ('.delItem');
        if (!delLink)
            return;

        delLink.addEvent ('click', function (e) {
            e.stop ();

            var delId = item.get ('emiid');
            var delIndex = item.get ('index');
            var eventId = $('evUid').get ('value');

            var calIndex = getCalendarEventIndex (eventId);
            if (calIndex < 0)
                return;

            // Delete emission from the calendar event pool
            delete (calendarObj [calIndex].pool [delIndex+'-'+delId]);

            // If pool is one emission or less, convert it to a simple emission
            var i = 0;
            var emiId;
            var emiLabel;
            for (key in calendarObj [calIndex].pool)
            {
                if (key == "size")
                    continue;
                i++;
                emiId = key;
                emiLabel = calendarObj [calIndex].pool [key];
            }
            calendarObj [calIndex].pool.size = i;
            if (i <= 1)
            {
                calendarObj [calIndex].label = emiLabel;
                calendarObj [calIndex].type = "Emission";
                delete (calendarObj [calIndex].pool [emiId]);
            }

            // Delete emission from the pool display
            if (calendarObj [calIndex].period > 0)
                addOrRemoveReccurentEvents (calendarObj [calIndex]);
            else
                createPeriodicEvents (calendarObj [calIndex].uid,
                                      calendarObj [calIndex].label,
                                      calendarObj [calIndex].type,
                                      calendarObj [calIndex].start,
                                      calendarObj [calIndex].end,
                                      calendarObj [calIndex].delay,
                                      calendarObj [calIndex].period,
                                      calendarObj [calIndex].drecur,
                                      calendarObj [calIndex].exdate,
                                      calendarObj [calIndex].pool);
            showEventsInfo (eventId + '-r0');

            // Delete emission from the ordered playlist
            item.destroy ();
        });
    };

    var reorderPool = function () {

        var index = getCalendarEventIndex ($('evUid').get('value'));
        if (index < 0)
            return;

        var newPool = new Object;
        var elements = $$('.poolItem');
        for (var i = 0 ; i < elements.length ; i++)
        {
            newPool [i+'-'+elements[i].get('emiid')] = calendarObj [index].pool [elements [i].get ('id')];
            elements[i].set ('id', i+'-'+elements[i].get('emiid'));
        }
        newPool.size = elements.length;

        calendarObj [index].pool = newPool;
    };

    var getHTMLEmissionsPool = function (eventId) {

        var index = getCalendarEventIndex (eventId);
        var html = '';
        if (index < 0)
            return (html);

        var pool = calendarObj [index].pool;

        html = '<ol class="poolList" id="seqPool">';
        if (pool.size > 0)
        {
            for (var key in pool)
            {
                if (key == "size")
                    continue;
                html += getPoolItemHtml (key, pool [key]);
            }
        }
        html += '</ol>';
        html += '<span id="totalLength" style="float: right; font-weight: bold;"></span>';

        return (html);
    };

    var showEventsInfo = function (eventId) {
        if (!$('boxR') || !$('contextualBox'))
            return;

        var eventNode = $(eventId);
        if (!eventNode)
        {
            $('boxR').hide ();
            return;
        }

        selectDiv (eventNode);

        $('contextualBox').empty ();
        $('boxR').show ();

        var evExdate = eventNode.get ('exdate');
        if (!evExdate)
            evExdate = "";

        html = '<input type="hidden" id="evId" name="evId" value="'+eventNode.get ('id')+'" />';
        html += '<input type="hidden" id="evUid" name="evUid" value="'+eventNode.get ('uid')+'" />';
        html += '<input type="hidden" id="evExdate" name="evExdate" value="'+evExdate+'" />';
        html += '<input type="hidden" id="evDelay" name="evDelay" value="'
                 +$('day0').get ('delay')+'" />';
        html += getTitleStr (eventNode.get ('evtype')+': '+
                             eventNode.get ('label'));

        html += getTableHeader ();
        html += getInputTextLine ('evtStart', strStart, 
                                  convertToTime (eventNode.get ('start').toInt (),
                                                 eventNode.getParent ().get ('delay').toInt ()));
        html += getInputTextLine ('evtEnd', strEnd, 
                                  convertToTime (eventNode.get ('end').toInt (),
                                                 eventNode.getParent ().get ('delay').toInt ()));

        html += getTextLine ('&nbsp;');
        html += getTextLine (strRecurrence);

        var nodeDrecur = eventNode.get ('drecur');
        var period;
        if (!nodeDrecur)
            period = 0;
        else
            period = nodeDrecur.toInt ();
        html += getInputCBLine ('recMon', strMon,       64, (period & 64));
        html += getInputCBLine ('recTue', strTue,       32, (period & 32));
        html += getInputCBLine ('recWed', strWed,       16, (period & 16));
        html += getInputCBLine ('recThu', strThu,       8,  (period & 8));
        html += getInputCBLine ('recFri', strFri,       4,  (period & 4));
        html += getInputCBLine ('recSat', strSat,       2,  (period & 2));
        html += getInputCBLine ('recSun', strSun,       1,  (period & 1));
        html += getTableFooter ();

        if (eventNode.get ('evtype') == 'Pool')
            html += getHTMLEmissionsPool (eventNode.get ('uid'));

        html += '<p style="text-align: center; margin-top: 10px;">' +
                    '<input type="submit" name="okEvent" id="okEvent" ' +
                        'value="'+strApply+'" />' +
                    '<input type="submit" name="delEvent" id="delEvent" ' +
                        'value="'+strDelete+'" />' +
                '<p>';

        $('contextualBox').innerHTML = html;

        resizePoolContent ();
        var poolItems = $$('.poolItem');
        if (poolItems.length > 0)
        {
            for (var i = 0 ; i < poolItems.length ; i++)
                deleteItemEvent (poolItems [i]);
            poolList = new Sortables ($('seqPool'),
                            { 'constrain': false,
                              onComplete:  reorderPool
                            });
        }

        $('delEvent').addEvent ('click', function (e) {
            e.stop ();

            var evId = $('evId').get ('value');
            var uid = $('evUid').get ('value');
            var evts = $$('.evt');

            if (evId && 
                $(evId).get ('drecur') &&
                $(evId).get ('drecur').toInt () > 0)
            {
                var dialog = new ModalBox ();
                dialog.setTitle ('iPreso');
                dialog.setText  (strDeleteAllOccur);
                dialog.addButton ('yes', strYesAll);
                dialog.addButton ('weekly', strDeleteWeeklyOccur);
                dialog.addButton ('no', strOnlyThisOne);
                dialog.addButton ('cancel', strCancel);
                dialog.show (function (result) {

                    if (result == "no")
                    {
                        var timestamp = "";
                        for (i = 0 ; i < evts.length ; i++)
                        {
                            if (evts [i].get ('id').match ("^"+evId))
                                timestamp = evts [i].get ('start');
                        }
                        if (timestamp.length)
                        {
                            $('evExdate').set ('value',
                                timestamp + ',' + $('evExdate').get ('value'));
                        }
                        updateEvent ();
                        $('boxR').hide ();
                    }
                    else if (result == "yes")
                    {
                        var dayPos = uid.lastIndexOf ('-d');
                        if (dayPos >= 0)
                        {
                            var baseUID = uid.substr (0, dayPos);
                            for (d = 0 ; d < 7 ; d++)
                            {
                                var fullUID = baseUID+'-d'+d+'-r0'
                                if ($(fullUID))
                                {
                                    evIndex = getCalendarEventIndex (baseUID+'-d'+d);
                                    if (evIndex >= 0)
                                    {
                                        calendarObj.splice (evIndex, 1);
                                        $(fullUID).destroy ();
                                    }
                                }
                            }
                        }
                        $('boxR').hide ();
                    }
                    else if (result == "weekly")
                    {
                        calendarIndex = getCalendarEventIndex (uid);
                        if ($(evId))
                            $(evId).destroy ();
                        else
                            alert (uid);
                        if (calendarIndex >= 0)
                            calendarObj.splice (calendarIndex, 1);

                        $('boxR').hide ();
                    }
                });
                return;
            }
            else
            {
                calendarIndex = getCalendarEventIndex (uid);
                if ($(evId))
                    $(evId).destroy ();
                else
                    alert (uid);
                if (calendarIndex >= 0)
                    calendarObj.splice (calendarIndex, 1);
                $('boxR').hide ();
            }
            var dayPos = uid.lastIndexOf ('-d');
            if (dayPos >= 0)
            {
                var day = uid.substr (uid.lastIndexOf ('-d'));
                switch (day)
                {
                    case "-d0":
                        $('recMon').set ('checked', false);
                        break;
                    case "-d1":
                        $('recTue').set ('checked', false);
                        break;
                    case "-d2":
                        $('recWed').set ('checked', false);
                        break;
                    case "-d3":
                        $('recThu').set ('checked', false);
                        break;
                    case "-d4":
                        $('recFri').set ('checked', false);
                        break;
                    case "-d5":
                        $('recSat').set ('checked', false);
                        break;
                    case "-d6":
                        $('recSun').set ('checked', false);
                        break;
                }
                updateEvent ();
                $('boxR').hide ();
            }
        });

        $('okEvent').addEvent ('click', updateEvent);
    };

    var updateAction = function () {

        var uid = $('evId').get ('value');
        if (!$(uid))
        {
            $('boxR').hide ();
            return;
        }

        var parentNode = $(uid).getParent ();

        var label   = $(uid).get ('label');
        var type    = $(uid).get ('evtype');
        var newPeriodInSec = $(uid).get ('period');

        var start       = $('evtTime').get ('value');
        var stop        = start;
        var delay       = $('evDelay').get ('value');

        var newRecur = 0;
        if ($('recMon').get ('checked'))
            newRecur += $('recMon').get ('value').toInt ();
        if ($('recTue').get ('checked'))
            newRecur += $('recTue').get ('value').toInt ();
        if ($('recWed').get ('checked'))
            newRecur += $('recWed').get ('value').toInt ();
        if ($('recThu').get ('checked'))
            newRecur += $('recThu').get ('value').toInt ();
        if ($('recFri').get ('checked'))
            newRecur += $('recFri').get ('value').toInt ();
        if ($('recSat').get ('checked'))
            newRecur += $('recSat').get ('value').toInt ();
        if ($('recSun').get ('checked'))
            newRecur += $('recSun').get ('value').toInt ();

        var uid = $('evUid').get ('value');
        var exdate = $('evExdate').get ('value');

        var i = getCalendarEventIndex (uid);

        if (newRecur == 0)
        {
            exdate = "";
            period = 0;
        }
        else if (newPeriodInSec == 0 && i >= 0)
            newPeriodInSec = 60*60*24*7;

        var evts = $$('.evt');
        for (j = 0 ; j < evts.length ; j++)
        {
            if (evts [j].get ('uid') == uid)
                evts [j].destroy ();
        }

        start   = convertToTimestamp (parentNode, start);
        stop    = convertToTimestamp (parentNode, stop);

        if (i >= 0)
        {
            calendarObj [i].start   = start;
            calendarObj [i].end     = stop;
            calendarObj [i].period  = newPeriodInSec;
            calendarObj [i].drecur  = newRecur;
            calendarObj [i].exdate  = exdate;
            calendarObj [i].delay   = delay;
            
            if (newPeriodInSec > 0)
                addOrRemoveReccurentEvents (calendarObj [i]);
            else
                createPeriodicEvents (uid, label, type, start, stop, delay, newPeriodInSec, newRecur, exdate, new Array());
        }

        uid = $('evId').get ('value');
        if (!$(uid))
        {
            $('boxR').hide ();
            return;
        }
        selectDiv ($(uid));
    };

    var updateEvent = function () {

        var uid = $('evId').get ('value');
        if (!$(uid))
        {
            $('boxR').hide ();
            return;
        }

        var parentNode = $(uid).getParent ();

        var label   = $(uid).get ('label');
        var type    = $(uid).get ('evtype');
        var newPeriodInSec = $(uid).get ('period').toInt ();

        var start       = $('evtStart').get ('value');
        var stop        = $('evtEnd').get ('value');
        var delay       = $('evDelay').get ('value');

        var newRecur = 0;
        if ($('recMon').get ('checked'))
            newRecur += $('recMon').get ('value').toInt ();
        if ($('recTue').get ('checked'))
            newRecur += $('recTue').get ('value').toInt ();
        if ($('recWed').get ('checked'))
            newRecur += $('recWed').get ('value').toInt ();
        if ($('recThu').get ('checked'))
            newRecur += $('recThu').get ('value').toInt ();
        if ($('recFri').get ('checked'))
            newRecur += $('recFri').get ('value').toInt ();
        if ($('recSat').get ('checked'))
            newRecur += $('recSat').get ('value').toInt ();
        if ($('recSun').get ('checked'))
            newRecur += $('recSun').get ('value').toInt ();

        var uid = $('evUid').get ('value');
        var exdate = $('evExdate').get ('value');

        var i = getCalendarEventIndex (uid);

        if (newRecur == 0)
        {
            exdate = "";
            period = 0;
        }
        else if (newPeriodInSec == 0 && i >= 0)
            newPeriodInSec = 60*60*24*7;

        var evts = $$('.evt');
        for (j = 0 ; j < evts.length ; j++)
        {
            if (evts [j].get ('uid') == uid)
                evts [j].destroy ();
        }

        start   = convertToTimestamp (parentNode, start);
        stop    = convertToTimestamp (parentNode, stop);

        if (i >= 0)
        {
            calendarObj [i].start   = start;
            calendarObj [i].end     = stop;
            calendarObj [i].period  = newPeriodInSec;
            calendarObj [i].drecur  = newRecur;
            calendarObj [i].exdate  = exdate;
            calendarObj [i].delay   = delay;

            if (newPeriodInSec > 0)
                addOrRemoveReccurentEvents (calendarObj [i]);
            else
                createPeriodicEvents (uid, label, type, start, stop, delay, newPeriodInSec, newRecur, exdate, calendarObj [i].pool);
        }

        uid = $('evId').get ('value');
        if (!$(uid))
        {
            $('boxR').hide ();
            return;
        }
        selectDiv ($(uid));
    };

    var addEventToPool = function (originalEvent, newEvent) {

        // If original event is not an emissions pool, transform it
        if (originalEvent.type != "Pool")
        {
            originalEvent.type = "Pool";
            originalEvent.pool = new Object ();
            originalEvent.pool ['0-'+originalEvent.plugid] = originalEvent.label;
            originalEvent.label = strEmissionsPool;
            originalEvent.pool.size = 1;
        }

        originalEvent.pool [originalEvent.pool.size+'-'+newEvent.plugid] = newEvent.label;
        return (originalEvent);
    };

    var isNewEvent = function (newEvent) {

        var result = true;

        if (!newEvent)
            return result;

        var dDay = -1;
        var dayPos = newEvent.uid.lastIndexOf ('-d');
        var dayNode;
        if (dayPos >= 0)
        {
            dDay = newEvent.uid.substr (dayPos + 2);
            dayNode = $('day'+dDay);
        }
        else
            dayNode = $('day0');
            
        var tmpDay;
        var tmpDayPos;
        for (i = 0 ; i < calendarObj.length ; i++)
        {
            if (calendarObj [i].uid == newEvent.uid)
                continue;

            tmpDay = -1;
            tmpDayPos = calendarObj[i].uid.lastIndexOf ('-d');
            if (tmpDayPos >= 0)
            {
                tmpDay = calendarObj [i].uid.substr (tmpDayPos + 2);
                if (tmpDay != dDay)
                    continue;
            }

            // Get the calendar's event associated to displayed week
            if (calendarObj [i].period > 0)
            {
                while (calendarObj [i].start < dayNode.get ('start').toInt () - $('day0').get ('delay').toInt())
                {
                    calendarObj [i].start   += calendarObj [i].period;
                    calendarObj [i].end     += calendarObj [i].period;

                    while (calendarObj [i].exdate.match (calendarObj [i].start))
                    {
                        calendarObj [i].start   += calendarObj [i].period;
                        calendarObj [i].end     += calendarObj [i].period;
                    }
                }
            }

            if (calendarObj [i].start > dayNode.get ('end') - $('day0').get ('delay').toInt ())
            {
                // Event is not the correct day
                continue;
            }

            if (calendarObj [i].start < newEvent.start &&
                calendarObj [i].end > newEvent.start)
            {
                // New event starts before the end of an existing one
                result = false;
                calendarObj [i] = addEventToPool (calendarObj [i], newEvent);
                objUid = calendarObj [i].uid;
                if (calendarObj [i].period > 0)
                    addOrRemoveReccurentEvents (calendarObj [i]);
                else
                    createPeriodicEvents (calendarObj [i].uid,
                                          calendarObj [i].label,
                                          calendarObj [i].type,
                                          calendarObj [i].start,
                                          calendarObj [i].end,
                                          calendarObj [i].delay,
                                          calendarObj [i].period,
                                          calendarObj [i].drecur,
                                          calendarObj [i].exdate,
                                          calendarObj [i].pool);
                showEventsInfo (objUid + '-r0');
                i = getCalendarEventIndex (objUid);
            }
        }
        return (result);
    };

    var addOrRemoveReccurentEvents = function (baseEvent) {

        var uid         = baseEvent.uid;
        var recurValue  = baseEvent.drecur;

        var baseUID;
        var dayValue;
        var day = uid.lastIndexOf ('-d');
        if (day < 0)
        {
            baseUID = uid;
            dayValue = 0;
        }
        else
        {
            baseUID = uid.substr (0, day);
            dayValue = 0;
        }

        if (day < 0 && recurValue > 0)
        {
            calendarIndex = getCalendarEventIndex (uid);
            calendarObj.splice (calendarIndex, 1);
        }

        var calendarIndex
        for (var i = 0 ; i < 7 ; i++)
        {
            switch (i)
            {
                case 0:
                    dayValue = 64;
                    break;
                case 1:
                    dayValue = 32;
                    break;
                case 2:
                    dayValue = 16;
                    break;
                case 3:
                    dayValue = 8;
                    break;
                case 4:
                    dayValue = 4;
                    break;
                case 5:
                    dayValue = 2;
                    break;
                case 6:
                    dayValue = 1;
                    break;
            }

            calendarIndex = getCalendarEventIndex (baseUID + '-d' + i);

            if (recurValue & dayValue)
            {
                if (calendarIndex < 0)
                {
                    var startTime = convertToTime (baseEvent.start, 
                                                    $('day'+i).get ('delay').toInt ());
                    var endTime = convertToTime (baseEvent.end, 
                                                    $('day'+i).get ('delay').toInt ());
                    var newEvent = new Object ();
                    newEvent.uid    = baseUID + '-d' + i;
                    newEvent.label  = baseEvent.label;
                    newEvent.type   = baseEvent.type;
                    newEvent.plugin = baseEvent.plugin;
                    newEvent.plugid = baseEvent.plugid;
                    newEvent.start  = convertToTimestamp ($('day'+i), startTime);
                    newEvent.end    = convertToTimestamp ($('day'+i), endTime);
                    newEvent.period = baseEvent.period;
                    newEvent.drecur = baseEvent.drecur;
                    newEvent.exdate = baseEvent.exdate;
                    newEvent.pool   = baseEvent.pool;
                    newEvent.delay  = $('day0').get ('delay').toInt ();
                    calendarObj.push (newEvent);
                    calendarIndex = getCalendarEventIndex (baseUID + '-d' + i);
                }
                else
                {
                    calendarObj [calendarIndex].period  = baseEvent.period;
                    calendarObj [calendarIndex].drecur  = baseEvent.drecur;
                    calendarObj [calendarIndex].exdate  = baseEvent.exdate;
                    if (baseEvent.uid == calendarObj [calendarIndex].uid)
                        calendarObj [calendarIndex].delay = baseEvent.delay;
                }

                createPeriodicEvents (calendarObj [calendarIndex].uid,
                                      calendarObj [calendarIndex].label,
                                      calendarObj [calendarIndex].type, 
                                      calendarObj [calendarIndex].start,
                                      calendarObj [calendarIndex].end, 
                                      calendarObj [calendarIndex].delay,
                                      calendarObj [calendarIndex].period, 
                                      calendarObj [calendarIndex].drecur,
                                      calendarObj [calendarIndex].exdate,
                                      calendarObj [calendarIndex].pool);
            }
            else if ((recurValue == 0) && (uid == baseUID+'-d'+i))
            {
                calendarObj [calendarIndex].uid     = baseUID;
                calendarObj [calendarIndex].period  = 0;
                calendarObj [calendarIndex].drecur  = 0;
                calendarObj [calendarIndex].exdate  = "";
                createPeriodicEvents (calendarObj [calendarIndex].uid,
                                  calendarObj [calendarIndex].label,
                                  calendarObj [calendarIndex].type, 
                                  calendarObj [calendarIndex].start,
                                  calendarObj [calendarIndex].end, 
                                  calendarObj [calendarIndex].delay,
                                  calendarObj [calendarIndex].period, 
                                  calendarObj [calendarIndex].drecur,
                                  calendarObj [calendarIndex].exdate,
                                  calendarObj [calendarIndex].pool);
            }
            else
            {
                if (calendarIndex >= 0)
                {
                    var evts = $$('.evt');
                    for (j = 0 ; j < evts.length ; j++)
                    {
                        if (evts [j].get ('uid').match ("^"+calendarObj [calendarIndex].uid))
                            evts [j].destroy ();
                    }
                    calendarObj.splice (calendarIndex, 1);
                }
            }
        }
    };

    var getCalendarEventIndex = function (uid) {

        var i;
        var found = -1;
        for (i = 0 ; found < 1 && i < calendarObj.length ; i++)
        {
            if (calendarObj [i].uid == uid)
                found = i;
        }
        return (found);
    }

    var onDwRequest = function (jsonObj) {

        if (jsonObj == null || jsonObj.length < 1 || jsonObj.status == undefined)
        {
            alert ('Failed');
            return;
        }

        if (jsonObj.status == "0")
        {
            if (jsonObj.error)
                alert (jsonObj.error);
            else
                alert ('Failed');
            return;
        }

        if (!jsonObj.path)
            alert ('Error !');
        else
        {
            // Request the file returned by the request
            window.location.replace('/Programmation/getfile/file/'+encodeURIComponent(jsonObj.path));
        }
    };

    var convertToTimestamp = function (dayNode, time) {
        time = time.split (':');
        if (time.length != 2)
            return (dayNode.get ('start').toInt ());

        var hour    = time [0].toInt ();
        var min     = time [1].toInt ();
        if (hour < 0 || hour > 23)
            hour = 0;
        if (min < 0 || min > 59)
            min = 0;

        return (dayNode.get ('start').toInt () + (min * 60) + (hour * 3600));
    };

    var convertToTime = function (timestamp, delay) {
    
        var secondsInTheDay = (timestamp + delay) % 86400;
        var hours   = Math.floor (secondsInTheDay / 3600);
        var minutes = Math.floor ((secondsInTheDay - (hours*3600)) / 60);
        if (hours < 10)
            hours = '0'+hours;
        if (minutes < 10)
            minutes = '0'+minutes;
        if (hours >= 24)
        {
            hours   = '23';
            minutes = '59';
        }
        return (hours + ':' + minutes);
    };

    var addNewCategory = function (title, elements) {

        var catalog = $('catalogBox');

        var toggler = new Element ('h3', { 'id':    'toggler_'+title,
                                           'class': 'catToggler',
                                           'text':  title });

        var content = new Element ('div', { 'id':   'elt_'+title,
                                            'class':'catElements' });

        var mediaList = new Element ('ol', { 'id':      'list_'+title,
                                             'class':   'mediaList' });
        for (var i = 0 ; i < elements.length ; i++)
        {
            var item = new Element ('li', { 'id': title+'_'+i});
            var itemDiv = new Element ('div', { 'id': elements [i].id,
                                                'plugin': elements [i].plugin,
                                                'label': elements [i].name,
                                                'class': 'draggable' });
            var itemPix = new Element ('img', { 'src':  elements [i].thumbLink,
                                                'class': 'thumb',
                                                'title': elements [i].name,
                                                'alt':  elements [i].name });

            var itemText = new Element ('span', {'class': 'catalogLabel',
                                                 'title': elements [i].name,
                                                 'text': elements [i].name});

            itemDiv.grab (itemPix, 'top');
            itemDiv.adopt (itemText);
            item.grab (itemDiv);
            mediaList.grab (item);
        }

        content.adopt (mediaList);
        catalog.adopt (toggler, content);
        catAccordion.addSection (toggler, content);
    };

    var replaceProgram = function (progId, progName) {

        var request = new Request.JSON ({
            url: '/Programmation/getsaveinfo/format/json',
            data: {'id': progId},
            method: 'post',
            onSuccess: function (jsonObj) {
                switch (jsonObj.status)
                {
                    case 1:
                        if ($('bBroadcast'))
                        {
                            alert (strErrorUseBroadcast);
                        }
                        else
                        {
                            if (confirm (jsonObj.msg))
                                saveProgram (progId, progName);
                        }
                        break;
                    case 2:
                        saveProgram (progId, progName);
                        break;
                    default:
                        alert (jsonObj.msg);
                }
            }
        }).send ();
    };

    var saveProgram = function (progId, progName) {

        var request = new Request.JSON ({
            url: '/Programmation/savecalendar/format/json',
            data: {'events':    JSON.encode (calendarObj),
                   'id':        progId,
                   'name':      progName},
            method: 'post',
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    var page = $('from').value;
                    if (!page.length)
                        page = "/Supervision";
                    jumpToPage (page);
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert ('Calendar cannot be saved: Error '+xhr.status);
            }
        }).send ();
    };

    var saveAndBroadcast = function (referer, progId, progName) {

        var request = new Request.JSON ({
            url: '/Programmation/savecalendar/format/json',
            data: {'events':    JSON.encode (calendarObj),
                   'id':        progId,
                   'name':      progName,
                   'referer':   referer},
            method: 'post',
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    var page = $('from').value;
                    if (!page.length)
                        page = "/Supervision";
                    jumpToPage (page);
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert ('Calendar cannot be saved: Error '+xhr.status);
            }
        }).send ();
    };

    var replaceAndBroadcast = function (referer, progId, progName) {

        var request = new Request.JSON ({
            url: '/Programmation/getsaveinfo/format/json',
            data: {'id': progId,
                   'referer': referer},
            method: 'post',
            onSuccess: function (jsonObj) {
                switch (jsonObj.status)
                {
                    case 1:
                        if (confirm (jsonObj.msg))
                            saveAndBroadcast (referer, progId, progName);
                        break;
                    case 2:
                        saveAndBroadcast (referer, progId, progName);
                        break;
                    default:
                        alert (jsonObj.msg);
                }
            }
        }).send ();
    };

    var saveCalendar = function () {

        var progId = $('progId').get ('value');
        var progName = $('progName').get ('value');
        if (progName.length < 1 && $('progSelect'))
        {
            optionNode = $('option-'+$('progSelect').get ('value'));
            if (optionNode)
                progName = optionNode.get ('text');
            else
            {
                $('progName').highlight('#f00').focus();
                return;
            }
        }

        if (progId == "-1")
        {
            // New program
            saveProgram (-1, progName);
            return;
        }

        if ($('origProgName').get ('value') == progName)
        {
            if ($('permProgDel').get ('value') != '1')
                alert (strCannotOverride);
            else
                replaceProgram (progId, progName);
            return;
        }
        else
        {
            if ($('permProgDel').get ('value') == '1' &&
                $('origProgName').get ('value').length)
            {
                var msgBox = strProgBasedOn 
                            + '"' + $('origProgName').get ('value') + '"<br />'
                            + strReplaceOld;

                var dialog = new ModalBox ();
                dialog.setTitle ('iPreso');
                dialog.setText  (msgBox);
                dialog.addButton ('yes', strYes);
                dialog.addButton ('no', strNo);
                dialog.addButton ('cancel', strCancel);
                dialog.show (function (result) {

                    if (result == "no")
                        saveProgram (-1, progName);
                    else if (result == "yes")
                        replaceProgram (progId, progName);
                });
            }
            else
                saveProgram (-1, progName);
            return;
        }
    };

    var broadcastCalendar = function () {

        // Check referer
        if (!$('referer') ||
            $('referer').get ('value').length <= 3)
        {
            alert (strNoReferer);
            return;
        }

        var referer = $('referer').get ('value').substr (3);
        var progId = $('progId').get ('value');
        var progName = $('progName').get ('value');
        if (progName.length < 1 && $('progSelect'))
        {
            optionNode = $('option-'+$('progSelect').get ('value'));
            if (optionNode)
                progName = optionNode.get ('text');
            else
            {
                $('progName').highlight('#f00').focus();
                return;
            }
        }

        // Then, depending of the program name (changed, new, identic)
        // and permissions (delete the old ?)
        // ask for replace old one or not
        if (progId == "-1")
        {
            // New program
            saveAndBroadcast (referer, -1, progName);
            return;
        }

        if ($('origProgName').get ('value') == progName)
        {
            if ($('permProgDel').get ('value') != '1')
                alert (strCannotOverride);
            else
                replaceAndBroadcast (referer, progId, progName);
            return;
        }
        else
        {
            if ($('permProgDel').get ('value') == '1' &&
                $('origProgName').get ('value').length)
            {
                var msgBox = strProgBasedOn 
                            + '"' + $('origProgName').get ('value') + '"<br />'
                            + strReplaceOld;

                var dialog = new ModalBox ();
                dialog.setTitle ('iPreso');
                dialog.setText  (msgBox);
                dialog.addButton ('yes', strYes);
                dialog.addButton ('no', strNo);
                dialog.addButton ('cancel', strCancel);
                dialog.show (function (result) {

                    if (result == "no")
                        saveAndBroadcast (referer, -1, progName);
                    else if (result == "yes")
                        replaceAndBroadcast (referer, progId, progName);
                });
            }
            else
                saveAndBroadcast (referer, -1, progName);
            return;
        }
    };

    var setChannelToGroup = function (groupRefer, progId) {

        var request = new Request.JSON ({
            url: '/Programmation/broadcast/format/json',
            data: {'group': groupRefer,
                   'prog':  progId},
            method: 'post',
            onSuccess: function (jsonObj) {

                if (jsonObj.status == 1)
                {
                    var page = $('from').value;
                    if (!page.length)
                        page = "/Supervision";
                    jumpToPage (page);
                }
                else
                    alert (jsonObj.msg);
            },
            onFailure: function (xhr) {
                alert ('Error '+xhr.status);
            }
        }).send ();
    };

    var moveToWeek = function (week, action, zoom) {

        var year = $('year').get ('text').toInt ();

        var request = new Request.HTML ({
            url: '/Programmation/movetoweek',
            data: {'week':  week,
                   'year':  year,
                   'zoom':  zoom,
                   'diff': action},
            method: 'post',
            onSuccess: function (tree, elts, html, script) {
                $('boxCalendar').empty ();
                $('boxCalendar').innerHTML = html;
                if (script.length > 0)
                    eval (script);

                var element = $('calendarTable');
                var calendarHeight = element.getStyle ('height').substr (0, element.getStyle ('height').lastIndexOf ('px'));
                secondsPerPixel = secondsPerPixelBase / calendarHeight.toInt ();

                loadCalendar (calendarObj);

                while (dragObjects.length > 0) {
                    var dragObj = dragObjects.pop ();
                    dragObj.detach ();
                }
                
                $$('.draggable').each (function (drag) {
                    makeDraggable (drag);
                });
            }
        }).send ();
    };

    var updateProgsList = function () {
        var request = new Request.JSON ({
            url: '/Programmation/getprogs/format/json',
            method: 'post',
            onComplete: function (jsonObj) {
                doUpdateProgsList (jsonObj);
            }
        }).send ();
    };

    var progSelectChange = function () {

        if (!$('progId'))
            return;
        $('progId').set ('value', this.get ('value'));
        displayCurrentCalendar ();
    };

    var doUpdateProgsList = function (jsonObj) {

        if (jsonObj == null)
            return;

        var select = $('progSelect');
        if (!select)
            return;

        select.removeEvent ('change', progSelectChange);
        select.addEvent ('change', progSelectChange);

        select.empty ();
        for (var i = 0 ; i < jsonObj.progs.length ; i++)
        {
            var option = new Element ('option', {
                                        'value':    jsonObj.progs [i].id,
                                        'id':       'option-'+jsonObj.progs [i].id,
                                        'text':     jsonObj.progs [i].name
                                                });
            select.grab (option);
        }

        if (jsonObj.newProg)
        {
            var option = new Element ('option', {
                                        'value':    '-1',
                                        'id':       'option-new',
                                        'text':     strCreateNewProgram
                                                });
            select.grab (option);
            if ($('progRename'))
            {
                $('progRename').show ();
                $('progRename').setStyle ('display', 'inline');
            }
        }
        else
        {
            if ($('progRename'))
                $('progRename').hide ();
        }

        displayCurrentCalendar ();
    }

    var displayCurrentCalendar = function () {

        if (!$('progId'))
            return;

        var progId = $('progId').get ('value');
        if (progId.length < 1)
            progId = -1;
        else
            progId = progId.toInt ();
        showProgrammation (progId);
    };

    var showProgrammation = function (id) {

        if (id < 0 || !($('option-'+id)))
        {
            if ($('option-new'))
            {
                $('option-new').set ('selected', true);
                id = -1;
            }
            else
            {
                var id = $('progSelect').get ('value');
                if ($('option-'+id))
                    $('option-'+id).set ('selected', true);
            }
        }
        else
        {
            if ($('option-'+id))
                $('option-'+id).set ('selected', true);
        }

        $('progId').set ('value', id);

        if (id < 0 && $('option-new'))
        {
            if ($('progName'))
            {
                $('progName').set ('value', $('newProgName').get ('value'));
                $('progName').show ();
                $('progName').setStyle ('display', 'inline');
            }
            if ($('progLinks'))
                $('progLinks').hide ();
        }
        else if (id >= 0)
        {
            if ($('progName'))
            {
                $('progName').set ('value', '');
                $('progName').hide ();
            }
            if ($('progLinks'))
            {
                $('progLinks').show ();
                $('progLinks').setStyle ('display', 'inline');
            }
        }
        else
        {
            if ($('progName'))
            {
                $('progName').hide ();
                $('progName').set ('value', '');
            }
            if ($('progLinks'))
                $('progLinks').hide ();
        }

        if (id >= 0)
        {
            $$('.evt').each (function (drag) {
                drag.destroy ();
            });
            calendarObj = new Array ();
            loadCalendar (calendarObj);
            updateCalendar ();
        }
        else
        {
            $$('.evt').each (function (drag) {
                drag.destroy ();
            });
            calendarObj = new Array ();
            loadCalendar (calendarObj);
            $('permProgDel').set ('value', '1');
            $('permProgEdit').set ('value', '1');
        }
    };

    if ($('progRename'))
    {
        $('progRename').addEvent ('click', function () {

            if (!$('progName'))
                return;

            if ($('bSave'))
            {
                $('bSave').show ();
                $('bSave').setStyle ('display', 'inline');
            }
            if ($('bBroadcast'))
            {
                $('bBroadcast').show ();
                $('bBroadcast').setStyle ('display', 'inline');
            }

            var selectedNode = $('option-' + $('progSelect').get ('value'));
            if (!selectedNode)
                return;
            $('progName').set ('value', selectedNode.get ('text'));
            $('progName').show ();
            $('progName').setStyle ('display', 'inline');
        });
    }

    if ($('progDelete'))
    {
        $('progDelete').addEvent ('click', function () {

            var emiName = $('option-' + $('progSelect').get ('value')).get ('text');

            var dialog = new ModalBox ();
            dialog.setTitle ('iPreso');
            dialog.setText  (strSureToDeleteProg+' '+emiName+'" ?');
            dialog.addButton ('yes', strYes);
            dialog.addButton ('no', strCancel);
            dialog.show (function (result) {

                if (result == "yes")
                {
                    var request = new Request.JSON ({
                        url: '/Programmation/delprog/format/json',
                        method: 'post',
                        data: { 'id': $('progSelect').get ('value') },
                        onSuccess: function (jsonObj) {
                            if (jsonObj == null || jsonObj == undefined)
                            {
                                alert ('Error');
                                return;
                            }

                            if (jsonObj.code != 1)
                            {
                                if (jsonObj.details != undefined)
                                    alert (jsonObj.details);
                                else
                                    alert ('Error');
                                return;
                            }
                            updateProgsList ();
                        }
                    }).send ();
                }
            });
        });
    }

    if ($('progDownload'))
    {
        $('progDownload').addEvent ('click', function () {
            var emiName = $('option-' + $('progSelect').get ('value')).get ('text');
            var request = new Request.JSON ({
                url: '/Programmation/dwprog/format/json',
                method: 'post',
                data: { 'id': $('progSelect').get ('value') },
                onComplete: function (jsonObj) {
                    onDwRequest (jsonObj);
                }
            }).send ();
        });
    }

    var element = $('calendarTable');
    var calendarHeight = element.getStyle ('height').substr (0, element.getStyle ('height').lastIndexOf ('px'));
    var secondsPerPixel = secondsPerPixelBase / calendarHeight.toInt ();

    catAccordion = new Fx.Accordion ($$('h3.catToggler'),
                                     $$('div.catElements'),
                    {
                        display: -1,
                        opacity: false,
                        onActive: function (toggler, elt) {
                            toggler.setStyle('color', '#333333');
                        },
                        onBackground: function (toggler, elt) {
                            toggler.setStyle('color', '#888888');
                        }
                    });

    if ($('bCancel'))
    {
        $('bCancel').addEvent ('click', function (e) {
            e.stop ();

            var page = $('from').value;
            if (!page.length)
                page = "/Supervision";
            jumpToPage (page);
        });
    }

    if ($('prevWeek') && $('nextWeek'))
    {
        $('prevWeek').addEvent ('click', function (e) {
            e.stop ();
            var zoom = $('zoomLevel').get ('text');
            moveToWeek ($('weekId').get ('text'), 'prev', zoom);
        });
        $('nextWeek').addEvent ('click', function (e) {
            e.stop ();
            var zoom = $('zoomLevel').get ('text');
            moveToWeek ($('weekId').get ('text'), 'next', zoom);
        });
    }
    if ($('zoomIn') && $('zoomOut'))
    {
        $('zoomOut').addEvent ('click', function (e) {
            e.stop ();
            var zoom = $('zoomLevel').get ('text');
            moveToWeek ($('weekId').get ('text'), '', zoom.toInt ()-1);
        });
        $('zoomIn').addEvent ('click', function (e) {
            e.stop ();
            var zoom = $('zoomLevel').get ('text');
            moveToWeek ($('weekId').get ('text'), '', zoom.toInt ()+1);
        });
    }

    if ($('bSave'))
        $('bSave').addEvent ('click', saveCalendar);

    if ($('bBroadcast'))
        $('bBroadcast').addEvent ('click', broadcastCalendar);

    if ($('boxR'))
        $('boxR').hide ();

    window.addEvent ('resize', resizeLibraryContent);

    updateCatalog ();
    var select = $('progSelect');
    if (!select)
        updateCalendar ();
    else
        updateProgsList ();
});
