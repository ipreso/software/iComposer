ModalBox = new Class ({

    modalBox: 0,
    modalBackgroud: 0,
    replyFunction: 0,
    returnValues: false,
    screenHeight: 0,

    initialize: function () {

        if ($('dialogBox'))
            $('dialogBox').destroy ();

        var windowDialog = new Element ('div',
                {
                    'id':       'dialogBox',
                    'class':    'dialogBox',
                    'result':   ''
                });

        var background = new Element ('div',
                {
                    'id':       'dialogBackground',
                    'class':    'dialogBackground'
                });

        var windowBar = new Element ('div',
            {
                'id':       'windowBar',
                'class':    'dynDiv_moveParentDiv windowBar'
            });

        var windowContent = new Element ('div',
            {
                'id':       'windowContent',
                'class':    'windowContent'
            });
        var windowForm = new Element ('form', { 'id': 'modalForm' });
        windowForm.adopt (windowContent);

        var windowFoot = new Element ('div',
            {
                'id':       'windowFooter',
                'class':    'windowFooter'
            });

        windowDialog.adopt (windowBar, windowForm, windowFoot);
        windowDialog.hide ();
        background.hide ();
        background.inject (document.body);
        windowDialog.inject (document.body);

        modalBox        = windowDialog;
        modalBackground = background;

        screenHeight = Math.max(
            Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
            Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
            Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        );
        background.setStyle ('height', screenHeight);
    },

    show: function (callback) {

        var screenDimensions    = modalBackground.getDimensions ();
        var boxDimensions       = modalBox.getDimensions ();

        if (modalBackground)
            modalBackground.show ();

        modalBox.setStyle ('top', (screenDimensions.height - boxDimensions.height) / 2);
        modalBox.setStyle ('left', (screenDimensions.width - boxDimensions.width)/ 2);

        var bar = modalBox.getElement ('#windowBar');
        ByRei_dynDiv.add (bar, 'windowBar');
        modalBox.show ();

        var self = this;
        self.replyFunction = callback;
        setTimeout (function() {
            self.returnResult ();
        }, 200); 
    },

    returnResult: function () {

        var self = this;
        var result;

        if ($('dialogBox'))
            result = $('dialogBox').get ('result');
        else
            result = 'Error';

        if (!result)
            result = '';

        if (result.length <= 0)
        {
            setTimeout (function() {
                self.returnResult ();
            }, 200); 
        }
        else
        {
            if (this.returnValues == true)
            {
                var formElement = document.forms.modalForm;
                var formProperties = new Array ();
                var i;
                for (i = 0 ; i < formElement.elements.length ; i++)
                {
                    if (formElement.elements [i].type == "checkbox" &&
                        formElement.elements [i].checked != true)
                    {
                        continue;
                    }
                    else
                    {
                        var prop = new Array ();
                        prop [0] = formElement.elements[i].name;
                        prop [1] = formElement.elements[i].value;
                        formProperties [i] = prop;
                    }
                }

                var values = JSON.encode (formProperties);
                self.replyFunction (result, values);
            }
            else
                self.replyFunction (result);

            if ($('dialogBox'))
                $('dialogBox').destroy ();
            if ($('dialogBackground'))
                $('dialogBackground').destroy ();
        }
    },

    setTitle: function (title) {

        var windowBar = modalBox.getElement ('#windowBar');
        if (!windowBar)
            return;
        windowBar.set ('text', title);
    },

    setText: function (text) {

        var windowContent = modalBox.getElement ('#windowContent');
        if (!windowContent)
            return;
        //windowContent.set ('text', text);
        windowContent.innerHTML = text;
    },

    setHTMLContent: function (html) {

        var windowContent = modalBox.getElement ('#windowContent');
        if (!windowContent)
            return;
        windowContent.innerHTML = html;
    },

    addButton: function (id, value) {

        var windowFooter = modalBox.getElement ('#windowFooter');
        if (!windowFooter)
            return;

        var button = new Element ('input',
                {
                    'type':     'submit',
                    'id':       'button-'+id,
                    'value':    value
                });
        windowFooter.adopt (button);

        var self = this;
        button.addEvent ('click', self.reply);
    },

    reply: function (e) {
        e.stop ();
        if ($('dialogBox'))
            $('dialogBox').set ('result', this.id.substr (7));
    },

    getValues: function (bool) {
        if (bool)
            this.returnValues = true;
        else
            this.returnValues = false;
    }
});
