-- MySQL dump 10.11
--
-- Host: localhost    Database: iComposer
-- ------------------------------------------------------
-- Server version	5.0.81-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `iComposer`
--

/*!40000 DROP DATABASE IF EXISTS `iComposer`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `iComposer` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `iComposer`;

--
-- Table structure for table `Admin_agreement`
--

DROP TABLE IF EXISTS `Admin_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_agreement` (
  `id` int(11) NOT NULL auto_increment,
  `contains` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_groups`
--

LOCK TABLES `Admin_agreement` WRITE;
/*!40000 ALTER TABLE `Admin_agreement` DISABLE KEYS */;
INSERT INTO `Admin_agreement` VALUES (1,'Version 1 en date du 23.12.2011\n \n\nEntre l''utilisateur et IPRESO, site Internet &eacute;dit&eacute; par la soci&eacute;t&eacute; NAPAFILIA.\n \nChacune des parties reconna&icirc;t qu''elle a toute autorit&eacute; pour poser les actes, prendre les d&eacute;cisions et donner les autorisations requises relativement &agrave; l''ex&eacute;cution du pr&eacute;sent contrat.\n\nEn utilisant les services offerts par IPreso, l''utilisateur d&eacute;clare utiliser les services en accord avec le droit applicable et les pr&eacute;sentes conditions g&eacute;n&eacute;rales.\n\nLes services de IPreso ne peuvent &ecirc;tre utilis&eacute;s qu''une fois un compte utilisateur cr&eacute;&eacute;. L''utilisateur re&ccedil;oit alors un identifiant et un mot de passe afin d''acc&eacute;der aux services de IPreso.\n \nLe compte utilisateur est personnel et non transmissible, de m&ecirc;me seul les utilisateurs peuvent utiliser leur syst&egrave;me IPreso hors interventions r&eacute;alis&eacute;es par NAPAFILIA ou des tiers d&eacute;sign&eacute;s.\n \nToute utilisation du service au moyen du mot de passe de l''utilisateur est r&eacute;put&eacute;e avoir &eacute;t&eacute; effectu&eacute;e par l''utilisateur. En cas d''utilisation frauduleuse ou anormale du mot de passe, l''administrateur du parc de l''utilisateur doit suspendre ou mettre fin au compte de l''op&eacute;rateur. NAPAFILIA peut &ecirc;tre appel&eacute; afin de proc&eacute;der &agrave; ces op&eacute;rations en dernier recourt.\n \nIPreso met &agrave; la disposition de l''utilisateur des outils lui permettant de cr&eacute;er et d''administrer des postes d''affichage, c''est-&agrave;-dire des informations pouvant &ecirc;tre rendues publiques.\n\nLa r&eacute;alisation et l''insertion du contenu sont effectu&eacute;es par l''utilisateur, par ses propres moyens techniques d''entr&eacute;e de donn&eacute;es.\n\nNAPAFILIA pourra &ecirc;tre amen&eacute; &agrave; imposer des contraintes &agrave; l''utilisateur dans l''&eacute;laboration et le fonctionnement de son compte. Ces contraintes techniques ne sont susceptibles de donner lieu &agrave; aucune indemnit&eacute; &agrave; quelque titre que ce soit au profit de l''utilisateur.\n\nNAPAFILIA pourra notamment limiter la taille de l''espace disque mis &agrave; la disposition de l''utililsateur, la taille et le nombre des fichiers transf&eacute;rables sur son poste de contr&ocirc;le.\n\nNAPAFILIA se r&eacute;serve le droit de modifier &agrave; tout instant le service.\n \nL''utilisateur utilise les services de IPreso &agrave; ses risques et p&eacute;rils.\n\nL''utilisateur reconna&icirc;t express&eacute;ment qu''en aucun cas NAPAFILIA ne pourra &ecirc;tre tenue responsable des cons&eacute;quences r&eacute;sultant de la modification ou la discontinuit&eacute; du service, pour quelque raison que ce soit et notamment en raison de contraintes d''ordre technique.\n\nNAPAFILIA ne donne aucune garantie, expresse ou implicite, &agrave; l''utilisateur relativement :\n\n- au poste de contr&ocirc;le, &agrave; son fonctionnement, &agrave; leurs composantes mat&eacute;rielles et logicielles, de m&ecirc;me qu''&agrave; leur acc&egrave;s via Internet.\n- aux retomb&eacute;es, financi&egrave;res ou non, r&eacute;elles ou appr&eacute;hend&eacute;es, positives ou non, r&eacute;sultant ou pouvant r&eacute;sulter de l''utilisation et de l''affichage.\n \nUn espace de stockage est mis &agrave; la disposition de l''utilisateur s''il le souhaite pour qu''il r&eacute;alise ses contenus.\n\nSi NAPAFILIA met &agrave; la disposition l''espace de stockage n&eacute;cessaire pour ses utilisateurs, NAPAFILIA ne peut &ecirc;tre tenu responsable du contenu rendu public, dans la mesure o&ugrave; NAPAFILIA n''a pas connaissance de celles-ci.\n \nIl revient de fait &agrave; l''administrateur d''un parc le devoir de supprimer un compte utilisateur ou de fermer temporairement le service, si cela lui semble n&eacute;cessaire, et en particulier en cas de violation des lois et r&eacute;glements en vigueur.\n\nSeul l''administrateur d''un parc r&eacute;pond de la conformit&eacute; de ce qu''il affiche avec la l&eacute;gislation en vigueur.\n\nEn particulier il est interdit:\n\n- d''utiliser des oeuvres qui sont prot&eacute;g&eacute;es par des droits d''auteur sans autorisation expresse de l''auteur ou de la personne qui en poss&egrave;de le droit d''exploitation;\n- de diffamer, d''insulter ou de menacer autrui;\n- de diffuser des pages dont le contenu est ill&eacute;gal;\n- de diffuser dans des lieux publics des pages dont le contenu ou la forme est pornographique;\n- d''inciter &agrave; commettre des crimes et d&eacute;lits ou &agrave; y participer;\n- d''atteindre au droit &agrave; l''image ou &agrave; la personne de qui que ce soit;\n \nNAPAFILIA se r&eacute;serve le droit de modifier les conditions g&eacute;n&eacute;rales d''utilisation. Dans cette hypoth&egrave;se, ces modifications feront l''objet d''une mention particuli&egrave;re au d&eacute;marrage.\n\nLe pr&eacute;sent contrat entre en vigueur d&egrave;s lecture par l''op&eacute;rateur et il se poursuit pour une dur&eacute;e ind&eacute;termin&eacute;e.\n \nNAPAFILIA se r&eacute;serve le droit de c&eacute;der ou de sous-traiter sans pr&eacute;avis tout ou partie des obligations du pr&eacute;sent contrat.\n \nLe contrat d''h&eacute;bergement est soumis &agrave; la loi fran&ccedil;aise.\n\nTout litige relatif &agrave; la validit&eacute;, l''interpr&eacute;tation ou l''ex&eacute;cution du pr&eacute;sent contrat et apr&egrave;s &eacute;chec de toute conciliation, sera soumis aux tribunaux comp&eacute;tents.');
/*!40000 ALTER TABLE `Admin_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Admin_groups`
--

DROP TABLE IF EXISTS `Admin_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_groups` (
  `id` int(11) NOT NULL auto_increment,
  `groupname` varchar(100) NOT NULL,
  `path` varchar(100) default 0,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `groupname` (`groupname`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_groups`
--

LOCK TABLES `Admin_groups` WRITE;
/*!40000 ALTER TABLE `Admin_groups` DISABLE KEYS */;
INSERT INTO `Admin_groups` VALUES (0,'iPreso Team',NULL),(1,'Administrateurs','0');
/*!40000 ALTER TABLE `Admin_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Admin_logs`
--

DROP TABLE IF EXISTS `Admin_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_logs` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime default NULL,
  `lvl` int(11) default NULL,
  `who` int(11) default NULL,
  `msg` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_logs`
--

LOCK TABLES `Admin_logs` WRITE;
/*!40000 ALTER TABLE `Admin_logs` DISABLE KEYS */;
INSERT INTO `Admin_logs` VALUES (1, NOW(), 6, 1, 'iComposer database clean installation.');
/*!40000 ALTER TABLE `Admin_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Admin_resources`
--

DROP TABLE IF EXISTS `Admin_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_resources` (
  `id` int(11) NOT NULL auto_increment,
  `typename` varchar(100) NOT NULL,
  `typeid` varchar(100) DEFAULT NULL,
  `action` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_resources`
--

LOCK TABLES `Admin_resources` WRITE;
/*!40000 ALTER TABLE `Admin_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `Admin_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Admin_perms`
--

DROP TABLE IF EXISTS `Admin_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_perms` (
  `id_res` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  PRIMARY KEY  (`id_group`,`id_res`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_perms`
--

LOCK TABLES `Admin_perms` WRITE;
/*!40000 ALTER TABLE `Admin_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `Admin_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Admin_users`
--

DROP TABLE IF EXISTS `Admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_users` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `advanced` int(1) default 0,
  `id_group` int(11) NOT NULL,
  `last_connection` datetime default NULL,
  `display_agreement` boolean default 0,
  `display_agreement_on_connection` boolean default 0,
  `language` varchar(32) DEFAULT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `id_group` (`id_group`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_users`
--

LOCK TABLES `Admin_users` WRITE;
/*!40000 ALTER TABLE `Admin_users` DISABLE KEYS */;
INSERT INTO `Admin_users` VALUES (1,'msimonetti','Marc','Simonetti','3ec8b92c3a47db2e80293ea82735379132a4c037','marc.simonetti@geekcorp.fr',1,0,NULL,0,0,NULL),
                                 (2,'laudoune','Laurent','Audounet','a3ce7b022667e6aba9380375441a6f56051f1337','audounet@gmail.com',0,1,NULL,0,0,NULL),
                                 (3,'denis','Denis','Bonnefille-Fourment','bdcdc6f55740ba530ea906302b358341a08f58a0','denis@napakeo.com',0,1,NULL,0,0,NULL),
                                 (4,'supportipreso','Support','iPreso','bf3fa5143210942255b8ea7fabf2d6e5321aaf73','hotline@ipreso.com',1,1,NULL,0,0,NULL);
/*!40000 ALTER TABLE `Admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prog_playlists`
--

DROP TABLE IF EXISTS `Prog_playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_playlists` (
  `hash` varchar(100) NOT NULL,
  `creation` datetime NOT NULL,
  `content` blob,
  PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_playlists`
--

LOCK TABLES `Prog_playlists` WRITE;
/*!40000 ALTER TABLE `Prog_playlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_playlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prog_manifests`
--

DROP TABLE IF EXISTS `Prog_manifests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_manifests` (
  `hash` varchar(100) NOT NULL,
  `creation` datetime NOT NULL,
  `content` blob,
  PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_manifests`
--

LOCK TABLES `Prog_manifests` WRITE;
/*!40000 ALTER TABLE `Prog_manifests` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_manifests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prog_layouts`
--

DROP TABLE IF EXISTS `Prog_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_layouts` (
  `hash` varchar(100) NOT NULL,
  `creation` datetime NOT NULL,
  `content` blob,
  `preview` blob,
  PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_layouts`
--

LOCK TABLES `Prog_layouts` WRITE;
/*!40000 ALTER TABLE `Prog_layouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_layouts` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `Prog_programs`
--

DROP TABLE IF EXISTS `Prog_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_programs` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `playlist` varchar(100),
  `manifest` varchar(100),
  `layout` varchar(100),
  `creation` datetime NOT NULL,
  `modification` datetime NOT NULL,
  `deletion` datetime NULL,
  PRIMARY KEY  (`id`),
  KEY `playlist` (`playlist`),
  KEY `manifest` (`manifest`),
  KEY `layout` (`layout`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_programs`
--

LOCK TABLES `Prog_programs` WRITE;
/*!40000 ALTER TABLE `Prog_programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_criticities`
--

DROP TABLE IF EXISTS `Players_criticities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_criticities` (
  `id` int(11) NOT NULL auto_increment,
  `criticityname` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `criticityname` (`criticityname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_criticities`
--

LOCK TABLES `Players_criticities` WRITE;
/*!40000 ALTER TABLE `Players_criticities` DISABLE KEYS */;
INSERT INTO `Players_criticities` VALUES (0,'UNKNOWN'),
                                         (1,'DEBUG'),
                                         (2,'INFO'),
                                         (3,'ERROR'),
                                         (4,'CRITICAL');
/*!40000 ALTER TABLE `Players_criticities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_groups`
--

DROP TABLE IF EXISTS `Players_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_groups` (
  `id` int(11) NOT NULL auto_increment,
  `groupname` varchar(100) NOT NULL,
  `description` varchar(100) default NULL,
  `program` int(11) default 0,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `groupname` (`groupname`),
  KEY `program` (`program`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_groups`
--

LOCK TABLES `Players_groups` WRITE;
/*!40000 ALTER TABLE `Players_groups` DISABLE KEYS */;
INSERT INTO `Players_groups` VALUES (0,'Default','Group of newly hosted or orphan iBoxes.', 0);
/*!40000 ALTER TABLE `Players_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_hosts`
--

DROP TABLE IF EXISTS `Players_hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_hosts` (
  `box_hash` varchar(100) NOT NULL,
  `box_group` int(11) NOT NULL,
  `box_status` varchar(100) NOT NULL,
  `box_sync` varchar(100) NOT NULL,
  `box_label` varchar(256) default NULL,
  `box_coordinates` varchar(50) default NULL,
  `box_mode` varchar(20) default NULL,
  `box_program` int(11) default NULL,
  `box_schedule` varchar(100) default NULL,
  `box_license` int(11) default NULL,
  `box_wp` varchar(100) default NULL,
  `sync_version` varchar(20) default NULL,
  `net_mac` varchar(30) default NULL,
  `net_ip` varchar(20) default NULL,
  `net_gw` varchar(20) default NULL,
  `net_dns` varchar(20) default NULL,
  `composer_ip` varchar(20) default NULL,
  `composer_name` varchar(256) default NULL,
  `pkg_version` varchar(20) default NULL,
  `pkg_source` varchar(256) default NULL,
  `last_connection` datetime default NULL,
  `last_ip` varchar(20) default NULL,
  PRIMARY KEY  (`box_hash`),
  KEY `box_group` (`box_group`),
  KEY `box_program` (`box_program`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_hosts`
--

LOCK TABLES `Players_hosts` WRITE;
/*!40000 ALTER TABLE `Players_hosts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Players_hosts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_licenses`
--

DROP TABLE IF EXISTS `Players_licenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_licenses` (
  `id` int(11) NOT NULL auto_increment,
  `hash` varchar(100) NOT NULL,
  `name` varchar(64) NOT NULL,
  `cert` blob,
  `private` blob,
  `license` blob,
  `creation` date,
  `expiration` date,
  `revoked` BIT,
  PRIMARY KEY  (`id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_hosts`
--

LOCK TABLES `Players_licenses` WRITE;
/*!40000 ALTER TABLE `Players_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `Players_licenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_logs`
--

DROP TABLE IF EXISTS `Players_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_logs` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime default NULL,
  `player` varchar(100) NOT NULL,
  `msg` varchar(512) default NULL,
  `id_criticity` int(11) NOT NULL,
  `seen` int(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `id_criticity` (`id_criticity`),
  KEY `player` (`player`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_logs`
--

LOCK TABLES `Players_logs` WRITE;
/*!40000 ALTER TABLE `Players_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Players_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_programs`
--

DROP TABLE IF EXISTS `Players_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_programs` (
  `id` int(11) NOT NULL auto_increment,
  `player` varchar(100) NOT NULL,
  `program` int(11) NOT NULL,
  `start` datetime default NULL,
  `end` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `player` (`player`),
  KEY `program` (`program`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_programs`
--

LOCK TABLES `Players_programs` WRITE;
/*!40000 ALTER TABLE `Players_programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Players_programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Players_downloads`
--

DROP TABLE IF EXISTS `Players_downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Players_downloads` (
  `id` int(11) NOT NULL auto_increment,
  `player` varchar(100) NOT NULL,
  `program` int(11) NOT NULL,
  `start` datetime default NULL,
  `end` datetime default NULL,
  `status` varchar(512) default NULL,
  PRIMARY KEY  (`id`),
  KEY `player` (`player`),
  KEY `program` (`program`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Players_downloads`
--

LOCK TABLES `Players_downloads` WRITE;
/*!40000 ALTER TABLE `Players_downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `Players_downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Actions_properties`
--

DROP TABLE IF EXISTS `Actions_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Actions_properties` (
  `item_id` varchar(100) NOT NULL,
  `context` varchar(100) NOT NULL,
  `action_property` text NOT NULL,
  PRIMARY KEY  (`item_id`,`context`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Actions_properties`
--

LOCK TABLES `Actions_properties` WRITE;
/*!40000 ALTER TABLE `Actions_properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `Actions_properties` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `Media_properties`
--

DROP TABLE IF EXISTS `Media_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Media_properties` (
  `item_id` varchar(100) NOT NULL,
  `property_id` varchar(100) NOT NULL,
  `media_property` text NOT NULL,
  PRIMARY KEY  (`item_id`,`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Media_properties`
--

LOCK TABLES `Media_properties` WRITE;
/*!40000 ALTER TABLE `Media_properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `Media_properties` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `Media_plugins`
--

DROP TABLE IF EXISTS `Media_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Media_plugins` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) default NULL,
  `version` varchar(20) default NULL,
  `preview` blob default NULL,
  `enable` smallint default 1,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Media_plugins`
--

LOCK TABLES `Media_plugins` WRITE;
/*!40000 ALTER TABLE `Media_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `Media_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Actions_commands`
--

DROP TABLE IF EXISTS `Actions_commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Actions_commands` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) default NULL,
  `version` varchar(20) default NULL,
  `enable` smallint default 1,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Actions_commands`
--

LOCK TABLES `Actions_commands` WRITE;
/*!40000 ALTER TABLE `Actions_commands` DISABLE KEYS */;
/*!40000 ALTER TABLE `Actions_commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prog_schedules`
--

DROP TABLE IF EXISTS `Prog_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_schedules` (
  `hash` varchar(100) NOT NULL,
  `creation` datetime NOT NULL,
  `content` blob,
  PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_schedules`
--

LOCK TABLES `Prog_schedules` WRITE;
/*!40000 ALTER TABLE `Prog_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prog_channels`
--

DROP TABLE IF EXISTS `Prog_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prog_channels` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(100) NOT NULL,
  `schedule` varchar(100),
  `creation` datetime NOT NULL,
  `modification` datetime NOT NULL,
  `deletion` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `schedule` (`schedule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prog_channels`
--

LOCK TABLES `Prog_channels` WRITE;
/*!40000 ALTER TABLE `Prog_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prog_channels` ENABLE KEYS */;
UNLOCK TABLES;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-07-30 17:16:52
