<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class QuickupdateController extends Zend_Controller_Action
{
	private $_acl;
	private $_logger;
	private $_translator;

	public function init ()
	{
		$contextSwitch = $this->_helper->getHelper ('contextSwitch');
        $contextSwitch->addActionContext ('getemissions', 'json')->initContext ();
	}

	public function preDispatch ()
	{
		if ($this->getRequest ()->getActionName () != 'handleflashupload')
        {
            if ($this->_request->getParam ('Session'))
                $_COOKIE ['PHPSESSID'] = $this->_request->getParam ('Session');

            if (!Zend_Auth::getInstance ()->hasIdentity ())
                $this->_helper->redirector ('index', 'login');

            $user = Zend_Registry::getInstance ()->get ('App_User');
            if (!$user)
            {
                Zend_Auth::getInstance()->clearIdentity();
                $this->_helper->redirector ('index', 'login');
            }

            $this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
            $this->view->variables      = "";
            $this->view->permissions    = $this->_acl;
        }
        else
        {
            // Upload via the Flash component doesn't support Zend Authentication
            $this->view->variables      = "";
        }
		// Get permissions
		$this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
		// Get logger
		$this->_logger = Zend_Registry::getInstance()->get('Zend_Log');
		$this->view->variables = "";
		$this->_translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
	}

	public function indexAction ()
	{
		$commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $this->view->orientation = true;
        else
            $this->view->orientation = false;

        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $this->view->variables .= "var strConfirmDelZone = '"
                                    .$translator->translate ("Are you sure you want to delete the zone ?")."';\n";
        $this->view->variables .= "var strConfirmDelEmission = '"
                                    .$translator->translate ("Are you sure you want to delete this emission: ")."';\n";
        $this->view->variables .= "var strZoneName = '"
                                    .$translator->translate ("Zone\'s name: ")."';\n";
        $this->view->variables .= "var strZoneDimension = '"
                                    .$translator->translate ("Zone\'s dimensions: ")."';\n";
        $this->view->variables .= "var strSequence = '"
                                    .$translator->translate ("Sequence of this zone")."';\n";
        $this->view->variables .= "var strNoEnd = '"
                                    .$translator->translate ("No end")."';\n";
        $this->view->variables .= "var strItemProperties = '"
                                    .$translator->translate ("Item\'s Properties")."';\n";
        $this->view->variables .= "var strOk = '"
                                    .$translator->translate ("Ok")."';\n";
        $this->view->variables .= "var strCancel = '"
                                    .$translator->translate ("Cancel")."';\n";
        $this->view->variables .= "var strErrorSaveCalendar = '"
                                    .$translator->translate ("Calendar can\'t be saved.")."';\n";
        $this->view->variables .= "var strProgCreated = '"
                                    .$translator->translate ("Programmation created: ")."';\n";
        $this->view->variables .= "var strQuickProg = '"
                                    .$translator->translate ("Quick programmation")."';\n";
        $this->view->variables .= "var strMon = '".$translator->translate('Monday')."';\n";
        $this->view->variables .= "var strTue = '".$translator->translate('Tuesday')."';\n";
        $this->view->variables .= "var strWed = '".$translator->translate('Wednesday')."';\n";
        $this->view->variables .= "var strThu = '".$translator->translate('Thursday')."';\n";
        $this->view->variables .= "var strFri = '".$translator->translate('Friday')."';\n";
        $this->view->variables .= "var strSat = '".$translator->translate('Saturday')."';\n";
        $this->view->variables .= "var strSun = '".$translator->translate('Sunday')."';\n";
        $this->view->variables .= "var strRecurrence = '".$translator->translate('Recurrence: ')."';\n";
        $this->view->variables .= "var strStart = '".$translator->translate('Start: ')."';\n";
        $this->view->variables .= "var strEnd = '".$translator->translate('End: ')."';\n";
        $this->view->variables .= "var strDefaultZoneName = '".$translator->translate('Zone #')."';\n";
        $this->view->variables .= "var strAtLeastOneZone = '".$translator->translate('Please create at least one area.')."';\n";
        $this->view->variables .= "var strEmiBasedOn = '".$translator->translate('Your emission is based on emission: ')."';\n";
        $this->view->variables .= "var strReplaceOldEmi = '".$translator->translate('Do you want to replace (erase) the old emission by the new one ?')."';\n";
        $this->view->variables .= "var strYes = '".$translator->translate('Yes')."';\n";
        $this->view->variables .= "var strNo = '".$translator->translate('No')."';\n";
        $this->view->variables .= "var strLoading = '".$translator->translate('Loading library...')."';\n";
		// QU
		$this->view->variables .= "var strSelectEmission = '"
           .$translator->translate ("Select show")."';\n";
		$this->view->variables .= "var strSaveEmission = '"
           .$translator->translate ("Do you want to save this show ?")."';\n";
        
        
        // Get parameters (referer & program Id)
        $emissionId = $this->_request->getParam ('emissionId');
        $progId     = $this->_request->getParam ('progId');
        $referer    = $this->_request->getParam ('referer');
        $fromPage   = $this->_request->getParam ('page');

        if (!strlen ($fromPage))
            $fromPage = "/Supervision";

        $emiLabel   = "";

        if (strlen ($emissionId) >= 1)
        {
            $programs   = new Progs_Programs ();
            $program    = $programs->findProgram ($emissionId);
            if ($program)
                $emiLabel   = $program->getName ();
            else
                $emissionId = "";
        }

        $this->view->referer    = $referer;
        $this->view->emission   = $emissionId;
        $this->view->from       = $fromPage;
        $this->view->emiLabel   = $emiLabel;

		//$this->view->emissionsList = $this->getEmissions();
	}
	
    protected function getEmissions ()
    {
        $result         = array ();
        $emissionsObj   = new Progs_Programs ();
        $emissions      = $emissionsObj->getProgsArray ();

        foreach ($emissions as $emission)
        {
            if ($this->_acl &&
                $this->_acl->isAllowed ('Emission', 'quickupdate', $emission ['id']))
                $result []  = array ('value'    => $emission ['id'],
                                     'name'     => $emission ['label']);
        }
        return ($result);
    }

	
	public function getemissionsAction ()
    {
        if ($this->_acl && $this->_acl->isAllowed ('Emission', 'new'))
            $newEmission = true;
        else
            $newEmission = false;

        $this->_helper->json->sendJson (
                    array ('newEmission'=> $newEmission,
                           'emissions'  => $this->getEmissions ()));
    }
	
}
