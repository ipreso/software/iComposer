<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class ProgrammationController extends Zend_Controller_Action
{
    private $_acl;

    public function init ()
    {
        $contextSwitch = $this->_helper->getHelper ('contextSwitch');
        $contextSwitch->addActionContext ('getcatalog',     'json')->initContext ();
        $contextSwitch->addActionContext ('getcalendar',    'json')->initContext ();
        $contextSwitch->addActionContext ('getsaveinfo',    'json')->initContext ();
        $contextSwitch->addActionContext ('savecalendar',   'json')->initContext ();
        $contextSwitch->addActionContext ('broadcast',      'json')->initContext ();
        $contextSwitch->addActionContext ('getprogs',       'json')->initContext ();
        $contextSwitch->addActionContext ('delprog',        'json')->initContext ();
        $contextSwitch->addActionContext ('dwprog',         'json')->initContext ();
    }

    public function preDispatch ()
    {
        if (!Zend_Auth::getInstance ()->hasIdentity ())
            $this->_helper->redirector ('index', 'login');

        $user = Zend_Registry::getInstance ()->get ('App_User');
        if (!$user)
        {
            Zend_Auth::getInstance()->clearIdentity();
            $this->_helper->redirector ('index', 'login');
        }

        $this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
        $this->view->variables      = "";
        $this->view->permissions    = $this->_acl;
    }

    public function movetoweekAction ()
    {
        $this->_helper->layout->disableLayout ();

        $action = $this->_request->getParam ('diff');
        $week   = $this->_request->getParam ('week');
        $year   = $this->_request->getParam ('year');
        $zoom   = $this->_request->getParam ('zoom');

        if ($week < 10 && $week [0] != "0")
            $weekStr = "0$week";
        else
            $weekStr = $week;
        $ref = strtotime ("${year}W${weekStr}");

        switch ($action)
        {
            case "prev":
                $diff = "-1 week";
                break;
            case "next":
                $diff = "+1 week";
                break;
            default:
                $diff = "now";
        }
        $newDate = strtotime ($diff, $ref);

        if ($zoom < 1)
            $zoom = 1;
        else if ($zoom > 5)
            $zoom = 5;

        $this->view->week = date ("W", $newDate);
        $this->view->year = date ("Y", $newDate);
        $this->view->zoom = $zoom;

        $month = date ("n", $newDate);
        if ($this->view->week == 1 && $month == 12)
        {
            // Week can be '01' on the last week of the year
            $this->view->year++;

        }
        if ($this->view->week >= 51 && $month == 1)
        {
            // Week can be '53' on the first days of the year
            $this->view->year--;

        }
    }

    public function indexAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strTime = '".$translator->translate('Time: ')."';\n";
        $this->view->variables .= "var strRecurrence = '".$translator->translate('Recurrence: ')."';\n";
        $this->view->variables .= "var strMon = '".$translator->translate('Monday')."';\n";
        $this->view->variables .= "var strTue = '".$translator->translate('Tuesday')."';\n";
        $this->view->variables .= "var strWed = '".$translator->translate('Wednesday')."';\n";
        $this->view->variables .= "var strThu = '".$translator->translate('Thursday')."';\n";
        $this->view->variables .= "var strFri = '".$translator->translate('Friday')."';\n";
        $this->view->variables .= "var strSat = '".$translator->translate('Saturday')."';\n";
        $this->view->variables .= "var strSun = '".$translator->translate('Sunday')."';\n";
        $this->view->variables .= "var strApply = '".$translator->translate('Apply')."';\n";
        $this->view->variables .= "var strDelete = '".$translator->translate('Delete')."';\n";
        $this->view->variables .= "var strDeleteAllOccur = '".$translator->translate('Delete all occurences ?')."';\n";
        $this->view->variables .= "var strDeleteWeeklyOccur = '".$translator->translate('Weekly occurences')."';\n";
        $this->view->variables .= "var strYes = '".$translator->translate('Yes')."';\n";
        $this->view->variables .= "var strYesAll = '".$translator->translate('Yes, all occurences')."';\n";
        $this->view->variables .= "var strNo = '".$translator->translate('No')."';\n";
        $this->view->variables .= "var strOnlyThisOne = '".$translator->translate('Only this one')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";
        $this->view->variables .= "var strStart = '".$translator->translate('Start: ')."';\n";
        $this->view->variables .= "var strEnd = '".$translator->translate('End: ')."';\n";
        $this->view->variables .= "var strBroadImpossible = '".$translator->translate('Broadcast is impossible: no group selected.')."';\n";
        $this->view->variables .= "var strWarnNotRenamed = '".$translator->translate("If you do not rename the programmation,\\ngroups will be affected by your changes.\\n\\nAre you sure to continue ?")."';\n";
        $this->view->variables .= "var strNotRenamed = '".$translator->translate("If you do not rename the programmation,\\ngroups will be affected by your changes.\\nUse the \"save & broadcast\" button if you confirm.")."';\n";
        $this->view->variables .= "var strCreateNewProgram = '".$translator->translate('Create new program')."';\n";
        $this->view->variables .= "var strSureToDeleteProg = '".$translator->translate('Are you sure to delete programmation: ')."';\n";
        $this->view->variables .= "var strProgBasedOn = '".$translator->translate('Your programmation is based on programmation: ')."';\n";
        $this->view->variables .= "var strReplaceOld = '".$translator->translate('Do you want to replace (erase) the old programmation by the new one ?')."';\n";
        $this->view->variables .= "var strCannotOverride = '".$translator->translate('You don\\\'t have permission to overwrite programmation. Please rename it.')."';\n";
        $this->view->variables .= "var strErrorUseBroadcast = '".$translator->translate("If you save this programmation, at least one group will be impacted. Please use the \"Save and Broadcast\" button to confirm.")."';\n";
        $this->view->variables .= "var strNoReferer = '".$translator->translate ("Error: concerned group is not found")."';\n";
        $this->view->variables .= "var strEmissionsPool = '".$translator->translate ("Emissions pool")."';\n";

        // Get parameters (referer & program Id)
        $progId     = $this->_request->getParam ('progId');
        $referer    = $this->_request->getParam ('referer');
        $fromPage   = $this->_request->getParam ('page');
        if (!strlen ($fromPage))
            $fromPage = "/Supervision";

        $progLabel  = "";
        $progUsed   = array ();

        $groupsObj  = new Players_Groups ();
        if (strlen ($progId) >= 1 && intval($progId) >= 0)
        {
            // Coming from 'Edit' button, so getting the concerned channel
            $channels   = new Progs_Channels ();
            $channel    = $channels->findChannel ($progId);
            if ($channel)
            {
                $groups = $groupsObj->getGroupsWithChannel ($progId);
                if ($groups && count ($groups))
                {
                    foreach ($groups as $group)
                        $progUsed [$group ['id']] = $group ['groupname'];
                }
                else
                {
                    $group = $groupsObj->findGroupById (substr ($referer, 3));
                    if ($group)
                        $progUsed [$group->getId ()] = $group->getName ();
                    else
                        $progUsed [] = $referer;
                }
                
                $progLabel  = $channel->getName ();
            }
            else
            {
                // New programmation by default, because the one edited is
                // unknown
                $progId = "";
                $group = $groupsObj->findGroupById (substr ($referer, 3));
                if ($group)
                    $progUsed [$group->getId ()] = $group->getName ();
                else
                    $progUsed [] = $referer;
            }
        }
        else
        {
            // New programmation by default
            if (strlen ($referer) > 3)
            {
                $group = $groupsObj->findGroupById (substr ($referer, 3));
                if ($group)
                    $progUsed [$group->getId ()] = $group->getName ();
                else
                    $progUsed [] = $referer;
            }
        }

        $this->view->referer    = $referer;
        $this->view->program    = $progId;
        $this->view->from       = $fromPage;
        $this->view->progLabel  = $progLabel;
        $this->view->groups     = $progUsed;

        $groupId = substr ($referer, 3);

        // Getting the group name
        $groups = new Players_Groups ();
        $group = $groups->findGroupById ($groupId);
        if (!$group)
        {
            $this->view->error = "Group #$groupId is unknown";
            return;
        }

        $this->view->groupName = $group->getName ();
    }

    public function broadcastAction ()
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');

        $paramGroup     = $this->_request->getParam ('group');
        $paramProg      = $this->_request->getParam ('prog');

        if (strlen ($paramGroup) < 1 ||
            strncmp ($paramGroup, "gr-", strlen ("gr-")) != 0)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => "No valid group to broadcast."));
        }

        list ($dummy, $groupId) = explode ('-', $paramGroup);

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Parc', 'changeProgram', $groupId) ||
            !$this->_acl->isAllowed ('Programmation', 'view', $paramProg))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => "Not allowed to set this Calendar for group #$groupId"));
        }

        $groups = new Players_Groups ();
        if (!$groups->replaceGroupChannel ($groupId, $paramProg))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => "Cannot set this Calendar for group #$groupId"));
        }
        $boxes = new Players_Boxes ();
        if (!$boxes->replaceBoxesChannel ($groupId, $paramProg))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => "Cannot set this Calendar for iBoxes of group #$groupId"));
        }

        $this->_helper->json->sendJson (array ('status' => 1));
    }

    public function dwprogAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $idProg = $this->_request->getParam ('id');
        $progsObj = new Progs_Channels ();
        $prog = $progsObj->findChannel ($idProg);
        if ($prog)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info ("Download of *".$prog->getName ()."* asked.");

            // Create backup file with library
            $path = $prog->createBackup (true);
            if ($path)
            {
                $this->_helper->json->sendJson (array ('status' => 1,
                                                       'path'   => "$path"));
            }
            else
            {
                $this->_helper->json->sendJson (array ('status' => 0,
                                                       'error'   => $translator->translate ('Cannot create programmation backup')));
            }
        }

        $this->_helper->json->sendJson (array ('status' => 0,
                                               'error'  => $translator->translate ('Programmation not found')));
    }

    public function getsaveinfoAction ()
    {
        $translator     = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $paramProg      = $this->_request->getParam ('id');
        $paramReferer   = $this->_request->getParam ('referer');

        if (strlen ($paramProg) < 1)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ("Invalid programmation ('$paramProg')")));
        }

        $channels   = new Progs_Channels ();
        $channel    = $channels->findValidChannel ($paramProg);
        if (!$channel)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ("Programmation was edited in the meantime. Please refresh your page to get the new one.")));
        }

        // Find groups of players having currently this programmation
        $groupsObj  = new Players_Groups ();
        $groups     = $groupsObj->getGroupsWithChannel ($channel->getId ());

        if ($paramReferer)
            $broadcastGroup = $groupsObj->findGroupById ($paramReferer);
        else
            $broadcastGroup = false;

        // No group, so we allow to save without dialog box
        if (count ($groups) < 1 && !$broadcastGroup)
            $this->_helper->json->sendJson (array ('status' => 2));

        // Check permissions for all groups
        if (!$this->_acl)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ("Unable to get associated permissions")));
        }

        $allowed    = array ();
        $forbidden  = array ();

        if ($broadcastGroup)
            $found = false;
        else
            $found = true;

        foreach ($groups as $group)
        {
            if (!$found && $group ['id'] == $broadcastGroup->getId ())
                $found = true;

            if ($this->_acl->isAllowed ('Parc', 'changeProgram', $group ['id']))
                $allowed [] = $group ['groupname'];
            else
                $forbidden [] = $group ['groupname'];
        }

        if (!$found)
        {
            // Broadcasted group was not in the impacted group. We automatically add it
            if ($this->_acl->isAllowed ('Parc', 'changeProgram', $broadcastGroup->getId ()))
                $allowed [] = $broadcastGroup->getName ();
            else
                $forbidden [] = $broadcastGroup->getName ();
        }

        if (count ($forbidden))
        {
            $msg = $translator->translate ("You can't override this programmation.\n".
                                           "Following groups of players are already broadcasting it\n".
                                           "and you don't have permissions to modify it:\n");
            foreach ($forbidden as $group)
                $msg .= "- $group\n";

            $this->_helper->json->sendJson (array ('status' => 0, 'msg'    => $msg));
        }
        else
        {
            $msg = $translator->translate ("Following groups of players will broadcast your new programmation:\n");
            foreach ($allowed as $group)
                $msg .= "- $group\n";
            $msg .= $translator->translate ("\nPlease confirm this operation.\n");

            $this->_helper->json->sendJson (array ('status' => 1, 'msg' => $msg));
        }
    }

    public function savecalendarAction ()
    {
        $logger         = Zend_Registry::getInstance()->get('Zend_Log');
        $translator     = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $paramEvents    = Zend_Json::decode ($this->_request->getParam ('events'));
        $paramId        = $this->_request->getParam ('id');
        $paramName      = $this->_request->getParam ('name');
        $paramReferer   = $this->_request->getParam ('referer');

        $channels       = new Progs_Channels ();
        $channelByName  = $channels->findChannelByName ($paramName);

        $parcObj        = new Players_Groups ();
        $boxes          = new Players_Boxes ();

        if ($paramReferer)
            $broadcastGroup = $parcObj->findGroupById ($paramReferer);
        else
            $broadcastGroup = false;

        if (strlen ($paramId) && strcmp ($paramId, "-1") != 0)
        {
            if (!$this->_acl ||
                !$this->_acl->isAllowed ('Programmation', 'edit', $paramId))
            {
                $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('You cannot edit this programmation.')));
            }

            if (($channelByName && $channelByName->getId () == $paramId) ||
                !$channelByName)
            {
                if (strlen ($paramName) <= 0)
                {
                    $this->_helper->json->sendJson (
                        array ('status' => 0,
                               'msg'    => $translator->translate ('Programmation\'s name is empty !')));
                }

                if ($broadcastGroup)
                    $found = false;
                else
                    $found = true;

                // Checking permissions
                $parcs = $parcObj->getGroupsWithChannel ($paramId);
                foreach ($parcs as $parc)
                {
                    if (!$found && $broadcastGroup->getId () == $parc ['id'])
                        $found = true;

                    if (!$this->_acl ||
                        !$this->_acl->isAllowed ('Parc', 'changeProgram', $parc ['id']))
                    {
                        $this->_helper->json->sendJson (
                            array ('status' => 0,
                                   'msg'    => $translator->translate ('Not allowed to set this Calendar for group ')
                                                .$parc ['groupname']));
                    }
                }

                // Override the program
                $channelId = $channels->updateChannel ($paramId, $paramName, $paramEvents);
                if ($channelId != $paramId)
                {
                    // Update permissions
                    $resources = new Admin_resources ();
                    $resources->updateResource ('Programmation', $paramId, $channelId);

                    $logger->info ("Updated program *$paramName* ($channelId).");

                    // Update players' Parcs having this programmation
                    $parcs = $parcObj->getGroupsWithChannel ($paramId);
                    foreach ($parcs as $parc)
                    {
                        $parcObj->replaceGroupChannel ($parc ['id'], $channelId);
                        $boxes->replaceBoxesChannel ($parc ['id'], $channelId);
                    }

                    // Update players' Parc of the asked broadcast
                    if (!$found)
                    {
                        $parcObj->replaceGroupChannel ($broadcastGroup->getId (), $channelId);
                        $boxes->replaceBoxesChannel ($broadcastGroup->getId (), $channelId);
                    }

                    $this->_helper->json->sendJson (array ('status' => 1,
                                                           'progId' => $channelId));

                }
                else
                {
                    if ($broadcastGroup)
                    {
                        $parcObj->replaceGroupChannel ($broadcastGroup->getId (), $channelId);
                        $boxes->replaceBoxesChannel ($broadcastGroup->getId (), $channelId);
                    }

                    // No change
                    $this->_helper->json->sendJson (array ('status' => 1,
                                                           'progId' => $channelId));
                }
            }
            else
            {
                // Name already exists, but is not related to the programmation
                // currently edited.
                $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('Program\'s name already exists. Please change it.')));
            }
        }
        else
        {
            if (!$this->_acl ||
                !$this->_acl->isAllowed ('Programmation', 'new'))
            {
                $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('You cannot create a new programmation.')));
            }

            if ($channelByName)
            {
                // A channel already exists with the same label
                $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('Program\'s name already exists. Please change it.')));
            }
            // Create a new program
            $channelId = $channels->insertChannel ($paramName, $paramEvents);
            if ($channelId !== false)
            {
                $newChannel = $channels->findChannel ($channelId);
                $logger->info ("Inserted program *".$newChannel->getName ()
                                ."* (".$channelId.").");
                
                // Broadcast this channel on the desired parc
                if ($broadcastGroup)
                {
                    $parcObj->replaceGroupChannel ($broadcastGroup->getId (), $newChannel->getId ());
                    $boxes->replaceBoxesChannel ($broadcastGroup->getId (), $newChannel->getId ());
                }
                $this->_helper->json->sendJson (array ('status' => 1,
                                                       'progId' => $newChannel->getId ()));
            }
            else
            {
                $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('Unable to create this program.')));
            }
        }
    }

    public function getcatalogAction ()
    {
        $emissions  = $this->getEmissions ();
        $actions    = $this->getActions ();

        $this->_helper->json->sendJson (array_merge ($emissions, $actions));
    }

    public function getcalendarAction ()
    {
        $weekId = $this->_request->getParam ('week');
        $year   = $this->_request->getParam ('year');
        $progId = $this->_request->getParam ('prog');

        if (!strlen ($progId))
        {
            // New programmation
            $this->_helper->json->sendJson (
                array ('status'     => 1,
                       'progDel'    => false,
                       'progEdit'   => true,
                       'events'     => array ()));
        }

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Programmation', 'view', $progId))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => 'You are not allowed to view this programmation.'));
        }

        $progDelete = $this->_acl->isAllowed ('Programmation', 'delete', $progId);
        $progEdit   = $this->_acl->isAllowed ('Programmation', 'edit', $progId);

        // Get Calendar for the Channel
        $channels   = new Progs_Channels ();
        $channel    = $channels->findChannel ($progId);
        if (!$channel)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => 'Programmation does not exist !'));
        }

        // Get groups playing this channel
        $groupsObj  = new Players_Groups ();
        $groups     = $groupsObj->getGroupsWithChannel ($channel->getId ());
        $usedByGroup = array ();
        if ($groups && count ($groups) > 0)
        {
            foreach ($groups as $group)
                $usedByGroup [] = $group ['id'];
        }

        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->findSchedule ($channel->getSchedule ());
        if (!$schedule)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => 'Calendar does not exist !'));
        }

        $events = new Progs_Events ($schedule->getContent ());
        if (!$events)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => 'Invalid Calendar format.'));
        }

        if (count ($events) < 1)
        {
            $this->_helper->json->sendJson (
                array ('status'     => 1,
                       'progName'   => $channel->getName (),
                       'progId'     => $channel->getId (),
                       'progEdit'   => $progEdit,
                       'progDel'    => $progDelete,
                       'usedBy'     => implode (",", $usedByGroup),
                       'events'     => array ()));
        }

        $returnedEvents = array ();
        foreach ($events->getEvents () as $event)
        {
            if (!$event->getHidden () &&
                strcmp ($event->getEvType (), "Unknown") != 0)
            {
                $returnedEvents [] = array ('start' => $event->getStart (),
                                            'end'   => $event->getEnd (),
                                            'delay' => $event->getDelay (),
                                            'label' => $event->getLabel (),
                                            'type'  => $event->getEvType (),
                                            'period'=> $event->getPeriod (),
                                            'plugin'=> $event->getPlugin (),
                                            'plugid'=> $event->getPluginId (),
                                            'exdate'=> $event->getExdate (),
                                            'uid'   => $event->getUID (),
                                            'pool'  => $event->getPool ());
            }
        }
        $this->_helper->json->sendJson (
            array ('status'     => 1,
                   'progName'   => $channel->getName (),
                   'progId'     => $channel->getId (),
                   'progEdit'   => $progEdit,
                   'progDel'    => $progDelete,
                   'usedBy'     => implode (",", $usedByGroup),
                   'events'     => $returnedEvents));
    }

    protected function getEmissions ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $emissions = array ();

        $pProgs = new Progs_Programs ();
        $programs = $pProgs->getProgsArray ();
        foreach ($programs as $program)
        {
            if ($this->_acl &&
                $this->_acl->isAllowed ('Emission', 'view', $program ['id']))
            {
                $emissions [] = array ('id'         => $program ['id'],
                                       'name'       => $program ['label'],
                                       'thumbLink'  => "/Catalog/getlayout/id/".
                                                        $program ['layout'].
                                                        "/layout.png");
            }
        }

        if (count ($emissions) < 1)
            return (array ());
    
        $result = array ('title'    => $translator->translate ('Emissions'),
                         'elements' => $emissions);
        return (array ($result));
    }

    protected function getActions ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $result = array ();

        $cmdObj     = new Actions_Commands ();
        $commands   = $cmdObj->getEnabledCommands ();

        foreach ($commands as $cmd)
        {
            $parsedElements = array ();

            $elements = $cmd->getCommands ();
            foreach ($elements as $key => $value)
            {
                $parsedElements [] = array (
                        'id'        => $value ['id'],
                        'plugin'    => $cmd->getInternalName (),
                        'name'      => $value ['name'],
                        'thumbLink' => "/images/Action.png");
            }

            if (count ($parsedElements) > 0)
                $result [] = array ('title'     => "Actions - ".$cmd->getName (),
                                    'elements'  => $parsedElements);
        }

        return ($result);
    }

    public function getprogsAction ()
    {
        if ($this->_acl && $this->_acl->isAllowed ('Programmation', 'new'))
            $newProg = true;
        else
            $newProg = false;

        $this->_helper->json->sendJson (
                    array ('newProg'=> $newProg,
                           'progs'  => $this->getProgrammations ()));
    }

    protected function getProgrammations ()
    {
        $result     = array ();
        $progsObj   = new Progs_Channels ();
        $progs      = $progsObj->getChannelsArray ();

        foreach ($progs as $prog)
        {
            if ($this->_acl &&
                $this->_acl->isAllowed ('Programmation', 'view', $prog ['id']))
            $result []  = array ('id'   => $prog ['id'],
                                 'name' => $prog ['label']);
        }
        return ($result);
    }

    public function delprogAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $idProg = $this->_request->getParam ('id');
        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Programmation', 'delete', $idProg))
        {
            $this->_helper->json->sendJson (
                    array ('code'       => 0,
                           'details'    => $translator->translate ('You are not allowed to delete this programmation')));
        }

        $progsObj = new Progs_Channels ();
        $prog = $progsObj->findChannel ($idProg);
        if (!$prog)
        {
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error: Programmation does not exists.');
            $this->_helper->json->sendJson ($result);
        }

        // Check if programmation is used by any group
        $playersObj = new Players_groups ();
        $groupsList = $playersObj->getGroupsWithChannel ($idProg);
        if (count ($groupsList) >= 1)
        {
            $groupsLabel = "";
            foreach ($groupsList as $group)
            {
                if (strlen ($groupsLabel))
                    $groupsLabel .= ", ";
                $groupsLabel .= $group ['groupname'];
            }
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error: Programmation is currently played by groups: ')
                                    .$groupsLabel
                                    .".\n". $translator->translate ('Please affect other programmations to these groups first.');
            $this->_helper->json->sendJson ($result);
        }

        if ($progsObj->deleteChannel ($idProg))
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info ("Deleted programmation *".$prog->getName()."*.");
            $this->_helper->json->sendJson (array ('code' => 1));
        }
        else
        {
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error.');
            $this->_helper->json->sendJson ($result);
        }
    }

    public function getfileAction ()
    {
        // Disable layout
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $logs = new Players_Logs ();

        $filename = $this->_request->getParam ('file');

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $path = $upload->library."/$filename";
        if (!is_readable ($path))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }

        $this->getResponse ()->clearHeaders ();

        header ("Content-Type: application/octet-stream");
        $total = filesize ($path);
        $blocksize  = (1 << 20); // 1M chunks
        $sent       = 0;
        $fileHandle = fopen ($path, "rb");

        header ("Content-Length: $total");

        $flushCounter = 0;
        set_time_limit (0);
        while ($sent < $total)
        {
            echo fread ($fileHandle, $blocksize);
            $sent += $blocksize;
            $flushCounter++;
            if ($flushCounter > 10)
            {
                ob_flush ();
                $flushCounter = 0;
            }
        }
        fclose ($fileHandle);

        // Delete this file
        if (file_exists ($path))
            unlink ($path);
    }
}
