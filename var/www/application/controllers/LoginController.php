<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class LoginController extends Zend_Controller_Action
{
    private $_user;

    public function getForm ()
    {
        return (new Form_Login (array ('action' => '/login/process',
                                       'method' => 'post')));
    }

    public function getAuthAdapter (array $params)
    {
        $dbAdapter      = Zend_Registry::get ('Zend_Db');
        $authAdapter    = new Zend_Auth_Adapter_DbTable ($dbAdapter);

        // Configure table stuff
        $authAdapter->setTableName ('Admin_users');
        $authAdapter->setIdentityColumn ('login');
        $authAdapter->setCredentialColumn ('password');
        $authAdapter->setCredentialTreatment ('SHA(MD5(?))');

        // Set the input
        $authAdapter->setIdentity ($params['username']);
        $authAdapter->setCredential ($params['password']);

        return ($authAdapter);
    }

    public function preDispatch ()
    {
        if (Zend_Auth::getInstance ()->hasIdentity ())
        {
            // If user is logged in, no auth form... except the
            // logout action !
            if ($this->getRequest ()->getActionName () != 'logout')
            {   
                //$this->_helper->redirector ('index', 'index');
                $this->_helper->redirector ('index', 'Supervision');
            }
        }
    }

    public function indexAction ()
    {
        $this->view->form = $this->getForm ();
    }

    public function processAction ()
    {
        $request = $this->getRequest ();

        // POST request ?
        if (!$request->isPost ())
            return ($this->_helper->redirector ('index'));

        // Is input well-formatted ?
        $form = $this->getForm ();
        if (!$form->isValid ($request->getPost ()))
        {
            // Invalid input
            $this->view->form = $form;
            return ($this->render ('index'));
        }

        // Are credentials correct ?
        $adapter = $this->getAuthAdapter ($form->getValues ());
        $auth = Zend_Auth::getInstance ();
        $result = $auth->authenticate ($adapter);
        if (!$result->isValid ())
        {
            $form->setDescription (Zend_Registry::getInstance()
                                    ->get ('Zend_Translate')
                                    ->translate ('Invalid credentials'));
            $this->view->form = $form;
            return ($this->render ('index'));
        }
        else
        {
            // Get the IP of the remote client
            $headers = apache_request_headers();
            if (array_key_exists ('X-Forwarded-For', $headers))
            {
                $remoteIP = $headers ['X-Forwarded-For']
                            .' via '.$_SERVER["REMOTE_ADDR"];
            }
            else
                $remoteIP = $_SERVER ['REMOTE_ADDR'];


            $users = new Admin_Users ();
            $user = $users->findLogin ($result->getIdentity ());
 
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->setEventItem ('user', $user->getId ());
            $logger->info("Logged in with IP $remoteIP.");

            // Update the 'last connection' field
            $users->setConnected ($user->getId());
            
            $usersAgreement = new Admin_Users ();
            
            $return = $usersAgreement->modifyDisplayAgreementOnConnection ($user->getId (),0);
            $this->_user = $usersAgreement->findUserById ($user->getId ());
            $this->view->user = $this->_user;
        }

        // We're authenticated. Go to the home page.
        //$this->_helper->redirector ('index', 'index');
        $this->_helper->redirector ('index', 'Supervision');
    }

    public function logoutAction ()
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->debug("Logged out.");

        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector ('index');
    }
}
