<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class AccountController extends Zend_Controller_Action
{
    private $_user;

    public function init ()
    {
    }

    public function preDispatch ()
    {
        if (!Zend_Auth::getInstance ()->hasIdentity ())
            $this->_helper->redirector ('index', 'login');

        $this->_user = Zend_Registry::getInstance ()->get ('App_User');
        if (!$this->_user)
        {
            Zend_Auth::getInstance()->clearIdentity();
            $this->_helper->redirector ('index', 'login');
        }
    }

    public function indexAction ()
    {
        $pass   = $this->_request->getParam ('userPass');
        $login  = $this->_request->getParam ('userLogin');
        $mail   = $this->_request->getParam ('userMail');
        $lang   = $this->_request->getParam ('userLang');
        $adv    = $this->_request->getParam ('userAdv');

        if (!empty ($pass) || !empty ($login) || 
            !empty ($mail) || !empty ($adv) ||
            !empty ($lang))
        {
            if (empty ($login))
                $login = $this->_user->getLogin ();
            if (empty ($mail))
                $mail = $this->_user->getEmail ();
            if (empty ($adv))
                $adv = 0;
            if (empty ($lang))
                $lang = "auto";

            $users = new Admin_Users ();
            $return = $users->modifyUser ($this->_user->getId (),
                                          $login,
                                          $this->_user->getFirstname (),
                                          $this->_user->getLastname (),
                                          $pass,
                                          $mail,
                                          $adv,
                                          $lang);
            if (count ($return) > 0)
            {
                $logger = Zend_Registry::getInstance()->get('Zend_Log');
                foreach ($return as $elt)
                {
                    $logger->info ("Changed *$login*: ".
                                    "*".$elt ['field']."* from *".
                                    $elt ['old']."* to *".
                                    $elt ['new']."*.");
                }
                $this->_helper->redirector ('index');
            }
            $this->_user = $users->findUserById ($this->_user->getId ());
        }

        $this->view->user = $this->_user;
    }
}
