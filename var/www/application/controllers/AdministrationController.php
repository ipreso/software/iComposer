<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class AdministrationController extends Zend_Controller_Action
{
    private $_acl;
    private $_group;

    protected function getGroupsAndUsersList ()
    {
        $groups = new Admin_Groups ();
        $users = new Admin_Users;

        if (!$this->_group)
            return (array ());

        $result = array ();
        $allGroups = $groups->getGroupsArray ();
        foreach ($allGroups as $group)
        {
            // Hide the iPreso Team group
            if ($group ['id'] == 0)
                continue;

            $groupObj = $groups->findGroupById ($group ['id']);
            if (!$this->allowedToModify ($groupObj))
            {
                // If our group, disabled administration.
                // Otherwise don't show it
                if ($group ['id'] == $this->_group->getId ())
                    $group ['admin'] = false;
                else
                    continue;
            }
            else
                $group ['admin'] = true;

            $usersInGroup = $users->getUsersArrayByGroup ($group ['id']);

            // We don't need all users' information !
            $usersBriefInGroup = array ();
            foreach ($usersInGroup as $user)
            {
                $usersBriefInGroup [] = array ('id'         => $user['id'],
                                               'firstname'  => $user['firstname'],
                                               'lastname'   => $user['lastname']);
            }
            $groupAndUsers = array ('group' => $group,
                                    'users' => $usersBriefInGroup);
            $result [] = $groupAndUsers;
        }

        // Check group-less users
        $usersNotInGroup = $users->getUsersArrayWithoutGroup ();
        if (count ($usersNotInGroup) > 0)
        {
            $groupAndUsers = 
                array (
                    'group' => 
                        array ('id' => -1,
                               'groupname' => Zend_Registry::getInstance()
                                                ->get ('Zend_Translate')
                                                ->translate ('Orphans')),
                    'users' => $usersNotInGroup);
            $result [] = $groupAndUsers;
        }

        return ($result);
    }

    public function init ()
    {
        // Can reply to JSON, XML and TXT requests !
        $contextSwitch = $this->_helper->getHelper ('contextSwitch');

        $specs = array ('suffix' => 'txt',  
                        'headers' => array ('Content-type' => 'text/plain'));
        $contextSwitch->addContext ('txt', $specs);

        $contextSwitch->addActionContext ('addgroup',   'json')->initContext ();
        $contextSwitch->addActionContext ('delgroup',   'json')->initContext ();
        $contextSwitch->addActionContext ('modifygroup','json')->initContext ();
        $contextSwitch->addActionContext ('listgroups', 'json')->initContext ();
        $contextSwitch->addActionContext ('commituser', 'json')->initContext ();
        $contextSwitch->addActionContext ('deluser',    'json')->initContext ();
        $contextSwitch->addActionContext ('getuser',    'json')->initContext ();
        $contextSwitch->addActionContext ('getplugins', 'json')->initContext ();
        $contextSwitch->addActionContext ('swap',       'json')->initContext ();
        $contextSwitch->addActionContext ('getperms',   'json')->initContext ();
        $contextSwitch->addActionContext ('saveperms',  'json')->initContext ();
        $contextSwitch->addActionContext ('setmaxperms','json')->initContext ();
        $contextSwitch->addActionContext ('getboxlogs', 'txt')->initContext ();
        $contextSwitch->addActionContext ('gethierarchy',       'json')->initContext ();
        $contextSwitch->addActionContext ('getboxeslogs',       'txt')->initContext ();
        $contextSwitch->addActionContext ('updatehierarchy',    'json')->initContext ();
        $contextSwitch->addActionContext ('getuserslogs',       array ('xml','txt'))->initContext ();
        $contextSwitch->addActionContext ('updateusersgroups',  'json')->initContext ();
		$contextSwitch->addActionContext ('editagreement', 'json')->initContext ();
    }

    public function preDispatch ()
    {
        if (!Zend_Auth::getInstance ()->hasIdentity ())
            $this->_helper->redirector ('index', 'login');

        $user = Zend_Registry::getInstance ()->get ('App_User');
        if (!$user)
        {
            Zend_Auth::getInstance()->clearIdentity();
            $this->_helper->redirector ('index', 'login');
        }
        $this->_group = $user->getGroup ();

        $this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
        $this->view->permissions = $this->_acl;
        $this->view->variables = "";

        // If all resources of the page is denied, redirect to the Denied page
        if (!$this->_acl || !$this->_acl->isAllowed ('Administration', 'view'))
            $this->_helper->redirector ('index', 'Supervision');
    }

    protected function getNavBar ($currentPage)
    {
        $translator = Zend_Registry::getInstance()->get ('Zend_Translate');
        $navBar = "<div id=\"submenu\">\n";
        $navBar .= "<ul>\n";

        if ($this->_acl && ($this->_acl->isAllowed ('Administration', 'view', 'logsusers')
                          || $this->_acl->isAllowed ('Administration', 'view', 'logsboxes')))
        {
            $navBar .= "<li".((strcmp ($currentPage, "logs") == 0) ? " id=\"current\" " : "")
                        ."><a class=\"submenu\" href=\"/Administration/logs\">"
                        .$translator->translate ("Logs")
                        ."</a></li>\n";
        }

        $navBar .= "<li".((strcmp ($currentPage, "users") == 0) ? " id=\"current\" " : "")
                    ."><a class=\"submenu\" href=\"/Administration/users\">"
                    .$translator->translate ("Users and Groups")
                    ."</a></li>\n";

        $navBar .= "<li".((strcmp ($currentPage, "perms") == 0) ? " id=\"current\" " : "")
                    ."><a class=\"submenu\" href=\"/Administration/permissions\">"
                    .$translator->translate ("Permissions")
                    ."</a></li>\n";

        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'plugins'))
        {
            $navBar .= "<li".((strcmp ($currentPage, "plugins") == 0) ? " id=\"current\" " : "")
                        ."><a class=\"submenu\" href=\"/Administration/plugins\">"
                        .$translator->translate ("Plugins")
                        ."</a></li>\n";
        }

        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'configuration'))
        {
            $navBar .= "<li".((strcmp ($currentPage, "configuration") == 0) ? " id=\"current\" " : "")
                        ."><a class=\"submenu\" href=\"/Administration/configuration\">"
                        .$translator->translate ("Configuration")
                        ."</a></li>\n";
        }

        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'agreement'))
        {
            $navBar .= "<li".((strcmp ($currentPage, "agreement") == 0) ? " id=\"current\" " : "")
                        ."><a class=\"submenu\" href=\"/Administration/agreement\">"
                        .$translator->translate ("AgreementsBar")
                        ."</a></li>\n";
        }

        $navBar .= "</ul>\n";
        $navBar .= "</div>\n";

        return ($navBar);
    }

    public function indexAction ()
    {
        $this->_helper->redirector ('logs');
    }

    public function usersAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strSave = '".$translator->translate('Save')."';\n";
        $this->view->variables .= "var strMaxPerms = '".$translator->translate('Set maximum permissions')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";

        $this->view->navbar = $this->getNavBar ("users");
    }

    public function permissionsAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strSave = '".$translator->translate('Save')."';\n";
        $this->view->variables .= "var strMaxPerms = '".$translator->translate('Set maximum permissions')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";

        $this->view->navbar = $this->getNavBar ("perms");
    }

    public function pluginsAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strSave = '".$translator->translate('Save')."';\n";
        $this->view->variables .= "var strMaxPerms = '".$translator->translate('Set maximum permissions')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";

        $this->view->navbar = $this->getNavBar ("plugins");
    }

    public function getpluginsAction ()
    {
        $this->_sendPluginsList ();
    }

    public function configurationAction ()
    {
        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Administration', 'view', 'configuration'))
            $this->_helper->redirector ('logs');

        $licenses   = Zend_Registry::getInstance ()->get ('Config_Ini_Licenses');
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strSave = '".$translator->translate('Save')."';\n";
        $this->view->variables .= "var strMaxPerms = '".$translator->translate('Set maximum permissions')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";

        $this->view->navbar = $this->getNavBar ("configuration");

        $network = $this->_request->getParam ('net-conf');
        if ($network && strlen ($network))
            file_put_contents ("/opt/iComposer/etc/network/interfaces", $network);

        $syncnow = $this->_request->getParam ('syncnow');
        if ($syncnow)
        {
            // Synchronize now !
            // Stop ntpd, synchronize with ntpdate, then restart ntpd
            system ("sudo /etc/init.d/openntpd stop 2>/dev/null 1>&2");
            system ("sudo /usr/sbin/ntpdate pool.ntp.org 2>/dev/null 1>&2");
            system ("sudo /etc/init.d/openntpd start 2>/dev/null 1>&2");
        }

        $licensenow = $this->_request->getParam ('licensenow');
        if ($licensenow)
        {
            // Update Composer now (license, but packages too)
            system ("sudo /etc/cron.hourly/icomposer 2>/dev/null 1>&2");
        }

        $this->view->hash       = $licenses->id;
        $this->view->expiration = $this->_execute ("/usr/bin/openssl x509 ".
                "-noout -enddate -in ".$licenses->cert." | ".
                "cut -d'=' -f2");
        $this->view->hostname   = $this->_execute ("/usr/bin/openssl x509 ".
                "-noout -subject -in ".$licenses->cert." | ".
                "cut -d'/' -f6 | cut -d'=' -f2");
    }

    public function agreementAction()
    {
		if (!$this->_acl ||
            !$this->_acl->isAllowed ('Administration', 'view', 'agreement'))
            $this->_helper->redirector ('agreement');
        
        $this->view->navbar = $this->getNavBar ("agreement");
    }
    
    public function editagreementAction()
    {   
        $agreement = $this->_request->getParam ('agreement');
        if ($agreement && strlen ($agreement))
        {
          $AdminAgreements = new Admin_Agreements ();
          $return = $AdminAgreements->modifyAgreement (1,$agreement);
        }
		$this->_helper->json->sendJson (array ('status'    => '1'));
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }

    public function swapAction ()
    {
        $element = $this->_request->getParam ('id');
        
        if (!$element)
            $this->_sendPluginsList ();

        list ($type, $name) = explode ('-', $element);
        switch ($type)
        {
            case 'hc':
                // Command
                $commands = new Actions_commands ();
                $commands->swap ($name);
                break;
            case 'hp':
                // Plugin
                $plugins = new Media_Plugins ();
                $plugins->swap ($name);
                break;
            default:
                // Unknown type
                $this->_sendPluginsList ();
        }
        $this->_sendPluginsList ();
    }

    protected function _sendPluginsList ()
    {
        $result     = array ();

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Administration', 'view', 'plugins'))
            return ($result);

        // Get plugins installed
        $plugins = new Media_Plugins ();
        $pluginsList = $plugins->getPlugins ();

        // Get commands installed
        $commands = new Actions_commands ();
        $cmdsList = $commands->getCommands ();

        $pluginsArray = array ();
        foreach ($pluginsList as $plugin)
        {
            $pluginsArray [] = array ('name'    => $plugin->getInternalName (),
                                      'label'   => $plugin->getName (),
                                      'version' => $plugin->getVersion (),
                                      'enable'  => $plugin->getEnable ());
        }
        $cmdsArray = array ();
        foreach ($cmdsList as $cmd)
        {
            $cmdsArray [] = array ('name'    => $cmd->getInternalName (),
                                   'label'   => $cmd->getName (),
                                   'version' => $cmd->getVersion (),
                                   'enable'  => $cmd->getEnable ());
        }

        $result = array ('Media'    => $pluginsArray,
                         'Cmd'      => $cmdsArray);

        $this->_helper->json->sendJson ($result);
    }

    public function logsAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->view->variables .= "var strSave = '".$translator->translate('Save')."';\n";
        $this->view->variables .= "var strMaxPerms = '".$translator->translate('Set maximum permissions')."';\n";
        $this->view->variables .= "var strCancel = '".$translator->translate('Cancel')."';\n";

        $this->view->navbar = $this->getNavBar ("logs");

        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'logsusers'))
        {
            $logs = new Admin_logs ();
            $this->view->userslogs = $logs->getLastLogs (10);
        }

        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'logsboxes'))
        {
            $logs = new Players_Logs ();
            $this->view->boxeslogs = $logs->getLastLogs (10);
        }
    }

    public function addgroupAction ()
    {
        // Create a group, child of the user's one.

        $groupname = $this->_request->getParam ('groupname');
        $groups = new Admin_Groups ();
        $id = $groups->addGroup ($groupname, $this->_group->getPath ().
                                             "/".$this->_group->getId ());

        // Log this action !
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->info("Created group *$groupname*.");

        $allGroups = $this->getGroupsAndUsersList ();
        $allGroups = array_values ($allGroups);
        $this->_helper->json->sendJson (array (
                'status'    => 1,
                'groups'    => $allGroups));
    }

    public function modifygroupAction ()
    {
        $newGroupName = $this->_request->getParam ('groupname');
        $groupId = $this->_request->getParam ('groupId');

        if (!is_numeric ($groupId))
            return (FALSE);

        $groups = new Admin_Groups ();
        $groupObj = $groups->findGroupById ($groupId);
        if ($groupObj !== NULL)
        {
            if (!$this->allowedToModify ($groupObj))
            {
                // Not allowed
                $this->_helper->json->sendJson (array (
                        'status'    => 0,
                        'msg'       => 'You are not allowed to update this group'));
            }

            $oldGroupName = $groupObj->getName ();
            if ($groups->setGroupName ($groupId, $newGroupName))
            {
                $logger = Zend_Registry::getInstance ()->get ('Zend_Log');
                $logger->info ("Changed group from *$oldGroupName* to *$newGroupName*.");
            }
        }

        $allGroups = $this->getGroupsAndUsersList ();
        $allGroups = array_values ($allGroups);
        $this->_helper->json->sendJson (
            array ('status' => 1,
                   'groups' => $allGroups));
    }

    public function deluserAction ()
    {
        $userParam = $this->_request->getParam ('id');
        if (is_numeric ($userParam))
        {
            $userId = intval ($userParam);
            
            $users = new Admin_Users ();
            $user = $users->findUserById ($userId);

            $groups = new Admin_Groups ();
            $groupObj = $groups->findGroupById ($user->getGroupId ());
            if (!$groupObj)
            {
                // Not allowed
                $this->_helper->json->sendJson (array (
                        'status'    => 0,
                        'msg'       => 'Invalid group'));
            }
            if (!$this->allowedToModify ($groupObj))
            {
                // Not allowed
                $this->_helper->json->sendJson (array (
                        'status'    => 0,
                        'msg'       => 'You are not allowed to delete users of this group'));
            }
            
            if ($user !== NULL && $user->getGroupId () >= 1)
            {
                $logger = Zend_Registry::getInstance ()->get ('Zend_Log');
                $logger->info ("Deleted user *".$user->getName ()."*.");

                $users->delUser ($user->getId ());
            }
        }

        $allGroups = $this->getGroupsAndUsersList ();
        $allGroups = array_values ($allGroups);
        $this->_helper->json->sendJson (
            array ('status' => 1,
                   'groups' => $allGroups));
    }

    private function allowedToModify ($groupObj)
    {
        $currentParents = $this->_group->getPath ();
        if ($currentParents !== NULL)
            $currentPath = "$currentParents/".$this->_group->getId ();
        else
            $currentPath = $this->_group->getId ();

        $groupParents = $groupObj->getPath ();
        if ($groupParents !== NULL)
            $groupPath = "$groupParents/".$groupObj->getId ();
        else
            $groupPath = $groupObj->getId ();

        if (($currentParents !== NULL &&
            strncmp ($currentPath, $groupPath, strlen ($currentPath)) != 0) ||
            strcmp ($currentPath, $groupPath) == 0)
            return (false);
        else
            return (true);
    }

    public function delgroupAction ()
    {
        $groupParam = $this->_request->getParam ('id');
        if (strncmp ($groupParam, "delGr", strlen ("delGr")) != 0)
            return (FALSE);
        $potentialId = substr ($groupParam, strlen ("delGr"));
        if (!is_numeric ($potentialId))
            return (FALSE);

        $groupId = intval ($potentialId);
        
        $groups = new Admin_Groups ();
        $group = $groups->findGroupById ($groupId);
        
        if ($group === NULL)
        {
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'This group does not exist'));
        }

        if (!$this->allowedToModify ($group))
        {
            // Not allowed
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'You are not allowed to delete this group'));
        }

        $logger = Zend_Registry::getInstance ()->get ('Zend_Log');
        $logger->info ("Deleted group *".$group->getName ()."*.");

        $groups->delGroup ($group->getId ());

        $allGroups = $this->getGroupsAndUsersList ();
        $allGroups = array_values ($allGroups);
        $this->_helper->json->sendJson (array (
                'status'    => 1,
                'groups'    => $allGroups));
    }

    public function listgroupsAction ()
    {
        $groups = $this->getGroupsAndUsersList ();
        $groups = array_values ($groups);
        $this->_helper->json->sendJson ($groups);
    }

    public function gethierarchyAction ()
    {
        $groups = $this->getGroupsHierarchy ();
        $groups = array_values ($groups);
        $this->_helper->json->sendJson ($groups);
    }

    public function updatehierarchyAction ()
    {
        $hierarchyParam = Zend_Json::decode ($this->_request->getParam ('perms'));
        
        foreach ($hierarchyParam as $group => $value)
            $this->setNewHierarchy ($group, $value, "0");

        $groups = $this->getGroupsHierarchy ();
        $groups = array_values ($groups);
        $this->_helper->json->sendJson ($groups);
    }

    private function setNewHierarchy ($group, $groupChildren, $parents)
    {
        $groups = new Admin_Groups ();

        list ($dummy, $id) = explode ('-', $group);

        if (strcmp ($dummy, "gr") != 0)
            return;

        if (is_array ($groupChildren))
        {
            $groups->setGroupPath ($id, $parents);
            foreach ($groupChildren as $group => $value)
                $this->setNewHierarchy ($group, $value, "$parents/$id");
        }
        else
        {
            $groups->setGroupPath ($id, $parents);

            $permissions = new Admin_Permissions ();
            $parentsArray = explode ('/', $parents);
            $permissions->logicANDPermissions ($parentsArray [count ($parentsArray)-1], $id);
        }
    }

    public function setmaxpermsAction ()
    {
        $groupId        = $this->_request->getParam ('group');

        $permissions    = new Admin_Permissions ();
        $groups         = new Admin_Groups ();

        $groupObj       = $groups->findGroupById ($groupId);
        if (!$groupObj)
        {
            $this->_helper->json->sendJson (array (
                'status'    => '0',
                'msg'       => 'Group does not exist.'));
        }

        $parentsArray   = $groupObj->getParents ();
        if (count ($parentsArray) < 1)
        {
            $this->_helper->json->sendJson (array (
                        'status'    => '0',
                        'msg'       => 'Error: group has no parent !'));
        }

        $perms = $permissions->getPermissions ($parentsArray [count ($parentsArray)-1]);
        $permissions->setPermissions ($groupId, $perms);
        $this->_helper->json->sendJson (array ('status'    => '1'));
    }

    public function getuserslogsAction ()
    {
        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'logsusers'))
        {
            $this->view->setEncoding ('UTF-8');
            $logs = new Admin_logs ();
            $this->view->nbLasts = 1000;
            $this->view->logs = $logs->getLastLogs ($this->view->nbLasts, "ASC");
        }
    }

    public function getboxeslogsAction ()
    {
        if ($this->_acl && $this->_acl->isAllowed ('Administration', 'view', 'logsboxes'))
        {
            $this->view->setEncoding ('UTF-8');
            $logs = new Players_Logs ();
            $this->view->nbLasts = 1000;
            $this->view->logs = $logs->getLastLogs ($this->view->nbLasts, "ASC");
        }
    }

    public function getboxlogsAction ()
    {
        $boxHash = $this->_request->getParam ('hash');
        if (!$boxHash)
        {
            $this->view->logs = "Error.";
            return;
        }

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Player', 'view', $boxHash))
        {
            $this->view->logs = "Not allowed.";
            return;
        }

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');

        $this->view->setEncoding ('UTF-8');
        if (file_exists ($upload->logs."/$boxHash.logs"))
            $this->view->logs = file_get_contents ($upload->logs."/$boxHash.logs");
        else
            $this->view->logs = "Empty logs file.";
    }

    public function getuserAction ()
    {
        $userId = $this->_request->getParam ('id');

        if (is_numeric ($userId))
        {
            $users = new Admin_users ();
            $user = $users->findUserById ($userId);
            
            $returnObj ['id']           = $user->getId ();
            $returnObj ['groupName']    = $user->getGroup () ?
                                            $user->getGroup ()->getName () : '';
            $returnObj ['login']        = $user->getLogin ();
            $returnObj ['firstname']    = $user->getFirstname ();
            $returnObj ['lastname']     = $user->getLastname ();
            $returnObj ['email']        = $user->getEmail ();
            $this->_helper->json->sendJson ($returnObj);
        }
        else
            $this->_helper->json->sendJson (array ());
    }

    public function commituserAction ()
    {
        $userParam = Zend_Json::decode ($this->_request->getParam ('user'));

        if (!$userParam)
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg' => 'Invalid user'));
            return;
        }

        $action = ($userParam ['id'] == -1) ? 'add' : 'edit';
        switch ($action)
        {
            case 'add':
                $this->_addUser ($userParam);
                break;
            case 'edit':
                $this->_editUser ($userParam);
                break;
        }

        $groups = $this->getGroupsAndUsersList ();
        $groups = array_values ($groups);
        $this->_helper->json->sendJson (
            array ('status' => 1,
                   'groups' => $groups));
    }

    protected function _addUser ($userParam)
    {
        if ($this->_group->getId () >= 1 && $userParam ['groupId'] < 1)
            $userParam ['groupId'] = $this->_group->getId ();

        $groups = new Admin_Groups ();
        $groupObj = $groups->findGroupById ($userParam ['groupId']);
        if (!$groupObj)
        {
            // Not allowed
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'Invalid group'));
        }

        if (!$this->allowedToModify ($groupObj))
        {
            // Not allowed
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'You are not allowed to add user in this group'));
        }

        $usersTable = new Admin_users ();
        $return = $usersTable->addUser ($userParam ['login'],
                                        $userParam ['firstname'],
                                        $userParam ['lastname'],
                                        $userParam ['pass'],
                                        $userParam ['email'],
                                        $userParam ['groupId']);
        if ($return > 1)
        {
            $groups = new Admin_Groups ();
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info("Created user *".
                            $userParam ['firstname']." ".
                            $userParam['lastname']."* in group *".
                            $groups->findGroupById ($userParam['groupId'])->getName ()."*.");
        }
        return ($return);
    }

    protected function _editUser ($userParam)
    {
        $usersTable = new Admin_users ();
        $user = $usersTable->findUserById ($userParam ['id']);
        if (!$user)
        {
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'Invalid user'));
        }

        $groups = new Admin_Groups ();
        $groupObj = $groups->findGroupById ($user->getGroupId ());
        if (!$groupObj)
        {
            // Not allowed
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'Invalid group'));
        }
        if (!$this->allowedToModify ($groupObj))
        {
            // Not allowed
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'You are not allowed to edit user of this group'));
        }

        $return = $usersTable->modifyUser ($userParam ['id'],
                                            $userParam ['login'],
                                            $userParam ['firstname'],
                                            $userParam ['lastname'],
                                            $userParam ['pass'],
                                            $userParam ['email']);

        if (count ($return) > 0)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            foreach ($return as $elt)
            {
                $logger->info ("Changed *".$userParam ['login']."*: ".
                                "*".$elt ['field']."* from *".
                                $elt ['old']."* to *".
                                $elt ['new']."*.");
            }
            return (true);
        }
        else
            return (false);
    }

    public function updateusersgroupsAction ()
    {
        $users = new Admin_Users ();
        $groups = new Admin_Groups ();

        $groupsArray = Zend_Json::decode ($this->_request->getParam ('users'));
        if ($groupsArray)
        {
            foreach ($groupsArray as $group)
            {
                if ($group['id'] < 0)
                    continue;

                foreach ($group['users'] as $user)
                {
                    // Check if permissions allow to drag from the old group
                    // and to drop in the new one...
                    $changedUser = $users->findUserById ($user);
                    if (!$changedUser)
                    {
                        $this->_helper->json->sendJson (array (
                                'status'    => 0,
                                'msg'       => 'Invalid user '+$user));
                    }
                    $groupObj = $groups->findGroupById ($changedUser->getGroupId ());
                    if (!$groupObj)
                    {
                        // Not allowed
                        $this->_helper->json->sendJson (array (
                                'status'    => 0,
                                'msg'       => 'Invalid old group'));
                    }
                    if (!$this->allowedToModify ($groupObj))
                    {
                        // Not allowed to drag from
                        $this->_helper->json->sendJson (array (
                                'status'    => 0,
                                'msg'       => 'You are not allowed to remove user from its group '.$groupObj->getName ()));
                    }
                    $groupObj = $groups->findGroupById ($group ['id']);
                    if (!$groupObj)
                    {
                        // Not allowed
                        $this->_helper->json->sendJson (array (
                                'status'    => 0,
                                'msg'       => 'Invalid new group'));
                    }
                    if (!$this->allowedToModify ($groupObj))
                    {
                        // Not allowed to drag from
                        $this->_helper->json->sendJson (array (
                                'status'    => 0,
                                'msg'       => 'You are not allowed to add this user to group '.$groupObj->getName ()));
                    }

                    if ($users->setUserGroup ($user, $group['id']))
                    {
                        $newGroup = $groups->findGroupById ($group['id']);

                        $logger = Zend_Registry::getInstance()->get('Zend_Log');
                        $logger->info("Changed *".  $changedUser->getName ().
                                        "* to group *".$newGroup->getName ()."*.");
                    }
                }
            }
        }

        $groups = $this->getGroupsAndUsersList ();
        $groups = array_values ($groups);
        $this->_helper->json->sendJson (
            array ('status' => 1,
                   'groups' => $groups));
    }

    public function savepermsAction ()
    {
        $groups     = new Admin_Groups ();
        $permissions= new Admin_Permissions ();

        $groupId    = $this->_request->getParam ('group');
        $perms      = Zend_Json::decode ($this->_request->getParam ('permissions'));
        
        $groupObj   = $groups->findGroupById ($groupId);
        if (!$groupObj)
        {
            $this->_helper->json->sendJson (array (
                'status'    => '0',
                'msg'       => 'Group does not exist.'));
        }

        if (!$this->allowedToModify ($groupObj))
        {
            // Not allowed to drag from
            $this->_helper->json->sendJson (array (
                    'status'    => 0,
                    'msg'       => 'You are not allowed to edit permission for this group'));
        }

        // Delete permissions
        $permissions->delGroupPermissions ($groupObj->getId ());

        // Add the new ones
        foreach ($perms as $newPerm)
        {
            $resId      = $newPerm [0];
            $resAllowed = $newPerm [1];

            if ($resAllowed)
                $permissions->addPermission ($resId, $groupObj->getId ());
        }

        // Filter with the direct parent permissions
        $parents = $groupObj->getParents ();
        if (count ($parents))
            $permissions->logicANDPermissions ($parents [count ($parents)-1], 
                                                $groupObj->getId ());

        // Filter children permissions (if the group is not allowed to do
        // something, its children are not allowed too)
        $children = $groups->getGroupChildren ($groupObj->getId ());
        foreach ($children as $filterGroup)
            $permissions->logicANDPermissions ($groupObj->getId (), $filterGroup);

        $this->_helper->json->sendJson (array ('status'    => '1'));
    }

    public function getpermsAction ()
    {
        $result     = array ();
        $groupId    = $this->_request->getParam ('grId');
        $groups     = new Admin_Groups ();
        $resources  = new Admin_Resources ();

        $group      = $groups->findGroupById ($groupId);
        if (!$group)
            $this->_helper->json->sendJson ($result);

        if (!$this->allowedToModify ($group) &&
            $group->getId () != $this->_group->getId ())
            $this->_helper->json->sendJson ($result);

        $perms      = new Admin_Permissions ();
        $perms->filterGroup ($groupId);

        $result ['groupid']     = $group->getId ();
        $result ['groupname']   = $group->getName ();

        if ($group->getId () == $this->_group->getId ())
            $result ['groupadmin'] = false;
        else
            $result ['groupadmin'] = true;

        $permissions = array ();

        // Get all defined type
        $types = $resources->getTypes ();
        foreach ($types as $type)
        {
            $idsForType = array ();
            // Get all IDs for each type
            $ids = $resources->getIdsForType ($type);
            foreach ($ids as $id)
            {
                // Get all actions for a given type and id
                $actionsForType = array ();
                $actions = $resources->getActions ($type, $id);
                $idLabel = $resources->getStringForResource ($type, $id);
                if ($idLabel == -1)
                {
                    $resources->deleteResource ($type, $id);
                    continue;
                }
                if ($id === NULL || $id < 0)
                    $idLabel = "";

                foreach ($actions as $action)
                {
                    // Get permission for designated group
                    $actionsForType [$action] = array ('id'         => $resources->getResourceId ($type, $action, $id),
                                                       'allowed'    => $perms->isAllowed ($type, $action, $id));
                }

                ksort ($actionsForType);
                $idsForType [$idLabel] = $actionsForType;
            }

            ksort ($idsForType);
            $permissions [$type] = $idsForType;
        }

        ksort ($permissions);

        $result ['permissions'] = $permissions;
        $this->_helper->json->sendJson ($result);
    }

    protected function getGroupsHierarchy ()
    {
        $groups = new Admin_Groups ();
        //$users = new Admin_Users;

        $allGroups = $groups->getGroupsArray ();

        $groupsSeq = array ();
        foreach ($allGroups as $group)
        {
            $groupObj = $groups->findGroupById ($group ['id']);
            if (!$groupObj)
                continue;

            if (!$this->allowedToModify ($groupObj))
            {
                if ($group ['id'] != $this->_group->getId ())
                    continue;
            }

            $groupAndUsers = array ('group'     => $group,
                                    'parents'   => explode ('/', $group ['path']),
                                    'children'  => array ());
            $groupsSeq [$group ['id']] = $groupAndUsers;
        }

        $groupsTree = array ();
        $finished = false;
        $depth = 0;
        while (!$finished)
        {
            $depth++;
            $finished = true;
            foreach ($groupsSeq as $key => $value)
            {
                if ($value ['group']['id'] == 0)
                    continue;

                if (count ($value ['parents']) != $depth)
                {
                    if ($finished && count ($value ['parents']) > $depth)
                        $finished = false;
                    continue;
                }

                $lastParent = $value ['parents'][count ($value ['parents']) - 1];
                if ($lastParent < 1)
                    $groupsTree [] = $value;
                else
                {
                    // Get the children reference of its direct parent
                    $path = &$groupsTree;
                    for ($i = 0 ; $i < count ($value ['parents']) ; $i++)
                    {
                        // Ignore iPreso Team group
                        if ($value ['parents'][$i] == 0)
                            continue;

                        for ($j = 0 ; $j < count ($path) ; $j ++)
                        {
                            if ($path [$j]['group']['id'] == $value ['parents'][$i])
                                $path = &$path [$j]['children'];
                        }
                    }
                    $path [] = $value;
                }
            }
        }

        return ($groupsTree);
    }
}
