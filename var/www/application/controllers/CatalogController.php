<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class CatalogController extends Zend_Controller_Action
{
    private $_acl;

    public function init ()
    {
        $contextSwitch = $this->_helper->getHelper ('contextSwitch');
        $contextSwitch->addActionContext ('delfromlibrary', 'json')->initContext ();
        $contextSwitch->addActionContext ('dwfromlibrary', 'json')->initContext ();
        $contextSwitch->addActionContext ('getcatalog', 'json')->initContext ();
        $contextSwitch->addActionContext ('getemissions', 'json')->initContext ();
        $contextSwitch->addActionContext ('getprog', 'json')->initContext ();
        $contextSwitch->addActionContext ('delprog', 'json')->initContext ();
        $contextSwitch->addActionContext ('dwemi', 'json')->initContext ();
        $contextSwitch->addActionContext ('commitproperties', 'json')->initContext ();
        $contextSwitch->addActionContext ('commitprog', 'json')->initContext ();
        $contextSwitch->addActionContext ('quickprog', 'json')->initContext ();
    }

    public function preDispatch ()
    {
        if ($this->getRequest ()->getActionName () != 'handleflashupload')
        {
            if ($this->_request->getParam ('Session'))
                $_COOKIE ['PHPSESSID'] = $this->_request->getParam ('Session');

            if (!Zend_Auth::getInstance ()->hasIdentity ())
                $this->_helper->redirector ('index', 'login');

            $user = Zend_Registry::getInstance ()->get ('App_User');
            if (!$user)
            {
                Zend_Auth::getInstance()->clearIdentity();
                $this->_helper->redirector ('index', 'login');
            }

            $this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
            $this->view->variables      = "";
            $this->view->permissions    = $this->_acl;
        }
        else
        {
            // Upload via the Flash component doesn't support Zend Authentication
            $this->view->variables      = "";
        }
    }

    public function indexAction ()
    {
        $commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $this->view->orientation = true;
        else
            $this->view->orientation = false;

        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $this->view->variables .= "var strConfirmDelZone = '"
                                    .$translator->translate ("Are you sure you want to delete the zone ?")."';\n";
        $this->view->variables .= "var strConfirmDelEmission = '"
                                    .$translator->translate ("Are you sure you want to delete this emission: ")."';\n";
        $this->view->variables .= "var strZoneName = '"
                                    .$translator->translate ("Zone\'s name: ")."';\n";
        $this->view->variables .= "var strZoneDimension = '"
                                    .$translator->translate ("Zone\'s dimensions: ")."';\n";
        $this->view->variables .= "var strSequence = '"
                                    .$translator->translate ("Sequence of this zone")."';\n";
        $this->view->variables .= "var strNoEnd = '"
                                    .$translator->translate ("No end")."';\n";
        $this->view->variables .= "var strItemProperties = '"
                                    .$translator->translate ("Item\'s Properties")."';\n";
        $this->view->variables .= "var strMaxZones = '"
                                    .$translator->translate ("Maximum number of zones is reached !")."';\n";
        $this->view->variables .= "var strOk = '"
                                    .$translator->translate ("Ok")."';\n";
        $this->view->variables .= "var strCancel = '"
                                    .$translator->translate ("Cancel")."';\n";
        $this->view->variables .= "var strCreateNewEmission = '"
                                    .$translator->translate ("Create new emission")."';\n";
        $this->view->variables .= "var strErrorSaveCalendar = '"
                                    .$translator->translate ("Calendar can\'t be saved.")."';\n";
        $this->view->variables .= "var strProgCreated = '"
                                    .$translator->translate ("Programmation created: ")."';\n";
        $this->view->variables .= "var strQuickProg = '"
                                    .$translator->translate ("Quick programmation")."';\n";
        $this->view->variables .= "var strMon = '".$translator->translate('Monday')."';\n";
        $this->view->variables .= "var strTue = '".$translator->translate('Tuesday')."';\n";
        $this->view->variables .= "var strWed = '".$translator->translate('Wednesday')."';\n";
        $this->view->variables .= "var strThu = '".$translator->translate('Thursday')."';\n";
        $this->view->variables .= "var strFri = '".$translator->translate('Friday')."';\n";
        $this->view->variables .= "var strSat = '".$translator->translate('Saturday')."';\n";
        $this->view->variables .= "var strSun = '".$translator->translate('Sunday')."';\n";
        $this->view->variables .= "var strRecurrence = '".$translator->translate('Recurrence: ')."';\n";
        $this->view->variables .= "var strStart = '".$translator->translate('Start: ')."';\n";
        $this->view->variables .= "var strEnd = '".$translator->translate('End: ')."';\n";
        $this->view->variables .= "var strDefaultZoneName = '".$translator->translate('Zone #')."';\n";
        $this->view->variables .= "var strAtLeastOneZone = '".$translator->translate('Please create at least one area.')."';\n";
        $this->view->variables .= "var strEmiBasedOn = '".$translator->translate('Your emission is based on emission: ')."';\n";
        $this->view->variables .= "var strReplaceOldEmi = '".$translator->translate('Do you want to replace (erase) the old emission by the new one ?')."';\n";
        $this->view->variables .= "var strYes = '".$translator->translate('Yes')."';\n";
        $this->view->variables .= "var strNo = '".$translator->translate('No')."';\n";
        $this->view->variables .= "var strLoading = '".$translator->translate('Loading library...')."';\n";

        // Get parameters (referer & program Id)
        $emissionId = $this->_request->getParam ('emissionId');
        $progId     = $this->_request->getParam ('progId');
        $referer    = $this->_request->getParam ('referer');
        $fromPage   = $this->_request->getParam ('page');

        if (!strlen ($fromPage))
            $fromPage = "/Supervision";

        $emiLabel   = "";

        if (strlen ($emissionId) >= 1)
        {
            $programs   = new Progs_Programs ();
            $program    = $programs->findProgram ($emissionId);
            if ($program)
                $emiLabel   = $program->getName ();
            else
                $emissionId = "";
        }

        $this->view->referer    = $referer;
        $this->view->emission   = $emissionId;
        $this->view->from       = $fromPage;
        $this->view->emiLabel   = $emiLabel;
    }

    public function getemissionsAction ()
    {
        if ($this->_acl && $this->_acl->isAllowed ('Emission', 'new'))
            $newEmission = true;
        else
            $newEmission = false;

        $this->_helper->json->sendJson (
                    array ('newEmission'=> $newEmission,
                           'emissions'  => $this->getEmissions ()));
    }

    protected function getEmissions ()
    {
        $result         = array ();
        $emissionsObj   = new Progs_Programs ();
        $emissions      = $emissionsObj->getProgsArray ();

        foreach ($emissions as $emission)
        {
            if ($this->_acl &&
                $this->_acl->isAllowed ('Emission', 'view', $emission ['id']))
                $result []  = array ('value'    => $emission ['id'],
                                     'name'     => $emission ['label']);
        }
        return ($result);
    }

    public function getcatalogAction ()
    {
        $media      = $this->getMedia ();
        $this->_helper->json->sendJson ($media);
    }

    protected function getMedia ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $result = array ();

        $pluginsObj = new Media_Plugins ();
        $plugins    = $pluginsObj->getPlugins ();

        foreach ($plugins as $plugin)
        {
            if (!$plugin->getEnable ())
                continue;

            $parsedElements = array ();
            $elements = $plugin->getLibrary ();
            foreach ($elements as $key => $value)
            {
                $thumbLink = "/Catalog/getfilepreview/type/".
                                $plugin->getInternalName ();

                if (array_key_exists ('preview', $value) &&
                    strcmp ($value ['preview'], 'default') == 0)
                    $thumbLink .= "/preview.png";
                else
                    $thumbLink .= "/id/$key/preview.png";

                $parsedElements [] = array (
                        'id'        => $key,
                        'plugin'    => $plugin->getInternalName (),
                        'length'    => $value ['length'],
                        'name'      => $value ['name'],
                        'prop'      => $plugin->getDefaultPropertiesHash (),
                        'startdate' => $plugin->getDefaultStartDate (),
                        'enddate'   => $plugin->getDefaultEndDate (),
                        'dw'        => $plugin->areItemsDownloadable (),
                        'del'       => $plugin->areItemsDeletable (),
                        'thumbLink' => $thumbLink);
            }

            if (count ($parsedElements) > 0)
                $result [] = array ('title'     => $plugin->getName (),
                                    'elements'  => $parsedElements);
        }

        return ($result);
    }

    public function getfilepreviewAction ()
    {
        // Return image in binary format
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();

        $plugin     = $this->_request->getParam ('type');
        $fileHash   = $this->_request->getParam ('id');

        $plugins = new Media_Plugins ();
        $preview = $plugins->getPreview ($plugin, $fileHash);
        if (!$preview)
            return ("");

        header ('Content-type: image/png');
        $this->_response->setBody ($preview);
    }

    public function getlayoutAction ()
    {
        // Return image in binary format
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();

        $idLayout = $this->_request->getParam ('id');
        $layouts = new Progs_Layouts ();
        $layout = $layouts->findLayout ($idLayout);
        if (!$layout || !$layout->getPreview ())
        {
            // UNKNOWN layout image
            $layout = $layouts->findLayout ('UNKNOWN');
            if (!$layout)
            {
                $unknown = file_get_contents ('/opt/iComposer/var/www/public/images/Unknown.png', null);
                $layouts->createLayout ('UNKNOWN', '', $unknown);
                $layout = $layouts->findLayout ('UNKNOWN');
            }
        }

        if (!$layout)
            return "";

        header ('Content-type: image/png');
        //header ('Content-type: text/plain');
        $this->_response->setBody ($layout->getPreview ());
    }

    public function getemilayoutAction ()
    {
        // Return image in binary format
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();

        $emissions  = new Progs_programs ();
        $layouts    = new Progs_Layouts ();
        $idProg     = $this->_request->getParam ('id');
        $emission   = $emissions->findValidProgram ($idProg);
        $idLayout   = $emission->getLayout ();
        $layout     = $layouts->findLayout ($idLayout);
        if (!$layout || !$layout->getPreview ())
        {
            // UNKNOWN layout image
            $layout = $layouts->findLayout ('UNKNOWN');
            if (!$layout)
            {
                $unknown = file_get_contents ('/opt/iComposer/var/www/public/images/Unknown.png', null);
                $layouts->createLayout ('UNKNOWN', '', $unknown);
                $layout = $layouts->findLayout ('UNKNOWN');
            }
        }

        if (!$layout)
            return "";

        header ('Content-type: image/png');
        //header ('Content-type: text/plain');
        $this->_response->setBody ($layout->getPreview ());
    }

    public function getprogAction ()
    {
        $progId = $this->_request->getParam ('id');
        $this->_sendProgram ($progId);
    }

    protected function _sendProgram ($progId)
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $orientEnabled = true;
        else
            $orientEnabled = false;

        $jsonObj = array ();

        $progsObj   = new Progs_Programs ();
        $program    = $progsObj->findProgram ($progId);
        if (!$program)
        {
            // New program, return default value for creation
            $jsonObj ['id']         = "";
            $jsonObj ['name']       = "";
            $jsonObj ['layout']     = "";
            $jsonObj ['emiEdit']    = true;
            $jsonObj ['emiDel']     = false;
            $jsonObj ['zones' ]     = $this->_getDefaultZones ();
            $this->_helper->json->sendJson ($jsonObj);
        }

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Emission', 'view', $progId))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate('You are not allowed to view this emission.')));
        }

        $progDelete = $this->_acl->isAllowed ('Emission', 'delete', $progId);
        $progEdit   = $this->_acl->isAllowed ('Emission', 'edit', $progId);
        //$progQuickUpdate = $this->_acl->isAllowed ('Emission', 'quickupdate', $progId);

        if ($orientEnabled)
        {
            require_once 'Actions/Plugin/Orientation.php';
            $className = 'Actions_Plugin_Orientation';
            $command = new $className ();
            $orientation = $command->getLayoutOrientation ($program->getLayout ());
            $jsonObj ['layout'] = $orientation.$program->getLayout ();
        }
        else
            $jsonObj ['layout'] = $program->getLayout ();

        $jsonObj ['id']         = $program->getId ();
        $jsonObj ['name']       = $program->getName ();
        $jsonObj ['emiEdit']    = $progEdit;
        $jsonObj ['emiDel']     = $progDelete;
        $jsonObj ['zones' ]     = $this->_getZonesArray ($program);

        $this->_helper->json->sendJson ($jsonObj);
    }

    public function _getZonesArray ($program)
    {
        $layouts = new Progs_Layouts ();
        $playlists = new Progs_Playlists ();

        $layout = $layouts->findLayout ($program->getLayout ());
        $playlist = $playlists->findPlaylist ($program->getPlaylist ());

        if (!$layout)
            return (array ());

        $zonesArray = $layout->getZones ();

        $result = array ();
        foreach ($zonesArray as $zone)
        {
            if ($playlist)
                $playlistOfZone = $playlist->getZone ($zone->getName ());
            else
                $playlistOfZone = array ();

            $result [] = 
                $this->_getNewZone ($zone->getName (),
                                    $zone->getLayer (),
                                    $zone->getX (),
                                    $zone->getY (),
                                    $zone->getWidth (),
                                    $zone->getHeight (),
                                    $playlistOfZone);
        }
        return ($result);
    }

    public function _getNewZone ($name, $layer, $x, $y, $w, $h, $playlist)
    {
        $zone = array ();

        $zone ['name']      = $name;
        $zone ['layer']     = $layer;
        $zone ['x']         = $x;
        $zone ['y']         = $y;
        $zone ['width']     = $w;
        $zone ['height']    = $h;
        $zone ['playlist']  = $playlist;

        return ($zone);
    }

    public function _getDefaultZones ()
    {
        $result = array ();

/*
        $result [] = $this->_getNewZone ('Main', 1, 0, 0, 100, 89, array ());
        $result [] = $this->_getNewZone ('Text', 2, 13, 90, 77, 10, array ());
        $result [] = $this->_getNewZone ('Logo', 3, 90, 90, 10, 10, array ());
        $result [] = $this->_getNewZone ('Clock', 4, 0, 90, 13, 10, array ());
*/

        return ($result);
    }

    protected function _sendUploadReply ($reply)
    {
        if (!array_key_exists ('id', $reply))
        {
            echo "<script language=\"javascript\" type=\"text/javascript\">"
                 ."alert ('".implode (',', $reply)."');"
                 ."</script>";
            return;
        }

        $id = $reply ['id'];
        switch ($reply ['code'])
        {
            case '0':
                // Error
                $color = "#bb0000";
                break;
            case '1':
                // Ok
                $color = "#009900";
                break;
            default:
                // Unknown
                $color = "#ff9900";
                break;
        }

        if (isset ($reply ['status']))
            $status = $reply ['status'];
        else
            $status = '???';

        if (isset ($reply ['details']))
            $status .= " - ".$reply ['details'];
        
        $json = array ();
        $json ['id']        = $id;
        $json ['color']     = $color;
        $json ['status']    = $status;
        echo json_encode ($json);
    }

    protected function convertFromSeconds ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    protected function convertFromBytesTo ($nbBytes, $unit)
    {
        switch ($unit)
        {
            case 'B':
                return ($nbBytes);
            case 'KB':
                return (round ($nbBytes / 1024, 2));
            case 'MB':
                return (round ($nbBytes / (1024*1024), 2));
            case 'GB':
                return (round ($nbBytes / (1024*1024*1024), 2));
            case 'TB':
                return (round ($nbBytes / (1024*1024*1024*1024), 2));
            default:
                return ("");
        }
    }

    protected function convertFromBytes ($nbBytes)
    {
        $result = array ('quantity' => $nbBytes,
                         'unit'     => 'B');

        while ($result ['quantity'] >= 1024)
        {
            $result ['quantity'] = round ($result ['quantity'] / 1024, 2);
            switch ($result ['unit'])
            {
                case 'B':
                    $result ['unit'] = 'KB';
                    break;
                case 'KB':
                    $result ['unit'] = 'MB';
                    break;
                case 'MB':
                    $result ['unit'] = 'GB';
                    break;
                case 'GB':
                    $result ['unit'] = 'TB';
                    break;
            }
        }

        return ($result);
    }

    protected function _getType ($path)
    {
        $cmd = "/usr/bin/file -p -b --mime-type '$path'";
        $f = popen ("$cmd", 'r');
        $line = fgets ($f, 512);
        pclose ($f);

        return (trim ($line));
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }

    protected function _uploadedTarFile ($path)
    {
        // Check the file is an iPreso archive
        $return = $this->_execute ("/bin/tar -tf \"$path\" | grep ./ipreso-backup.ini");
        if (strlen ($return) < 1)
            return (false);

        // Move the file into the library for decompression
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $libraryPath = $upload->library."/".basename ($path);
        if (!mkdir ($libraryPath))
            return (false);
        if (!rename ($path, "$libraryPath/".basename ($path)))
            return (false);
        
        // Decompress the file
        system ("/bin/tar -C \"$libraryPath\" -xf \"$libraryPath/".basename ($path)."\"");

        // Get the type of archive
        $type = $this->_execute ("/bin/grep -E '^type:' \"$libraryPath/ipreso-backup.ini\" | ".
                                 "/usr/bin/cut -d':' -f2");
        if (strlen ($type) < 1)
        {
            system ("/bin/rm -rf \"$libraryPath\"");
            return (false);
        }

        // TODO
        // Call the appropriate loader
        // (emission/programmation/configuration/...)
        $return = false;
        switch ($type)
        {
            case "Emission":
                $progsObj = new Progs_Programs ();
                $return = $progsObj->loadBackup ($libraryPath);
                break;
            case "Programmation":
                // Load Schedule and library
                $progsObj = new Progs_Channels ();
                $returnProg = $progsObj->loadBackup ($libraryPath);
                if ($returnProg)
                {
                    // Load emissions
                    $directory = opendir ($libraryPath);
                    if ($directory)
                    {
                        $return = true;
                        while ($return &&
                                ($entry = @readdir ($directory)))
                        {
                            if (is_dir ($libraryPath.'/'.$entry) ||
                                $entry == '.' ||
                                $entry == '..')
                                continue;
                            if (strncmp ($entry, basename ($path), strlen ($entry)) == 0)
                                continue;

                            if (strncmp (
                                    $this->_getType ($libraryPath.'/'.$entry),
                                    "application/x-tar",
                                    strlen ("application/x-tar")) == 0)
                            {
                                // File is an archive (emission ?) -> load it
                                $return = $this->_uploadedTarFile ($libraryPath.'/'.$entry);
                            }
                        }
                    }
                    if ($return != false)
                    {
                        // Prog is okay
                        $return = $returnProg;
                    }
                }
                break;
            case "Configuration":
                break;
            case "Backup":
                break;
            default:
                $return = false;
        }

        system ("/bin/rm -rf \"$libraryPath\"");
        return ($return);
    }

    public function handlefileuploadAction ()
    {
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");

        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $id = $this->_request->getParam ('id');
        if ($id)
        {
            $nfo = uploadprogress_get_info($id);
            if (!$nfo) {
                $this->_sendUploadReply (array (
                    'id'        => '0',
                    'code'      => '0',
                    'status'    => $translator->translate ('Failed'),
                    'details'   => $translator->translate ('No file uploaded')
                                        ));
                return;
            }

            $total = $this->convertFromBytes ($nfo ['bytes_total']);
            $done = $this->convertFromBytesTo ($nfo ['bytes_uploaded'],
                                                $total['unit']);

            $speed = $this->convertFromBytes ($nfo ['speed_average']);

            $result = array ();
            $result ['postId']          = $id;
            $result ['id']              = substr ($nfo ['fieldname'], 6);
            $result ['percentage']      = 
                round (($nfo ['bytes_uploaded'] / $nfo ['bytes_total']) * 100);

            $statusString = "$done / ".$total['quantity']." ".$total['unit']
                            ." (".$result ['percentage']."%) - "
                            .$translator->translate ("Average speed: ")
                            .$speed ['quantity']." ".$speed ['unit']."/s - "
                            .$translator->translate ("ETA: ")
                            .$this->convertFromSeconds ($nfo ['est_sec']);
            
            $result ['progressString']  = $statusString;

            if ($result)
                $this->_helper->json->sendJson ($result);
            else
                $this->_helper->json->sendJson (array ('plip'));
        }

        // Upload complete
        if (!count ($_FILES))
        {
            $this->_sendUploadReply (array (
                'id'        => '0',
                'code'      => '0',
                'status'    => $translator->translate ('Failed'),
                'details'   => $translator->translate ('No file uploaded')
                                    ));
            return;
        }

        $keys = array_keys ($_FILES);
        $fileId = $keys [0];
        $file = $_FILES [$fileId];

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        if (strlen ($upload->sandbox) < 1)
            $upload->sandbox = "/tmp/sandbox";

        // Replace special characters
        $file ['name'] = preg_replace ("/['&!;:\/\[\(\]\)]/","",$file ['name']);

        // Replace spaces with "_"
        $file ['name'] = preg_replace('/\s+/', '_', $file ['name']);

        if (!file_exists ($upload->sandbox))
            mkdir ($upload->sandbox, 0777, true);

        // Move the file to the sandbox
        if (move_uploaded_file ($file ["tmp_name"], $upload->sandbox."/".$file["name"]))
        {

            // Special case : files "__LOGO__.png": Wallpaper for boxes !
            if (strcmp ($file ["name"], "__LOGO__.png") == 0 &&
            rename ($upload->sandbox."/".$file["name"],
                    $upload->library."/__LOGO__.png"))
            {
                $this->_sendUploadReply (array (
                    'id'        => substr ($fileId, 6),
                    'code'      => '1',
                    'status'    => $translator->translate ('Ok: new background logo sent.')));
            }
            else if (strncmp ($this->_getType ($upload->sandbox."/".$file ["name"]),
                                                  "application/x-tar",
                                                  strlen ("application/x-tar")) == 0)
            {
                // Maybe an ipreso special file...
                $return = $this->_uploadedTarFile ($upload->sandbox."/".$file ["name"]);
                if (!$return)
                {
                    $this->_sendUploadReply (array (
                        'id'        => substr ($fileId, 6),
                        'code'      => '0',
                        'status'    => $translator->translate ('Failed'),
                        'details'   => $translator->translate ('Not valid iPreso file.')
                                        ));
                }
                else
                {
                    $this->_sendUploadReply (array (
                        'id'        => substr ($fileId, 6),
                        'code'      => '1',
                        'status'    => $translator->translate ('Ok')));
                }
                if (file_exists ($upload->sandbox."/".$file ["name"]))
                    unlink ($upload->sandbox."/".$file ["name"]);
            }
            else
            {
                $plugins = new Media_Plugins ();
                if (!$plugins->addFile ($upload->sandbox."/".$file ["name"]))
                {
                    if (file_exists ($upload->sandbox."/".$file ["name"]))
                        unlink ($upload->sandbox."/".$file ["name"]);

                    $this->_sendUploadReply (array (
                        'id'        => substr ($fileId, 6),
                        'code'      => '0',
                        'status'    => $translator->translate ('Failed'),
                        'details'   => $translator->translate ('File is not supported.')
                                        ));
                }
                else
                {
                    $this->_sendUploadReply (array (
                        'id'        => substr ($fileId, 6),
                        'code'      => '1',
                        'status'    => $translator->translate ('Ok')));
                }
            }
        }
        else
        {
            $this->_sendUploadReply (array (
                'id'        => substr ($fileId, 6),
                'code'      => '0',
                'status'    => $translator->translate ('Failed'),
                'details'   => $translator->translate ('Cannot save the file.')
                                ));
        }
    }

    public function uploadAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $this->_helper->layout->disableLayout ();
        $upVersion = 'basic';
        $this->view->upVersion = $upVersion;
        $this->view->variables .= "var deleteString = '"
                                    .$translator->translate ('Delete')."';\n";
        $this->view->variables .= "var cancelString = '"
                                    .$translator->translate ('Cancel')."';\n";
        $this->view->variables .= "var abortedString = '"
                                    .$translator->translate ('Transfer aborted')."';\n";
    }
    

    public function delfromlibraryAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $result = array ();

        $item = $this->_request->getParam ('item');
        list ($pluginName, $id) = explode ('-', $item);

        // Check if item exists
        $pluginsObj = new Media_Plugins ();
        $plugin     = $pluginsObj->getPlugin ($pluginName);
        if (!$plugin->fileExists ($id))
        {
            $result ['code'] = 0;
            $result ['details'] = $translator->translate ('Error: File does not exists.');
            $this->_helper->json->sendJson ($result);
        }

        // Check if media is in some manifest files
        $manifests = new Progs_Manifests ();
        $manifestList = $manifests->getManifestsWithFileHash ($id);

        if (count ($manifestList) < 1)
            $this->_deleteFileFromLib ($plugin, $id);

        // Check if manifest files are used by programs
        $programs = new Progs_Programs ();
        $programList = $programs->getProgramWithManifest ($manifestList);
        if (count ($programList) < 1)
            $this->_deleteFileFromLib ($plugin, $id);

        $result ['code']    = 0;
        $result ['details'] = $translator->translate ('Error: File exists in programs: ')
                                . implode (',', $programList)
                                .".\n". $translator->translate ('Please delete programs first.');
        $this->_helper->json->sendJson ($result);
    }

    public function dwfromlibraryAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $result = array ();

        $item = $this->_request->getParam ('item');
        list ($pluginName, $id) = explode ('-', $item);

        // Check if item exists
        $pluginsObj = new Media_Plugins ();
        $plugin     = $pluginsObj->getPlugin ($pluginName);
        if (!$plugin->fileExists ($id))
        {
            $result ['status'] = 0;
            $result ['details'] = $translator->translate ('Error: File does not exists.');
            $this->_helper->json->sendJson ($result);
        }

        // Return path to download
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $localPath = $plugin->getFilePath($id);
        $result ['status']    = 1;
        $result ['path']      = str_replace ($upload->library."/",'', $localPath);
        $this->_helper->json->sendJson ($result);
    }

    private function _deleteFileFromLib ($plugin, $hash)
    {
            $plugin->delFile ($hash);

            // No manifest with this file
            $result ['code'] = 1;
            $this->_helper->json->sendJson ($result);
    }

    public function delprogAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $idProg = $this->_request->getParam ('id');

        // Check if emission exists
        $progsObj = new Progs_Programs ();
        $prog = $progsObj->findProgram ($idProg);
        if (!$prog)
        {
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error: Emission does not exists.');
            $this->_helper->json->sendJson ($result);
        }

        // Check if emission is used by a programmation
        $channelsObj = new Progs_Channels ();
        $channelsList = $channelsObj->getChannelsWithEmission ($prog);

        if (count ($channelsList) >= 1)
        {
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error: Emission exists in programmations: ')
                                    . implode (',', $channelsList)
                                    .".\n". $translator->translate ('Please delete programmations first.');
            $this->_helper->json->sendJson ($result);
        }

        // Delete emission
        if ($progsObj->deleteProgram ($idProg))
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info ("Deleted emission *".$prog->getName()."*.");
            $this->_helper->json->sendJson (array ('code' => 1));
        }
        else
        {
            $result ['code']    = 0;
            $result ['details'] = $translator->translate ('Error.');
            $this->_helper->json->sendJson ($result);
        }
    }
    
    public function dwemiAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $idProg = $this->_request->getParam ('id');
        $progsObj = new Progs_Programs ();
        $prog = $progsObj->findProgram ($idProg);
        if ($prog)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info ("Download of *".$prog->getName ()."* asked.");

            // Create backup file with library
            $path = $prog->createBackup (true);
            if ($path)
            {
                $this->_helper->json->sendJson (array ('status' => 1,
                                                       'path'   => "$path"));
            }
            else
            {
                $this->_helper->json->sendJson (array ('status' => 0,
                                                       'error'   => $translator->translate ('Cannot create emission backup')));
            }
        }

        $this->_helper->json->sendJson (array ('status' => 0,
                                               'error'  => $translator->translate ('Emission not found')));
    }

    public function getpropertiesAction ()
    {
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $paramPlugin    = $this->_request->getParam ('plugin');
        $paramId        = $this->_request->getParam ('item');
        $paramStart     = $this->_request->getParam ('startdate');
        $paramEnd       = $this->_request->getParam ('enddate');
        $paramProp      = $this->_request->getParam ('properties');

        if ($paramStart == "0")
            $paramStart = "";
        if ($paramEnd == "0")
            $paramEnd = "";

        if (!$paramPlugin || !$paramId)
        {
            echo "<span>".$translator->translate ('Error')."</span>\n";
            return;
        }

        $pluginsObj = new Media_Plugins ();
        $plugin     = $pluginsObj->getPlugin ($paramPlugin);
        if (!$plugin)
            return ("<span>".$translator->translate ('Error')."</span>\n");

        $html = $plugin->getHTMLProperties ($paramId, $paramProp);

        // Color elements should have a special treatment
        $html .= "<script type=\"text/javascript\">\n"
                ."  var colorInputs = $$('div.colorInput');\n"
                ."  for (var i = 0 ; i < colorInputs.length ; i++) {\n"
                ."      var input = $(colorInputs[i].id);\n"
                ."      var rainbow = new MooRainbow (input.id,\n"
                ."                          {'id': 'rainbow'+i,\n"
                ."                           'imgPath': '/images/',\n"
                ."                           'onComplete': function (color) {\n"
                ."                              this.element.setStyle ('background-color', color.hex);\n"
                ."                              this.element.set ('color', color.hex);\n"
                ."                          }});\n"
                ."      rainbow.manualSet (input.get ('color'),'hex');\n"
                ."  }\n"
                ."  var datepickerInputs = $$('.datepicker');\n"
                ."  for (var i = 0 ; i < datepickerInputs.length ;i ++) {\n"
                ."        var picker = new Pikaday({\n"
                ."              field: datepickerInputs[i],\n"
                ."              format: 'YYYY-MM-DD',\n"
                ."              firstDay: 1,\n"
                ."              minDate: moment('2016-01-01', 'YYYY-MM-DD').toDate(),\n"
                ."              defaultDate: moment(datepickerInputs[i].get('value'), 'YYYY-MM-DD').toDate(),\n"
                ."              setDefaultDate: true,\n"
                ."          });\n"
                ."        if (datepickerInputs[i].id == 'datepicker-startdate') {\n"
                ."            if (\"$paramStart\".length > 0) {\n"
                ."              picker.setDate(\"$paramStart\");\n"
                ."            } else {\n"
                ."              picker.setDate(moment());\n"
                ."            }"
                ."        }\n"
                ."        if (datepickerInputs[i].id == 'datepicker-enddate') {\n"
                ."            if (\"$paramEnd\".length > 0) {\n"
                ."              picker.setDate(\"$paramEnd\");\n"
                ."            }"
                ."        }\n"
                ."  }\n"
                ."</script>";

        echo $html;
    }

    public function commitpropertiesAction ()
    {
        $paramPlugin    = $this->_request->getParam ('plugin');
        $paramItem      = $this->_request->getParam ('item');
        $paramPropId    = $this->_request->getParam ('propId');

        $jsonObj        = array ();
        $jsonObj ['propId'] = $paramPropId;

        $paramProperties = Zend_Json::decode ($this->_request->getParam ('properties'));

        $pluginsObj = new Media_Plugins ();
        $plugin     = $pluginsObj->getPlugin ($paramPlugin);
        if (!$plugin)
            $this->_helper->json->sendJson ($jsonObj);

        // Transform the properties parameters into understandable array
        $properties = array ();
        foreach ($paramProperties as $prop)
            $properties [$prop [0]] = $prop [1];

        if (empty ($paramItem))
            $paramItem = "";
        if (empty ($paramPropId))
            $paramPropId = "";

        $newProperties = $plugin->setProperties ($paramItem,
                                                 $paramPropId,
                                                 $properties);

        $jsonObj ['propId'] = $newProperties->getId ();
        $jsonObj ['duration'] = $plugin->getProperty ($newProperties, "duration");
        $jsonObj ['startdate'] = $plugin->getProperty ($newProperties, "startdate");
        $jsonObj ['enddate'] = $plugin->getProperty ($newProperties, "enddate");

        $this->_helper->json->sendJson ($jsonObj);
    }

    public function quickprogAction ()
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $progProperties = Zend_Json::decode ($this->_request->getParam ('programmation'));
        $emissionId     = $this->_request->getParam ('emission');
        $progName       = $this->_request->getParam ('name');
        $channels       = new Progs_Channels ();
        $emissions      = new Progs_Programs ();

        if (!strlen ($emissionId))
        {
            $this->_helper->json->sendJson (array ('status' => 0,
                                                   'msg'    => $translator->translate('Error: no emission to program !')));
        }

        $emission = $emissions->findProgram ($emissionId);
        if (!$emission)
        {
            $this->_helper->json->sendJson (array ('status' => 0,
                                                   'msg'    => $translator->translate('Error: Cannot find the emission in database !')));
        }

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Programmation', 'new'))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ('You cannot create a new programmation.')));
        }

        if (!strlen ($progName))
        {
            $this->_helper->json->sendJson (
                array ('status' => 2,
                        'msg'   => $translator->translate ('Invalid programmation name.')));
        }

        $channelByName  = $channels->findChannelByName ($progName);
        if ($channelByName)
        {
            $this->_helper->json->sendJson (
                array ('status' => 2,
                        'msg'   => $translator->translate ('Programmation with the same name already exists.').
                                    "\n".
                                    $translator->translate ('Please give a new name for this programmation.')));
        }

        // Create a new program
        $events = $this->getQuickProg ($emission, $progProperties);
        $channelId = $channels->insertChannel ($progName, $events);
        if ($channelId !== false)
        {
            $newChannel = $channels->findChannel ($channelId);
            $logger->info ("Inserted program *".$newChannel->getName ()
                            ."* (".$channelId.").");

            $user = Zend_Registry::getInstance ()->get ('App_User');
            if ($user->getAdvancedProgrammation ())
                $nextPage = "/Programmation";
            else
                $nextPage = "/Supervision";

            $this->_helper->json->sendJson (array ('status' => 1,
                                                   'progId' => $newChannel->getId (),
                                                   'progName' => $newChannel->getName (),
                                                   'nextPage' => $nextPage));
        }
        else
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translate->translate ('Unable to create this program.')));
        }
    }

    protected function getQuickProg ($emission, $progProperties)
    {
        $events = array ();

        // Parse programmation properties
        $properties = array ();
        foreach ($progProperties as $prop)
            $properties [$prop [0]] = $prop [1];

        list ($hour, $min) = explode (':', $properties ['evtStart']);
        $dateStart = $hour*3600 + $min*60;
        list ($hour, $min) = explode (':', $properties ['evtEnd']);
        $dateEnd = $hour*3600 + $min*60;

        $powerEnabled = false;

        $commands = new Actions_Commands ();
        $powerPlugin = $commands->getCommand ("Powerctrl");
        if ($powerPlugin && $powerPlugin->getEnable ())
            $powerEnabled = true;

        $thisWeek   = date ("W");
        $thisYear   = date ("Y");
        $thisMonth  = date ("n");
        if ($thisWeek == 1 && $thisMonth == 12)
        {
            // Week can be '01' on the last week of the year
            $thisYear++;
        }
        if ($thisWeek >= 51 && $thisMonth == 1)
        {
            $thisYear--;
        }

        if ($thisWeek < 10 && $thisWeek [0] != "0")
            $thisWeek = "0$thisWeek";

        $weekStart  = strtotime ("${thisYear}W${thisWeek}");
        for ($i = 0 ; $i < 7 ; $i++)
        {
            if (!array_key_exists ("day$i", $properties))
                continue;

            $dday = mktime (0, 0, 0, date ("m", $weekStart),
                                     date ("d", $weekStart) + $i,
                                     date ("Y", $weekStart));
            $delay = date ("Z", $dday);

            // Add the emission
            $event = array ();
            $event ['start']    = date ("U", $dday) + $dateStart;
            $event ['end']      = date ("U", $dday) + $dateEnd;
            $event ['type']     = 'Emission';
            $event ['label']    = $emission->getName ();
            $event ['uid']      = "quickprog@ipreso.com-d$i";
            $event ['period']   = 60*60*24*7;
            $event ['exdate']   = '';

            $events [] = $event;

            if ($powerEnabled)
            {
                // Add wake-up and shutdown
                $event = array ();
                $event ['start']    = date ("U", $dday) + $dateStart;
                $event ['end']      = date ("U", $dday) + $dateStart;
                $event ['type']     = 'Action';
                $event ['label']    = $powerPlugin->getStringCommand ('power_on');
                $event ['uid']      = "quickprogstart@ipreso.com-d$i";
                $event ['period']   = 60*60*24*7;
                $event ['plugin']   = 'Powerctrl';
                $event ['plugid']   = 'power_on';
                $event ['exdate']   = '';
                $events [] = $event;
                $event = array ();
                $event ['start']    = date ("U", $dday) + $dateEnd;
                $event ['end']      = date ("U", $dday) + $dateEnd;
                $event ['type']     = 'Action';
                $event ['label']    = $powerPlugin->getStringCommand ('power_off');
                $event ['plugin']   = 'Powerctrl';
                $event ['plugid']   = 'power_off';
                $event ['uid']      = "quickprogstop@ipreso.com-d$i";
                $event ['period']   = 60*60*24*7;
                $event ['exdate']   = '';
                $events [] = $event;
            }
        }
        return ($events);
    }

    public function commitprogAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $program = $this->_request->getParam ('program');
        $program ['name'] = preg_replace ("/['&!;:\/\[\(\]\)]/", "_", $program ['name']);

        if (!isset ($program ['zones']))
        {
            // No zone
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate('Please create at least one area.')));
        }

        $programs = new Progs_Programs ();
        if (strlen ($program ['id']))
        {
            // Edited or renamed from already existing emission
            // Update the emission (delete it and create a new one)
            $this->editProg ($program);
        }
        else
        {
            // New emission or copy to create
            $this->createNewProg ($program);
        }
    }

    protected function editProg ($program)
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $programs = new Progs_Programs ();

        // Update the edited emission
        if (!$this->_acl || !$this->_acl->isAllowed ('Emission', 'edit', $program ['id']))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ('You cannot edit emission: '.$program ['name'])));
        }

        // First, check the program ID is from a valid and not-deleted emission
        $originalProg = $programs->findProgram ($program ['id']);
        if ($originalProg->isDeleted ())
        {
            // Original emission has been deleted (renamed) in the meantime
            // To avoid having two emissions with the same name, and because it's
            // impossible to solve conflicts generated by parallel edition, we
            // return an error
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ('Emission was edited in the meantime. Please refresh your page to get the new one or just create a copy (not rename) the emission.')));
        }

        $newProgId = $programs->updateProgram ($program);
        if ($newProgId != $program ['id'])
        {
            // Update permissions
            $resources = new Admin_resources ();
            $resources->updateResource ('Emission', $program ['id'], $newProgId);

            // Compare old and new name
            $newProg      = $programs->findProgram ($newProgId);
            if (strcmp ($originalProg->getName (), $newProg->getName ()) != 0)
            {
                // Name have been updated
                // => name shall be updated in all programmations (channels)
                $channels = new Progs_Channels ();
                $channels->updateEmissionName ($originalProg->getName (),
                                               $newProg->getName ());
            }

            $logger->info ("Updated emission *".$program ['name']
                            ."* (".$newProgId.").");
        }
        $this->_helper->json->sendJson (
            array ('status' => 1,
                   'id'     => $newProgId));
    }

    protected function createNewProg ($program)
    {
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $programs = new Progs_Programs ();

        // Check permissions
        if (!$this->_acl || !$this->_acl->isAllowed ('Emission', 'new'))
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate('You cannot create a new emission.')));
        }

        // Check that program's name doesn't already exists
        if ($programs->findProgramByName ($program ['name']) != NULL)
        {
            $this->_helper->json->sendJson (
                    array ('status' => 0,
                           'msg'    => $translator->translate ('Emission\'s name already exists. Please change it.')));
        }

        $programId = $programs->insertProgram ($program);
        if ($programId != false)
        {
            $newProgram = $programs->findProgram ($programId);
            $logger->info ("Created emission *".$newProgram->getName ()
                            ."* (".$programId.").");

            $this->_helper->json->sendJson (
                array ('status' => 1,
                       'id'     => $programId));
        }
        else
        {
            $this->_helper->json->sendJson (
                array ('status' => 0,
                       'msg'    => $translator->translate ('Unable to save this emission.')));
        }
    }
    
    public function gettmpfileAction ()
    {
        $path = $this->getfileAction ();

        // Delete this file
        if (file_exists ($path))
            unlink ($path);
    }

    public function getfileAction ()
    {
        // Disable layout
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $logs = new Players_Logs ();

        $filename = $this->_request->getParam ('file');

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $path = $upload->library."/$filename";
        if (!is_readable ($path))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }
        
        $this->getResponse ()->clearHeaders ();

        header ("Content-Type: application/octet-stream");
        $total = filesize ($path);
        $blocksize  = (1 << 20); // 1M chunks
        $sent       = 0;
        $fileHandle = fopen ("$path", "rb");

        header ("Content-Length: $total");

        $flushCounter = 0;
        set_time_limit (0);
        while ($sent < $total)



        {
            echo fread ($fileHandle, $blocksize);
            $sent += $blocksize;
            $flushCounter++;
            if ($flushCounter > 10)
            {
                ob_flush ();
                $flushCounter = 0;
            }

        }
        fclose ($fileHandle);

        return ($path);
    }
    
}

