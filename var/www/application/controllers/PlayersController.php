<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class PlayersController extends Zend_Controller_Action
{
    private $_box;

    public function preDispatch ()
    {
        // Get the iBox which is talking to us...
        if (strcmp ($_SERVER ['SSL_CLIENT_VERIFY'], 'SUCCESS') != 0)
            $this->_helper->redirector ('denied', 'error');

        // Subject looks like :
        // 'C = FR, ST = Occitanie, O = Napafilia, CN = f66f8b83fa39dd1ec76df0e578cf35f6, emailAddress = admin@ipreso.com'
        $SSL_subjects = explode(',',$_SERVER['SSL_CLIENT_S_DN']);
        while(($SSL_attribute = next ($SSL_subjects)))
        {
            list ($key,$val) = explode('=',$SSL_attribute);
            $SSL_Client [$key] = $val;
        }
        
        if (!isset ($SSL_Client ['CN']))
            $this->_helper->redirector ('denied', 'error');

        $hash = $SSL_Client ['CN'];

        // A Player has requested this URL. Disabling all graphic stuffs :
        // - no layout
        $this->_helper->layout->disableLayout();
        // - only one view to render all replies (because only one parser on the other side !)
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setScriptAction ('output');

        $boxes = new Players_Boxes ();
        $box = $boxes->findiBoxWithHash ($hash);

        if (!$box)
        {
            $logs = new Players_Logs ();
            $logs->saveMsg ($hash, "Box with hash $hash not found.", 'ERROR');
            if (strcmp ($this->getRequest ()->getActionName (), "notknown") != 0 &&
                strcmp ($this->getRequest ()->getActionName (), "subscribe") != 0 &&
                strcmp ($this->getRequest ()->getActionName (), "hostname") != 0)
                $this->_helper->redirector ('notknown');
            $this->_box = NULL;
        }
        else
            $this->_box = $box;
    }

    public function logsAction ()
    {
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');

        $hash = $this->_request->getParam ('hash');
        $logs = $this->_request->getParam ('logs');
        if (!$hash || !$logs)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Parameter are missing or invalid.";
            return;
        }
        if (strcmp ($this->_box->getHash (), $hash) != 0)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "LOGS's data don't match with license's owner";
            return;
        }

        // Decode logs and put it into a file
        if ($logs [strlen ($logs)-1] == "\n" ||
               $logs [strlen ($logs)-1] == "\r")
            $logs = substr ($logs, 0, -1);

        if (!file_exists ($upload->logs."/$hash.logs"))
            system ("touch ".$upload->logs."/$hash.logs");

        file_put_contents ($upload->sandbox."/logs-$hash.gz", $logs);
        system ("gzip -d ".$upload->sandbox."/logs-$hash.gz");
        system ("diff -a ".$upload->logs."/$hash.logs "
                          .$upload->sandbox."/logs-$hash | "
                ."grep -E '^> ' | cut -c3- >> ".$upload->logs."/$hash.logs");

        unlink ($upload->sandbox."/logs-$hash");

        $this->view->hash   = $this->_box->getHash ();
        $this->view->status = "OK";
        $this->view->cmdToSend = array ();
    }

    public function commandsAction ()
    {
        $logs       = new Players_Logs ();
        $confPlug   = Zend_Registry::getInstance()->get('Config_Ini_Plugins');
        $repo       = $confPlug->repository;

        $boxes      = new Players_Boxes ();
        $box        = $boxes->findiBoxWithHash ($this->_box->getHash ());

        $cmds    = new Actions_Commands ();
        $cmdsList= $cmds->getCommands ();

        $packageName = "";
        $packagesToInstall = "";
        $packagesToRemove = "";

        if (!$cmdsList || count ($cmdsList) < 1)
        {
            $this->view->status = "OK";
            return;
        }

        $verBox = preg_replace('/(\d+).(\d+).(\d+)/', '${1}.${2}', $box->getPkgVersion ());

        $this->view->content = "";
        foreach ($cmdsList as $cmd)
        {
            // TODO: Manage old version of boxes

            // Send packages to download or remove
            $packageName = "iplayer-".strtolower ($cmd->getInternalName ());
            $enable = ($cmd->getEnable () ? "1" : "0");
            if ($enable)
            {
                $packagesToInstall .= "$packageName=".$cmd->getVersion ()." ";
            }
            else
                $packagesToRemove .= "$packageName ";
        }
        if (strlen ($packagesToInstall))
            $this->view->content .= "I: $packagesToInstall\n";
        if (strlen ($packagesToRemove))
            $this->view->content .= "U: $packagesToRemove\n";

        $this->view->status = "OK";
    }

    public function pluginsAction ()
    {
        $logs       = new Players_Logs ();
        $confPlug   = Zend_Registry::getInstance()->get('Config_Ini_Plugins');
        $repo       = $confPlug->repository;

        $boxes      = new Players_Boxes ();
        $box        = $boxes->findiBoxWithHash ($this->_box->getHash ());

        $plugins    = new Media_Plugins ();
        $pluginsList= $plugins->getPlugins ();

        $packageName = "";
        $packagesToInstall = "";
        $packagesToRemove = "";

        if (!$pluginsList || count ($pluginsList) < 1)
        {
            $this->view->status = "OK";
            return;
        }

        $verBox = preg_replace('/(\d+).(\d+).(\d+)/', '${1}.${2}', $box->getPkgVersion ());

        $this->view->content = "";
        foreach ($pluginsList as $plugin)
        {
            // TODO: Manage old version of boxes
            // Do not send recent plugins to older versions of boxes
            //$verPlugin = preg_replace('/(\d+).(\d+).(\d+)*/', '${1}.${2}', $plugin->getVersion ());
            /*
            if ($verPlugin > $verBox)
            {
                //$logs->saveMsg ($box->getHash (), 
                //                    "Box $verBox.x deprecated: cannot download ".$plugin->getName()
                //                    ." $verPlugin.x", 'WARNING');
                continue;
            }

            $deb = "iplugin-".strtolower ($plugin->getInternalName ())."_".$plugin->getVersion ()."_i386.deb";
            // Get MD5 of the deb package
            if (!file_exists ("$repo/$deb"))
            {
                $this->view->status = "ERROR";
                $this->view->details = "Composer have a corrupted plugins repository.";
                $logs->saveMsg ($box->getHash (), 
                                "File $repo/$deb is not in repository.", 'ERROR');
                return;
            }

            $md5 = md5_file ("$repo/$deb");
            $enable = ($plugin->getEnable () ? "1" : "0");

            $this->view->content .= strtolower ($plugin->getInternalName ()).":"
                                    .$plugin->getVersion ().":"
                                    ."$md5:"
                                    ."$enable\n";
            */

            // Send packages to download or remove
            $packageName = "iplayer-".strtolower ($plugin->getInternalName ());
            $enable = ($plugin->getEnable () ? "1" : "0");
            if ($enable)
            {
                $packagesToInstall .= "$packageName=".$plugin->getVersion ()." ";
            }
            else
                $packagesToRemove .= "$packageName ";
        }
        if (strlen ($packagesToInstall))
            $this->view->content .= "I: $packagesToInstall\n";
        if (strlen ($packagesToRemove))
            $this->view->content .= "U: $packagesToRemove\n";

        $this->view->status = "OK";
    }

    public function aliveAction ()
    {
        // Insert the new box in the database
        $boxParam = Zend_Json::decode ($this->_request->getParam ('box'));
        $logs = new Players_Logs ();

        if (!$boxParam)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Box parameter is missing or invalid.";
            return;
        }

        $hash           = $boxParam ['hash'];
        $status         = $boxParam ['status'];
        $program        = $boxParam ['program'];
        $manifest       = $boxParam ['manifest'];
        $layout         = $boxParam ['layout'];
        $playlist       = $boxParam ['playlist'];
        $schedule       = $boxParam ['schedule'];
        $wallpaper      = $boxParam ['wp'];

        $headers = apache_request_headers();
        if (array_key_exists ('X-Forwarded-For', $headers))
            $remoteIP = $headers ['X-Forwarded-For'];
        else
            $remoteIP = $_SERVER ['REMOTE_ADDR'];
        list ($dummy, $syncVer) = explode ('/', $_SERVER['HTTP_USER_AGENT']);

        if (strcmp ($this->_box->getHash (), $hash) != 0)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "ALIVE's data don't match with license's owner";
            return;
        }

        $boxes      = new Players_Boxes ();
        $box        = $boxes->findiBoxWithHash ($hash);

        // Update last IP and last date
        $boxes->updateLastConnection ($hash, $remoteIP, $syncVer);

        // Update current status
        $boxes->setBoxStatus ($hash, $status);

        // Update played schedule
        $boxes->setBoxSchedule ($hash, $schedule);

        // Update wallpaper of the box
        $boxes->setBoxWallpaper ($hash, $wallpaper);

        $this->view->cmdToSend = array ();
        $programs   = new Progs_Programs ();
        $channels   = new Progs_Channels ();
        $schedules  = new Progs_Schedules ();

        $dbChannel      = $channels->findChannel ($box->getProgram ());
        $liveSchedule   = $schedules->findSchedule ($schedule);
        $liveProg       = $programs->findProgramByFiles ($program, $manifest,
                                                                   $layout,
                                                                   $playlist);

        if (!$dbChannel)
        {
            $this->view->status  = "ERROR";
            $this->view->details = "Program in Database is missing or invalid.";
            $logs->saveMsg ($hash, "Invalid Program: id#".$box->getProgram (), 'INFO');
            return;
        }

        if (!$liveSchedule)
        {
            $this->view->hash   = $hash;
            $this->view->status = "OK";
            $this->view->cmdToSend [] = array ('cmd' => 'CALENDAR');
            $logs->saveMsg ($hash, "Unknown calendar. Update schedule asked.", 'INFO');
            return;
        }

        if (!$liveProg)
        {
            if (strlen ($program))
            {
                $logs->saveMsg ($hash,
                                "Unknown emission: '".$program."' with meta-files.",
                                'INFO');
            }
            else
            {
                $logs->saveMsg ($hash,
                                "No current emission. (switching ?).",
                                'INFO');
            }
        }
        else
        {
            $programsObj = new Players_programs ();
            if (!$programsObj->setCurrentProgram ($hash, $liveProg->getId ()))
            {
                $this->view->status  = "ERROR";
                $this->view->details = "Cannot update the program for box $hash";
                return;
            }
        }
        
        $this->view->hash   = $hash;
        $this->view->status = "OK";

        // Update configuration
        $boxStatus = $boxes->getBoxSyncStatus ($hash);
        if (strcmp ($boxStatus, "DESYNCHRONIZED") == 0 ||
            strcmp ($boxStatus, "ERROR") == 0)
        {
            $this->view->cmdToSend [] = array ('cmd' => 'UPDATE');
            $logs->saveMsg ($hash, "UPDATE asked.", 'DEBUG');
        }

        // Update Wallpaper
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $path = $upload->library."/__LOGO__.png";
        if (is_readable ($path) &&
            strcmp (md5_file ($path), $wallpaper) != 0 &&
            strcmp ($box->getPkgVersion (), "1.0.9") >= 0)
        {
            $this->view->cmdToSend [] = array ('cmd' => 'WALLPAPER');
            $logs->saveMsg ($hash, "UPDATE WALLPAPER asked.", 'DEBUG');
            $logs->saveMsg ($hash, "Local ($path): ".md5_file ($path), 'DEBUG');
            $logs->saveMsg ($hash, "Remote: $wallpaper", 'DEBUG');
        }

        // Update the Schedule
        if (strcmp ($schedule, $dbChannel->getSchedule ()) != 0)
        {
            $this->view->cmdToSend [] = array ('cmd' => 'CALENDAR');
            $logs->saveMsg ($hash, "Update schedule to program ".$dbChannel->getName (), 'DEBUG');
        }

        // Update the current program if meta files are not the same
        $dbProg = $programs->findProgramByName ($program);
        if ($dbProg && strcmp ($program, $dbProg->getName ()) == 0)
        {
            if (strcmp ($manifest, $dbProg->getManifest ()) != 0 ||
                strcmp ($playlist, $dbProg->getPlaylist ()) != 0 ||
                strcmp ($layout, $dbProg->getLayout ()) != 0)
            {
                $this->view->cmdToSend [] = array ('cmd' => 'CHGPROGRAM',
                                                   'params' => $dbProg->getName ());
                $logs->saveMsg ($hash, "Update from old '" . $program 
                                            ."' to new '" . 
                                        $dbProg->getName ()."' sent.", 'DEBUG');
            }
        }

        // Get message from Commands' plugins
        $tableCommands = new Actions_Commands ();
        $commands = $tableCommands->getEnabledCommands ();
        foreach ($commands as $command)
        {
            $cmdToSend = $command->getPlayerCommand ($hash);
            if ($cmdToSend)
                $this->view->cmdToSend [] = $cmdToSend;
        }
    }

    public function notknownAction ()
    {
        $this->view->status = "FAILED";
        $this->view->details = "Unknown certificate";
        $this->view->cmdToSend = array (
                                    array ('cmd' => 'SUBSCRIBE')
                                       );
    }

    public function confirmupdateAction ()
    {
        $boxes = new Players_Boxes ();
        $boxParam = Zend_Json::decode ($this->_request->getParam ('box'));
        if (!$boxParam)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Box parameter is missing or invalid.";
            $boxes->setBoxSyncStatus ($this->_box->getHash (), "ERROR");
            return;
        }

        if (strcmp ($boxParam ['ip'], $this->_box->getIp ()) != 0 ||
            strcmp ($boxParam ['gw'], $this->_box->getGw ()) != 0 ||
            strcmp ($boxParam ['dns'], $this->_box->getDns ()) != 0 ||
            strcmp ($boxParam ['pkgsrc'], $this->_box->getPkgSource ()) != 0 ||
            strcmp ($boxParam ['label'], $this->_box->getLabel ()) != 0 ||
            strcmp ($boxParam ['coordinates'], $this->_box->getCoordinates ()) != 0 ||
            strcmp ($boxParam ['composer'], $this->_box->getComposerHost ()) != 0 ||
            strcmp ($boxParam ['composer_ip'], $this->_box->getComposerIp ()) != 0)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Configuration is not updated successfully";
            $boxes->setBoxSyncStatus ($this->_box->getHash (), "ERROR");
            return;
        }
    
        $this->view->status = "OK";
        $boxes->setBoxSyncStatus ($this->_box->getHash (), "OK");
        $logs = new Players_Logs ();
        $logs->saveMsg ($this->_box->getHash (), "UPDATE done.", 'DEBUG');
    }

    public function getfileAction ()
    {
        $request = Zend_Json::decode ($this->_request->getParam ('request'));

        if (strcmp ($request ['file'], "schedule") == 0)
        {
            $channels   = new Progs_Channels ();
            $schedules  = new Progs_Schedules ();

            $channel    = $channels->findChannel ($this->_box->getProgram ());
            $schedule   = $schedules->findSchedule ($channel->getSchedule ());
            $clearContent = $schedule->getContent ();

            // Don't need program name to send the schedule file
            //$clearContent = file_get_contents ('http://www.google.com/calendar/ical/elkmh8m92t38cj2nfcftnf5hlo%40group.calendar.google.com/private-73b9281998c52fe97e86d36cc57dab46/basic.ics');
            //$clearContent = file_get_contents ("/tmp/calendar.ics");

            if (empty ($clearContent))
                $clearContent = "# Empty file\n";

            // Cypher the content with the Box's key
            $cryptedContent = $this->_getCypheredFile ($this->_box->getHash (),
                                                       $clearContent);
            if (!$cryptedContent)
            {
                $this->view->status     = "ERROR";
                $this->view->details    = "Error while crypting file.";
                return;
            }

            $this->view->status = "OK";
            $this->view->content = $cryptedContent;
            return;
        }
        
        // Get the program
        $programs = new Progs_Programs ();
        $program = $programs->findProgramByName ($request ['program']);
        if (!$program)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Program '".$request ['program']
                                        ."' does not exist.";
            return;
        }

        $downloads = new Players_Downloads ();
        $downloads->setCurrentDownload ($this->_box->getHash (), 
                                        $program->getId ());

        // Get the asked content
        switch ($request ['file'])
        {
            case 'manifest':
                $clearContent = $this->_getManifest ($program->getManifest ());
                break;
            case 'layout':
                $clearContent = $this->_getLayout ($program->getLayout ());
                break;
            case 'playlist':
                $clearContent = $this->_getPlaylist ($program->getPlaylist ());
                break;
            default:
                $this->view->status  = "ERROR";
                $this->view->details = "Unknown file '".$request ['file']."'";
                return;
        }

        if (empty ($clearContent))
            $clearContent = "# Empty file\n";

        // Cypher the content with the Box's key
        $cryptedContent = $this->_getCypheredFile ($this->_box->getHash (),
                                                   $clearContent);
        if (!$cryptedContent)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Error while crypting file.";
            return;
        }

        $this->view->status = "OK";
        $this->view->content = $cryptedContent;
    }

    protected function _getManifest ($hash)
    {
        $manifests = new Progs_Manifests ();
        $manifest = $manifests->findManifest ($hash);
        if (!$manifest)
            return (NULL);

        return ($manifest->getContent ());
    }

    protected function _getLayout ($hash)
    {
        $layouts = new Progs_Layouts ();
        $layout = $layouts->findLayout ($hash);
        if (!$layout)
            return (NULL);

        return ($layout->getContent ());
    }

    protected function _getPlaylist ($hash)
    {
        $playlists = new Progs_Playlists ();
        $playlist = $playlists->findPlaylist ($hash);
        if (!$playlist)
            return (NULL);

        return ($playlist->getContent ());
    }

    protected function _getCypheredFile ($box, $content)
    {
        $ini = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        $logs = new Players_Logs ();

        // Get Box License...
        $licenses   = new Players_Licenses ();
        $license    = $licenses->getValidLicense ($box);
        if (!$license)
        {
            $logs->saveMsg ($box, "No valid license.", 'DEBUG');
            return (FALSE);
        }

        // Check if license is valid and get file path
        $certificate = $license->getCertificate ();
        if (!$certificate)
        {
            $logs->saveMsg ($box, "No valid certificate.", 'DEBUG');
            return (FALSE);
        }

        // Escape single quotes
        $content = str_replace ("'", "'\''", $content);

        // Cypher the content
        $cmd = "echo -n '$content' | "
                ."/usr/bin/openssl smime -encrypt -binary "
                  ."-out '".$ini->tmp_dir."/$box.crypt' "
                  ."'".$license->getPath()."'";
        $result = $this->_execute ($cmd);
        if (!file_exists ($ini->tmp_dir."/$box.crypt"))
        {
            $logs->saveMsg ($box, "Cannot encrypt file.", 'DEBUG');
            return (FALSE);
        }

        // Sign the content
        $cmd = "/usr/bin/openssl smime -sign -inkey '".$ini->key."' "
                ."-signer '".$ini->cert."' "
                ."-in '".$ini->tmp_dir."/$box.crypt' -out '".$ini->tmp_dir."/$box.signed'";
        $result = $this->_execute ($cmd);
        unlink ($ini->tmp_dir."/$box.crypt");
        if (!file_exists ($ini->tmp_dir."/$box.signed"))
        {
            $logs->saveMsg ($box, "Cannot sign file.", 'DEBUG');
            return (FALSE);
        }

        // Return the new content
        $newContent = file_get_contents ($ini->tmp_dir."/$box.signed");
        unlink ($ini->tmp_dir."/$box.signed");
        return ($newContent);
    }

    public function updateAction ()
    {
        $this->view->status = "OK";
        $this->view->cmdToSend = array (
                                    array ('cmd'    => 'CONFIRM',
                                           'params' => 'UPDATE')
                                       );
        $this->view->content = 
            "composer_host = ".     $this->_box->getComposerHost ()."\n".
            "composer_ip = ".       $this->_box->getComposerIp ()."\n".
            "box_ip = ".            $this->_box->getIp ()."\n".
            "box_gw = ".            $this->_box->getGw ()."\n".
            "box_dns = ".           $this->_box->getDns ()."\n".
            "box_name = ".          $this->_box->getLabel ()."\n".
            "box_coordinates = ".   $this->_box->getCoordinates ()."\n".
            "pkg_src = ".           $this->_box->getPkgSource ()."\n";

        $boxes = new Players_Boxes ();
        $boxes->setBoxSyncStatus ($this->_box->getHash (), "SYNCHRONIZING");
        $logs = new Players_Logs ();
        $logs->saveMsg ($this->_box->getHash (), "UPDATE sent.", 'DEBUG');
    }

    public function pluginmsgAction ()
    {
        $message = $this->_request->getParam ('msg');
        $hash = $this->_box->getHash ();

        list ($cmd, $msg) = explode ('/', $message);

        $tableCommands = new Actions_Commands ();
        $pluginObj = $tableCommands->getCommand ($cmd);
        if (!$pluginObj)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Unknow plugin '$cmd'";
            return;
        }

        $result = $pluginObj->msgFromPlayer ($hash, $msg);
        if ($result === true)
        {
            $this->view->status     = "OK";
        }
        else
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "'$result'";
        }
    }

    public function updlicenseAction ()
    {
        $logs = new Players_Logs ();
        $hash = $this->_box->getHash ();

        // Try to download the license and revoke the old ones
        $logs->saveMsg ($hash, "Downloading license", 'INFO');
        $license = $this->_downloadLicense ($hash);
        if ($license)
        {
            $this->view->status = "OK";
            $this->view->cmdToSend = array ();
        }
        else
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Cannot download up-to-date license.";
            $logs->saveMsg ($hash, "Unable to update License's iBox in DB", 'ERROR');
        }
    }

    public function unsubscribeAction ()
    {
        $logs = new Players_Logs ();
        $hash = $this->_box->getHash ();

        // Check the composer know it
        $licenses = new Players_Licenses ();
        $license = $licenses->getValidLicense ($hash);
        if (!$license)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "'$hash' has no valid license on Composer";
            $logs->saveMsg ($hash, "Cannot unsubscribe box '$hash': no license", 'INFO');
            return;
        }

        // Remove the box from the database
        $boxes = new Players_Boxes ();
        if (!$boxes->delBox ($hash))
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Error while deleting Box in DataBase.";
            $logs->saveMsg ($hash, "Unable to remove iBox from DB", 'ERROR');
            return ;
        }

        $this->view->status = "OK";
        $this->view->cmdToSend = array ();
        $logs->saveMsg ($hash, "Unsubscribed", 'INFO');
    }

    public function hostnameAction ()
    {
        $licenses   = Zend_Registry::getInstance()->get('Config_Ini_Licenses');
        $commonname = $this->_execute ("/usr/bin/openssl x509 ".
                "-noout -subject -in ".$licenses->cert." | ".
                "cut -d'/' -f6 | cut -d'=' -f2");

        // Reply to the box with our certificate hostname
        $this->view->status     = "OK";
        $this->view->details    = $commonname;
    }

    public function subscribeAction ()
    {
        // If preDispatch function is allowing the process to comes here,
        // that means the certicate :
        // - is valid (correct CA signature, not expired)
        // - has correct CN syntax ('Player-[ABCDEFGH]')

        $logs = new Players_Logs ();

        list ($dummy, $syncVer) = explode ('/', $_SERVER['HTTP_USER_AGENT']);
        if (strcmp ($dummy, 'iSynchro') != 0)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Bad HTTP User Agent.";
            $logs->saveMsg ($hash, "Tried to subscribe with '$dummy' agent", 'ERROR');
            return;
        }

        // Get box information
        $boxParam = Zend_Json::decode ($this->_request->getParam ('box'));
        if (!$boxParam)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Box parameter is missing or invalid.";
            $logs->saveMsg ($hash, "Invalid subscribe request", 'ERROR');
            return;
        }

        $hash           = $boxParam ['hash'];
        $mac            = $boxParam ['mac'];
        $ip             = $boxParam ['ip'];
        $gw             = $boxParam ['gw'];
        $dns            = $boxParam ['dns'];
        $pkgsrc         = $boxParam ['pkgsrc'];
        $version        = $boxParam ['pkgver'];
        $label          = $boxParam ['label'];
        $mode           = $boxParam ['mode'];
        $composerHost   = $boxParam ['composer'];
        $composerIp     = $boxParam ['composer_ip'];
        $coordinates    = $boxParam ['coordinates'];
        $wallpaper      = $boxParam ['wp'];

        $headers = apache_request_headers();
        if (array_key_exists ('X-Forwarded-For', $headers))
            $remoteIP = $headers ['X-Forwarded-For'];
        else
            $remoteIP = $_SERVER ['REMOTE_ADDR'];

        // Check the composer can manage it (license is known)
        $licenses = new Players_Licenses ();
        $license = $licenses->getValidLicense ($hash);
        if (!$license)
        {
            // Try to download it
            $logs->saveMsg ($hash, "Downloading license", 'INFO');
            $license = $this->_downloadLicense ($hash);
        }
        if (!$license)
        {
            $this->view->status     = "FAILED";
            $this->view->details     = "Composer cannot manage Box '$hash'";
            $logs->saveMsg ($hash, "Unable to get license", 'ERROR');
            return;
        }

        // Insert the new box in the database
        $boxes = new Players_Boxes ();
        if (!$boxes->addBox ($hash, $mac, $ip, $gw, $dns,
                             $pkgsrc, $label, $mode, $coordinates,
                             $composerHost, $composerIp, 
                             $remoteIP, $version, $syncVer,
                             $wallpaper))
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Error while inserting Box in DataBase.";
            $logs->saveMsg ($hash, "Unable to save iBox in DB", 'ERROR');
            //$logs->saveMsg ($hash, "$mac, $ip, $gw, $dns, $pkgsrc, $label, $mode, $coordinates, $composerHost, $composerIp, $remoteIP, $version, $syncVer, $wallpaper", 'DEBUG');
            return ;
        }

        // Reply to the box we know him, he can send us ALIVE commands
        $this->view->status = "OK";
        $this->view->cmdToSend = array (
                                    array ('cmd' => 'ALIVE')
                                       );
        $logs->saveMsg ($hash, "Subscribed", 'INFO');
    }

    protected function _downloadLicense ($hash)
    {
        $licParams = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        $cmd = "/usr/bin/wget -q -t1 -T3 "
                ."--output-document=".$licParams->tmp_dir."/$hash.crt "
                ."--certificate=".$licParams->cert." "
                ."--private-key=".$licParams->key." "
                ."https://".$licParams->server."/".$licParams->id."/$hash.crt 2>&1";

        $result = $this->_execute ($cmd);
        if ($result === NULL)
            return (NULL);

        $licenses = new Players_Licenses ();
        $newLicense = $licenses->insertNewLicense ($hash, $licParams->tmp_dir."/$hash.crt");

        if (!file_exists ($licParams->tmp_dir."/$hash.crt"))
            unlink ($licParams->tmp_dir."/$hash.crt");

        return ($newLicense);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }

    public function enddownloadAction ()
    {
        $logs = new Players_Logs ();
        $boxParam = Zend_Json::decode ($this->_request->getParam ('box'));
        if (!$boxParam)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Box parameter is missing or invalid.";
            return;
        }

        // Get the program
        $programs = new Progs_Programs ();
        $program = $programs->findProgramByName ($boxParam ['program']);
        if (!$program)
        {
            $this->view->status     = "ERROR";
            $this->view->details    = "Program '".$boxParam ['program']
                                        ."' does not exist.";
            return;
        }

        $downloads = new Players_Downloads ();
        $downloads->setDownloadFinished ($this->_box->getHash (),
                                         $program->getId (),
                                         $boxParam ['status']);

        $this->view->status = "OK";
        $logs->saveMsg ($this->_box->getHash (), 
                        "Download of '".$program->getName ()."' finished: ".$boxParam ['status'], 
                        'DEBUG');
    }

    public function downloadmediaAction ()
    {
        // Disable layout
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $logs = new Players_Logs ();

        $filename = $this->_request->getParam ('file');
        $filehash = $this->_request->getParam ('hash');

        if (empty ($filename) || empty ($filehash))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $path = $upload->library."/$filename";
        if (!is_readable ($path))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }
        
        if (strcmp (md5_file ($path), $filehash) != 0)
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }

        $logs->saveMsg ($this->_box->getHash (), 
                        "Downloading file $filename", 'INFO');

        $this->getResponse ()->clearHeaders ();

        header ("Content-Type: application/octet-stream");

        $total      = filesize ($path);
        $blocksize  = (1 << 20); // 1M chunks
        $sent       = 0;
        $fileHandle = fopen ($path, "rb");

        header ("Content-Length: $total");

        $flushCounter = 0;
        set_time_limit (0);
        while ($sent < $total)
        {
            echo fread ($fileHandle, $blocksize);
            $sent += $blocksize;
            $flushCounter++;
            if ($flushCounter > 10)
            {
                ob_flush ();
                $flushCounter = 0;
            }
        }
        fclose ($fileHandle);
    }

    public function downloadwpAction ()
    {
        // Disable layout
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $logs = new Players_Logs ();

        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $path = $upload->library."/__LOGO__.png";
        if (!is_readable ($path))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }
        
        $logs->saveMsg ($this->_box->getHash (), 
                        "Downloading file $path", 'INFO');

        $this->getResponse ()->clearHeaders ();

        header ("Content-Type: application/octet-stream");

        $total      = filesize ($path);
        $blocksize  = (1 << 20); // 1M chunks
        $sent       = 0;
        $fileHandle = fopen ($path, "rb");

        header ("Content-Length: $total");

        $flushCounter = 0;
        set_time_limit (0);
        while ($sent < $total)
        {
            echo fread ($fileHandle, $blocksize);
            $sent += $blocksize;
            $flushCounter++;
            if ($flushCounter > 10)
            {
                ob_flush ();
                $flushCounter = 0;
            }
        }
        fclose ($fileHandle);
    }

    public function downloadpkgAction ()
    {
        // Disable layout
        $this->_helper->viewRenderer->setNoRender ();
        $this->_helper->layout->disableLayout ();
        $logs = new Players_Logs ();

        $filename = $this->_request->getParam ('file');
        $filehash = $this->_request->getParam ('hash');

        if (empty ($filename) || empty ($filehash))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }

        $plugins = Zend_Registry::getInstance()->get('Config_Ini_Plugins');
        $path = $plugins->repository."/$filename";
        if (!is_readable ($path))
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }
        
        if (strcmp (md5_file ($path), $filehash) != 0)
        {
            header ("HTTP/1.0 404 Not Found");
            header ("Status: 404 Not Found");
            return;
        }

        $logs->saveMsg ($this->_box->getHash (), 
                        "Downloading package $filename", 'INFO');

        $this->getResponse ()->clearHeaders ();

        header ("Content-Type: application/octet-stream");

        $total      = filesize ($path);
        $blocksize  = (1 << 20); // 1M chunks
        $sent       = 0;
        $fileHandle = fopen ($path, "rb");

        header ("Content-Length: $total");

        $flushCounter = 0;
        set_time_limit (0);
        while ($sent < $total)
        {
            echo fread ($fileHandle, $blocksize);
            $sent += $blocksize;
            $flushCounter++;
            if ($flushCounter > 10)
            {
                ob_flush ();
                $flushCounter = 0;
            }
        }
        fclose ($fileHandle);
    }
}
