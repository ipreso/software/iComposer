<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class SupervisionController extends Zend_Controller_Action
{
    private $_acl;
    private $_user;

    public function preDispatch ()
    {
        if (!Zend_Auth::getInstance ()->hasIdentity ())
            $this->_helper->redirector ('index', 'login');

        $user = Zend_Registry::getInstance ()->get ('App_User');
        if (!$user)
        {
            Zend_Auth::getInstance()->clearIdentity();
            $this->_helper->redirector ('index', 'login');
        }

        $this->view->variables = "";
        $this->_acl = Zend_Registry::getInstance ()->get ('Group_permissions');
        $this->view->permissions = $this->_acl;
    }

    public function init ()
    {
        $contextSwitch = $this->_helper->getHelper ('contextSwitch');

        $contextSwitch->addActionContext ('listgroups', 'json')->initContext ();
        $contextSwitch->addActionContext ('updgroups', 'json')->initContext ();
        $contextSwitch->addActionContext ('editgroup', 'json')->initContext ();
        $contextSwitch->addActionContext ('delgroup', 'json')->initContext ();
        $contextSwitch->addActionContext ('addgroup', 'json')->initContext ();
        $contextSwitch->addActionContext ('getgroup', 'json')->initContext ();
        $contextSwitch->addActionContext ('delbox', 'json')->initContext ();
        $contextSwitch->addActionContext ('getbox', 'json')->initContext ();
        $contextSwitch->addActionContext ('editbox', 'json')->initContext ();
        $contextSwitch->addActionContext ('doaction', 'json')->initContext ();
    }

    protected function _sendBoxesList ()
    {
        $result     = array ();
        $pGroups    = new Players_Groups ();
        $pBoxes     = new Players_Boxes ();
        $dbChannels = new Progs_Channels ();

        $allGroups = $pGroups->getGroupsArray ();
        foreach ($allGroups as $group)
        {
            if (!$this->_acl ||
                !$this->_acl->isAllowed ('Parc', 'view', $group ['id']))
                continue;

            $group ['deletable']    = $this->_acl->isAllowed ('Parc', 'delete', $group ['id']);
            $group ['associable']   = $this->_acl->isAllowed ('Parc', 'boxAssociation', $group ['id']);

            $boxes = $pBoxes->getBoxesArrayByGroup ($group ['id']);

            $groupStatus = array ();

            // We only need some information
            $boxesList = array ();
            foreach ($boxes as $box)
            {
                // Color is choosen according to box status, box sync,
                // and program modification
                $boxStatus      = $box ['box_status'];
                $boxSync        = $box ['box_sync'];

                $channel = $dbChannels->findChannel ($box ['box_program']);
                if (!$channel)
                    $boxSync = "ERROR";
                else if (strcmp ($box ['box_schedule'], $channel->getSchedule ()) == 0)
                    $boxSchedule = "OK";
                else
                    $boxSchedule = "SYNC";

                if (strcmp ($boxStatus, "OK") != 0)
                {
                    switch ($boxStatus)
                    {
                        case "OFF":
                            $statusColor = "OFF";
                            break;
                        case "REBOOT":
                            $statusColor = "REBOOT";
                            break;
                        case "ERROR":
                        case "TIMEOUT":
                        default:
                            $statusColor = "ERROR";
                    }
                }
                elseif (strcmp ($boxSync, "ERROR") == 0)
                    $statusColor = "ERROR";
                elseif (strcmp ($boxSync, "OK") == 0 &&
                        strcmp ($boxStatus, "OK") == 0 &&
                        strcmp ($boxSchedule, "OK") == 0)
                    $statusColor = "OK";
                else
                    $statusColor = "SYNC";
/*
                elseif (strcmp ($boxSync, "SYNCHRONIZING") == 0)
                    $statusColor = "SYNC";
                elseif (strcmp ($boxPlaylist, "DOWNLOAD") == 0)
                    $statusColor = "SYNC";
                else
                    $statusColor = "DESYNC";
*/

                $boxesList [] = 
                    array ('label'      => strlen ($box ['box_label']) ? 
                                            $box ['box_label'] : $box ['box_hash'],
                           'hash'       => $box ['box_hash'],
                           'status'     => $statusColor,
                           'viewable'   => $this->_acl->isAllowed ('Player', 'view', $box ['box_hash']),
                           'deletable'  => $this->_acl->isAllowed ('Player', 'delete', $box ['box_hash']));

                if (!isset ($groupStatus [$statusColor]))
                    $groupStatus [$statusColor] = 1;
            }

            if (isset ($groupStatus ['ERROR']))
                $group ['status'] = 'ERROR';
            elseif (isset ($groupStatus ['DESYNC']) ||
                    isset ($groupStatus ['SYNC']))
                $group ['status'] = 'DESYNC';
            elseif (isset ($groupStatus ['OK']) ||
                    isset ($groupStatus ['REBOOT']))
                $group ['status'] = 'OK';
            elseif (isset ($groupStatus ['OFF']))
                $group ['status'] = 'OFF';
            else
                $group ['status'] = 'OFF';

            $boxesInGroup = array ('group'      => $group,
                                   'children'   => $boxesList);

            $result [] = $boxesInGroup;
        }

        $this->_helper->json->sendJson ($result);
    }

    public function indexAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $this->view->variables .= "var strViewProgram = '".$translator->translate('View the program')."';\n";
        $this->view->variables .= "var strEditProgram = '".$translator->translate('Edit the program')."';\n";

        $channelsList = array ();

        $pChannels = new Progs_Channels ();
        $channels = $pChannels->getChannelsArray ();
        foreach ($channels as $channel)
        {
            // Just to create resource if missing...
            if ($this->_acl)
                $this->_acl->isAllowed ('Programmation', 'edit', $channel ['id']);

            // List only permitted programs
            if ($this->_acl &&
                $this->_acl->isAllowed ('Programmation', 'view', $channel ['id']))
                $channelsList [] = array (
                                    'id'    => $channel ['id'],
                                    'name'  => $channel ['label']);
        }

        $this->view->channelsList = $channelsList;
        
        $this->_user = Zend_Registry::getInstance ()->get ('App_User');
        if($this->_user->getDisplayAgreement () != 1 && $this->_user->getDisplayAgreementOnConnection () != 1)
        {
          echo "<SCRIPT LANGUAGE='javascript'>createPopupLegalAgreement()</SCRIPT>";
        }
    }
    
    public function getFormAgreement ()
    {
        return (new Form_Login ());
    }
    
    public function agreementAction ()
    { 
       $this->_helper->layout->disableLayout ();
       $this->_user = Zend_Registry::getInstance ()->get ('App_User');
       $users = new Admin_Users ();
       $return = $users->modifyDisplayAgreementOnConnection ($this->_user->getId (),1);
       $this->_user = $users->findUserById ($this->_user->getId ());
       $this->view->user = $this->_user;
    }
    
    public function displayagreementAction ()
    {
      $this->_helper->layout->disableLayout ();
      $userDisplayAgreement = $this->_request->getParam ('userDisplayAgreement');
      $this->_user = Zend_Registry::getInstance ()->get ('App_User');
      $users = new Admin_Users ();
      $valueCheckBox;
      
      if (!empty ($userDisplayAgreement))
      {
        $valueCheckBox = 1;
        $return = $users->modifyDisplayAgreement ($this->_user->getId (),$valueCheckBox);
        $logger = Zend_Registry::getInstance()->get('Zend_Log');
        $logger->setEventItem ('user', $this->_user->getId ());
        $logger->info("No longer displays legal notices.");
      }
      
      $this->_user = $users->findUserById ($this->_user->getId ());
      $this->view->user = $this->_user;
      echo "<script type='text/javascript'>self.close()</script>";
    }

    private function _setGroupProgram ($groupId, $progId)
    {
        $boxes = new Players_Boxes ();
        $groups = new Players_Groups ();
        $group = $groups->findGroupById ($groupId);
        if (!$group)
            return;

        $programs = new Progs_Programs ();
        $program = $programs->findProgram ($progId);
        if (!$program)
            return;

        $groups->setGroupProgram ($groupId, $progId);
        if ($boxes->setGroupProgram ($groupId, $progId))
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info ("Changed program of Group *".$group->getName ().
                            "* to *".$program->getName ()."*.");
        }
    }

    public function listgroupsAction ()
    {
        $this->_sendBoxesList ();
    }

    public function updgroupsAction ()
    {
        $groupsList = Zend_Json::decode (
                            $this->_request->getParam ('hosts'));

        if ($groupsList)
        {
            $boxes = new Players_Boxes ();
            $groups = new Players_Groups ();

            foreach ($groupsList as $group)
            {
                foreach ($group ['hosts'] as $host)
                {
                    if ($boxes->setHostGroup ($host, $group ['id']))
                    {
                        $updatedBox = $boxes->findiBoxWithHash ($host);
                        $newGroup = $groups->findGroupById ($group ['id']);
                        $logger = Zend_Registry::getInstance()->get('Zend_Log');
                        $logger->info ("Changed *"
                                        .$updatedBox->getLabel()
                                        ."* to group *"
                                        .$newGroup->getName ()."*.");

                        $progId = $groups->getGroupProgramId ($group ['id']);
                        $boxes->setBoxProgram ($host, $progId);
                    }
                }
            }
        }
        $this->_sendBoxesList ();
    }

    public function getgroupAction ()
    {
        $user = Zend_Registry::getInstance ()->get ('App_User');
        $groupParam = $this->_request->getParam ('id');
        if (is_numeric ($groupParam))
        {
            $groupId = intval ($groupParam);

            if (!$this->_acl ||
                !$this->_acl->isAllowed ('Parc', 'view', $groupId))
                $this->_helper->json->sendJson (array ('id'             => '',
                                                       'name'           => '',
                                                       'description'    => '',
                                                       'program'        => '',
                                                       'programId'      => '',
                                                       'edit'           => false,
                                                       'viewProg'       => false,
                                                       'editProg'       => false,
                                                       'cmds'           => array ()));

            $groups     = new Players_Groups ();
            $progs      = new Progs_Channels ();
            $commands   = new Actions_Commands ();

            $group = $groups->findGroupById ($groupId);

            $prog = $progs->findChannel ($group->getProgram ());
            if ($prog)
            {
                $currentProgId  = $group->getProgram ();
                if ($currentProgId >= 0 && $this->_acl->isAllowed ('Programmation', 'view', $currentProgId))
                {
                    $progView = true;
                    $progEdit = $this->_acl->isAllowed ('Programmation', 'edit', $currentProgId);
                    $currentProg = $prog->getName ();

                    if (!$user->getAdvancedProgrammation ())
                    {
                        // Getting schedule, gettings events, getting first emission
                        $schedules  = new Progs_Schedules ();
                        if (($schedule  = $schedules->findSchedule ($prog->getSchedule ())) &&
                            ($eventsObj = new Progs_Events ($schedule->getContent ())))
                        {
                            $eventsList = $eventsObj->getEvents ();
                            $found = false;
                            $event = NULL;
                            for ($i = 0 ; !$found && $i < count ($eventsList) ; $i++)
                            {
                                $event = $eventsList [$i];
                                if (strcmp ($event->getEvType (), "Emission") != 0)
                                    continue;
                                else
                                    $found = true;
                            }

                            if ($found && $event)
                            {
                                // Getting ID, Label and permissions for
                                // this emission
                                $emissions = new Progs_Programs ();
                                $currentEmi = $event->getLabel ();
                                $currentEmiObj = $emissions->findProgramByName ($currentEmi);
                                $currentEmiId = $currentEmiObj->getId ();

                                $emiView = $this->_acl->isAllowed ('Emission', 'view', $currentEmiId);
                                $emiEdit = $this->_acl->isAllowed ('Emission', 'edit', $currentEmiId);
                            }
                            else
                            {
                                $emiView = false;
                                $emiEdit = false;
                                $currentEmiId   = "";
                                $currentEmi     = "unknown";
                            }
                        }
                    }
                }
                else
                {
                    $currentProg    = "unknown";
                    $currentProgId  = "";
                    $progView       = false;
                    $progEdit       = false;
                }
            }
            else
            {
                $currentProg    = "unknown";
                $currentProgId  = "";
                $progView       = false;
                $progEdit       = false;
                $emiView        = false;
                $emiEdit        = false;
                $currentEmiId   = "";
                $currentEmi     = "unknown";
            }

            $jsonGroup ['id']           = $group->getId ();
            $jsonGroup ['name']         = $group->getName ();
            $jsonGroup ['description']  = $group->getDescription ();
            if ($this->_acl->isAllowed ('Parc', 'pluginsCommands', $groupId))
                $jsonGroup ['cmds']     = $commands->getPluginsProperties ('gr-'.$group->getId ());
            else
                $jsonGroup ['cmds']     = array ();

            if ($user->getAdvancedProgrammation ())
            {
                $jsonGroup ['program']  = $currentProg;
                $jsonGroup ['programId']= $currentProgId;
                $jsonGroup ['viewProg'] = $progView;
                $jsonGroup ['editProg'] = $progEdit;
            }
            else
            {
                $jsonGroup ['program']  = $currentProg;
                $jsonGroup ['programId']= $currentProgId;
                $jsonGroup ['emi']      = $currentEmi;
                $jsonGroup ['emiId']    = $currentEmiId;
                $jsonGroup ['viewEmi']  = $emiView;
                $jsonGroup ['editEmi']  = $emiEdit;
            }

            $jsonGroup ['chgProg']      = $this->_acl->isAllowed ('Parc',
                                                                  'changeProgram', 
                                                                  $groupId);
            $jsonGroup ['edit']         = $this->_acl->isAllowed ('Parc',
                                                                  'edit', 
                                                                  $groupId);

            $this->_helper->json->sendJson ($jsonGroup);
        }

        $this->_helper->json->sendJson (array ('id'             => '',
                                               'name'           => '',
                                               'description'    => '',
                                               'program'        => '',
                                               'programId'      => '',
                                               'edit'           => false,
                                               'viewProg'       => false,
                                               'editProg'       => false,
                                               'cmds'           => array ()));
    }

    public function doactionAction ()
    {
        $idParam    = $this->_request->getParam ('id');
        $fctParam   = $this->_request->getParam ('fct');
        $cmdParam   = $this->_request->getParam ('cmd');

        list ($type, $id) = explode ('-', $idParam);
        switch ($type)
        {
            case 'gr':
                $allowed = $this->_acl->isAllowed ('Parc', 'pluginsCommands', $id);
                break;
            case 'box':
                $allowed = $this->_acl->isAllowed ('Player', 'pluginsCommands', $id);
                break;
            default:
                $allowed = false;
        }

        if (!$allowed)
            $this->_helper->json->sendJson (array ('msg' => 'Permission denied'));

        $commands   = new Actions_Commands ();
        $command    = $commands->getCommand ($cmdParam);
        if (!$command)
            $this->_helper->json->sendJson (array ('msg' => 'Unknown command: '.$cmdParam));

        $msg    = $command->doAction ($idParam, $fctParam);
        $html   = $command->getContextProperties ($idParam);

        $this->_helper->json->sendJson (
            array ('html'   => $html,
                    'msg'   => $msg));
    }

    public function getboxAction ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $hash = $this->_request->getParam ('hash');

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Player', 'view', $hash))
            $this->_helper->json->sendJson (array ('hash'           => '',
                                                   'group'          => '',
                                                   'status'         => '',
                                                   'sync'           => '',
                                                   'program'        => '',
                                                   'emission'       => '',
                                                   'label'          => '',
                                                   'coordinates'    => '',
                                                   'mode'           => '',
                                                   'sync_ver'       => '',
                                                   'mac'            => '',
                                                   'ip'             => '',
                                                   'gw'             => '',
                                                   'dns'            => '',
                                                   'composer_ip'    => '',
                                                   'composer'       => '',
                                                   'pkg_ver'        => '',
                                                   'lic_exp'        => '',
                                                   'lic_status'     => '',
                                                   'lastConn'       => '',
                                                   'lastIp'         => '',
                                                   'editable'       => false,
                                                   'cmds'           => array ()));

        $boxes = new Players_Boxes ();
        $groups = new Players_Groups ();
        $commands   = new Actions_Commands ();

        $box = $boxes->findiBoxWithHash ($hash);
        if ($box)
        {
            $jsonHost ['hash']          = $box->getHash ();
            $jsonHost ['group']         = $groups->findGroupById (
                                            $box->getGroupId ())->getName ();
            $jsonHost ['programId']     = $box->getProgram ();
            $jsonHost ['label']         = $box->getLabel ();
            $jsonHost ['coordinates']   = $box->getCoordinates ();
            $jsonHost ['mode']          = $box->getMode ();
            $jsonHost ['sync_ver']      = $box->getSyncVersion ();
            $jsonHost ['mac']           = $box->getMac ();
            $jsonHost ['ip']            = $box->getIp ();
            $jsonHost ['gw']            = $box->getGw ();
            $jsonHost ['dns']           = $box->getDns ();
            $jsonHost ['composer_ip']   = $box->getComposerIp ();
            $jsonHost ['composer']      = $box->getComposerHost ();
            $jsonHost ['pkg_ver']       = $box->getPkgVersion ();
            $jsonHost ['lastConn']      = $box->getLastConn ();
            $jsonHost ['lastIp']        = $box->getLastIP ();
            if ($this->_acl->isAllowed ('Player', 'pluginsCommands', $box->getHash ()))
                $jsonHost ['cmds']      = $commands->getPluginsProperties ('box-'.$box->getHash ());
            else
                $jsonHost ['cmds']      = array ();

            $jsonHost ['editable']      = $this->_acl->isAllowed ('Player', 'edit', $box->getHash ());

            // Get the program (for group, ibox, and in download)
            $dbProgs        = new Progs_Programs ();
            $playedPrograms = new Players_Programs ();
            $dwPrograms     = new Players_Downloads ();
            $licenses       = new Players_Licenses ();
            $dbChannels     = new Progs_Channels ();

            $groupProg  = $dbProgs->findProgram ($box->getProgram ());
            $boxProg    = $playedPrograms->getCurrentEmission ($box->getHash ());
            //$boxDw      = $dwPrograms->getCurrentDownload ($box->getHash ());

            if (!$boxProg)
                $jsonHost ['emission'] = $translator->translate ("Unknown");
            else
                $jsonHost ['emission'] = $boxProg->getName ();

            $channel = $dbChannels->findChannel ($box->getProgram ());
            if (!$channel)
                $jsonHost ['program'] = $translator->translate ("Unknown");
            else
                $jsonHost ['program'] = $channel->getName ();

            switch ($box->getStatus ())
            {
                case "OK":
                    $jsonHost ['status'] = 
                            $translator->translate ("Ok");
                    break;
                case "REBOOT":
                    $jsonHost ['status'] = 
                            $translator->translate ("Rebooting...");
                    break;
                case "OFF":
                    $jsonHost ['status'] =
                            $translator->translate ("Off");
                    break;
                case "TIMEOUT":
                    $jsonHost ['status'] =
                            $translator->translate ("Error: too long unanswered");
                    break;
                case "UNKNOWN":
                    $jsonHost ['status'] =
                            $translator->translate ("Error: unknown status");
                    break;
                default:
                    $jsonHost ['status'] = $box->getStatus ();
                    break;
            }
            switch ($box->getSync ())
            {
                case "OK":
                    $jsonHost ['sync'] = 
                            $translator->translate ("Up-to-date");
                    break;
                case "DESYNCHRONIZED":
                    $jsonHost ['sync'] = 
                            $translator->translate ("Late");
                    break;
                case "SYNCHRONIZING":
                    $jsonHost ['sync'] = 
                            $translator->translate ("In progress");
                    break;
                case "ERROR":
                default:
                    $jsonHost ['sync'] = 
                            $translator->translate ("Error");
                    break;
            }

/*
            $ourLicense = $licenses->getLicense ($box->getLicenseId ());
            if (!$ourLicense)
                $jsonHost ['lic_status'] = $translator->translate ("Error");
            else
            {
                // Get the valid license
                $validLicense = $licenses->getValidLicense ($box->getHash ());
                if (!$validLicense)
                {
                    // Invalid license. Two possibilites : expiraiton or revoked
                    if ($ourLicense->isRevoked ())
                        $jsonHost ['lic_status'] = $translator->translate ("Revoked license.");
                    else
                        $jsonHost ['lic_status'] = $translator->translate ("Expired license.");
                }
                elseif ($validLicense->getId () != $ourLicense->getId ())
                    $jsonHost ['lic_status'] = $translator->translate ("Error: please contact the iPreso Team");
                else
                    $jsonHost ['lic_status'] = $translator->translate ("Ok");
            }
            $jsonHost ['lic_exp'] = $ourLicense->getExpiration ();
*/
            $jsonHost ['lic_exp'] = "TODO";

            $this->_helper->json->sendJson ($jsonHost);
        }

        $this->_helper->json->sendJson (array ('hash'           => '',
                                               'group'          => '',
                                               'status'         => '',
                                               'sync'           => '',
                                               'program'        => '',
                                               'emission'       => '',
                                               'label'          => '',
                                               'coordinates'    => '',
                                               'mode'           => '',
                                               'sync_ver'       => '',
                                               'mac'            => '',
                                               'ip'             => '',
                                               'gw'             => '',
                                               'dns'            => '',
                                               'composer_ip'    => '',
                                               'composer'       => '',
                                               'pkg_ver'        => '',
                                               'lic_exp'        => '',
                                               'lic_status'     => '',
                                               'lastConn'       => '',
                                               'lastIp'         => '',
                                               'editable'       => false,
                                               'cmds'           => array ()));
    }

    public function delgroupAction ()
    {
        $groupParam = $this->_request->getParam ('id');
        if (is_numeric ($groupParam))
        {
            $groupId = intval ($groupParam);

            if (!$this->_acl ||
                !$this->_acl->isAllowed ('Parc', 'delete', $groupId))
                $this->_sendBoxesList ();

            $groups = new Players_Groups ();
            $boxes = new Players_Boxes ();

            $group = $groups->findGroupById ($groupId);

            if ($group !== NULL && $groups->delGroup ($group->getId ()))
            {
                $logger = Zend_Registry::getInstance ()->get ('Zend_Log');
                $logger->info ("Deleted group of iBoxes: *".$group->getName ()."*.");

                // Change all its boxes to default group
                $boxesArray = $boxes->getBoxesArrayByGroup ($groupId);
                if ($boxesArray && count ($boxesArray))
                {
                    foreach ($boxesArray as $box)
                        $boxes->setHostGroup ($box ['box_hash'], 0);
                }
            }
        }
        $this->_sendBoxesList ();
    }

    public function delboxAction ()
    {
        $hash = $this->_request->getParam ('hash');

        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Player', 'delete', $hash))
            $this->_sendBoxesList ();

        $boxes = new Players_Boxes ();
        $box = $boxes->findiBoxWithHash ($hash);

        if ($box !== NULL && $boxes->delBox ($box->getHash ()));
        {
            $logger = Zend_Registry::getInstance ()->get ('Zend_Log');
            $logger->info ("Deleted iBox: *".$box->getLabel ()."*.");
        }
        $this->_sendBoxesList ();
    }

    public function addgroupAction ()
    {
        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Parc', 'new'))
            return;

        $groupname = $this->_request->getParam ('name');
        $groups = new Players_Groups ();
        $id = $groups->addGroup ($groupname);

        // Log this action !
        if ($id > 0)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            $logger->info("Created group of iBoxes: *$groupname*.");
        }
        $this->_sendBoxesList ();
    }

    public function editgroupAction ()
    {
        $groupParam     = Zend_Json::decode ($this->_request->getParam ('group'));
        $pluginsParam   = Zend_Json::decode ($this->_request->getParam ('plugins'));
        
        $groups = new Players_Groups ();
        $result = $groups->modifyGroup ($groupParam ['id'],
                                        $groupParam ['name'],
                                        $groupParam ['description']); 
        if (count ($result) > 0)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            foreach ($result as $change)
            {
                $logger->info ("Changed group of iBoxes ".
                               "*".$groupParam ['name']."*: ".
                               "*".$change ['field']."* from ".
                               "*".$change ['old']."* to ".
                               "*".$change ['new']."*.");
            }
        }

        $players    = new Players_Boxes ();
        $playersList = $players->getBoxesArrayByGroup ($groupParam ['id']);

        // Process plugins actions for each players of the group
        $commands   = new Actions_Commands ();
        foreach ($pluginsParam as $fieldNValue)
        {
            if (empty ($fieldNValue [0]))
                continue;

            list ($plugin, $param) = explode ('-', $fieldNValue [0]);
            if (empty ($plugin))
                continue;

            $command = $commands->getCommand ($plugin);
            if (!$command)
                continue;

            $command->setParamGroup ($groupParam ['id'], $param, $fieldNValue [1]);
            if (!$playersList || count ($playersList) < 1)
                continue;

            foreach ($playersList as $box)
                $command->setParamBox ($box ['box_hash'], $param, $fieldNValue [1]);
        }
        $this->_sendBoxesList ();
    }

    public function editboxAction ()
    {
        $boxParam       = Zend_Json::decode ($this->_request->getParam ('box'));
        $pluginsParam   = Zend_Json::decode ($this->_request->getParam ('plugins'));

        // Some characters are forbidden
        $boxParam = str_replace ('#', '_', $boxParam);
        $boxParam = str_replace ('\'', ' ', $boxParam);
        
        if (!$this->_acl ||
            !$this->_acl->isAllowed ('Player', 'edit', $boxParam ['hash']))
            $this->_sendBoxesList ();

        $boxes = new Players_Boxes ();
        $result = $boxes->modifyBox ($boxParam ['hash'],
                                     $boxParam ['label'],
                                     $boxParam ['coordinates'],
                                     $boxParam ['ip'],
                                     $boxParam ['gw'],
                                     $boxParam ['dns'],
                                     $boxParam ['composer_ip']);

        if (count ($result) > 0)
        {
            $logger = Zend_Registry::getInstance()->get('Zend_Log');
            foreach ($result as $change)
            {
                $logger->info ("Changed iBoxes ".
                               "*".substr ($boxParam ['hash'], -8).
                               "*: ".
                               "*".$change ['field']."* from ".
                               "*".$change ['old']."* to ".
                               "*".$change ['new']."*.");
            }
        }

        if ($this->_acl->isAllowed ('Player', 'pluginsCommands', $boxParam ['hash']))
        {
            $commands   = new Actions_Commands ();
            foreach ($pluginsParam as $fieldNValue)
            {
                if (empty ($fieldNValue [0]))
                    continue;

                list ($plugin, $param) = explode ('-', $fieldNValue [0]);
                if (empty ($plugin) || empty ($param))
                    continue;

                $command = $commands->getCommand ($plugin);
                if (!$command)
                    continue;

                $command->setParamBox ($boxParam ['hash'], $param, $fieldNValue [1]);
            }
        }
        $this->_sendBoxesList ();
    }
}
