<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
class Zend_View_Helper_ComposerNavBar extends Zend_View_Helper_Abstract
{
    private function composerLink ($link, $optionalLiId = "", $label = "")
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $controller = Zend_Controller_Front::getInstance ();
        $ctrlName = $controller->getRequest ()->getControllerName ();

        if (strlen ($optionalLiId))
            $result = "<li id=\"$optionalLiId\">";
        else
            $result = "<li>";

        if (!strlen ($label))
            $label = $link;

        if (strcmp ($ctrlName, $link) == 0)
        {
            // Current link
            if (strlen ($optionalLiId))
                $result = "<li id=\"$optionalLiId\">";
            else
                $result = "<li>";

            $result .= "<a href=\"/$link\" id=\"current\" class=\"menu\">"
                        .$translator->translate ("$label")
                        ."</a></li>\n";
        }
        else
        {
            // Available link
            $result .= "<a href=\"/$link\" class=\"menu\">"
                        .$translator->translate ("$label")
                        ."</a></li>\n";
        }
        return ($result);
    }

    public function composerNavBar ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $result = "<ul>\n";

        // "Disconnect" link if authed
        if (Zend_Auth::getInstance ()->hasIdentity ())
        {
            $user = Zend_Registry::getInstance ()->get ('App_User');
            if (!$user)
            {
                Zend_Auth::getInstance()->clearIdentity();
                $this->_helper->redirector ('index', 'login');
            }

            // "My Account" link
            //$result .= $this->composerLink ('Account', '', $translator->translate ('My Account'));

            $groupObj = $user->getGroup ();
            if ($groupObj !== NULL)
                $group = $groupObj->getName ();
            else
                $group = NULL;

            $acl = Zend_Registry::getInstance ()->get ('Group_permissions');
            // Optional "Administration" link
            if ($acl && $acl->isAllowed ('Administration', 'view'))
                $result .= $this->composerLink ('Administration');
            
            // Composer navigation
            $result .= $this->composerLink ('Catalog', '', 'Studio');
            if ($user->getAdvancedProgrammation ())
                $result .= $this->composerLink ('Programmation');
            $result .= $this->composerLink ('Supervision');    
                
            // If the user is a quick updater
            //$permissions = new Admin_Permissions ();
            //if ($permissions->isQuickUpdaterUser($acl))
            //{
            //	$result .= $this->composerLink ('Quickupdate', '', $translator->translate ('Quick Update'));
            //}    
        }

        $result .= "</ul>\n";
        return ($result);
    }
    
}
