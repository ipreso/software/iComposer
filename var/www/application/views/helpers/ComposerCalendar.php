<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
class Zend_View_Helper_ComposerCalendar extends Zend_View_Helper_Abstract
{
    private $day;
    private $dayWeek;
    private $week;
    private $month;
    private $year;
    private $daysString;
    private $currentDayWeek;
    private $daysInfos;
    private $delay;

    public function composerCalendar ($week, $year, $zoom)
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        // Getting months, weeks and days informations
        if ($week < 10 && $week [0] != "0")
            $weekStr = "0$week";
        else
            $weekStr = $week;

        $ref = strtotime ("${year}W${weekStr}");

        $this->day      = date ("d", $ref);
        $this->week     = date ("W", $ref);
        $this->month    = date ("m", $ref);
        $this->year     = date ("Y", $ref);
        $this->dayWeek  = date ("w", $ref) - 1;
        if ($this->dayWeek < 0)
            $this->dayWeek = 6;

        $currentWeek    = date ("W");
        $currentYear    = date ("Y");
        if ($currentWeek == $week && $currentYear == $year)
        {
            $this->currentDayWeek = date ("w") - 1;
            if ($this->currentDayWeek < 0)
                $this->currentDayWeek = 6;
        }
        else
            $this->currentDayWeek = -1;

        $this->daysString     = array ($translator->translate ('Mon.'),
                                       $translator->translate ('Tue.'),
                                       $translator->translate ('Wed.'),
                                       $translator->translate ('Thu.'),
                                       $translator->translate ('Fri.'),
                                       $translator->translate ('Sat.'),
                                       $translator->translate ('Sun.'));

        $this->daysInfos = array();
        for ($i = 0 ; $i < 7 ; $i++)
        {
            $dday = mktime (0, 0, 0, date ("m", $ref), date ("d", $ref) + $i, date ("Y", $ref));
            $delay = date ("Z", $dday);
            $this->daysInfos [$i] =
                     array ('today'     => ($this->currentDayWeek == $i),
                            'daySt'     => $this->daysString [$i],
                            'delay'     => $delay,
                            'start'     => date ("U", $dday),
                            'end'       => date ("U", mktime (0, 0, 0, date ("m", $ref), date ("d", $ref) + $i + 1, date ("Y", $ref))) - 1,
                            'day'       => date ("d", $dday),
                            'month'     => date ("m", $dday));
        }



        $result = '

<div style="float: left; font-size: 10px;">
    <img src="/images/Left.png" id="prevWeek" />'
        .$translator->translate ("Week")
        ." <span id=\"weekId\">$week</span>"
        ." (<span id=\"year\">$year</span>)".'
    <img src="/images/Right.png" id="nextWeek" />
</div>
<div style="text-align: right; font-size: 10px;">'
        .$translator->translate ("Zoom: ")."
    <img src=\"/images/Left.png\" id=\"zoomOut\" />
    <span id=\"zoomLevel\">$zoom</span>/5
    <img src=\"/images/Right.png\" id=\"zoomIn\" />".'
</div>
<div id="calendarBox">'
      .$this->getCalendarHeaders ()
      .$this->getCalendar ($zoom).'
</div>';

        return ($result);
    }

    function getCalendarHeaders ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $cal = '
                <table id="calendarHeader" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 40px;"></td>';

        for ($i = 0 ; $i < 7 ; $i++)
        {
            $cal .= '<th scope="col">
                        <div class="wk-day';
            if ($this->daysInfos [$i]['today'])
                $cal .= ' todayHeader';
            $cal .= '">'.$this->daysInfos [$i]['daySt']
                        .' '
                        .$this->daysInfos [$i]['day']
                        .'/'
                        .$this->daysInfos [$i]['month']
                        .'</div></th>';
        }
        $cal .= '     <td style="width: 17px;"></td>
                    </tr>
                </table>
                ';

        return ($cal);
    }

    function getCalendar ($zoomLevel)
    {
        $cal = '<div id="calendarSizeTable">
                 <table id="calendarTable" cellpadding="0" cellspacing="0" class="calendarSize-z'.$zoomLevel.'">
                    <tr height="1px">
                        <td style="width: 40px;" class="hours"></td>
                        <td colspan="7">
                          <div class="markersTimeW1">
                           <div class="markersTimeW2">
                            <div class="markersTime">';

        for ($i = 0 ; $i < 24 ; $i++)
        {
            $cal .= "<div class=\"markersHour hourSize-z$zoomLevel\"><div class=\"markersHalfHour halfHourSize-z$zoomLevel\" /></div>";
            $cal .= "\n";
        }
        
        $cal .= '           </div>
                           </div>
                          </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="hours">';

        for ($i = 0 ; $i < 24 ; $i++)
        {
            $cal .= "<div class=\"hourLabel hourLabelSize-z$zoomLevel\">".(($i < 10) ? "0$i" : $i).":00</div>";
        }
        $cal .= "\n</td>\n";

        for ($i = 0 ; $i < 7 ; $i++)
        {
            if ($this->daysInfos [$i]['today'])
                $cal .= "<td class=\"todayColumn\">";
            else
                $cal .= "<td>";

            $cal .= "<div id=\"day$i\" class=\"eventCol dayDrop calendarSize-z$zoomLevel\"
                        start=\"".$this->daysInfos [$i]['start']."\" 
                        delay=\"".$this->daysInfos [$i]['delay']."\"
                        end=\"".$this->daysInfos [$i]['end']."\">";
            $cal .= "</div></td>";
        }

        $cal .= ' </tr>
                </table>
              </div>';

        return ($cal);
    }
}
