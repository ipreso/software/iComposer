<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
class Zend_View_Helper_ComposerTableLine extends Zend_View_Helper_Abstract
{
    public function composerTableLine ($onRight, $onLeft, $part = "")
    {
        $result = "";
        switch ($part)
        {
            case 'HEAD':
                $result = "<table cellpadding=\"3px\" border=\"0\" style=\"width: 100%;\">\n";
                break;
            case 'FOOT':
                $result = "</table>\n";
                break;
            case 'MERGED':
                $result = $this->showMergedLine ($onRight);
                break;;
            case 'LINE':
            default:
                $result = $this->showLine ($onRight, $onLeft);
        }
        return ($result);
    }

    private function showLine ($onRight, $onLeft)
    {
        $result = "<tr><td class=\"tdRight\">$onRight</td>"
                     ."<td class=\"tdLeft\">$onLeft</td></tr>\n";
        return ($result);
    }

    private function showMergedLine ($onRight)
    {
        return ("<tr><td class=\"tdCenter\" colspan=\"2\">"
                ."$onRight</td></tr>\n");
    }
}
