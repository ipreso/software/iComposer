<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
class Zend_View_Helper_ComposerLogInfos extends Zend_View_Helper_Abstract
{
    private function composerLink ($link, $optionalLiId = "", $label = "")
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        $controller = Zend_Controller_Front::getInstance ();
        $ctrlName = $controller->getRequest ()->getControllerName ();

        if (strlen ($optionalLiId))
            $result = "<li id=\"$optionalLiId\">";
        else
            $result = "<li>";

        if (!strlen ($label))
            $label = $link;

        if (strcmp ($ctrlName, $link) == 0)
        {
            // Current link
            if (strlen ($optionalLiId))
                $result = "<li id=\"$optionalLiId\">";
            else
                $result = "<li>";

            $result .= "<a href=\"/$link\" id=\"current\" class=\"menu\">"
                        .$translator->translate ("$label")
                        ."</a></li>\n";
        }
        else
        {
            // Available link
            $result .= "<a href=\"/$link\" class=\"menu\">"
                        .$translator->translate ("$label")
                        ."</a></li>\n";
        }
        return ($result);
    }

    public function composerLogInfos ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        $result = "";
        $result .= "<ul>\n";

        // "Disconnect" link if authed
        if (Zend_Auth::getInstance ()->hasIdentity ())
        {
            $result .= "<li><a href=\"/login/logout\" class=\"menu\">"
                       . Zend_Registry::getInstance()
                            ->get ('Zend_Translate')
                            ->translate ('Disconnect')
                       ."</a></li>\n";

            $user = Zend_Registry::getInstance ()->get ('App_User');
            if (!$user)
            {
                Zend_Auth::getInstance()->clearIdentity();
                $this->_helper->redirector ('index', 'login');
            }
            $groupObj = $user->getGroup ();
            if ($groupObj !== NULL)
                $group = $groupObj->getName ();
            else
                $group = NULL;

            // "My Account" link
            $result .= $this->composerLink (
                    'Account',
                    '', 
                    $user->getName ()." (".$user->getGroup ()->getName ().")");
        }

        $result .= "</ul>\n";
        return ($result);
    }
}
