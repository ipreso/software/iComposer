<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
class Zend_View_Helper_ComposerGetLastLogs extends Zend_View_Helper_Abstract
{
    public function composerGetLastLogs ($line_limit)
    {
        $result = array ();

        $acl = Zend_Registry::getInstance ()->get ('Group_permissions');
        if (!$acl || $acl->isAllowed ('Administration', 'view', 'logsusers'))
            return ($result);

        $users = new Admin_Users ();
        $usersInGroup = $users->fetchAll ($users->select ()
                                          ->where ('id_group = ?', $id_group)
                                          ->order ('lastname'));

        foreach ($usersInGroup as $user)
        {
            $result[] = $user->toArray ();
        }

        return ($result);
    }
}
