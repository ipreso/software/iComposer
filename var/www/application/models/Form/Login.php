<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Form_Login extends Zend_Form
{
    public function init ()
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');

        // Username and Password fields
        $username = $this->addElement ('text', 'username', 
                        array ('filters'    => array ('StringTrim', 'StringToLower'),
                               'validators' => array ('Alnum',
                                                      array ('StringLength', false, array (3,20))),
                               'required'  => true,
                               'label'     => $translator->translate ('Your username:')));

        $password = $this->addElement ('password', 'password', 
                        array ('filters'    => array ('StringTrim'),
                               'validators' => array (array ('StringLength', false, array (4,20))),
                               'required'  => true,
                               'label'     => $translator->translate ('Your password:')));

        // Login button
        $login = $this->AddElement ('submit', 'login',
                                    array ('required'   => true,
                                           'ignore'     => true,
                                           'label'      => $translator->translate ('Connect')));


        // How to form the HTML code...
        $this->setDecorators (array ('FormElements',
                                     array ('HtmlTag', array ('tag' => 'dl',
                                                              'class' => 'zend_form')),
                                     array ('Description', array ('placement' => 'prepend')),
                                     'Form'));
    }
}
