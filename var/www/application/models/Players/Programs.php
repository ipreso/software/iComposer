<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Programs extends Zend_Db_Table
{
    protected $_name = 'Players_programs';

    public function setCurrentProgram ($hash, $program)
    {
        // Get the previous program for this iBox
        $select = $this->select ()->where ('player = ?', $hash)
                                  ->order ('start DESC')
                                  ->limit (1);
        $row = $this->fetchRow ($select);
        if ($row !== NULL)
        {
            // Program unchanged ?
            if ($row->program == $program)
                return (true);

            // Set the date
            $row->end = new Zend_Db_Expr ('NOW()');
            $row->save ();
        }

        // Create a new line with the current program
        $data = array ( 'player'    => $hash,
                        'program'   => $program,
                        'start'     => new Zend_Db_Expr ('NOW()'));
        return ($this->insert ($data));
    }

    public function getCurrentEmission ($hash)
    {
        $programs = new Progs_Programs ();

        // Get the last program for this iBox
        $select = $this->select ()->where ('player = ?', $hash)
                                  ->order ('start DESC')
                                  ->limit (1);
        $row = $this->fetchRow ($select);
        if ($row === NULL)
            return (NULL);

        return ($programs->findProgram ($row->program));
    }
}
