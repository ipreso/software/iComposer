<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Boxes extends Zend_Db_Table
{
    protected $_name = 'Players_hosts';

    public function findiBoxWithShortHash ($shortHash)
    {
        // 'shortHash' are the last 8 characters of the real Hash
        if (strlen ($shortHash) != 8)
            return (NULL);

        $select = $this->select ()->where ('box_hash LIKE ?', '%'.$shortHash);
        $row = $this->fetchRow ($select);
        
        if ($row === NULL)
            return (NULL);

        $box = new Players_Box ($row);
        return ($box);
    }

    public function findiBoxWithHash ($hash)
    {
        // 'hash' is 32 characters long
        if (strlen ($hash) != 32)
            return (NULL);

        $select = $this->select ()->where ('box_hash = ?', $hash);
        $row = $this->fetchRow ($select);
        
        if ($row === NULL)
            return (NULL);

        $box = new Players_Box ($row);
        return ($box);
    }

    public function addBox ($hash, $mac, $ip, $gw, $dns,
                            $pkgsrc, $label, $mode, $coordinates,
                            $composerHost, $composerIp,
                            $remoteIp, $version, $syncVersion,
                            $wallpaper)
    {
        $logs = new Players_Logs ();
        $logs->saveMsg ($hash, "Adding box $hash to DB...", 'INFO');

        // Check if Box already exists
        if ($this->findiBoxWithHash ($hash))
        {
            $logs->saveMsg ($hash, "FAILED adding box $hash to DB (already exists)", 'INFO');
            return (NULL);
        }

        $boxesGroups = new Players_Groups ();

        $data = array ('box_hash'           => $hash,
                       'box_group'          => 0,
                       'box_status'         => 'UNKNOWN',
                       'box_sync'           => 'OK',
                       'box_label'          => $label,
                       'box_coordinates'    => $coordinates,
                       'box_mode'           => $mode,
                       'box_program'        => $boxesGroups->getGroupProgramId (0),
                       'box_schedule'       => NULL,
                       'box_wp'             => $wallpaper,
                       'sync_version'       => $syncVersion,
                       'net_mac'            => $mac,
                       'net_ip'             => $ip,
                       'net_gw'             => $gw,
                       'net_dns'            => $dns,
                       'composer_ip'        => $composerIp,
                       'composer_name'      => $composerHost,
                       'pkg_version'        => $version,
                       'pkg_source'         => $pkgsrc,
                       'last_ip'            => $remoteIP,
                       'last_connection'    => new Zend_Db_Expr ('NOW()'));

        return ($this->insert ($data));
    }

    public function delBox ($hash)
    {
        // Delete related licenses
        $licenses = new Players_Licenses ();
        $licenses->delLicenses ($hash);

        $where = $this->getAdapter ()->quoteInto ('box_hash = ?', $hash);
        $this->delete ($where);
        return (TRUE);
    }

    public function updateLicense ($hash, $licenseId)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        $row ['box_license'] = $licenseId;
        $row->save ();
        return (TRUE);
    }

    public function updateLastConnection ($hash, $ip, $version)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();

        $row ['last_connection'] = new Zend_Db_Expr('NOW()');
        if (strcmp ($row ['last_ip'], $ip) != 0)
        {
            // TODO Log this change !
            $row ['last_ip'] = $ip;
        }
        if (strcmp ($row ['pkg_version'], $version) != 0)
        {
            $row ['pkg_version'] = $version;
        }
        $row->save ();
    }

    public function updateBoxesStatus ()
    {
        $set = array ('box_status'  => 'TIMEOUT');
        $where = $this->getAdapter ()
                        ->quoteInto ('TIMEDIFF(NOW(),last_connection) > ?',
                                     '00:02:30')
               . $this->getAdapter ()
                        ->quoteInto ('AND box_status != ?', 'OFF')
               . $this->getAdapter ()
                        ->quoteInto ('AND box_status != ?', 'TIMEOUT')
               . $this->getAdapter ()
                        ->quoteInto ('AND box_status != ?', 'UNKNOWN');

        $this->update ($set, $where);
    }

    public function getBoxesArrayByGroup ($groupId)
    {
        $result = array ();

        $this->updateBoxesStatus ();

        // Get boxes
        $select = $this->select ()->setIntegrityCheck(false)
                                  ->from (array ('b' => 'Players_hosts'),
                                          array ('box_label',
                                                 'box_hash',
                                                 'net_mac',
                                                 'box_program',
                                                 'box_schedule',
                                                 'box_sync',
                                                 'box_status'))
                                  ->where ('box_group = ?', $groupId)
                                  ->order ('box_label');
        $row = $this->fetchAll ($select);
        foreach ($row as $user)
            $result [] = $user->toArray ();
        return ($result);
    }

    public function setHostGroup ($hash, $idGroup)
    {
        // Update the "id_group" field for the host Hash
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if ($row ['box_group'] != $idGroup)
        {
            $row ['box_group'] = $idGroup;
            $row->save ();
            return (true);
        }
        return (false);
    }

    public function getBoxStatus ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['box_status']);
    }

    public function getBoxSyncStatus ($hash)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        return ($row ['box_sync']);
    }

    public function setBoxStatus ($hash, $status)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        if (strcmp ($status, "OK") == 0 &&
            strcmp ($row ['box_status'], "OK") != 0)
        {
            // Status changed to OK
            $logs = new Players_Logs ();
            $logs->saveMsg ($hash, "Status is ON and OK.", 'INFO');
        }

        $row ['box_status'] = $status;
        $row->save ();
        return (true);
    }

    public function setBoxWallpaper ($hash, $wallpaper)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        $row ['box_wp'] = $wallpaper;
        $row->save ();
        return (true);
    }

    public function setBoxSyncStatus ($hash, $status)
    {
        $rowset = $this->find ($hash);
        if (!$rowset)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        $row ['box_sync'] = $status;
        $row->save ();
        return (true);
    }

    public function setBoxProgram ($hash, $program)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if ($row ['box_program'] != $program)
        {
            $row ['box_program'] = $program;
            $row->save ();
            return (true);
        }
        return (false);
    }

    public function setBoxSchedule ($hash, $schedule)
    {
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if ($row ['box_schedule'] != $schedule)
        {
            $row ['box_schedule'] = $schedule;
            $row->save ();
            return (true);
        }
        return (false);
    }

    public function setGroupProgram ($idGroup, $program)
    {
        $set = array ('box_program' => $program);
        $where = $this->getAdapter ()
                        ->quoteInto ('box_group = ?', $idGroup)
               . $this->getAdapter ()
                        ->quoteInto ('AND box_program != ?', $program);
        if ($this->update ($set, $where) > 0)
            return (true);
        else
            return (false);
    }

    public function replaceBoxesChannel ($groupId, $newChannel)
    {
        $set = array ('box_program' => $newChannel);
        $where = $this->getAdapter ()->quoteInto ('box_group = ?', $groupId);
        if ($this->update ($set, $where) > 0)
            return (true);
        else
        {
            // Should return true even if no box are affected !
            return (true);
        }
    }

    protected function _checkIP ($ip)
    {
        // Empty IP is for DHCP
        if (strlen ($ip) < 1)
            return "";

        // Check we have xx.xx.xx.xx/yy (with yy the bitmask)
        list ($dotted_ip, $netmask) = explode ('/', $ip);
        if (strlen ($netmask) > 0)
            return $ip;

        // No bitmask, we guess it from the IP Class
        list ($b1, $b2, $b3, $b4) = explode ('.', $ip);
        if (intval ($b1) <= 0)
            return (FALSE);

        if (intval ($b1) <= 126)
            return ("$ip/8");
        if (intval ($b1) <= 191)
            return ("$ip/16");
        
        return ("$ip/24");
    }

    public function modifyBox ($hash, 
                                $label, $coordinates,
                                $ip, $gw, $dns,
                                $composer_ip)
    {
        $return = array ();

        $rowset = $this->find ($hash);
        if (count ($rowset) < 1)
            return (array ());

        $row = $rowset->current ();

        $checkedIP = $this->_checkIP ($ip);

        $this->_modifyField ($row, 'box_label', substr ($label, 0, 127), $return);
        $this->_modifyField ($row, 'box_coordinates', substr ($coordinates, 0, 31), $return);
        if ($checkedIP !== FALSE)
        {
            $this->_modifyField ($row, 'net_ip', substr ($checkedIP, 0, 18), $return);
            $this->_modifyField ($row, 'net_gw', substr ($gw, 0, 18), $return);
            $this->_modifyField ($row, 'net_dns', substr ($dns, 0, 18), $return);
        }
        $this->_modifyField ($row, 'composer_ip', substr ($composer_ip, 0, 18), $return);

        if (count ($return) > 0)
            $row ['box_sync'] = 'DESYNCHRONIZED';

        $row->save ();
        return ($return);
    }

    private function _modifyField (&$row, $field, $value, &$return)
    {
        if (strcmp ($row [$field], $value) != 0)
        {
            $return [] = array ('field' => $field,
                                'old'   => $row [$field],
                                'new'   => $value);
            $row [$field] = $value;
        }
    }
}
