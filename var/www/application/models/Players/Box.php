<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Box
{
    private $_box_hash;
    private $_box_group;
    private $_box_status;
    private $_box_sync;
    private $_box_program;
    private $_box_schedule;
    private $_box_label;
    private $_box_coordinates;
    private $_box_mode;
    private $_box_license;
    private $_box_wp;
    private $_sync_version;
    private $_net_mac;
    private $_net_ip;
    private $_net_gw;
    private $_net_dns;
    private $_composer_ip;
    private $_composer_name;
    private $_pkg_version;
    private $_pkg_source;
    private $_lastConn;
    private $_lastIp;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();

        $this->_box_hash        = $rowArray ['box_hash'];
        $this->_box_group       = $rowArray ['box_group'];
        $this->_box_status      = $rowArray ['box_status'];
        $this->_box_sync        = $rowArray ['box_sync'];
        $this->_box_program     = $rowArray ['box_program'];
        $this->_box_schedule    = $rowArray ['box_schedule'];
        $this->_box_label       = $rowArray ['box_label'];
        $this->_box_coordinates = $rowArray ['box_coordinates'];
        $this->_box_mode        = $rowArray ['box_mode'];
        $this->_box_license     = $rowArray ['box_license'];
        $this->_box_wp          = $rowArray ['box_wp'];
        $this->_sync_version    = $rowArray ['sync_version'];
        $this->_net_mac         = $rowArray ['net_mac'];
        $this->_net_ip          = $rowArray ['net_ip'];
        $this->_net_gw          = $rowArray ['net_gw'];
        $this->_net_dns         = $rowArray ['net_dns'];
        $this->_composer_ip     = $rowArray ['composer_ip'];
        $this->_composer_name   = $rowArray ['composer_name'];
        $this->_pkg_version     = $rowArray ['pkg_version'];
        $this->_pkg_source      = $rowArray ['pkg_source'];
        $this->_lastConn        = $rowArray ['last_connection'];
        $this->_lastIp          = $rowArray ['last_ip'];
    }

    public function getHash ()          { return ($this->_box_hash); }
    public function getGroupId ()       { return ($this->_box_group); }
    public function getStatus ()        { return ($this->_box_status); }
    public function getSync ()          { return ($this->_box_sync); }
    public function getProgram ()       { return ($this->_box_program); }
    public function getSchedule ()      { return ($this->_box_schedule); }
    public function getLabel ()         { return ($this->_box_label); }
    public function getCoordinates ()   { return ($this->_box_coordinates); }
    public function getMode ()          { return ($this->_box_mode); }
    public function getLicenseId ()     { return ($this->_box_license); }
    public function getWallpaperHash () { return ($this->_box_wp); }
    public function getSyncVersion ()   { return ($this->_sync_version); }
    public function getMac ()           { return ($this->_net_mac); }
    public function getIp ()            { return ($this->_net_ip); }
    public function getGw ()            { return ($this->_net_gw); }
    public function getDns ()           { return ($this->_net_dns); }
    public function getComposerIp ()    { return ($this->_composer_ip); }
    public function getComposerHost ()  { return ($this->_composer_name); }
    public function getPkgVersion ()    { return ($this->_pkg_version); }
    public function getPkgSource ()     { return ($this->_pkg_source); }
    public function getLastConn ()      { return ($this->_lastConn); }
    public function getLastIP ()        { return ($this->_lastIp); }
}
