<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Licenses extends Zend_Db_Table
{
    protected $_name = 'Players_licenses';

    public function getValidLicense ($hash)
    {
        $conf = Zend_Registry::getInstance()->get('Config_Ini_Licenses');
        if (!file_exists ($conf->keyring."/$hash.crt"))
            return FALSE;

        $license = new Players_License ($hash, $conf->keyring."/$hash.crt");
        if ($license->getCertificate ())
            return ($license);
        else
            return FALSE;
    }

    public function insertNewLicense ($hash, $tmppath)
    {
        if (!file_exists ($tmppath))
            return FALSE;

        // Delete old licenses of the box
        $this->delLicenses ($hash);
        
        // Add certificate to our keyring
        $conf = Zend_Registry::getInstance()->get('Config_Ini_Licenses');
        rename ($tmppath, $conf->keyring."/$hash.crt");

        return ($this->getValidLicense ($hash));
    }

    public function delLicenses ($hash)
    {
        $conf = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        if (file_exists ($conf->keyring."/$hash.crt"))
            unlink ($conf->keyring."/$hash.crt");

        return (TRUE);
    }
}
