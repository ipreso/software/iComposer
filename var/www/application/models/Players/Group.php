<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Group
{
    private $_id;
    private $_name;
    private $_desc;
    private $_programId;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_id          = $rowArray ['id'];
        $this->_name        = $rowArray ['groupname'];
        $this->_desc        = $rowArray ['description'];
        $this->_programId   = $rowArray ['program'];
    }

    public function getId ()
    {
        return ($this->_id);
    }

    public function getName ()
    {
        return ($this->_name);
    }

    public function getDescription ()
    {
        return ($this->_desc);
    }

    public function getProgram ()
    {
        return ($this->_programId);
    }
}
