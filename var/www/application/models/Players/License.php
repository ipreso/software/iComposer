<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_License
{
    private $_hash;
    private $_certificate;
    private $_path;

    public function __construct ($hash, $path)
    {
        // Parse the result
        $this->_hash        = $hash;
        $this->_path        = $path;
        $this->_certificate = openssl_x509_parse (file_get_contents($path));
    }

    public function getExpiration ()
    {
        return ($this->_expiration);
    }
    
    public function isRevoked ()
    {
        return (false);
    }

    public function getPath ()
    {
        return ($this->_path);
    }

    public function getCertificate ()
    {
        $conf = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        // Check the certificate (signed by our CA, not expired, ...)
        $cmd = "/usr/bin/openssl verify "
                    ."-CAfile '".$conf->ca."' '".$this->_path."' 2>&1 | "
                    ."grep -i error | wc -l | "
                    ."awk '{print \"if [ 0 -ne \"$1\" ]; then "
                    ."echo \\\"Error\\\"; else echo \\\"Ok\\\"; fi \"}' | sh";

        $certificateValidity = $this->_execute ($cmd);
        if (strcmp ($certificateValidity, "Ok") != 0)
        {
            // Certificate is not valid (expired, not signed, ...)
            unlink ($cert);
            return (false);
        }

        // Return the certificate file
        return (file_get_contents ($this->_path));
    }
    
    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        pclose ($f);

        return (trim ($result));
    }
}
