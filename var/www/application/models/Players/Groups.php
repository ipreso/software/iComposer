<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Players_Groups extends Zend_Db_Table
{
    protected $_name = 'Players_groups';

    public function findGroup ($group)
    {
        $select = $this->select()->where('groupname = ?', $group);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $group = new Players_Group ($row);
        return ($group);
    }

    public function findGroupById ($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $group = new Players_Group ($row);
        return ($group);
    }

    public function addGroup ($group)
    {
        // Returns the ID of the group created
        $groupId = $this->insert (array ('groupname'    => $group,
                                         'description'  => '',
                                         'program'      => -1));
        return ($groupId);
    }

    public function delGroup ($group)
    {
        if ($group < 1)
            return (FALSE);

        $where = $this->getAdapter()->quoteInto('id = ?', $group);
        $this->delete ($where);
        return (TRUE);
    }

    public function setGroupName ($id, $newName)
    {
        if ($id < 1)
            return (FALSE);

        $set = array ('groupname' => $newName);
        $where = $this->getAdapter ()->quoteInto ('id = ?', $id);
        if ($this->update ($set, $where) > 0)
            return (TRUE);
        else
            return (FALSE);
    }

    public function getGroupsArray ()
    {
        $result = array ();

        $boxes = new Players_Boxes ();

        $select = $this->select ()
                       ->setIntegrityCheck (false)
                       ->from (array ('g' => 'Players_groups'),
                               array ('id','groupname','description'))
                       ->order ('id');

        $allGroups = $this->fetchAll ($select);

        foreach ($allGroups as $group)
        {
            $result [] = $group->toArray ();
        }

        return ($result);
    }

    public function modifyGroup ($id, $name, $description)
    {
        $return = array ();

        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (array ());

        $row = $rowset->current ();

        $this->_modifyField ($row, 'groupname', $name, $return);
        $this->_modifyField ($row, 'description', $description, $return);

        $row->save ();
        return ($return);
    }

    public function setGroupProgram ($id, $programId)
    {
        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (false);

        $row = $rowset->current ();
        if ($row ['program'] != $programId)
        {
            $row ['program'] = $programId;
            $row->save ();
            return (true);
        }
        else
            return (false);
    }

    public function replaceGroupChannel ($groupId, $newChannel)
    {
        $rowset = $this->find ($groupId);
        if (count ($rowset) < 1)
            return (false);

        $row = $rowset->current ();
        if (!$row)
            return (false);

        if ($row ['program'] != $newChannel)
        {
            $row ['program'] = $newChannel;
            $row->save ();
        }
        return (true);
    }

    public function getGroupsWithChannel ($channelId)
    {
        $result = array ();

        $select = $this->select ()
                        ->where ('program = ?', $channelId);

        $groups = $this->fetchAll ($select);

        foreach ($groups as $group)
            $result [] = $group->toArray ();

        return ($result);
    }

    public function getGroupProgramId ($id)
    {
        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (false);

        $row = $rowset->current ();
        if ($row)
            return ($row ['program']);
        else
            return (-1);
    }

    private function _modifyField (&$row, $field, $value, &$return)
    {
        if (strcmp ($row [$field], $value) != 0)
        {
            $return [] = array ('field' => $field,
                                'old'   => $row [$field],
                                'new'   => $value);
            $row [$field] = $value;
        }
    }
}
