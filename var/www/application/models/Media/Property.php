<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Media_Property
{
    private $_itemId;
    private $_propId;
    private $_value;

    public function __construct ($row = "")
    {
        if (!empty ($row))
        {
            $rowArray = $row->toArray ();
            $this->_itemId  = $rowArray ['item_id'];
            $this->_propId  = $rowArray ['property_id'];
            $this->_value   = unserialize ($rowArray ['media_property']);
        }
        else
        {
            $this->_itemId  = '';
            $this->_value   = array ();
            $content = serialize ($this->_value);
            $this->_propId = md5 ($content);
        }
    }

    public function setItem ($itemId)
    {
        $this->_itemId = $itemId;
    }

    public function getValues ()
    {
        return ($this->_value);
    }

    public function clean ()
    {
        $this->_value = array ();
    }

    public function getSerializedValues ()
    {
        return (serialize ($this->_value));
    }

    public function getId ()
    {
        return ($this->_propId);
    }

    public function getItem ()
    {
        return ($this->_itemId);
    }

    public function getHTMLValues ()
    {
        if (!is_array ($this->_value))
            return (NULL);

        $html = "<table style=\"width: 100%;\">\n";
        foreach ($this->_value as $property)
        {
            // Label
            $html .= "<tr>"
                        ."<td class=\"tdRight\">"
                            ."<span>".$property ['label']."</span>\n"
                        ."</td><td class=\"tdLeft\">";

            // Element and its attributes
            $html .= "<".$property ['element'];
            foreach ($property ['attributes'] as $key => $value)
                $html .= " $key=\"$value\"";
            $html .= ">";

            $html .= $property ['html'];
            $html .= "</".$property ['element'].">\n";

            $html .= "</td></tr>\n";
        }
        $html .= "</table>\n";

        return ($html);
    }

    public function setValues ($values)
    {
        if (!is_array ($values))
            return (NULL);

        $this->_value = $values;
        $content = serialize ($this->_value);
        $this->_propId = md5 ($content);

        return ($this->_propId);
    }

    public function addProperty ($property)
    {
        if (!is_array ($property))
            return ('plip');

        $this->_value [] = $property;
        $content = serialize ($this->_value);
        $this->_propId = md5 ($content);
        return ($this->_propId);
    }
}
