<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Media_Plugins extends Zend_Db_Table
{
    protected $_name = 'Media_plugins';

    public function getPlugins ()
    {
        $result = array ();

        $allPlugins = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allPlugins as $plugin)
            $result [] = new Media_Plugin ($plugin);

        return ($result);
    }

    public function getPlugin ($name)
    {
        $select = $this->select ()->where ('name = ?', $name);
        $plugin = $this->fetchRow ($select);
        if ($plugin === NULL)
            return (NULL);

        return (new Media_Plugin ($plugin));
    }

    public function swap ($name)
    {
        $set = array ('enable' => new Zend_Db_Expr('MOD(enable+1,2)'));
        $where = $this->getAdapter ()->quoteInto ('name = ?', $name);
        if ($this->update ($set, $where) > 0)
            return (TRUE);
        else
            return (FALSE);
    }

    public function getPreview ($type, $hash)
    {
        // Get the plugin of the specified type
        $select = $this->select ()->where ('name = ?', $type);
        $plugin = $this->fetchRow ($select);
        if ($plugin === NULL)
            return (NULL);

        // Get the preview of the file
        $pluginObj = new Media_Plugin ($plugin);
        if (!$pluginObj)
            return (NULL);
        
        $preview = $pluginObj->getPreview ($hash);
        if (!$preview)
        {
            // Create preview picture thanks to the Media_plugin_{type}.png
            $preview = file_get_contents ("/usr/share/icomposer/www/application/models/Media/Plugin/$type.png", null);
            $set = array ('preview'  => $preview);
            $where = $this->getAdapter ()->quoteInto ('name = ?', $type);
            $this->update ($set, $where);
        }

        return ($preview);
    }

    public function addFile ($path)
    {
        $pluginsForFile = array ();

        $allPlugins = $this->fetchAll ($this->select ()->order ('id'));
        foreach ($allPlugins as $plugin)
        {
            $pluginObj = new Media_Plugin ($plugin);
            if ($pluginObj->isManagingFile ($path))
                $pluginsForFile [] = $pluginObj;
        }

        // Plugin compatible ?
        if (count ($pluginsForFile) < 1)
            return (false);

        // Move the file into the global library
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        if (!file_exists ($upload->library))
            @mkdir ($upload->library, 0777, true);

        // Is the same filename already existing in the library ?
        $name = basename ($path);
        if (file_exists ($upload->library."/$name"))
        {
            // Yes, a file already exists with the same name
            // Possible choice:
            // 1/ Overwrite the old file:
            //    -> Update the plugin table (new md5)
            //    -> Update all manifest files
            //    -> Update all playlist files
            //    -> Do the previous updates only if user is authorized to do so
            //    = really complicated
            // 2/ Forbid the upload of this file
            //    -> User should delete the initial file first
            //    -> User should update the related emission to do so
            //    = constraints on the user side
            // Choice:
            //  #2, because of the urgency to code fix and for
            //      simplicity/robustness
            error_log("File $name already exists. Please delete the original or rename this uploaded file.");
            return (false);
        }
        else
        {
            // Move it to this library
            rename ($path, $upload->library."/$name");
        }

        // Add the file to plugins managing it
        $result = true;
        foreach ($pluginsForFile as $pluginOk)
        {
            if ($result)
                $result = $pluginOk->addFile ($upload->library."/$name");
            else
                $pluginOk->addFile ($upload->library."/$name");
        }

        return ($result);
    }
}
