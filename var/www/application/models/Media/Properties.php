<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Media_Properties extends Zend_Db_Table
{
    protected $_name = 'Media_properties';

    public function getProperty ($itemId, $propertyId)
    {
        if (empty ($propertyId))
            $propertyId = '-';

        $select = $this->select ()->where ('item_id = ?', $itemId)
                                  ->where ('property_id = ?', $propertyId);
        $property = $this->fetchRow ($select);
        if ($property === NULL)
            return ($property);

        return (new Media_Property ($property));
    }

    public function saveProperties ($property)
    {
        $propertyId = $property->getId ();
        $itemId     = $property->getItem ();
        $values     = $property->getSerializedValues ();

        $select = $this->select ()->where ('item_id = ?', $itemId)
                                  ->where ('property_id = ?', $propertyId);
        $property = $this->fetchRow ($select);
        if ($property != NULL)
            return ($propertyId);

        $confirm = $this->insert (
                       array ('item_id'         => $itemId,
                              'property_id'     => $propertyId,
                              'media_property'  => $values));
        return ($confirm);
    }
}
