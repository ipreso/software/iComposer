<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Media_Plugin
{
    private $_id;
    private $_name;
    private $_description;
    private $_version;
    private $_preview;
    private $_enable;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();

        $this->_id          = $rowArray ['id'];
        $this->_name        = $rowArray ['name'];
        $this->_description = $rowArray ['description'];
        $this->_version     = $rowArray ['version'];
        $this->_preview     = $rowArray ['preview'];

        if ($rowArray ['enable'] == 0)
            $this->_enable = false;
        else
            $this->_enable = true;
    }

    public function getId ()            { return ($this->_id); }
    public function getDescription()    { return ($this->_description); }
    public function getVersion ()       { return ($this->_version); }
    public function getEnable ()        { return ($this->_enable); }

    public function getName ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        try
        {
            if (method_exists ($plugin, 'getName'))
                return ($plugin->getName ());
            else
                return ($this->_name);
        }
        catch (Exception $e)
        {
            return ($this->_name);
        }
    }

    public function getInternalName ()
    {
        return ($this->_name);
    }

    public function isManagingFile ($path)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();
        return ($plugin->isManagingFile ($path));
    }

    public function areItemsDeletable ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        try
        {
            if (method_exists ($plugin, 'areItemsDeletable'))
                return ($plugin->areItemsDeletable ());
            else
                return (false);
        }
        catch (Exception $e)
        {
            return (false);
        }
    }

    public function areItemsDownloadable ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        try
        {
            if (method_exists ($plugin, 'areItemsDownloadable'))
                return ($plugin->areItemsDownloadable ());
            else
                return (false);
        }
        catch (Exception $e)
        {
            return (false);
        }
    }

    public function addFile ($path)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        try
        {
            if (method_exists ($plugin, 'addFile'))
                return ($plugin->addFile ($path));
            else
                return (false);
        }
        catch (Exception $e)
        {
            return (false);
        }
    }

    public function delFile ($hash)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        if (method_exists ($plugin, 'delFile'))
            return ($plugin->delFile ($hash));
        else
            return (false);
    }

    public function getLibrary ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $plugins = new Media_Properties ();
        
        return ($plugin->getItems ());
    }

    public function getPreview ($hash)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $preview = NULL;
        if (method_exists ($plugin, 'getPreview'))
            $preview = $plugin->getPreview ($hash);

        // If the method is unavailable for the plugin or returns nothing,
        // return the default preview image
        if (!$preview)
            $preview = $this->_preview;

        return ($preview);
    }

    public function fileExists ($id)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $result = FALSE;
        if (method_exists ($plugin, 'fileExists'))
            $result = $plugin->fileExists ($id);

        return ($result);
    }

    public function getFilePath ($id)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';

        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $result = FALSE;
        if (method_exists ($plugin, 'getFilePath'))
            $result = $plugin->getFilePath ($id);

        return ($result);
    }

    public function getHTMLProperties ($itemId, &$propertiesId)
    {
        $propertiesTable = new Media_Properties ();
        
        $properties = NULL;
        if (!empty ($itemId) && !empty ($propertiesId))
            $properties = $propertiesTable->getProperty ($itemId, $propertiesId);
        if ($properties === NULL)
        {
            // Get default properties
            require_once 'Media/Plugin/'.$this->_name.'.php';
            $className = 'Media_Plugin_'.$this->_name;
            $plugin = new $className ();
            if (method_exists ($plugin, 'getDefaultProperties'))
            {
                $properties = $plugin->getDefaultProperties ();
                $properties->setItem ($itemId);
                $propertiesId = $properties->getId ();

                // Save properties in DB
                if ($properties)
                    $propertiesTable->saveProperties ($properties);
            }
        }

        // Return properties in HTML format
        if ($properties)
            return ($properties->getHTMLValues ());
    }

    public function getProperty ($properties, $fieldName)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        if (method_exists ($plugin, 'getProperty'))
            return ($plugin->getProperty ($properties, $fieldName));
        else
            return (NULL);
    }

    public function setProperties ($itemId, $propId, $properties)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $propertiesTable = new Media_Properties ();
        $propObj = $propertiesTable->getProperty ($itemId, $propId);
        if (!$propObj)
        {
            // Get default properties
            require_once 'Media/Plugin/'.$this->_name.'.php';
            $className = 'Media_Plugin_'.$this->_name;
            $plugin = new $className ();
            if (method_exists ($plugin, 'getDefaultProperties'))
                $propObj = $plugin->getDefaultProperties ();
        }

        if (!$propObj)
            return ('');

        $propObj->setItem ($itemId);

        if (method_exists ($plugin, 'addProperties'))
        {
            $propObj->clean ();
            $propObj = $plugin->addProperties ($propObj, $properties);

            // Save properties in DB
            $propertiesTable->saveProperties ($propObj);
        }
        return ($propObj);
    }

    public function getPlaylistLine ($item)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $line = '';
        if (method_exists ($plugin, 'serializeProperties'))
            $line = $plugin->serializeProperties ($item);

        // If the method is unavailable for the plugin or returns nothing,
        // return the default preview image
        if (empty ($line))
            $line = '# Error while creating line for '.$this->_name;

        return ($line);
    }

    public function getManifestFiles ($item)
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $result = array ();
        if (method_exists ($plugin, 'getMD5'))
            $result = $plugin->getMD5 ($item);

        return ($result);
    }

    public function getProperties ($line = "")
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $properties = NULL;
        if (method_exists ($plugin, 'unserializeProperties'))
        {
            $properties = $plugin->unserializeProperties ($line);

            $propTable = new Media_Properties ();
            $propTable->saveProperties ($properties);
        }

        if (!$properties && method_exists ($plugin, 'getDefaultProperties'))
            $properties = $plugin->getDefaultProperties ();

        if (!$properties)
            return (new Media_Property ());

        return ($properties);
    }

    public function getDefaultProperties ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $properties = NULL;
        if (method_exists ($plugin, 'getDefaultProperties'))
            $properties = $plugin->getDefaultProperties ();

        if (!$properties)
            $properties = new Media_Property ();
        return ($properties);
    }

    public function getDefaultStartDate ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $date = NULL;
        if (method_exists ($plugin, 'getDefaultStartDate'))
            $date = $plugin->getDefaultStartDate ();

        return ($date);
    }

    public function getDefaultEndDate ()
    {
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();

        $date = NULL;
        if (method_exists ($plugin, 'getDefaultEndDate'))
            $date = $plugin->getDefaultEndDate ();

        return ($date);
    }

    public function getDefaultPropertiesHash ()
    {
        // Get default properties
        require_once 'Media/Plugin/'.$this->_name.'.php';
        $className = 'Media_Plugin_'.$this->_name;
        $plugin = new $className ();
        if (method_exists ($plugin, 'getDefaultProperties'))
        {
            $properties     = $plugin->getDefaultProperties ();

            // Save properties in DB
            if ($properties)
            {
                $propertiesTable = new Media_Properties ();
                $propertiesTable->saveProperties ($properties);
                return ($properties->getId ());
            }
        }
        return (NULL);
    }
}
