<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
abstract class Media_Plugin_Skeleton
{
    protected $_name = 'Undefined';

    abstract public function getItems               ();
    abstract public function getDefaultProperties   ();
    abstract public function addProperties          ($propObj, $newPropArray);
    abstract public function getProperty            ($propObj, $field);
    abstract public function unserializeProperties  ($line);
    abstract public function serializeProperties    ($item);

    public function getName ()              { return ($this->_name); }
    public function areItemsDeletable ()    { return (false); }
    public function areItemsDownloadable () { return (false); }
    public function getDefaultStartDate ()  { return (NULL); }
    public function getDefaultEndDate ()    { return (NULL); }
    public function isManagingFile ($path)  { return (false); }

    protected function _getType ($path)
    {
        $cmd = "/usr/bin/file -p -b --mime-type '$path'";
        $f = popen ("$cmd", 'r');
        $line = fgets ($f, 512);
        pclose ($f);

        return (trim ($line));
    }

    protected function createTextProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'text',
                                             'name'     => $name,
                                             'title'    => $tip,
                                             'value'    => $value);
        return ($property);
    }

    protected function createPasswordProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'password',
                                             'name'     => $name,
                                             'title'    => $tip,
                                             'value'    => $value);
        return ($property);
    }

    protected function createColorProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'div';
        $property ['attributes']    = array (
                                        'id'   => "mooRainbow-$name",
                                        'class'=> "colorInput",
                                        'name' => "$name",
                                        'color'=> "$value",
                                        'title'    => $tip,
                                        'style'=> "background-color: $value;");
        $property ['html']          = '';

        return ($property);
    }

    protected function createDateProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['attributes']    = array (
                                        'id'    => "datepicker-$name",
                                        'class' => "datepicker",
                                        'name'  => "$name",
                                        'value' => "$value",
                                        'title' => $tip
                                        );
        $property ['html']          = '';

        return ($property);
    }

    protected function createTextareaProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'textarea';
        $property ['html']          = $value;
        $property ['attributes']    = array ('name' => $name,
                                             'title'    => $tip,
                                             'style' => "width: 300px; height: 80px;");

        return ($property);
    }

    protected function createSelectProperty ($name, $label, $values, $select, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'select';
        $property ['attributes']    = array ('name'     => $name,
                                             'title'    => $tip);

        $property ['html'] = '';
        foreach ($values as $valueId => $valueLabel)
        {
            $property ['html'] .= "<option value=\"$valueId\"";
            if (strcmp ($valueId, $select) == 0)
                $property ['html'] .= " selected=\"true\"";
            $property ['html'] .= ">$valueLabel</option>\n";
        }
        return ($property);
    }

    protected function getTextValue ($property)
    {
        return ($property['attributes']['value']);
    }

    protected function getColorValue ($property)
    {
        return ($property['attributes']['color']);
    }

    protected function getSelectValue ($property)
    {
        $index  = 0;
        $step   = 1000;

        if (strlen ($property ['html']) <= $step)
        {
            $option = preg_replace ("/(.)*value=\"([^\"]+)\" selected(.)*/is",
                                    "\\2", $property ['html']);
        }
        else
        {
            // Split text since preg_replace is not working with long text
            $arrayString = explode ("\n", $property ['html']);
            $option = preg_replace ("/(.)*value=\"([^\"]+)\" selected(.)*/is",
                                    "\\2", $arrayString [$index]);
            while ($index+1 < count ($arrayString) &&
                    $option !== NULL &&
                    strlen ($option) >= strlen ($arrayString [$index]))
            {
                $index++;
                $option = preg_replace ("/(.)*value=\"([^\"]+)\" selected(.)*/is",
                                        "\\2", $arrayString [$index]);
            }
        }

        return ($option);
    }

    protected function getTextareaValue ($property)
    {
        return ($property ['html']);
    }

    protected function getLibraryPath ()
    {
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        return ($upload->library);
    }

    protected function getTranslation ($string)
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        return ($translator->translate ($string));
    }
}
