<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Commands extends Zend_Db_Table
{
    protected $_name = 'Actions_commands';

    public function getCommands ()
    {
        $result = array ();

        $allCommands = $this->fetchAll ($this->select ()->order ('name'));
        foreach ($allCommands as $command)
            $result [] = new Actions_Command ($command);

        return ($result);
    }

    public function getEnabledCommands ()
    {
        $result = array ();

        $allCommands = $this->fetchAll ($this->select ()
                                            ->where ('enable = ?', '1')
                                            ->order ('name'));
        foreach ($allCommands as $command)
            $result [] = new Actions_Command ($command);

        return ($result);
    }

    public function getCommand ($name)
    {
        $select = $this->select ()->where ('name = ?', $name);
        $command = $this->fetchRow ($select);
        if ($command === NULL)
            return (NULL);

        return (new Actions_Command ($command));
    }

    public function swap ($name)
    {
        $select = $this->select ()->where ('name = ?', $name);
        $command = $this->fetchRow ($select);
        if ($command === NULL)
            return (FALSE);

        if ($command ['enable'] == "0")
        {
            // Enable it !
            $command ['enable'] = "1";
            $command->save ();
        }
        else
        {
            // Disable it !
            $command ['enable'] = "0";
            $command->save ();

            // Remove all associated properties !
            $propTable = new Actions_Properties ();
            $propTable->delPropertiesWithId ($name);
        }
    }

    public function getPluginsProperties ($context)
    {
        $result = array ();

        $allCommands = $this->fetchAll ($this->select ()
                                            ->where ('enable = ?', '1')
                                            ->order ('name'));
        foreach ($allCommands as $command)
        {
            $cmd = new Actions_Command ($command);
            $html = $cmd->getContextProperties ($context);
            if ($html)
                $result [] = array ('cmd'   => $cmd->getName (),
                                    'html'  => $html);
        }
        return ($result);
    }
}
