<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Properties extends Zend_Db_Table
{
    protected $_name = 'Actions_properties';

    public function getProperties ($itemId, $context)
    {
        if (empty ($context))
            $context = '-';

        $select = $this->select ()->where ('item_id = ?', $itemId)
                                  ->where ('context = ?', $context);
        $property = $this->fetchRow ($select);
        if ($property === NULL)
            return (NULL);

        return (new Actions_Property ($property));
    }

    public function delPropertiesWithId ($itemId)
    {
        $where = $this->getAdapter ()->quoteInto ('item_id = ?', $itemId);
        $this->delete ($where);
    }

    public function saveProperties ($property)
    {
        $itemId     = $property->getItem ();
        $context    = $property->getContext ();
        $values     = $property->getSerializedValues ();

        $select = $this->select ()->where ('item_id = ?', $itemId)
                                  ->where ('context = ?', $context);
        $property = $this->fetchRow ($select);
        if ($property != NULL)
        {
            $property ['action_property'] = $values;
            $property->save ();
            return (true);
        }
        else
        {
            $confirm = $this->insert (
                           array ('item_id'         => $itemId,
                                  'context'         => $context,
                                  'action_property' => $values));
            return ($confirm);
        }
    }
}
