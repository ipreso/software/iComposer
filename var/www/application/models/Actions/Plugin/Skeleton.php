<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
abstract class Actions_Plugin_Skeleton
{
    protected $_name = 'Undefined';

    public function getName () { return ($this->_name); }

    public function getPlayerCommand ($hash)        { return (false); }
    public function msgFromPlayer ($hash, $msg)     { return ("NOT IMPLEMENTED"); }

    public function setParamGroup ($id, $param, $value)     { return (false); }
    public function setParamBox ($hash, $param, $value)     { return (false); }

    public function getCommands ()                  { return (array ()); }
    public function getStringCommand ($id)          { return (""); }
    
    public function doAction ($context, $function)
    {
        if (method_exists ($this, $function))
            return ($this->$function ($context));
        else
            return ("'$function' is not implemented.");
    }

    public function getContextProperties ($context)
    {
        $skeletonProperties = array ();
        $skeletonProperties [] = $this->createLabel ("Please implement function 'getContextProperties' for this plugin.", '#aa0000');

        $property = new Actions_Property ();

        $property->setItem          ($this->_name);
        $property->setContext       ($context);
        $property->setProperties    ($skeletonProperties);

        return ($property->getHTMLValues ());
    }

/*****************************************************************************
 * Elements for GUI
 *****************************************************************************/

    protected function createLabel ($text, $color, $tip = "")
    {
        $property = array ();
        $property ['label']         = '';
        $property ['element']       = 'font';
        $property ['html']          = "$text";
        $property ['attributes']    = array ('title'    => $tip,
                                             'color'    => $color);

        return ($property);
    }

    protected function createButton ($desc, $label, $functionToCall, $id, $tip = "")
    {
        $property = array ();
        $property ['label']         = $desc;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'submit',
                                             'class'    => 'doButton',
                                             'value'    => $label,
                                             'id'       => $id,
                                             'cmd'      => $this->_name,
                                             'title'    => $tip,
                                             'fct'      => $functionToCall);
        return ($property);
    }

    protected function createTextProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'input';
        $property ['html']          = '';
        $property ['attributes']    = array ('type'     => 'text',
                                             'name'     => $this->_name."-$name",
                                             'title'    => $tip,
                                             'value'    => $value);
        return ($property);
    }

    protected function createColorProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'div';
        $property ['attributes']    = array (
                                        'id'   => "mooRainbow-$name",
                                        'class'=> "colorInput",
                                        'name' => $this->_name."-$name",
                                        'color'=> "$value",
                                        'title'    => $tip,
                                        'style'=> "background-color: $value;");
        $property ['html']          = '';

        return ($property);
    }

    protected function createTextareaProperty ($name, $label, $value, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'textarea';
        $property ['html']          = $value;
        $property ['attributes']    = array ('name' => $this->_name."-$name",
                                             'title'    => $tip,
                                             'style' => "width: 300px; height: 80px;");

        return ($property);
    }

    protected function createSelectProperty ($name, $label, $values, $select, $tip = "")
    {
        $property = array ();
        $property ['label']         = $label;
        $property ['element']       = 'select';
        $property ['attributes']    = array ('name' => $this->_name."-$name",
                                             'title' => $tip);

        $property ['html'] = '';
        foreach ($values as $valueId => $valueLabel)
        {
            $property ['html'] .= "<option value=\"$valueId\"";
            if (strcmp ($valueId, $select) == 0)
                $property ['html'] .= " selected=\"true\"";
            $property ['html'] .= ">$valueLabel</option>\n";
        }
        return ($property);
    }

    protected function getTranslation ($string)
    {
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        return ($translator->translate ($string));
    }
}
