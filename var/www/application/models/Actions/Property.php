<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Property
{
    private $_itemId;
    private $_context;
    private $_value;

    public function __construct ($row = "")
    {
        if (!empty ($row))
        {
            $rowArray = $row->toArray ();
            $this->_itemId  = $rowArray ['item_id'];
            $this->_context = $rowArray ['context'];
            $this->_value   = unserialize ($rowArray ['action_property']);
        }
        else
        {
            $this->_itemId  = '';
            $this->_context = '';
            $this->_value   = array ();
        }
    }

    public function setItem ($item)
    {
        $this->_itemId = $item;
    }
    public function setContext ($context)
    {
        $this->_context = $context;
    }
    public function getItem ()
    {
        return ($this->_itemId);
    }
    public function getContext ()
    {
        return ($this->_context);
    }
    public function setProperties ($properties)
    {
        if (!is_array ($properties))
            return (NULL);

        $this->_value = $properties;
    }

    public function getSerializedValues ()
    {
        return (serialize ($this->_value));
    }

    public function getHTMLValues ()
    {
        if (!is_array ($this->_value))
            return (NULL);

        $html = "<table style=\"width: 100%;\">\n";
        foreach ($this->_value as $property)
        {
            // Label
            $html .= "<tr>"
                        ."<td class=\"tdRight\">"
                            ."<span>".$property ['label']."</span>\n"
                        ."</td><td class=\"tdLeft\">";

            // Element and its attributes
            $html .= "<".$property ['element'];
            if (is_array ($property ['attributes']))
            {
                foreach ($property ['attributes'] as $key => $value)
                    $html .= " $key=\"$value\"";
            }
            $html .= ">";

            $html .= $property ['html'];
            $html .= "</".$property ['element'].">\n";

            $html .= "</td></tr>\n";
        }
        $html .= "</table>\n";

        return ($html);
    }
}
