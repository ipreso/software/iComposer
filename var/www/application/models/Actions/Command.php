<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Command
{
    private $_id;
    private $_name;
    private $_description;
    private $_version;
    private $_enable;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();

        $this->_id          = $rowArray ['id'];
        $this->_name        = $rowArray ['name'];
        $this->_description = $rowArray ['description'];
        $this->_version     = $rowArray ['version'];

        if ($rowArray ['enable'] == 0)
            $this->_enable = false;
        else
            $this->_enable = true;
    }

    public function getId ()            { return ($this->_id); }
    public function getDescription()    { return ($this->_description); }
    public function getVersion ()       { return ($this->_version); }
    public function getEnable ()        { return ($this->_enable); }

    public function getName ()
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        try
        {
            if (method_exists ($command, 'getName'))
                return ($command->getName ());
            else
                return ($this->_name);
        }
        catch (Exception $e)
        {
            return ($this->_name);
        }
    }

    public function getInternalName ()
    {
        return ($this->_name);
    }

    public function getContextProperties ($context)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->getContextProperties ($context));
    }

    public function doAction ($context, $fct)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->doAction ($context, $fct));
    }

    public function getPlayerCommand ($hash)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->getPlayerCommand ($hash));
    }

    public function msgFromPlayer ($hash, $msg)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->msgFromPlayer ($hash, $msg));
    }

    public function getCommands ()
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->getCommands ());
    }

    public function getStringCommand ($id)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->getStringCommand ($id));
    }

    public function setParamBox ($hash, $param, $value)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->setParamBox ($hash, $param, $value));
    }
    public function setParamGroup ($id, $param, $value)
    {
        require_once 'Actions/Plugin/'.$this->_name.'.php';
        $className = 'Actions_Plugin_'.$this->_name;
        $command = new $className ();

        return ($command->setParamGroup ($id, $param, $value));
    }
}
