<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_Agreements extends Zend_Db_Table
{
    protected $_name = 'Admin_agreement';

    public function getLastLogs ($limit, $order = "DESC")
    {
        $result;

        $lastLogs = $this->fetchAll ($this->select ()
                                     ->setIntegrityCheck(false)
                                     ->from (array ('l' => 'Admin_agreement'))
                                     );
        
        $result=$lastLogs[0]['contains'];
        return ($result);
    }
    
    public function modifyAgreement($id,$contains)
    {
    	  $return = array ();

        $rowset = $this->find ($id);
        if (count ($rowset) == 0)
        {
          $this->_addAgreement ($contains);
        }

        $row = $rowset->current ();

        $this->_modifyField ($row, 'contains', $contains, $return);

        $row->save ();

        return ($return);
    }
    
    private function _modifyField (&$row, $field, $value, &$return)
    {
        if (strcmp ($row [$field], $value) != 0)
        {
            $return [] = array ('field' => $field,
                                'old'   => $row [$field],
                                'new'   => $value);

            $row [$field] = $value;
        }
    }
    
    private function _addAgreement ($contains)
    {
        $data = array ('id'      => 1,
                       'contains'  => $contains);

        $this->insert ($data);
    }
}
