<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_Resources extends Zend_Db_Table
{
    public static $ALLOW_PARENTS    = 1;
    public static $ALLOW_CHILDREN   = 2;
    public static $ALLOW_ALL        = 4;

    protected $_name = 'Admin_resources';

    public function exists ($type, $action, $id = "")
    {
        $select = $this->select ()
                    ->where ('typename = ?',    $type)
                    ->where ('action = ?',      $action);

        if (strlen ($id) && strcmp ($id, "_GLOBAL_") != 0)
            $select = $select->where ('typeid = ?', $id);
        else
            $select = $select->where ('typeid IS NULL OR typeid="_GLOBAL_"');

        $result = $this->fetchAll ($select);
        if ($result && count ($result) > 0)
            return (true);
        else
            return (false);
    }

    public function getResourceId ($type, $action, $id = "")
    {
        $select = $this->select ()
                    ->where ('typename = ?',    $type)
                    ->where ('action = ?',      $action)
                    ->limit (1);

        if (strlen ($id) && strcmp ($id, "_GLOBAL_") != 0)
            $select = $select->where ('typeid = ?', $id);
        else
            $select = $select->where ('typeid IS NULL OR typeid="_GLOBAL_"');

        $result = $this->fetchRow ($select);
        if ($result && count ($result) > 0)
        {
            $resource = $result->toArray ();
            return ($resource ['id']);
        }
        else
            return (false);
    }

    public function create ($type, $action, $id = "")
    {
        $res_id = $this->insert (
            array ('typename'   => $type,
                    'typeid'    => $id,
                    'action'    => $action));

        return ($res_id);
    }

    public function deleteResource ($type, $id)
    {
        $permissions = new Admin_Permissions ();
        $select = $this->select ()
                        ->where ('typename = ?', $type)
                        ->where ('typeid = ?', $id);

        $resources = $this->fetchAll ($select);
        foreach ($resources as $res)
        {
            $resource = $res->toArray ();
            $permissions->delResPermissions ($resource ['id']);
            $where = $this->getAdapter ()->quoteInto ('id = ?', $resource ['id']);
            $this->delete ($where);
        }
    }

    public function updateResource ($type, $oldId, $newId)
    {
        $set = array ('typeid' => $newId);
        $where = $this->getAdapter ()->quoteInto ('typeid = ?', $oldId)
                .$this->getAdapter ()->quoteInto ('AND typename = ?', $type);

        if ($this->update ($set, $where) > 0)
            return (TRUE);
        else
            return (FALSE);
    }

    public function getTypes ()
    {
        $select = $this->select ()
                    ->distinct ()
                    ->from (array ('r' => 'Admin_resources'),
                            array ('r.typename'));

        $types = $this->fetchAll ($select);
        $result = array ();

        foreach ($types as $type)
        {
            $typeArray = $type->toArray ();
            $result [] = $typeArray ['typename'];
        }

        return ($result);
    }

    public function getIdsForType ($type)
    {
        $select = $this->select ()
                    ->distinct ()
                    ->from (array ('r' => 'Admin_resources'),
                            array ('r.typeid'))
                    ->where ('typename = ?', $type);

        $ids = $this->fetchAll ($select);
        $result = array ();

        foreach ($ids as $id)
        {
            $idArray = $id->toArray ();
            $result [] = $idArray ['typeid'];
        }

        return ($result);
    }

    public function getActions ($type, $id)
    {
        $select = $this->select ()
                        ->distinct ()
                        ->from (array ('r' => 'Admin_resources'),
                                array ('r.action'))
                        ->where ('typename = ?', $type);

        if ($id !== NULL)
            $select = $select->where ('typeid = ?', $id);
        else
            $select = $select->where ('typeid IS NULL');

        $actions = $this->fetchAll ($select);
        $result = array ();
        foreach ($actions as $action)
        {
            $actionArray = $action->toArray ();
            $result [] = $actionArray ['action'];
        }

        return ($result);
    }

    public function getStringForResource ($type, $id)
    {
        if ($id === NULL || strcmp ($id, "_GLOBAL_") == 0)
            return (NULL);

        $result = NULL;
        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
        switch ($type)
        {
            case "Administration":
                if (strcmp ($id, "logsusers") == 0)
                    $result = "Logs Users";
                else if (strcmp ($id, "logsboxes") == 0)
                    $result = "Logs iBoxes";
                else if (strcmp ($id, "plugins") == 0)
                    $result = "Plugins";
                else if (strcmp ($id, "configuration") == 0)
                    $result = "Configuration";
                else if (strcmp ($id, "agreement") == 0)
                    $result = "Agreement";
                break;
            case "Emission":
                $obj = new Progs_Programs ();
                $item = $obj->findValidProgram ($id);
                if (!$item)
                    $this->deleteResource ($type, $id);
                else
                    $result = $item->getName ();
                break;
            case "Parc":
                $obj = new Players_Groups ();
                $item = $obj->findGroupById ($id);
                if (!$item)
                    $result = -1;
                else
                    $result = $item->getName ();
                break;
            case "Player":
                $obj = new Players_Boxes ();
                $item = $obj->findiBoxWithHash ($id);
                if (!$item)
                    $result = -1;
                else
                    $result = $item->getLabel ();
                break;
            case "Programmation":
                $obj = new Progs_Channels ();
                $item = $obj->findValidChannel ($id);
                if (!$item)
                    $result = -1;
                else
                    $result = $item->getName ();
                break;
            default:
                    $result = NULL;
        }

        return ($result);
    }

    public function getDefaultPermissions ($type, $action, $id)
    {
        $result = NULL;

        switch ($type)
        {
            case "Administration":
                $result = self::$ALLOW_ALL;
                break;
            case "Emission":
                $result = self::$ALLOW_PARENTS | self::$ALLOW_CHILDREN;
                break;
            case "Parc":
                $result = self::$ALLOW_PARENTS | self::$ALLOW_CHILDREN;
                break;
            case "Player":
                $result = self::$ALLOW_ALL;
                break;
            case "Programmation":
                $result = self::$ALLOW_PARENTS | self::$ALLOW_CHILDREN;
                break;
            default:
                $result = self::$ALLOW_ALL;
        }

        return ($result);
    }
}
