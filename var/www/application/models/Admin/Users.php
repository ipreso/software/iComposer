<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_Users extends Zend_Db_Table
{
    protected $_name = 'Admin_users';

    public function findLogin ($username)
    {
        $select = $this->select()->where('login = ?', $username);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $user = new Admin_User ($row);
        return ($user);
    }

    public function findUserById ($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $user = new Admin_User ($row);
        return ($user);
    }

    public function delUser ($user)
    {
        if ($user <= 1)
            return (FALSE);

        $where = $this->getAdapter()->quoteInto('id = ?', $user);
        $this->delete ($where);
    }

    public function setConnected ($id)
    {
        // Update the "last_connection" field for user ID
        $rowset = $this->find ($id);
        $row = $rowset->current ();
        $row['last_connection'] = new Zend_Db_Expr('NOW()');
        $row->save ();
    }

    public function getUsersArrayByGroup ($groupId)
    {
        $result = array ();
        
        $select = $this->select ()->where ('id_group = ?', $groupId)
                                  ->order (array ('lastname', 'firstname'));
        $row = $this->fetchAll ($select);
        foreach ($row as $user)
        {
            $result [] = $user->toArray ();
        }
        return ($result);
    }

    public function getUsersArrayWithoutGroup ()
    {
        $result = array ();

        $select = $this->select ()
                    ->from (array ('u' => 'Admin_users'))
                    ->joinLeft (array ('g' => 'Admin_groups'),
                                'g.id = u.id_group',
                                array ())
                    ->where ('g.id is NULL');

        $row = $this->fetchAll ($select);

        foreach ($row as $user)
        {
            $result [] = $user->toArray ();
        }
        return ($result);
    }

    public function setUserGroup ($idUser, $idGroup)
    {
        // Update the "id_group" field for user ID
        $rowset = $this->find ($idUser);
        $row = $rowset->current ();
        if ($row['id_group'] != $idGroup &&
            $row['id_group'] != 0 &&
            $idGroup != 0)
        {
            $row['id_group'] = $idGroup;
            $row->save ();
            return (true);
        }
        return (false);
    }
    
    public function addUser ($login,
                             $firstname,
                             $lastname,
                             $password = "",
                             $email = "",
                             $id_group = -1)
    {
        // Check if user already exists
        if ($this->findLogin ($login))
            return (NULL);

        $data = array ('login'      => $login,
                       'firstname'  => $firstname,
                       'lastname'   => $lastname,
                       'password'   => 
                            new Zend_Db_Expr (
                                    $this->getAdapter()
                                         ->quoteInto('SHA(MD5(?))', $password)),
                       'email'      => $email,
                       'id_group'   => $id_group,
                       'advanced'   => 0,
                       'language'   => 'auto');

        return ($this->insert ($data));
    }

    public function modifyUser ($id,
                                $login,
                                $firstname,
                                $lastname,
                                $password,
                                $email,
                                $advanced = "",
                                $lang = "")
    {
        $return = array ();

        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (array ());

        $row = $rowset->current ();

        $this->_modifyField ($row, 'login', $login, $return);
        $this->_modifyField ($row, 'firstname', $firstname, $return);
        $this->_modifyField ($row, 'lastname', $lastname, $return);
        $this->_modifyField ($row, 'email', $email, $return);
        $this->_modifyField ($row, 'language', $lang, $return);
        if (strlen ($advanced))
        {
            if ($advanced)
                $this->_modifyField ($row, 'advanced', 1, $return);
            else
                $this->_modifyField ($row, 'advanced', 0, $return);
        }

        if (strlen ($password))
        {
            $row ['password'] = new Zend_DB_Expr (
                                      $this->getAdapter ()
                                           ->quoteInto ('SHA(MD5(?))', $password));
            $return [] = array ('field' => 'password',
                                'old'   => '[xxx]',
                                'new'   => '[yyy]');
        }


        $row->save ();

        return ($return);
    }
    
    public function modifyDisplayAgreement($id,$displayAgreement)
    {
    	$return = array ();

        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (array ());

        $row = $rowset->current ();

        $this->_modifyField ($row, 'display_agreement', $displayAgreement, $return);

        $row->save ();

        return ($return);
    }
    
    public function modifyDisplayAgreementOnConnection($id,$displayAgreementOnConnection)
    {
    	$return = array ();

        $rowset = $this->find ($id);
        if (count ($rowset) < 1)
            return (array ());

        $row = $rowset->current ();

        $this->_modifyField ($row, 'display_agreement_on_connection', $displayAgreementOnConnection, $return);

        $row->save ();

        return ($return);
    }

    private function _modifyField (&$row, $field, $value, &$return)
    {
        if (strcmp ($row [$field], $value) != 0)
        {
            $return [] = array ('field' => $field,
                                'old'   => $row [$field],
                                'new'   => $value);

            $row [$field] = $value;
        }
    }
}
