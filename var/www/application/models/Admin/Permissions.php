<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_Permissions extends Zend_Db_Table
{
    protected $_name = 'Admin_perms';
    protected $_permissions;
    protected $_groupId = FALSE;

    public function filterGroup ($groupId)
    {
        $select = $this->select ()
                        ->setIntegrityCheck (false)
                        ->from (array ('P' => 'Admin_perms'),
                                array ('P.id_group'))
                        ->where ('id_group = ?', $groupId)
                        ->join (array ('R' => 'Admin_resources'),
                                'R.id = P.id_res',
                                array ('R.typename',
                                       'R.typeid',
                                       'action'));
        $this->_permissions = array ();
        $perms = $this->fetchAll ($select);
        foreach ($perms as $permission)
        {
            $permArray = $permission->toArray ();
            if (!strlen ($permArray ['typeid']))
                $permArray ['typeid'] = '_GLOBAL_';

            $this->_permissions [$permArray ['typename']][$permArray ['typeid']][$permArray ['action']] = true;
        }
        $this->_groupId = $groupId;
    }

    public function getPermissions ($groupId = "")
    {
        if (!strlen ($groupId))
            $groupId = $this->_groupId;

        $select = $this->select ()
                        ->setIntegrityCheck (false)
                        ->from (array ('P' => 'Admin_perms'),
                                array ('P.id_group'))
                        ->where ('id_group = ?', $groupId)
                        ->join (array ('R' => 'Admin_resources'),
                                'R.id = P.id_res',
                                array ('R.typename',
                                       'R.typeid',
                                       'action'));
        $permissions = array ();
        $perms = $this->fetchAll ($select);
        foreach ($perms as $permission)
        {
            $permArray = $permission->toArray ();
            if (!strlen ($permArray ['typeid']))
                $permArray ['typeid'] = '_GLOBAL_';

            $permissions [$permArray ['typename']][$permArray ['typeid']][$permArray ['action']] = true;
        }
        return ($permissions);
    }

    public function setPermissions ($groupId, $permissions)
    {
        $resources = new Admin_Resources ();

        $this->delGroupPermissions ($groupId);
        foreach ($permissions as $type => $permsForType)
        {
            foreach ($permsForType as $id => $permsForId)
            {
                foreach ($permsForId as $action => $allowed)
                {
                    $resId = $resources->getResourceId ($type, $action, $id);
                    $this->addPermission ($resId, $groupId);
                }
            }
        }
    }

    public function isAllowed ($type, $action, $id = "")
    {
        if (!strlen ($id))
            $id = '_GLOBAL_';

        if (array_key_exists ($type,    $this->_permissions) &&
            array_key_exists ($id,      $this->_permissions [$type]) &&
            array_key_exists ($action,  $this->_permissions [$type][$id]) &&
            $this->_permissions [$type][$id][$action])
            return (true);

        // Does the resource exists ? If not, creating it
        $resources = new Admin_Resources ();
        if (!$resources->exists ($type, $action, $id))
        {
            $resId          = $resources->create ($type, $action, $id);
            $defaultPerms   = $resources->getDefaultPermissions ($type, $action, $id);

            // Getting the current group
            if ($this->_groupId === FALSE)
                return (false);
            $groups = new Admin_Groups ();
            $currentGroup = $groups->findGroupById ($this->_groupId);
            if (!$currentGroup)
                return (false);

            if ($defaultPerms & Admin_Resources::$ALLOW_ALL)
            {
                // All group can have access to this new resource
                $groups = new Admin_Groups ();
                $groupsList = $groups->getGroupsArray ();
                foreach ($groupsList as $group)
                    $permId = $this->addPermission ($resId, $group ['id']);
            }
            else
            {
                if ($defaultPerms & Admin_Resources::$ALLOW_PARENTS)
                {
                    // All parents are allowed to access this new resource
                    $parents = $currentGroup->getParents ();
                    if (count ($parents))
                    {
                        foreach ($parents as $parent)
                            $this->addPermission ($resId, $parent);
                    }
                }
                if ($defaultPerms & Admin_Resources::$ALLOW_CHILDREN)
                {
                    // All children are allowed to access this new resource
                    $children = $groups->getGroupChildren ($this->_groupId);
                    if (count ($children))
                    {
                        foreach ($children as $child)
                            $this->addPermission ($resId, $child);
                    }
                }

                if (!$this->hasPermission ($resId, $this->_groupId))
                {
                    // Always permissions to us...
                    $this->addPermission ($resId, $this->_groupId);
                }
            }

            $this->filterGroup ($this->_groupId);
            if (array_key_exists ($type,    $this->_permissions) &&
                array_key_exists ($id,      $this->_permissions [$type]) &&
                array_key_exists ($action,  $this->_permissions [$type][$id]) &&
                $this->_permissions [$type][$id][$action])
                return (true);
        }

        return (false);
    }

    protected function hasPermission ($resource_id, $group_id)
    {
        $select = $this->select ()
                            ->where ('id_res = ?',    $resource_id)
                            ->where ('id_group = ?',  $group_id)
                            ->limit (1);
        
        $result = $this->fetchRow ($select);
        if ($result && count ($result) > 0)
            return (true);
        else    
            return (false);
    }

    public function addPermission ($res_id, $group_id)
    {
        $perm_id = $this->insert (
            array ('id_res'     => $res_id,
                   'id_group'   => $group_id));

        return ($perm_id);
    }

    public function logicANDPermissions ($groupBase, $groupTarget)
    {
        $select = $this->select ()
                    ->from      (array ('P1' => 'Admin_perms'))
                    ->joinLeft  (array ('P2' => 'Admin_perms'),
                                 "P1.id_res=P2.id_res AND P2.id_group=$groupBase",
                                 array ())
                    ->where     ("P2.id_group IS NULL")
                    ->where     ("P1.id_group = ?", $groupTarget);

        $rows = $this->fetchAll ($select);
        foreach ($rows as $permission)
        {
            $permission = $permission->toArray ();
            $this->delPermission ($permission ['id_res'], $groupTarget);
        }
    }

    public function delResPermissions ($res_id)
    {
        $where = $this->getAdapter ()->quoteInto ('id_res = ?', $res_id);
        $this->delete ($where);
    }

    public function delGroupPermissions ($group_id)
    {
        $where = $this->getAdapter ()->quoteInto ('id_group = ?', $group_id);
        $this->delete ($where);
    }

    public function delPermission ($res_id, $group_id)
    {
        $where = $this->getAdapter ()->quoteInto ('id_group = ?', $group_id);
        $where .= $this->getAdapter ()->quoteInto ('AND id_res = ?', $res_id);
        $this->delete ($where);
    }
    
    public function isQuickUpdaterUser($acl)
    {
    	$quickUpdater	= false;
        $emissionsObj   = new Progs_Programs ();
        $emissions      = $emissionsObj->getProgsArray ();
        foreach ($emissions as $emission)
        {
            if ($acl &&
                $acl->isAllowed ('Emission', 'quickupdate', $emission ['id']))
                $quickUpdater  = true;
        }
        return $quickUpdater;
    }
    
}
