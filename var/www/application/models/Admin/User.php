<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_User
{
    private $_id;
    private $_login;
    private $_name;
    private $_firstname;
    private $_lastname;
    private $_groupId;
    private $_email;
    private $_advanced;
    private $_language;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_id          = $rowArray ['id'];
        $this->_login       = $rowArray ['login'];
        $this->_firstname   = $rowArray ['firstname'];
        $this->_lastname    = $rowArray ['lastname'];
        $this->_name        = $this->_firstname." ".$this->_lastname;
        $this->_groupId     = $rowArray ['id_group'];
        $this->_email       = $rowArray ['email'];
        $this->_advanced    = $rowArray ['advanced'];
        $this->_language    = $rowArray ['language'];
        $this->_displayAgreement    = $rowArray ['display_agreement'];
        $this->_displayAgreementOnConnection    = $rowArray ['display_agreement_on_connection'];
    }

    public function getId ()
    {
        return ($this->_id);
    }

    public function getName ()
    {
        return ($this->_name);
    }

    public function getGroupId ()
    {
        return ($this->_groupId);
    }

    public function getGroup ()
    {
        $groups = new Admin_Groups ();
        return ($groups->findGroupById ($this->_groupId));
    }

    public function getAdvancedProgrammation ()
    {
        return ($this->_advanced);
    }

    public function getLogin ()
    {
        return ($this->_login);
    }

    public function getFirstname ()
    {
        return ($this->_firstname);
    }

    public function getLastname ()
    {
        return ($this->_lastname);
    }

    public function getEmail ()
    {
        return ($this->_email);
    }

    public function getLanguage ()
    {
        return ($this->_language);
    }
    
    public function getDisplayAgreement ()
    {
        return ($this->_displayAgreement);
    }
    
        public function getDisplayAgreementOnConnection ()
    {
        return ($this->_displayAgreementOnConnection);
    }
}
