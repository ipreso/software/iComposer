<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Admin_Groups extends Zend_Db_Table
{
    protected $_name = 'Admin_groups';

    public function findGroup ($group)
    {
        $select = $this->select()->where('groupname = ?', $group);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $group = new Admin_Group ($row);
        return ($group);
    }

    public function findGroupById ($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $group = new Admin_Group ($row);
        return ($group);
    }

    public function addGroup ($group, $path = 0)
    {
        // Returns the ID of the group created
        $groupId = $this->insert (array ('groupname' => $group,
                                         'path'      => $path));

        // Create permissions
        $permissions = new Admin_Permissions ();
        $parents = explode ('/', $path);
        $allIsAllowed = $permissions->getPermissions ($parents [count ($parents)-1]);
        $permissions->setPermissions ($groupId, $allIsAllowed);

        return ($groupId);
    }

    public function delGroup ($group)
    {
        if ($group <= 1)
            return (FALSE);

        $permissions = new Admin_Permissions ();
        $permissions->delGroupPermissions ($group);

        $where = $this->getAdapter()->quoteInto('id = ?', $group);
        $this->delete ($where);
    }

    public function setGroupName ($id, $newName)
    {
        if ($id <= 1)
            return (FALSE);

        $set = array ('groupname' => $newName);
        $where = $this->getAdapter ()->quoteInto ('id = ?', $id);
        if ($this->update ($set, $where) > 0)
            return (TRUE);
        else
            return (FALSE);
    }

    public function setGroupPath ($id, $path)
    {
       if ($id < 1)
            return (FALSE); 

        $set = array ('path' => $path);
        $where = $this->getAdapter ()->quoteInto ('id = ?', $id);
        if ($this->update ($set, $where) > 0)
            return (TRUE);
        else
            return (FALSE);
    }

    public function getGroupsArray ()
    {
        $result = array ();

        $allGroups = $this->fetchAll ($this->select ()->order ('id'));

        foreach ($allGroups as $group)
        {
            $result [] = $group->toArray ();
        }

        return ($result);
    }

    public function getGroupChildren ($parentGroup)
    {
        $result = array ();

        $select = $this->select()->where("path LIKE '%/$parentGroup' OR path = '$parentGroup'");
        $rows = $this->fetchAll ($select);
        foreach ($rows as $row)
        {
            $group = $row->toArray ();
            $result = array_merge ($result, array ($group ['id']), $this->getGroupChildren ($group ['id']));
        }
        return ($result);
    }
}
