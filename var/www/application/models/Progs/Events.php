<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Events
{
    private $events;

    public function __construct ($iCalContent = "")
    {
        $this->events = array ();

        if (!strlen ($iCalContent) || strpos ($iCalContent, "\n") === false)
            return (false);

        $lines = explode ("\n", $iCalContent);
        foreach ($lines as $line)
        {
            switch ($line)
            {
                case "BEGIN:VEVENT":
                    $eventLines = array ();
                    break;
                case "END:VEVENT":
                    $event = new Progs_Event ($eventLines);
                    if ($event)
                        $this->events [] = $event;
                    unset ($eventLines);
                    break;
                default:
                    if (isset ($eventLines) && is_array ($eventLines))
                        $eventLines [] = $line;
            }
        }
    }

    public function getEvents ()
    {
        return ($this->events);
    }

    public function getiCal ()
    {
        $ical = "BEGIN:VCALENDAR\n";
        foreach ($this->events as $event)
        {
            $ical .= $event->getiCalFormat ();
        }
        $ical .= "END:VCALENDAR\n";

        return ($ical);
    }

    public function addEvent ($event)
    {
        $this->events [] = $event;
        return ($this->events);
    }
}
