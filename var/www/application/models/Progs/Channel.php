<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Channel
{
    private $_id;
    private $_label;
    private $_schedule;
    private $_creation;
    private $_modification;
    private $_deletion;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_id              = $rowArray ['id'];
        $this->_label           = $rowArray ['label'];
        $this->_schedule        = $rowArray ['schedule'];
        $this->_creation        = $rowArray ['creation'];
        $this->_modification    = $rowArray ['modification'];
        $this->_deletion        = $rowArray ['deletion'];
    }

    public function getId ()        { return ($this->_id); }
    public function getName ()      { return ($this->_label); }
    public function getSchedule ()  { return ($this->_schedule); }

    public function isDeleted ()
    {
        return ($this->_deletion != NULL);
    }

    public function createBackup ($withLibrary = false)
    {
        // Create unique temporary directory. Could not be in /opt/tmp because
        // of the size limitation of this partition
        $upload     = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $tmpdir     = tempnam ($upload->library, 'prog-');
        $archiveDir = $upload->library;

        unlink ($tmpdir);
        mkdir ($tmpdir, 0770);

        // Put signed meta-files in this directory
        if (!$this->_createMetaBackup ($tmpdir))
        {
            system ("rm -rf $tmpdir");
            return (false);
        }

        // Create backup of every scheduled emissions
        if (!$this->_createEmissionsBackup ($tmpdir))
        {
            system ("rm -rf $tmpdir");
            return (false);
        }

        if ($withLibrary)
        {
            if (!$this->_copyLibrary ($tmpdir))
            {
                system ("rm -rf $tmpdir");
                return (false);
            }
        }

        // Create the info file
        $info =     "type:Programmation\n";
        $info .=    "label:".$this->_label."\n";
        $info .=    "creation:".$this->_creation."\n";
        $info .=    "modification:".$this->_modification."\n";
        file_put_contents ("$tmpdir/ipreso-backup.ini", $info);

        // Compress the directory into an unique archive file
        $archiveFile = $archiveDir."/prog-".$this->_label.".bkp";
        system ("/bin/tar -C $tmpdir -cf '$archiveFile' . 2>/dev/null 1>&2");

        // Delete the directory
        system ("rm -rf $tmpdir");

        if (!file_exists ("$archiveFile"))
            return (false);

        // Return an UID on this archive-file
        return (basename ("$archiveFile"));
    }

    private function _createMetaBackup ($path)
    {
        // Create subdirectory
        @mkdir ("$path/meta", 0770);

        // Get meta file
        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->findSchedule ($this->getSchedule ());

        if (!$schedule)
            return (false);

        $signedSchedule = $this->_signFile ($schedule->getContent ());

        file_put_contents ("$path/meta/schedule.ini", $signedSchedule);
        return (true);
    }

    private function _createEmissionsBackup ($path)
    {
        // Get schedule of the programmation
        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->findSchedule ($this->getSchedule ());
        if (!$schedule)
            return (false);

        // Get every emissions in schedule
        $emissionsList = $schedule->getEmissions ();

        // Create backup of each emissions
        $emissionsObj = new Progs_Programs ();
        foreach ($emissionsList as $emission)
        {
            $emissionObj = $emissionsObj->findProgramByName ($emission);
            if (!$emissionObj)
                return (false);
            if (!$emissionObj->createBackup (false, $path))
                return (false);
        }
        return (true);
    }

    private function _signFile ($content)
    {
        $ini = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        // Escape single quotes
        $content = str_replace ("'", "'\''", $content);

        // Create the content file
        $cmd = "echo -n '$content' | "
                ."/usr/bin/openssl smime -sign -inkey '".$ini->key."' "
                ."-signer '".$ini->cert."'";

        $signedContent = $this->_execute ($cmd);
        return ($signedContent);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }

    private function _copyLibrary ($path)
    {
        @mkdir ("$path/library", 0770);

        // Get schedule of the programmation
        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->findSchedule ($this->getSchedule ());
        if (!$schedule)
            return (false);

        // Get manifest of every emissions
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
        $emissionsObj = new Progs_Programs ();
        $manifestsObj = new Progs_Manifests ();
        $emissionsList = $schedule->getEmissions ();
        $filesList = array ();
        foreach ($emissionsList as $emissionName)
        {
            $emission = $emissionsObj->findProgramByName ($emissionName);
            if (!$emission)
                return (false);
            $manifest = $manifestsObj->findManifest ($emission->getManifest ());
            if (!$manifest)
                return (false);
            $content = $manifest->getContent ();
            $strings = explode ("\n", $content);
            foreach ($strings as $line)
            {
                if (strlen ($line) < 30 || $line [0] == '#')
                    continue;

                list($hash, $name) = explode (":", $line);
                $filesList [] = $upload->library."/$name";
            }
        }

        // Merge manifests files into one with no duplicated files
        $filesList = array_unique ($filesList);

        // Copy each file from the Composer's library
        foreach ($filesList as $file)
        {
            if (!copy ($file, "$path/library/".basename($file)))
                return (false);
        }

        return (true);
    }
}
