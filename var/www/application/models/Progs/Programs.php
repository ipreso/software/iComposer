<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Programs extends Zend_Db_Table
{
    protected $_name = 'Prog_programs';

    public function findProgram ($id)
    {
        if (empty ($id))
            return (NULL);

        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $prog = new Progs_Program ($row);
        return ($prog);
    }

    public function findValidProgram ($id)
    {
        if (empty ($id))
            return (NULL);

        $select = $this->select()
                        ->where ('id = ?', $id)
                        ->where ('deletion IS NULL');
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $prog = new Progs_Program ($row);
        return ($prog);
    }

    public function findProgramByName ($name)
    {
        if (empty ($name))
            return (NULL);

        $select = $this->select()->where ('label = ?', $name)
                                 ->where ('deletion IS NULL')
                                 ->order ('creation DESC');

        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $prog = new Progs_Program ($row);
        return ($prog);
    }

    public function findProgramByFiles ($name, $manifest,
                                              $layout,
                                              $playlist)
    {
        if (empty ($name))
            return (NULL);

        $select = $this->select()->where ('label = ?', $name)
                                 ->where ('manifest = ?', $manifest)
                                 ->where ('layout = ?', $layout)
                                 ->where ('playlist = ?', $playlist);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $prog = new Progs_Program ($row);
        return ($prog);
    }

    public function getProgsArray ()
    {
        $result = array ();

        $select = $this->select ()
                    ->from (array ('p' => 'Prog_programs'),
                            array ('id', 'label',
                                   'playlist', 'layout', 'manifest'))
                    ->where ('deletion IS NULL')
                    ->order ('label');
        $allProgs = $this->fetchAll ($select);

        foreach ($allProgs as $prog)
        {
            $result [] = $prog->toArray ();
        }

        return ($result);
    }

    public function getProgramWithManifest ($manifestList)
    {
        $result = array ();

        foreach ($manifestList as $manifest)
        {
            $select = $this->select ()
                        ->from (array ('p' => 'Prog_programs'),
                                array ('label'))
                        ->where ('deletion IS NULL')
                        ->where ('manifest = ?', $manifest);
            $allProgs = $this->fetchAll ($select);
            foreach ($allProgs as $prog)
                $result [] = $prog ['label'];
        }

        return ($result);
    }

    public function deleteProgram ($id)
    {
        $rowset = $this->find ($id);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);

        $row ['deletion'] = new Zend_Db_Expr('NOW()');
        $row->save ();
        return (true);
    }

    public function insertProgram ($program)
    {
        $layouts    = new Progs_Layouts ();
        $playlists  = new Progs_Playlists ();
        $manifests  = new Progs_Manifests ();

        $layout = $layouts->updateLayout ($program ['zones'], 
                                          $program ['layout'], false);
        $playlist = $playlists->updatePlaylist ($program ['zones'], false);
        $manifest = $manifests->updateManifest ($program ['zones']);

        // Program's name is unique !
        while ($this->findProgramByName ($program ['name']) != NULL)
            $program ['name'] .= '_';

        $programId = $this->insert (
            array ('label'          => $program ['name'],
                   'description'    => '',
                   'playlist'       => $playlist,
                   'manifest'       => $manifest,
                   'layout'         => $layout,
                   'creation'       => new Zend_Db_Expr('NOW()'),
                   'modification'   => new Zend_Db_Expr('NOW()'),
                   'deletion'       => NULL));
        if (!$programId)
            return (false);
        else
            return ($programId);
    }

    public function updateProgram ($program)
    {
        $layouts    = new Progs_Layouts ();
        $playlists  = new Progs_Playlists ();
        $manifests  = new Progs_Manifests ();

        $layout = $layouts->updateLayout ($program ['zones'],
                                          $program ['layout'], false);
        $playlist = $playlists->updatePlaylist ($program ['zones'], false);
        $manifest = $manifests->updateManifest ($program ['zones']);

        $rowset = $this->find ($program ['id']);
        if (!$rowset)
            return (array ());
        $row = $rowset->current ();
        if (!$row)
            return (array ());

        $returnedProgram = $row ['id'];

        // If something changed, create a new line with the updated
        // program.
        if (strcmp ($row ['layout'], $layout) != 0      ||
            strcmp ($row ['playlist'], $playlist) != 0  ||
            strcmp ($row ['manifest'], $manifest) != 0  ||
            strcmp ($row ['label'], $program ['name']) != 0)
        {
            $returnedProgram = $this->insert (
                array ('label'          => $program ['name'],
                       'description'    => '',
                       'playlist'       => $playlist,
                       'manifest'       => $manifest,
                       'layout'         => $layout,
                       'creation'       => new Zend_Db_Expr('NOW()'),
                       'modification'   => new Zend_Db_Expr('NOW()'),
                       'deletion'       => NULL));
        }
        // Delete the old program
        if ($row ['id'] != $returnedProgram)
        {
            $row ['deletion'] = new Zend_Db_Expr('NOW()');
            $row->save ();
        }
        return ($returnedProgram);
    }
    
    public function loadBackup ($path)
    {
        // Get backup information
        $label          = $this->_getIniValue ($path, "label");
        $creation       = $this->_getIniValue ($path, "creation");
        $modification   = $this->_getIniValue ($path, "modification");

        // Check if name already exists
        // If emission already exists, we don't exit this function
        // We only pass through the treatments, and at the end, we're checking
        // the existing emission has the same meta files
        $progAlreadyExists = $this->findProgramByName ($label);

        // Add content of library directory to Composer
        if (is_dir ("$path/library"))
            $directory = opendir ("$path/library");
        else
            $directory = false;

        if (!$progAlreadyExists && $directory)
        {
            $plugins = new Media_Plugins ();
            while ($entry = @readdir ($directory))
            {
                if (is_dir ($directory.'/'.$entry) ||
                    $entry == '.' ||
                    $entry == '..')
                    continue;

                $length = strlen (".scroll");
                $start  = $length * -1;
                if (substr ($entry, $start) === ".scroll")
                {
                    // Special case: scroll file
                    // -> Copy it into the library directory
                    $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
                    copy ("$path/library/$entry", $upload->library."/$entry");
                    continue;
                }

                // Don't throws error because duplicate files are returning
                // false. It should be 'false' only in case of error,
                // a message when file already exists

                //TODO: remove comments
                //if (!$plugins->addFile ("$path/library/$entry"))
                //    return (false);

                // TODO: remove following code once above code
                //  has been uncommented
                $plugins->addFile ("$path/library/$entry");
            }
        }

        // Insert layout, playlist and manifest in DB
        $layouts    = new Progs_Layouts ();
        $manifests  = new Progs_Manifests ();
        $playlists  = new Progs_Playlists ();

        $licenses   = Zend_Registry::getInstance()->get('Config_Ini_Licenses');
        $layout     = $this->_execute ("/usr/bin/openssl smime -verify ".
                                        "-in \"$path/meta/layout.ini\" ".
                                        "-CAfile ".$licenses->ca." 2>/dev/null");
        $playlist   = $this->_execute ("/usr/bin/openssl smime -verify ".
                                        "-in \"$path/meta/playlist.ini\" ".
                                        "-CAfile ".$licenses->ca." 2>/dev/null");
        $manifest   = $this->_execute ("/usr/bin/openssl smime -verify ".
                                        "-in \"$path/meta/manifest.ini\" ".
                                        "-CAfile ".$licenses->ca." 2>/dev/null");
        if (strlen ($layout) <= 0 ||
            strlen ($playlist) <= 0 ||
            strlen ($manifest) <= 0)
        {
            // Try to not check certificate (if license has expired after backup)
            $layout   = $this->_execute ("/usr/bin/openssl smime -verify -noverify ".
                                         "-in \"$path/meta/layout.ini\" ".
                                         "-CAfile ".$licenses->ca." 2>/dev/null");
            $playlist = $this->_execute ("/usr/bin/openssl smime -verify -noverify ".
                                         "-in \"$path/meta/playlist.ini\" ".
                                         "-CAfile ".$licenses->ca." 2>/dev/null");
            $manifest = $this->_execute ("/usr/bin/openssl smime -verify -noverify ".
                                         "-in \"$path/meta/manifest.ini\" ".
                                         "-CAfile ".$licenses->ca." 2>/dev/null");
            if (strlen ($layout) <= 0 ||
                strlen ($playlist) <= 0 ||
                strlen ($manifest) <= 0)
                return (false);

            $label .= " - EXPIRED";
        }

        $layout     = str_replace ("\r\n", "\n", $layout);
        $manifest   = str_replace ("\r\n", "\n", $manifest);
        $playlist   = str_replace ("\r\n", "\n", $playlist);

        // Add the final NewLine
        if ($layout [strlen ($layout) - 1] != "\n")
            $layout .= "\n";
        if ($manifest [strlen ($manifest) - 1] != "\n")
            $manifest .= "\n";
        if ($playlist [strlen ($playlist) - 1] != "\n")
            $playlist .= "\n";

        $hashLayout     = $layouts->loadLayout ($layout);
        $hashManifest   = $manifests->loadManifest ($manifest);
        $hashPlaylist   = $playlists->loadPlaylist ($playlist);

        if (!$progAlreadyExists)
        {
            // Insert Emission
            $progId = $this->insert (
                array ('label'          => $label,
                       'description'    => '',
                       'playlist'       => $hashPlaylist,
                       'manifest'       => $hashManifest,
                       'layout'         => $hashLayout,
                       'creation'       => $creation,
                       'modification'   => $modification,
                       'deletion'       => NULL));
        }
        else
        {
            if ($progAlreadyExists->getPlaylist () != $hashPlaylist ||
                $progAlreadyExists->getLayout () != $hashLayout ||
                $progAlreadyExists->getManifest () != $hashManifest)
            {
                echo "Emission '$label' already exists and is different.";
                return (false);
            }
            else
            {
                return ("Emission '$label' already exists (identical).");
            }
        }

        return ("Emission '$label' loaded.");
    }

    private function _getIniValue ($path, $label)
    {
        return ($this->_execute (
                    "/bin/grep -E '^$label:' \"$path/ipreso-backup.ini\" | ".
                    "/usr/bin/cut -d':' -f2"));
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }
    
}
