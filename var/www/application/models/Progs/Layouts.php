<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Layouts extends Zend_Db_Table
{
    protected $_name            = 'Prog_layouts';
    protected $_previewWidth    = 160;
    protected $_previewHeight   = 90;

    public function findLayout ($hash)
    {
        $select = $this->select()->where('hash = ?', $hash);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $layout = new Progs_Layout ($row);
        return ($layout);
    }

    public function createLayout ($hash, $content, $preview)
    {
        $hashConfirm = $this->insert (
            array ('hash'       => $hash,
                   'creation'   => new Zend_Db_Expr('NOW()'),
                   'content'    => $content,
                   'preview'    => $preview));

        if (!$hashConfirm)
            return (false);
        else
            return (true);
    }
    
    public function loadLayout ($content)
    {
        $commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $orientation = true;
        else
            $orientation = false;

        if ($orientation)
        {
            $currentOrientation = "l";
            require_once 'Actions/Plugin/Orientation.php';
            $className = 'Actions_Plugin_Orientation';
            $orientationPlugin = new $className ();

            if (preg_match ('/^# Orientation: (.)/', $content, $matches) &&
                count ($matches) > 1)
                $layoutId = $matches [1];
            else
                $layoutId = 'l';

            $currentOrientation = $layoutId [0];
        }

        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            // Create the preview thanks to the content
            if ($orientation)
                $newPreview = $this->createPreviewImage ($content, $currentOrientation);
            else
                $newPreview = $this->createPreviewImage ($content);

            // Create the layout
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr('NOW()'),
                       'content'    => $content,
                       'preview'    => $newPreview));

            if ($orientation)
                $orientationPlugin->setLayoutOrientation ($hashConfirm, 
                                                          $currentOrientation);

            return ($hashConfirm);
        }

        if ($orientation)
            $orientationPlugin->setLayoutOrientation ($hash, 
                                                      $currentOrientation);
        return ($hash);
    }

    public function updateLayout ($layoutData, $layoutId = "", $returnOnlyContent)
    {
        $commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $orientation = true;
        else
            $orientation = false;

        if ($orientation)
        {
            $currentOrientation = "l";
            require_once 'Actions/Plugin/Orientation.php';
            $className = 'Actions_Plugin_Orientation';
            $orientationPlugin = new $className ();
            if (strlen ($layoutId) &&
                ($layoutId [0] == 'p' || $layoutId [0] == 'l'))
            {
                $currentOrientation = $layoutId [0];
                $layoutId = substr ($layoutId, 1);
            }
            else
                $currentOrientation = "'$layoutId'";
        }

        // Get old layout
        if (strlen ($layoutId))
        {
            $rowset = $this->find ($layoutId);
            $row = $rowset->current ();
            if ($row)
            {
                $oldLayout = new Progs_Layout ($row);

                // Get old zones
                $zonesObj = $oldLayout->getZones ();
                $zonesList = array ();
                foreach ($zonesObj as $oldZone)
                    $zonesList [$oldZone->getName ()] = $oldZone;
            }
        }

        // Create the content...
        if ($orientation)
            $content = "# Orientation: $currentOrientation\n";
        else
            $content = "";

        $position = 1;
        $incomplete = false;
        $layoutData = array_reverse ($layoutData);
        foreach ($layoutData as $zone)
        {
            $zoneName   = str_replace (':','', $zone['name']);
            $zoneName   = str_replace ('\'','_', $zoneName);
            $zoneHidden = 0;
            $zoneLayer  = $position++;
            
            if (isset ($zone ['x']) &&
                isset ($zone ['y']) &&
                isset ($zone ['width']) &&
                isset ($zone ['height']))
            {
                $zoneX      = $zone ['x'];
                $zoneY      = $zone ['y'];
                $zoneWidth  = $zone ['width'];
                $zoneHeight = $zone ['height'];
            }
            else if (isset ($zonesList) && 
                     array_key_exists ($zoneName, $zonesList))
            {
                $zoneX      = $zonesList[$zoneName]->getX ();
                $zoneY      = $zonesList[$zoneName]->getY ();
                $zoneWidth  = $zonesList[$zoneName]->getWidth ();
                $zoneHeight = $zonesList[$zoneName]->getHeight ();
            }

            $zoneFormat = 0;
            $zoneLine = "$zoneName"
                            .":$zoneHidden"
                            .":$zoneLayer"
                            .":$zoneX"
                            .":$zoneY"
                            .":$zoneWidth"
                            .":$zoneHeight"
                            .":$zoneFormat";
            $content .= "$zoneLine\n";
        }

    	if ($returnOnlyContent)
        {
         return ($content);	
        }
        
        $hash = md5 ($content);

        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            // Create the preview thanks to the content
            if ($orientation)
                $newPreview = $this->createPreviewImage ($content, $currentOrientation);
            else
                $newPreview = $this->createPreviewImage ($content);

            // Create the layout
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr('NOW()'),
                       'content'    => $content,
                       'preview'    => $newPreview));

            if ($orientation)
                $orientationPlugin->setLayoutOrientation ($hashConfirm, 
                                                          $currentOrientation);

            return ($hashConfirm);
        }

        if ($orientation)
            $orientationPlugin->setLayoutOrientation ($hash, 
                                                      $currentOrientation);
        return ($hash);
    }

    private function createPreviewImage ($content, $orientation = "")
    {
        $commands = new Actions_Commands ();
        $orientationPlugin = $commands->getCommand ("Orientation");
        if ($orientationPlugin && $orientationPlugin->getEnable ())
            $orientEnabled = true;
        else
            $orientEnabled = false;

        if ($orientEnabled && $orientation == "p")
        {
            $this->_previewWidth    = 90;
            $this->_previewHeight   = 160;
        }
        else
        {
            $this->_previewWidth    = 160;
            $this->_previewHeight   = 90;
        }

        $imageRes   = imagecreatetruecolor($this->_previewWidth - 1,
                                           $this->_previewHeight);

        $zoneColor  = imagecolorallocate ($imageRes, 220, 220, 220);
        $borderColor= imagecolorallocate ($imageRes, 100, 100, 100);

        $zonesArray = explode ("\n", $content);
        foreach ($zonesArray as $zone)
        {
            $zone = trim ($zone);
            if (strlen ($zone) < 1)
                continue;
            if ($zone [0] == '#')
                continue;

            $zoneObj = new Progs_Zone ($zone);

            $zoneX      = $zoneObj->getX ()     * $this->_previewWidth / 100;
            $zoneY      = $zoneObj->getY ()     * $this->_previewHeight / 100;
            $zoneWidth  = $zoneObj->getWidth () * $this->_previewWidth / 100;
            $zoneHeight = $zoneObj->getHeight ()* $this->_previewHeight / 100;

            if ($zoneY + $zoneHeight >= $this->_previewHeight)
                $zoneHeight = $this->_previewHeight - $zoneY - 1;
            if ($zoneX + $zoneWidth >= $this->_previewWidth)
                $zoneWidth = $this->_previewWidth - $zoneX - 1;
            
            imagefilledrectangle ($imageRes, 
                                  $zoneX, $zoneY,
                                  $zoneX + $zoneWidth, $zoneY + $zoneHeight,
                                  $zoneColor);
            imagerectangle ($imageRes, 
                            $zoneX, $zoneY,
                            $zoneX + $zoneWidth, $zoneY + $zoneHeight,
                            $borderColor);
        }

        if (imagepng ($imageRes, '/tmp/tmp_layout.png'))
            $preview = file_get_contents ('/tmp/tmp_layout.png', null);
        else
            $preview = NULL;
        imagedestroy ($imageRes);
        
        return ($preview);
    }
}
