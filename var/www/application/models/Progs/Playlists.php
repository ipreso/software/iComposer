<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Playlists extends Zend_Db_Table
{
    protected $_name            = 'Prog_playlists';

    public function findPlaylist ($hash)
    {
        $select = $this->select()->where('hash = ?', $hash);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $playlist = new Progs_Playlist ($row);
        return ($playlist);
    }

    public function updatePlaylist ($zones, $returnOnlyContent)
    {
        // Create the content...
        $content = "";
        foreach ($zones as $zone)
        {
            $zone ['name'] = str_replace ('\'', '_', $zone ['name']);
            $zone ['name'] = str_replace (':', '', $zone ['name']);
            $content .= "[".$zone ['name']."]\n";

            if (!isset ($zone ['playlist']))
                continue;

            foreach ($zone ['playlist'] as $item)
            {
                $itemObj = new Progs_Item ($item ['plugin'],
                                           $item ['id'],
                                           $item ['name'],
                                           $item ['length'],
                                           $item ['prop']);
                $content .= $itemObj->getPlaylistLine ()."\n";
            }
        }
        
        if ($returnOnlyContent)
        {
         return ($content);	
        }

        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }
    
    public function loadPlaylist ($content)
    {
        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }
    
}
