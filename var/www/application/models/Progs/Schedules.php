<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Schedules extends Zend_Db_Table
{
    protected $_name = 'Prog_schedules';

    public function findSchedule ($hash)
    {
        if (!$hash)
            return (NULL);

        $select = $this->select()->where('hash = ?', $hash);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $schedule = new Progs_Schedule ($row);
        return ($schedule);
    }

    public function updateSchedule ($events)
    {
        $eventsObj = new Progs_Events ();

        foreach ($events as $event)
        {
            if (strcasecmp ($event ['type'], 'UNKNOWN') == 0)
                continue;

            $eventObj = new Progs_Event ();
            $eventObj->setStart     ($event ['start']);
            $eventObj->setEnd       ($event ['end']);
            $eventObj->setEvType    ($event ['type']);
            $eventObj->setLabel     ($event ['label']);
            $eventObj->setUid       ($event ['uid']);
            $eventObj->setPeriod    ($event ['period']);
            $eventObj->setExdate    ($event ['exdate']);
            if (isset ($event ['plugin']) && isset ($event ['plugid']))
                $eventObj->setPlugin ($event ['plugin'],
                                      $event ['plugid']);
            if (isset ($event ['pool']))
                $eventObj->setPool ($event ['pool']);

            $eventsObj->addEvent ($eventObj);
        }

        // Add pre-download of all emissions
        $emissions = array ();
        foreach ($events as $event)
        {
            if (strcmp ($event ['type'], "Emission") == 0)
                $emissions [$event ['start']] = $event ['label'];
        }

        // Remove duplicates
        $emissions = array_unique ($emissions);

        if (count ($emissions) > 0)
        {
            // Sort by date
            ksort ($emissions);

            $eventObj = new Progs_Event ();
            $eventObj->setStart     (0);
            $eventObj->setHidden    (true);
            $eventObj->setCommand   ("{ sleep 15 ; ");
            foreach ($emissions as $emission)
            {
                $eventObj->setCommand   ($eventObj->getCommand () .
                                         '/usr/bin/iSynchro --download "'.$emission.'" ; ');

            }
            $eventObj->setCommand ($eventObj->getCommand () . ' }');
            $eventsObj->addEvent ($eventObj);
        }

        // Create the content
        $content = $eventsObj->getiCal ();

        if (strlen ($content) < 1)
            $content = "#";

        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }

    public function loadSchedule ($content)
    {
        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }
}
