<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Schedule
{
    private $_hash;
    private $_creation;
    private $_content;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_hash        = $rowArray ['hash'];
        $this->_creation    = $rowArray ['creation'];
        $this->_content     = $rowArray ['content'];
    }

    public function getHash ()    { return ($this->_hash); }
    public function getContent () { return ($this->_content); }

    public function getEmissions ()
    {
        // Get every emissions in schedule
        $eventsObj      = new Progs_Events ($this->_content);
        $eventsList     = $eventsObj->getEvents ();
        $emissionsList  = array ();

        $event = NULL;
        for ($i = 0 ; $i < count ($eventsList) ; $i++)
        {
            $event = $eventsList [$i];
            if (strcmp ($event->getEvType (), "Pool") == 0)
            {
                foreach ($event->getPool () as $key => $emission)
                {
                    if (strcmp ($key, "size") == 0)
                        continue;
                    $emissionsList [] = $emission;
                }
            }
            else if (strcmp ($event->getEvType (), "Emission") == 0)
                $emissionsList [] = $event->getLabel ();
            else
                continue;
        }

        $emissionsList = array_unique ($emissionsList);
        return ($emissionsList);
    }
}
