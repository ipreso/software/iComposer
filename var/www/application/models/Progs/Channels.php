<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Channels extends Zend_Db_Table
{
    protected $_name = 'Prog_channels';

    public function findChannel ($id)
    {
        if (!strlen ($id))
            return (NULL);

        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $channel = new Progs_Channel ($row);
        return ($channel);
    }

    public function findValidChannel ($id)
    {
        if (!strlen ($id))
            return (NULL);

        $select = $this->select ()
                        ->where ('id = ?', $id)
                        ->where ('deletion IS NULL');
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $channel = new Progs_Channel ($row);
        return ($channel);
    }

    public function findChannelByName ($name)
    {
        if (empty ($name))
            return (NULL);

        $select = $this->select()->where ('label = ?', $name)
                                 ->where ('deletion IS NULL')
                                 ->order ('creation DESC');

        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $channel = new Progs_Channel ($row);
        return ($channel);
    }

    public function findChannelWithSchedule ($schedule)
    {
        $select = $this->select()->where ('schedule = ?', $schedule)
                                 ->order ('creation DESC');
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $channel = new Progs_Channel ($row);
        return ($channel);
    }

    public function getChannelsArray ()
    {
        $result = array ();

        $select = $this->select ()
                    ->from (array ('c' => 'Prog_channels'),
                            array ('id', 'label', 'schedule'))
                    ->where ('deletion IS NULL')
                    ->order ('label ASC');
        $allChannels = $this->fetchAll ($select);

        foreach ($allChannels as $channel)
        {
            $result [] = $channel->toArray ();
        }

        return ($result);
    }

    public function deleteChannel ($id)
    {
        $rowset = $this->find ($id);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);

        $row ['deletion'] = new Zend_Db_Expr('NOW()');
        $row->save ();
        return (true);
    }

    public function insertChannel ($name, $events)
    {
        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->updateSchedule ($events);

        // Channel's name is unique !
        while ($this->findChannelByName ($name) != NULL)
            $name .= '_';

        $channelId = $this->insert (
            array ('label'          => $name,
                   'schedule'       => $schedule,
                   'creation'       => new Zend_Db_Expr('NOW()'),
                   'modification'   => new Zend_Db_Expr('NOW()'),
                   'deletion'       => NULL));
        if (!$channelId)
            return (false);
        else
            return ($channelId);
    }

    public function updateChannel ($id, $name, $events)
    {
        $schedules  = new Progs_Schedules ();
        $schedule   = $schedules->updateSchedule ($events);

        $rowset = $this->find ($id);
        if (!$rowset)
            return (false);
        $row = $rowset->current ();
        if (!$row)
            return (false);

        $returnedChannel = $row ['id'];

        // If something changed, create a new line with the updated
        // channel.
        if (strcmp ($row ['schedule'],  $schedule)  != 0 ||
            strcmp ($row ['label'],     $name)      != 0)
        {
            $returnedChannel = $this->insert (
                array ('label'          => $name,
                       'schedule'       => $schedule,
                       'creation'       => new Zend_Db_Expr('NOW()'),
                       'modification'   => new Zend_Db_Expr('NOW()'),
                       'deletion'       => NULL));
        }

        // Override old programmation
        if ($row ['id'] != $returnedChannel)
        {
            $row ['deletion'] = new Zend_Db_Expr('NOW()');
            $row->save ();
        }

        return ($returnedChannel);
    }

    public function updateEmissionName ($oldName, $newName)
    {
        // Get every Programmation playing the 'oldName' emission
        $progsList  = $this->getChannelsWithEmissionStr ($oldName);
        $schedules  = new Progs_Schedules ();

        foreach ($progsList as $programName)
        {
            $channel = $this->findChannelByName ($programName);
            if (!$channel)
                continue;

            // Update every emission 'oldName' by the emission 'newName'
            $schedule = $schedules->findSchedule ($channel->getSchedule ());
            $content  = $schedule->getContent ();
            $content  = str_replace ("\"$oldName\"", "\"$newName\"", $content);
            $newSchedule = $schedules->loadSchedule ($content);

            if ($newSchedule == $channel->getSchedule ())
                continue;

            // Update the Schedule of the Programmation
            $rowset = $this->find ($channel->getId ());
            if (!$rowset)
                continue;
            $row = $rowset->current ();
            if (!$row)
                continue;
            // Create the programmation with the new Schedule
            $newChannelId = $this->insert (
                    array ('label'          => $channel->getName (),
                           'schedule'       => $newSchedule,
                           'creation'       => new Zend_Db_Expr('NOW()'),
                           'modification'   => new Zend_Db_Expr('NOW()'),
                           'deletion'       => NULL));
            // Delete the programmation with the old Schedule
            if ($row ['id'] != $newChannelId)
            {
                $row ['deletion'] = new Zend_Db_Expr('NOW()');
                $row->save ();

                // Update permissions
                $resources = new Admin_resources ();
                $resources->updateResource ('Programmation', $row ['id'], $newChannelId);

                $logger = Zend_Registry::getInstance()->get('Zend_Log');
                $logger->info ("Updated program *".$channel->getName ()."* ($newChannelId).");

                // Update players' Parcs having this programmation
                $parcObj = new Players_Groups ();
                $boxes   = new Players_Boxes ();
                $parcs = $parcObj->getGroupsWithChannel ($row ['id']);
                foreach ($parcs as $parc)
                {
                    $parcObj->replaceGroupChannel ($parc ['id'], $newChannelId);
                    $boxes->replaceBoxesChannel ($parc ['id'], $newChannelId);
                }
            }
        }
    }

    public function getChannelsWithEmission ($emission)
    {
        return ($this->getChannelsWithEmissionStr ($emission->getName ()));
    }

    public function getChannelsWithEmissionStr ($strEmission)
    {
        $result = array ();

        // Get schedules of active programmations
        // And check they don't use given emission
        $select = $this->select ()
                    ->setIntegrityCheck (false)
                    ->from (array ('c' => 'Prog_channels'),
                            array ('id', 'label'))
                    ->join (array ('s' => 'Prog_schedules'),
                            'c.schedule = s.hash',
                            array ('hash'))
                    ->where ('c.deletion IS NULL')
                    ->where ('c.schedule = s.hash')
                    ->where ('s.content LIKE \'%"'.$strEmission.'"%\'')
                    ->order ('c.label ASC');
        $allChannels = $this->fetchAll ($select);

        foreach ($allChannels as $channel)
        {
            $result [] = $channel['label'];
        }

        return ($result);
    }

    public function loadBackup ($path)
    {
        // Get backup information
        $label          = $this->_getIniValue ($path, "label");
        $creation       = $this->_getIniValue ($path, "creation");
        $modification   = $this->_getIniValue ($path, "modification");

        // Check if name already exists
        // If program already exists, we don't exit this function
        // We only pass through the treatments, and at the end, we're checking
        // the existing prog has the same meta files
        $progAlreadyExists = $this->findChannelByName ($label);

        // Add content of library directory to Composer
        if (is_dir ("$path/library"))
            $directory = opendir ("$path/library");
        else
            $directory = false;

        if (!$progAlreadyExists && $directory)
        {
            $plugins = new Media_Plugins ();
            while ($entry = @readdir ($directory))
            {
                if (is_dir ($directory.'/'.$entry) ||
                    $entry == '.' ||
                    $entry == '..')
                    continue;

                $length = strlen (".scroll");
                $start  = $length * -1;
                if (substr ($entry, $start) === ".scroll")
                {
                    // Special case: scroll file
                    // -> Copy it into the library directory
                    $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');
                    copy ("$path/library/$entry", $upload->library."/$entry");
                    continue;
                }

                // Don't throws error because duplicate files are returning
                // false. It should be 'false' only in case of error,
                // a message when file already exists

                //TODO: remove comments
                //if (!$plugins->addFile ("$path/library/$entry"))
                //    return (false);

                // TODO: remove following code once above code
                //  has been uncommented
                $plugins->addFile ("$path/library/$entry");
            }
        }

        // Insert schedule in DB
        $schedules  = new Progs_Schedules ();
        $licenses   = Zend_Registry::getInstance()->get('Config_Ini_Licenses');
        $schedule   = $this->_execute ("/usr/bin/openssl smime -verify ".
                                        "-in \"$path/meta/schedule.ini\" ".
                                        "-CAfile ".$licenses->ca." 2>/dev/null");
        if (strlen ($schedule) <= 0)
        {
            // Try to not check certificate (if license has expired after backup)
            $schedule   = $this->_execute ("/usr/bin/openssl smime -verify -noverify ".
                                            "-in \"$path/meta/schedule.ini\" ".
                                            "-CAfile ".$licenses->ca." 2>/dev/null");
            if (strlen ($schedule) <= 0)
                return (false);

            $label .= " - EXPIRED";
        }

        $schedule = str_replace ("\r\n", "\n", $schedule);
        if ($schedule [strlen ($schedule) - 1] != "\n")
            $schedule .= "\n";
        $hashSchedule = $schedules->loadSchedule ($schedule);

        if (!$progAlreadyExists)
        {
            // Insert Programmation
            $returnedChannel = $this->insert (
                array ('label'          => $label,
                       'schedule'       => $hashSchedule,
                       'creation'       => $creation,
                       'modification'   => $modification,
                       'deletion'       => NULL));
        }
        else
        {
            if ($progAlreadyExists->getSchedule () != $hashSchedule)
            {
                echo "Programmation '$label' already exists and is different.";
                return (false);
            }
            else
            {
                return ("Programmation '$label' already exists (identical).");
            }
        }

        return ("Programmation '$label' loaded.");
    }

    private function _getIniValue ($path, $label)
    {
        return ($this->_execute (
                    "/bin/grep -E '^$label:' \"$path/ipreso-backup.ini\" | ".
                    "/usr/bin/cut -d':' -f2"));
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }
}
