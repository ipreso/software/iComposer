<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Playlist
{
    private $_hash;
    private $_creation;
    private $_content;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_hash        = $rowArray ['hash'];
        $this->_creation    = $rowArray ['creation'];
        $this->_content     = $rowArray ['content'];
    }

    public function getHash ()    { return ($this->_hash); }
    public function getContent () { return ($this->_content); }

    public function getZone ($zoneName)
    {
        $playlist = array ();

        $lines = explode ("\n", $this->_content);
        $finished = false;
        for ($i = 0 ; !$finished && $i < count ($lines) ; $i++)
        {
            $line = trim ($lines [$i]);
            if (strlen ($line) < 1)
                continue;
            if ($line [0] == '#')
                continue;

            if (strcmp ($line, "[$zoneName]") == 0)
            {
                $end = false;
                for ($j = $i + 1 ; !$end && $j < count ($lines) ; $j++)
                {
                    $line = trim ($lines [$j]);
                    if (strlen ($line) < 1)
                        continue;
                    if ($line [0] == '#')
                        continue;
                    if ($line [0] == '[')
                        $end = true;
                    else
                    {
                        $item = new Progs_Item ($line);
                        $playlist [] = $item->getSequenceProperties ();
                    }
                }
                $finished = true;
            }
        }

        return ($playlist);
    }
}
