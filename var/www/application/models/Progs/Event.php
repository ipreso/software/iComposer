<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Event
{
    private $_start         = 0;
    private $_end           = 0;
    private $_delay         = 0;
    private $_type          = "";
    private $_hidden        = true;
    private $_command       = "";
    private $_label         = "";
    private $_zone          = "";
    private $_periodicity   = 0;
    private $_uid           = "";
    private $_plugin        = "";
    private $_plugid        = "";
    private $_exdate        = "";
    private $_pool          = array();

    private $_START         = "DTSTART";
    private $_END           = "DTEND";
    private $_SUMMARY       = "SUMMARY";
    private $_LOCATION      = "LOCATION";
    private $_RRULE         = "RRULE";
    private $_UID           = "UID";
    private $_EXDATE        = "EXDATE";
    
    public function __construct ($iCalArray = NULL)
    {
        if (!$iCalArray)
            return (false);

        // Parse each line of the parameter (iCal format)
        if (count ($iCalArray) < 1)
            return (false);

        foreach ($iCalArray as $line)
        {
            list ($key, $value) = explode (':', $line, 2);
            $pos = strpos ($key, ';');
            if ($pos !== false)
                $key = substr ($key, 0, $pos);
                
            switch ($key)
            {
                case $this->_START:
                    list ($day, $time) = explode ('T', $value);
                    $year   = substr ($day, 0, 4);
                    $month  = substr ($day, 4, 2);
                    $day    = substr ($day, 6, 2);
                    $hour   = substr ($time, 0, 2);
                    $min    = substr ($time, 2, 2);
                    $sec    = substr ($time, 4, 2);
                    $this->_start = mktime ($hour, $min, $sec, $month, $day, $year);
                    $this->_delay = date ('Z', $this->_start);
                    $this->_start = $this->_start + $this->_delay;
                    break;
                case $this->_END:
                    list ($day, $time) = explode ('T', $value);
                    $year   = substr ($day, 0, 4);
                    $month  = substr ($day, 4, 2);
                    $day    = substr ($day, 6, 2);
                    $hour   = substr ($time, 0, 2);
                    $min    = substr ($time, 2, 2);
                    $sec    = substr ($time, 4, 2);
                    $this->_end = mktime ($hour, $min, $sec, $month, $day, $year);
                    $this->_end = $this->_end + date ('Z', $this->_end);
                    break;
                case $this->_SUMMARY:
                    $this->_command = $value;
                    break;
                case $this->_LOCATION:
                    $this->_zone = $value;
                    break;
                case $this->_RRULE:
                    list ($freq,        $interval)  = explode (';', $value, 2);
                    list ($freqLabel,   $freqValue) = explode ('=', $freq, 2);
                    list ($intLabel,    $intValue)  = explode ('=', $interval, 2);
                    switch ($freqValue)
                    {
                        case "MINUTELY":
                            $this->_periodicity = $intValue * 60;
                            break;
                        case "HOURLY":
                            $this->_periodicity = $intValue * 60*60;
                            break;
                        case "DAILY":
                            $this->_periodicity = $intValue * 60*60*24;
                            break;
                        case "WEEKLY":
                            $this->_periodicity = $intValue * 60*60*24*7;
                            break;
                        default:
                            $this->_periodicity = 0;
                    }
                    break;
                case $this->_UID:
                    $this->_uid = $value;
                    break;
                case $this->_EXDATE:
                    $pos = strpos ($value, ',');
                    if ($pos === false)
                    {
                        list ($day, $time) = explode ('T', $value);
                        $year   = substr ($day, 0, 4);
                        $month  = substr ($day, 4, 2);
                        $day    = substr ($day, 6, 2);
                        $hour   = substr ($time, 0, 2);
                        $min    = substr ($time, 2, 2);
                        $sec    = substr ($time, 4, 2);
                        $this->_exdate = mktime ($hour, $min, $sec, $month, $day, $year);
                        $this->_exdate = $this->_exdate + date ('Z', $this->_exdate).",";
                    }
                    else
                    {
                        $exdates = explode (',', $value);
                        $this->_exdate = "";
                        foreach ($exdates as $exdate)
                        {
                            list ($day, $time) = explode ('T', $exdate);
                            $year   = substr ($day, 0, 4);
                            $month  = substr ($day, 4, 2);
                            $day    = substr ($day, 6, 2);
                            $hour   = substr ($time, 0, 2);
                            $min    = substr ($time, 2, 2);
                            $sec    = substr ($time, 4, 2);
                            $tmp_exdate = mktime ($hour, $min, $sec, $month, $day, $year);
                            $tmp_exdate = $tmp_exdate + date ('Z', $tmp_exdate);
                            $this->_exdate .= "$tmp_exdate,";
                        }
                    }
                    break;
            }
        }
        
        if (strlen ($this->_command))
        {
            if (!strlen ($this->_label))
            {
                // Get Emission label
                $pos = strpos ($this->_command, ' ');
                if ($pos !== false)
                {
                    if (strncmp ($this->_command, "/usr/bin/iSynchro --program ",
                                        strlen ("/usr/bin/iSynchro --program ")) == 0
                        ||
                        strncmp ($this->_command, "/usr/bin/iZoneMgr ",
                                        strlen ("/usr/bin/iZoneMgr ")) == 0)
                    {
                        // iSynchro allows to download the programmation before
                        // stopping queue
                        // But keeping iZoneMgr for compatibility with old version
                        $this->_hidden  = false;
                        $this->_type    = "Emission";
                        if (strpos ($this->_command, '"') === false)
                            $prog = $this->_command." ?";
                        else
                            list ($dummy, $prog) = explode ('"', $this->_command);
                        $this->_label   = $prog;

                        // Get Emission ID according to the name
                        $emissions      = new Progs_programs ();
                        $emission       = $emissions->findProgramByName ($this->_label);
                        if ($emission)
                        {
                            $this->_plugid  = $emission->getId ();
                        }
                        else
                        {
                            $logger = Zend_Registry::getInstance()->get('Zend_Log');
                            $logger->info("ERROR - Emission *".$this->_label."* does not exist !");
                        }
                    }
                    else if (strncmp ($this->_command, "/usr/bin/iSynchro --programs ",
                                        strlen ("/usr/bin/iSynchro --programs ")) == 0)
                    {
                        $this->_hidden  = false;
                        $this->_type    = "Pool";
                        if (strpos ($this->_command, '"') === false)
                            $prog = $this->_command." ?";
                        else
                            list ($dummy, $progs) = explode ('"', $this->_command);

                        $translator = Zend_Registry::getInstance ()->get ('Zend_Translate');
                        $this->_label   = $translator->translate ("Emissions pool");
                        $this->_label = str_replace ("\\","", $this->_label);

                        $pool = explode (';', $progs);
                        // Get Emission ID according to the name
                        $poolSize = 0;
                        foreach ($pool as $prog)
                        {
                            if (strlen ($prog) < 1)
                                continue;
                            $emissions      = new Progs_programs ();
                            $emission       = $emissions->findProgramByName ($prog);
                            if ($emission !== NULL)
                            {
                                $this->_pool [count($this->_pool)."-".$emission->getId ()] = $prog;
                                $poolSize++;
                            }
                        }
                        $this->_pool ['size'] = $poolSize;
                    }
                    else if (strncmp ($this->_command, "/usr/bin/iSynchro",
                                            strlen ("/usr/bin/iSynchro")) == 0)
                    {
                        $this->_hidden  = true;
                        $this->_label   = "Hidden";
                    }
/*
                    list ($cmd, $params) = explode (' ', $this->_command, 2);
                    switch ($cmd)
                    {
                        case "/usr/bin/iSynchro":
                            $this->_hidden  = true;
                            $this->_label   = "Hidden";
                            break;
                        case "/usr/bin/iZoneMgr":
                            $this->_hidden  = false;
                            $this->_type    = "Emission";
                            if (strpos ($this->_command, '"') === false)
                                $prog = $this->_command." ?";
                            else
                                list ($dummy, $prog) = explode ('"', $this->_command);
                            $this->_label   = $prog;
                            break;
                    }
*/
                }
            }

            if (!strlen ($this->_label))
            {
                // Get Action label
                if (strncmp ("/var/lib/iplayer/cmd/",
                             $this->_command,
                             strlen ("/var/lib/iplayer/cmd/")) == 0)
                {
                    $command = $this->getActionLabel ($this->_command);
                    if ($command)
                    {
                        $this->_hidden  = false;
                        $this->_type    = "Action";
                        $this->_plugin  = $command ['plugin'];
                        $this->_plugid  = $command ['plugid'];
                        $this->_label   = $command ['label'];
                    }
                }
            }

            if (!strlen ($this->_label))
            {
                // Not an hidden sync, not a plugin, not an emission
                $this->_hidden  = false;
                $this->_type    = "Unknown";
                $this->_label   = $this->_command;
            }
        }
    }

    protected function getActionLabel ($command)
    {
        // Search in commands plugins
        $commands = new Actions_Commands ();
        $allCmds  = $commands->getEnabledCommands ();
        $found = false;
        for ($i = 0 ; !$found && $i < count ($allCmds) ; $i++)
        {
            $cmd = $allCmds [$i];
            $elements = $cmd->getCommands ();
            foreach ($elements as $value)
            {
                if (strcmp ("/var/lib/iplayer/cmd/".$value ['cmd'], 
                            $command) == 0)
                {
                    $found = true;
                    $result = $value;
                    $plugin = $cmd->getInternalName ();
                }
            }
        }

        if ($found)
            return (array ('plugin' => $plugin,
                           'plugid' => $result ['id'],
                           'label'  => $result ['name']));
        else
            return (false);
    }

    public function getStart    () { return ($this->_start);        }
    public function getEnd      () { return ($this->_end);          }
    public function getDelay    () { return ($this->_delay);        }
    public function getEvType   () { return ($this->_type);         }
    public function getHidden   () { return ($this->_hidden);       }
    public function getCommand  () { return ($this->_command);      }
    public function getLabel    () { return ($this->_label);        }
    public function getZone     () { return ($this->_zone);         }
    public function getPeriod   () { return ($this->_periodicity);  }
    public function getUID      () { return ($this->_uid);          }
    public function getPlugin   () { return ($this->_plugin);       }
    public function getPluginId () { return ($this->_plugid);       }
    public function getExdate   () { return ($this->_exdate);       }
    public function getPool     () { return ($this->_pool);         }

    public function setStart    ($param)    { $this->_start     = $param;   }
    public function setEnd      ($param)    { $this->_end       = $param;   }
    public function setEvType   ($param)    { $this->_type      = $param;   }
    public function setHidden   ($param)    { $this->_hidden    = $param;   }
    public function setCommand  ($param)    { $this->_command   = $param;   }
    public function setLabel    ($param)    { $this->_label     = $param;   }
    public function setZone     ($param)    { $this->_zone      = $param;   }
    public function setUid      ($param)    { $this->_uid       = $param;   }
    public function setPeriod   ($param)    { $this->_periodicity = $param; }
    public function setExdate   ($param)    { $this->_exdate    = $param;   }
    public function setPool     ($param)    { $this->_pool      = $param;   }

    public function setPlugin   ($plugin, $id)
    {
        $this->_plugin  = $plugin;
        $this->_plugid  = $id;
    }

    public function getiCalFormat ()
    {
        if (!strlen ($this->_command))
        {
            switch ($this->_type)
            {
                case "Emission":
                    //$this->_command = '/usr/bin/iZoneMgr -p "'.$this->_label.'"';
                    $this->_command = '/usr/bin/iSynchro --program "'.$this->_label.'"';
                    break;
                case "Pool":
                    $programsList = "";
                    foreach ($this->_pool as $key => $program)
                    {
                        if (strcmp ($key, "size") == 0)
                            continue;

                        if (strlen ($programsList) > 0)
                            $programsList .= ";";
                        $programsList .= "$program";
                    }
                    $this->_command = "/usr/bin/iSynchro --programs \"$programsList\"";
                    break;
                case "Action":
                    $commands = new Actions_Commands;
                    $command = $commands->getCommand ($this->_plugin);
                    if ($command)
                        $this->_command = '/var/lib/iplayer/cmd/'.$command->getStringCommand ($this->_plugid);
                    else
                        $this->_command = "echo \"".$this->_plugin." is unknown\"";
                    break;
                case "Media":
                    $this->_command = "MEDIA:".$this->_plugin." ".$this->_plugid;
                    break;
                default:
                    $this->_command = "UNKNOWN: ".$this->_label;
            }
        }
    
        $rrule = "";
        if ($this->_periodicity && $this->_periodicity > 59)
        {
            // Get the correct interval and unit
            if ($this->_periodicity >= 60*60*24*7 &&
                $this->_periodicity % (60*60*24*7) == 0)
            {
                $freq       = "WEEKLY";
                $interval   = round ($this->_periodicity / (60*60*24*7));
            }
            else if ($this->_periodicity >= 60*60*24 &&
                     $this->_periodicity % (60*60*24) == 0)
            {
                $freq       = "DAILY";
                $interval   = round ($this->_periodicity / (60*60*24));
            }
            else if ($this->_periodicity >= 60*60 &&
                     $this->_periodicity % (60*60) == 0)
            {
                $freq       = "HOURLY";
                $interval   = round ($this->_periodicity / (60*60));
            }
            else
            {
                $freq       = "MINUTELY";
                $interval   = round ($this->_periodicity / 60);
            }

            if ($interval >= 1)
                $rrule = "RRULE:FREQ=$freq;INTERVAL=$interval\n";
        }

        // Set the start and end times to UTC
        $utcStart   = $this->_start - date ("Z", $this->_start);
        $utcEnd     = $this->_end   - date ("Z", $this->_end);

        // Set the exception dates
        if (strlen ($this->_exdate) && $this->_periodicity > 0)
        {
            $exdateArray = array ();
            if (strpos ($this->_exdate, ',') !== false)
            {
                $exdateArray = explode (',', $this->_exdate);
                foreach ($exdateArray as $exdateString)
                {
                    if (strlen ($exdateString) && (int)$exdateString > 0)
                        $exdateArray [] = (int)$exdateString - date ("Z", (int)$exdateString);
                }
            }
            else
                $exdateArray [] = (int)$this->_exdate - date ("Z", (int)$this->_exdate);

            sort ($exdateArray);
            $exdateFinalArray = array ();
            $timeIterator = $utcStart;
            foreach ($exdateArray as $exdateInt)
            {
                if (!$exdateInt)
                    continue;

                while ($timeIterator < $exdateInt)
                    $timeIterator += $this->_periodicity;

                if ($timeIterator == $exdateInt)
                    $exdateFinalArray [] = $exdateInt;
            }

            if (count ($exdateFinalArray))
            {
                $exdateLine = "EXDATE:";
                foreach ($exdateFinalArray as $exdateInt)
                    $exdateLine .= date ("Ymd\THis", $exdateInt);
                $exdateLine .= "\n";
            }
            else
                $exdateLine = "";
        } 
        else
            $exdateLine = "";

        $ical = "BEGIN:VEVENT\n"
                ."UID:"     .$this->_uid                        ."\n"
                ."DTSTART:" .date ("Ymd\THis", $utcStart)       ."\n"
                ."DTEND:"   .date ("Ymd\THis", $utcEnd)         ."\n"
                ."SUMMARY:" .$this->_command                    ."\n"
                .$rrule
                .$exdateLine
                ."END:VEVENT\n";

        return ($ical);
    }
}
