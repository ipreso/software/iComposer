<?php
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Manifests extends Zend_Db_Table
{
    protected $_name = 'Prog_manifests';

    public function findManifest ($hash)
    {
        $select = $this->select()->where('hash = ?', $hash);
        $row = $this->fetchRow ($select);

        if ($row === NULL)
            return (NULL);

        $manifest = new Progs_Manifest ($row);
        return ($manifest);
    }

    public function updateManifest ($zones)
    {
        // Get the filelist
        $filesList = array ();
        foreach ($zones as $zone)
        {
            if (!isset ($zone ['playlist']))
                continue;

            foreach ($zone ['playlist'] as $item)
            {
                $itemObj = new Progs_Item ($item ['plugin'],
                                           $item ['id'],
                                           $item ['name'],
                                           $item ['length'],
                                           $item ['prop']);

                $files = $itemObj->getManifestFiles ();
                foreach ($files as $file)
                    $filesList [] = $file ['hash'].":".$file ['file'];
            }
        }

        // Create the content
        $content = "";
        $filesList = array_unique ($filesList);
        foreach ($filesList as $file)
            $content .= "$file\n";

        if (strlen ($content) < 1)
            $content = "#";

        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }

    public function getManifestsWithFileHash ($id)
    {
        $select = $this->select()->where('content like ?', '%'.$id.':%');
        $rowset = $this->fetchAll ($select);
        if (!$rowset)
            return (array ());

        $result = array ();
        foreach ($rowset as $row)
            $result [] = $row ['hash'];

        return ($result);
    }

    public function loadManifest ($content)
    {
        $hash = md5 ($content);
        $rowset = $this->find ($hash);
        $row = $rowset->current ();
        if (!$row)
        {
            $hashConfirm = $this->insert (
                array ('hash'       => $hash,
                       'creation'   => new Zend_Db_Expr ('NOW()'),
                       'content'    => $content));
            return ($hashConfirm);
        }
        return ($hash);
    }
}
