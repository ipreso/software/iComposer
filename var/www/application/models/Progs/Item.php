<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Item
{
    private $_plugin;
    private $_id;
    private $_name;
    private $_length;
    private $_startdate;
    private $_enddate;
    private $_properties;

    public function __construct ()
    {
        if (func_num_args () == 1)
        {
            $line = func_get_arg (0);

            $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
            if (preg_match ($pattern, $line, $matches))
            {
                $this->_plugin      = $matches [1];
                $this->_id          = $matches [2];
                $this->_name        = $matches [3];
                $options            = $matches [4];

                $this->_length  = 0;
                $this->_startdate   = 0;
                $this->_enddate     = 0;

                $plugins = new Media_Plugins ();
                $plugin = $plugins->getPlugin ($this->_plugin);
                if (!$plugin)
                    $this->_properties = new Media_Property ();
                else
                {
                    $this->_properties = $plugin->getProperties ($line);
                    if (!$this->_properties)
                        $this->_properties = new Media_Property ();
                    else
                    {
                        $this->_length = $plugin->getProperty ($this->_properties,
                                                               'duration');
                        $this->_startdate = $plugin->getProperty ($this->_properties, 'startdate');
                        $this->_enddate = $plugin->getProperty ($this->_properties, 'enddate');
                    }
                }
            }
            else
            {
                $this->_plugin      = 'UNKNOWN';
                //$this->_plugin      = $line;
                $this->_id          = 'UNKNOWN';
                $this->_name        = 'UNKNOWN';
                $this->_length      = 'UNKNOWN';
                $this->_startdate       = 0;
                $this->_enddate         = 0;
                $this->_properties  = new Media_Property ();
            }
        }
        else if (func_num_args () == 5)
        {
            // Order is : $plugin, $id, $name, $length, $other_options
            $this->_plugin      = func_get_arg (0);
            $this->_id          = func_get_arg (1);
            $this->_name        = func_get_arg (2);
            $this->_length      = func_get_arg (3);
            $propId             = func_get_arg (4);
            $this->_startdate       = 0;
            $this->_enddate         = 0;

            $properties = new Media_Properties ();
            $this->_properties = $properties->getProperty ($this->_id, $propId);

            if (!$this->_properties)
            {
                $pluginsObj = new Media_Plugins ();
                $plugin = $pluginsObj->getPlugin ($this->_plugin);
                $this->_properties = $plugin->getDefaultProperties ();
            } 
        }
        else if (func_num_args () == 7)
        {
            // Order is : $plugin, $id, $name, $length, $start, $end, $other_options
            $this->_plugin      = func_get_arg (0);
            $this->_id          = func_get_arg (1);
            $this->_name        = func_get_arg (2);
            $this->_length      = func_get_arg (3);
            $this->_startdate       = func_get_arg (4);
            $this->_enddate         = func_get_arg (5);
            $propId             = func_get_arg (6);

            $properties = new Media_Properties ();
            $this->_properties = $properties->getProperty ($this->_id, $propId);

            if (!$this->_properties)
            {
                $pluginsObj = new Media_Plugins ();
                $plugin = $pluginsObj->getPlugin ($this->_plugin);
                $this->_properties = $plugin->getDefaultProperties ();
            } 
        }
    }

    public function getProperties ()
    {
        return ($this->_properties);
    }

    public function getSequenceProperties ()
    {
        $result = array ();

        $result ['plugin']  = $this->_plugin;
        $result ['name']    = $this->_name;
        $result ['length']  = $this->_length;
        $result ['id']      = $this->_id;
        $result ['prop']    = $this->_properties->getId ();
        $result ['startdate']   = $this->_startdate;
        $result ['enddate']     = $this->_enddate;

        return ($result);
    }

    public function getPlaylistLine ()
    {
        // Create a line for the playlist, giving all information to the
        // player's plugin

        $plugins = new Media_Plugins ();
        $plugin = $plugins->getPlugin ($this->_plugin);
        if (!$plugin)
            return ("# Plugin not found: '".$this->_plugin."'");

        $result = $plugin->getPlaylistLine ($this);
        if (!$result)
            return ("# Error while creating item for plugin '"
                        .$this->_plugin."'");
        else
            return ($result);
    }

    public function getManifestFiles ()
    {
        // Create a line for the playlist, giving all information to the
        // player's plugin

        $plugins = new Media_Plugins ();
        $plugin = $plugins->getPlugin ($this->_plugin);
        if (!$plugin)
            return (array ());

        return ($plugin->getManifestFiles ($this));
    }
}
