<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Layout
{
    private $_hash;
    private $_creation;
    private $_content;
    private $_preview;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_hash        = $rowArray ['hash'];
        $this->_creation    = $rowArray ['creation'];
        $this->_content     = $rowArray ['content'];
        $this->_preview     = $rowArray ['preview'];
    }

    public function getHash ()    { return ($this->_hash); }
    public function getPreview () { return ($this->_preview); }
    public function getContent () { return ($this->_content); }

    public function getZones ()
    {
        $zonesResult = array ();

        $zonesArray = explode ("\n", $this->_content);
        foreach ($zonesArray as $zone)
        {
            $zone = trim ($zone);
            if (strlen ($zone) < 1)
                continue;
            if ($zone [0] == '#')
                continue;

            $zoneObj = new Progs_Zone ($zone);
            if ($zoneObj)
                $zonesResult [] = $zoneObj;
        }

        return ($zonesResult);
    }
}
