<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Zone
{
    private $_name;
    private $_hidden;
    private $_layer;
    private $_x;
    private $_y;
    private $_width;
    private $_height;
    private $_format;

    public function __construct ($line)
    {
        list ($this->_name,
              $this->_hidden,
              $this->_layer,
              $this->_x,
              $this->_y,
              $this->_width,
              $this->_height,
              $this->_format) = explode (':', $line);
    }

    public function getName () { return ($this->_name); }
    public function getLayer () { return ($this->_layer); }
    public function getX () { return ($this->_x); }
    public function getY () { return ($this->_y); }
    public function getWidth () { return ($this->_width); }
    public function getHeight () { return ($this->_height); }
}
