<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Progs_Program
{
    private $_id;
    private $_label;
    private $_playlist;
    private $_manifest;
    private $_layout;
    private $_creation;
    private $_modification;
    private $_deletion;

    public function __construct ($row)
    {
        // Parse the result
        $rowArray = $row->toArray ();
        $this->_id              = $rowArray ['id'];
        $this->_label           = $rowArray ['label'];
        $this->_playlist        = $rowArray ['playlist'];
        $this->_manifest        = $rowArray ['manifest'];
        $this->_layout          = $rowArray ['layout'];
        $this->_creation        = $rowArray ['creation'];
        $this->_modification    = $rowArray ['modification'];
        $this->_deletion        = $rowArray ['deletion'];
    }

    public function getId ()        { return ($this->_id); }
    public function getName ()      { return ($this->_label); }
    public function getLayout ()    { return ($this->_layout); }
    public function getPlaylist ()  { return ($this->_playlist); }
    public function getManifest ()  { return ($this->_manifest); }

    public function isDeleted ()
    {
        return ($this->_deletion != NULL);
    }

    public function createBackup ($withLibrary = false, $basepath = "")
    {
        // Create unique temporary directory. Could not be in /opt/tmp because
        // of the size limitation of this partition
        if (strlen ($basepath))
        {
            $tmpdir     = tempnam ($basepath, 'emi-');
            $archiveDir = $basepath;
        }
        else
        {
            $upload     = Zend_Registry::getInstance()->get('Config_Ini_Upload');
            $tmpdir     = tempnam ($upload->library, 'emi-');
            $archiveDir = $upload->library;
        }
        unlink ($tmpdir);
        mkdir ($tmpdir, 0770);

        // Put signed meta-files in this directory
        if (!$this->_createMetaBackup ($tmpdir))
        {
            system ("rm -rf $tmpdir");
            return (false);
        }

        if ($withLibrary)
        {
            if (!$this->_copyLibrary ($tmpdir))
            {
                system ("rm -rf $tmpdir");
                return (false);
            }
        }

        // Create the info file
        $info =     "type:Emission\n";
        $info .=    "label:".$this->_label."\n";
        $info .=    "creation:".$this->_creation."\n";
        $info .=    "modification:".$this->_modification."\n";
        file_put_contents ("$tmpdir/ipreso-backup.ini", $info);

        // Compress the directory into an unique archive file
        $archiveFile = $archiveDir."/emi-".$this->_label.".bkp";
        system ("/bin/tar -C $tmpdir -cf '$archiveFile' . 2>/dev/null 1>&2");

        // Delete the directory
        system ("rm -rf $tmpdir");

        if (!file_exists ("$archiveFile"))
            return (false);

        // Return an UID on this archive-file
        return (basename ("$archiveFile"));
    }

    private function _copyLibrary ($path)
    {
        @mkdir ("$path/library", 0770);

        $manifests = new Progs_Manifests ();
        $manifest = $manifests->findManifest ($this->getManifest ());

        // Parse Manifest file and copy each file from the Composer's library
        // into our archived library
        $upload = Zend_Registry::getInstance()->get('Config_Ini_Upload');

        $content = $manifest->getContent ();
        $strings = explode ("\n", $content);
        foreach ($strings as $line)
        {
            if (strlen ($line) < 30 || $line [0] == '#')
                continue;

            list($hash, $name) = explode (":", $line);
            if (!copy ($upload->library."/$name", "$path/library/$name"))
                return (false);
        }
        return (true);
    }

    private function _createMetaBackup ($path)
    {
        // Create subdirectory
        @mkdir ("$path/meta", 0770);

        $layouts    = new Progs_Layouts ();
        $playlists  = new Progs_Playlists ();
        $manifests  = new Progs_Manifests ();

        // Get meta files
        $layout     = $layouts->findLayout ($this->getLayout ());
        $playlist   = $playlists->findPlaylist ($this->getPlaylist ());
        $manifest   = $manifests->findManifest ($this->getManifest ());

        if (!$layout || !$playlist || !$manifest)
            return (false);

        $signedLayout   = $this->_signFile ($layout->getContent ());
        $signedPlaylist = $this->_signFile ($playlist->getContent ());
        $signedManifest = $this->_signFile ($manifest->getContent ());

        file_put_contents ("$path/meta/layout.ini", $signedLayout);
        file_put_contents ("$path/meta/playlist.ini", $signedPlaylist);
        file_put_contents ("$path/meta/manifest.ini", $signedManifest);
        return (true);
    }

    private function _signFile ($content)
    {
        $ini = Zend_Registry::getInstance()->get('Config_Ini_Licenses');

        // Escape single quotes
        $content = str_replace ("'", "'\''", $content);

        // Create the content file
        $cmd = "echo -n '$content' | "
                ."/usr/bin/openssl smime -sign -inkey '".$ini->key."' "
                ."-signer '".$ini->cert."'";

        $signedContent = $this->_execute ($cmd);
        return ($signedContent);
    }

    protected function _execute ($cmd)
    {
        $result = "";
        $f = popen ("$cmd", 'r');
        while (($line = fgets ($f, 512)))
            $result .= $line;
        if (pclose ($f) != 0)
            return (NULL);

        return (trim ($result));
    }
}
