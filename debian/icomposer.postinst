#!/bin/bash
# postinst script for ipreso.icomposer
#

set -e

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <postinst> `abort-remove'
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

function getCurrentHostname
{
    echo -n "Getting Napafilia's hostname... "
    export HOST=$( hostname -f | sed -r 's/^(.*ipreso\.com).*$/\1/g' )
    if [ -z "${HOST}" ] ; then
        echo "Failed"
        exit 1
    else
        echo "${HOST}"
    fi

    echo -n "Getting current certificate CommonName... "
    export CN=$( find /etc/napafilia/ssl/ -name \*.ipreso.com.pem | \
                    head -n1 | \
                    xargs openssl x509 -noout -subject \
                        -nameopt multiline \
                        -in | \
                    grep -i commonname | \
                    awk '{print $3}' )
    if [ -z "${CN}" ] ; then
        echo "Failed"
        exit 1
    else
        echo "${CN}"
    fi

    if [ "${HOST}" != "${CN}" ] ; then
        echo "Error: Hostname and CommonName are different."
        exit 1
    fi
}

function configureNtp
{
    echo -n "Checking NTP configuration... "
    LINE=$( grep -E '^server.*ipreso.com.*' /etc/ntp.conf || echo "" )
    if [ -z "${LINE}" ] ; then
        echo "not configured."
        echo -n "Configuring NTP to use ntp.ipreso.com... "
        echo 'server ntp.ipreso.com iburst' >> /etc/ntp.conf
        systemctl reload-or-restart ntp &>/dev/null
    fi
    echo "Ok"
}

function configureRngTools
{
    # Rng-tools is used to generate entropy.
    # Stretch needs it to quickly start (without keyboard input)
    echo -n "Checking entropy generator... "
    LINE=$( grep -E '^HRNGDEVICE' /etc/default/rng-tools || echo "" )
    if [ -z "${LINE}" ] ; then
        echo "not configured"
        echo -n "Configure and Restart Rng-Tools... "
        echo "HRNGDEVICE=/dev/urandom" | tee -a /etc/default/rng-tools &>/dev/null
        service rng-tools status &>/dev/null || service rng-tools start &>/dev/null
    fi
    echo "Ok"
}

function configureLogs
{
    # Configure Players' logs reception
    RES=$( grep XXXXX /etc/rsyslog.d/*conf &>/dev/null || echo $? )
    if [ "${RES}" != "1" ] ; then
        # Logs are not configured
        # - Replace hostname of the iPlayers virtualhost
        echo -n "Configure Rsyslog certificates with ${CN}... "
        sed -ri "s/XXXXX/${CN}/g" /etc/rsyslog.d/*.conf &>/dev/null
        echo "Ok"

        echo -n "Reload Rsyslog configuration... "
        systemctl reload-or-restart rsyslog &>/dev/null
        echo "Ok"
    fi
    
    return
}

function setPHPVariable
{
    if [ -z "$1" -o -z "$2" ] ; then
        return;
    fi

    sed -i -r "s/^;?$1.*$/$1 = $2/g" /etc/php/7.0/apache2/php.ini
}

function configurePhp
{
    echo -n "Configuring PHP variables... "
    setPHPVariable max_execution_time 600
    setPHPVariable post_max_size 1000M
    setPHPVariable upload_max_filesize 1000M
    setPHPVariable session.gc_maxlifetime 5400
    echo "Ok"
}

function configureDB
{
    # Get DB, User and Password
    CONFIG="/usr/share/icomposer/www/application/config.ini"
    export DB_NAME=$( grep -E '^params.dbname' ${CONFIG} | \
                        head -n1 | awk '{print $3}' )
    export DB_USER=$( grep -E '^params.username' ${CONFIG} | \
                        head -n1 | awk '{print $3}' )
    export DB_PASS=$( grep -E '^params.password' ${CONFIG} | \
                        head -n1 | awk '{print $3}' )

    # Process/Check initial installation
    echo "Checking Database..."
    configureDBInitialPermissions

    return
}

function configureDBInitialPermissions
{
    RES=$( mysql -u root -D "${DB_NAME}" -e 'show tables' &>/dev/null || \
            echo $? )

    if [ -n "${RES}" ] ; then
        # Mysql command returns an error
        # It's the default installation, without iComposer DB
        createDB
    fi
}

function createDB
{
    # Secure MariaDB installation
    echo "- Secure database"
    secureDB

    # Create iComposer database
    echo "- Create ${DB_NAME} database"
    mysqlRootCmd "CREATE DATABASE ${DB_NAME}"

    QUERY="GRANT ALL PRIVILEGES ON ${DB_NAME}.* "
    QUERY+="TO '${DB_USER}'@'localhost' IDENTIFIED BY '${DB_PASS}'"
    mysqlRootCmd "${QUERY}"

    mysqlRootCmd "FLUSH PRIVILEGES"

    # Create iComposer tables
    echo "- Importing default data"
    RES=$( mysql -u "${DB_USER}" --password="${DB_PASS}" -D "${DB_NAME}" \
        < /usr/share/icomposer/www/application/database.sql 2>&1 )
}

function secureDB
{
    # Secure MariaDB installation, based on script mysql_secure_installation

    # - Delete anonymous users
    mysqlRootCmd "DELETE FROM mysql.user WHERE User='';"

    # - Only allow local root access
    QUERY="DELETE FROM mysql.user WHERE "
    QUERY+="User='root' AND "
    QUERY+="Host NOT IN ('localhost', '127.0.0.1', '::1');"
    mysqlRootCmd "${QUERY}"


    # - Drop test databases
    mysqlRootCmd "DROP DATABASE IF EXISTS test;"
    mysqlRootCmd "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"

    # - Flush privileges
    mysqlRootCmd "FLUSH PRIVILEGES;"
}

function mysqlRootCmd
{
    # Execute query as root
    RES=$( mysql -u root -e "$1" 2>&1 )
}

function mysqlUserCmd
{
    # Execute query as root
    RES=$( mysql -u "${DB_USER}" --password="${DB_PASS}" -e "$1" 2>&1 )
}

function configureWebsite
{
    RESTART=0
    RELOAD=0

    echo "Disabing unexpected website... "
    # Disable all websites except ours
    for WEBSITE in $( ls -1 /etc/apache2/sites-enabled/ | \
                        grep -v "icomposer-" ) ; do
        echo -n "${WEBSITE}... "
        RES=$( a2dissite "${WEBSITE}" 2>&1 )
        echo "Ok"
        RELOAD=1
    done

    # Customize ServerName with the expected certificate
    echo -n "Getting current Hostname... "
    if [ -z "${HOST}" ] ; then
        echo "Failed"
        exit 1
    else
        echo "${HOST}"
    fi

    # Enable some Apache mods
    if [ ! -f "/etc/apache2/mods-enabled/ssl.load" ] ; then
        # - Enable ssl
        echo -n "Enabling Apache module SSL... "
        RES=$( a2enmod ssl 2>&1 )
        echo "Ok"
        RESTART=1
    fi
    if [ ! -f "/etc/apache2/mods-enabled/headers.load" ] ; then
        # - Enable headers
        echo -n "Enabling Apache module Headers... "
        RES=$( a2enmod headers 2>&1 )
        echo "Ok"
        RESTART=1
    fi
    if [ ! -f "/etc/apache2/mods-enabled/rewrite.load" ] ; then
        # - Enable rewrite
        echo -n "Enabling Apache module Rewrite... "
        RES=$( a2enmod rewrite 2>&1 )
        echo "Ok"
        RESTART=1
    fi

    # Replace hostname of the Website virtualhost
    echo -n "Customize Website VirtualHosts... "
    sed -ri "s/XXXXX/${HOST}/g" /etc/apache2/sites-available/icomposer-website.conf &>/dev/null
    # Replace hostname of the iPlayers virtualhost
    sed -ri "s/XXXXX/${CN}/g" /etc/apache2/sites-available/icomposer-players.conf &>/dev/null
    echo "Ok"

    if [ ! -f "/etc/apache2/sites-enabled/icomposer-website.conf" ] ; then
        echo -n "Enabling public iComposer website... "
        RES=$( a2ensite "icomposer-website.conf" 2>&1 )
        echo "Ok"
        RELOAD=1
    fi
    if [ ! -f "/etc/apache2/sites-enabled/icomposer-players.conf" ] ; then
        echo -n "Enabling private iPlayers website... "
        RES=$( a2ensite "icomposer-players.conf" 2>&1 )
        echo "Ok"
        RELOAD=1
    fi

    # Customize website configuration
    echo -n "Customize Website configuration... "
    sed -ri "s/XXXXX/${HOST}/g" /usr/share/icomposer/www/application/config.ini &>/dev/null
    echo "Ok"

    if [ "${RESTART}" -gt "0" ] ; then
        echo -n "Restarting Apache2 service... "
        RES=$( systemctl restart apache2.service 2>&1 )
        echo "Ok"
    elif [ "${RELOAD}" -gt "0" ] ; then
        echo -n "Reloading Apache2 service... "
        RES=$( systemctl reload apache2.service 2>&1 )
        echo "Ok"
    fi
}

configureLibrary () {

    if [ -z "$1" ] ; then
        return 1
    fi

    LIBRARY=$1

    mkdir -p ${LIBRARY}

    # Set correct permissions
    chown -R www-data "${LIBRARY}"
}

configureKeyring () {

    if [ -z "$1" ] ; then
        return 1
    fi

    LIBRARY=$1

    mkdir -p ${LIBRARY}

    # Set correct permissions
    chown -R www-data "${LIBRARY}"
}

configureSandbox () {

    if [ -z "$1" ] ; then
        return 1
    fi

    SANDBOX_FILE=$1
    SANDBOX_DIR=$( dirname "${SANDBOX_FILE}" )
    SANDBOX_MOUNT="${SANDBOX_DIR}/icomposer.sandbox"

    # Check if Sandbox already exists or not
    if [ -f "${SANDBOX_FILE}" ] ; then
        echo -n "Sandbox already exists. Cleaning it... "
        rm -rf ${SANDBOX_MOUNT}/*
        echo "Ok"
        return 0
    fi

    # Create the sandbox file (1GB)
    echo -n "Creating new Sandbox file... "
    dd if=/dev/zero of="${SANDBOX_FILE}" count=1048576 bs=1024 &>/dev/null
    if [ "$?" != "0" ] ; then
        return 1
    fi
    echo "Ok"

    # Format the file
    echo -n "Formatting Sandbox partition... "
    mkfs.ext3 -Fq "${SANDBOX_FILE}" &>/dev/null
    if [ "$?" != "0" ] ; then
        return 1
    fi
    echo "Ok"

    # Create the mount point
    echo -n "Creating Sandbox mount point... "
    if [ -d "${SANDBOX_MOUNT}" ] ; then
        rm -rf "${SANDBOX_MOUNT}"
    fi
    mkdir -p "${SANDBOX_MOUNT}"
    echo "Ok"

    # Enable automatic mount
    echo -n "Enabling default Sandbox mount... "
    echo "${SANDBOX_FILE} ${SANDBOX_MOUNT} ext3 defaults,user 0 2" >> /etc/fstab
    echo "Ok"

    # Mount the file
    echo -n "Mounting Sandbox partition... "
    mount "${SANDBOX_MOUNT}"
    if [ "$?" != "0" ] ; then
        return 1
    fi
    echo "Ok"

    # Set correct permissions
    chown -R www-data:www-data "${SANDBOX_MOUNT}"

    return 0
}

case "$1" in
    configure)
        getCurrentHostname
        configureNtp
        configureRngTools
        configureLogs
        configurePhp
        configureSandbox /var/cache/icomposer.ext3
        configureLibrary /srv/icomposer/library
        configureKeyring /srv/icomposer/players
        configureDB
        configureWebsite
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
