#
# Regular cron jobs for the icomposer package
#
#
PATH=/sbin:/bin:/usr/sbin:/usr/bin

# Packages upgrade and plugins download
*/15 * * * *    root    apt-get update && apt-get upgrade -y && icomposer-plugins

# NTP Sync
*/5  * * * *    root    icomposer-ntp
