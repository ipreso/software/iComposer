# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# *****************************************************************************
#  Description : Makefile to build iPlayer project.
#  Auteur      : Marc Simonetti
# *****************************************************************************

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

all:
	@echo "Nothing to build."

install:
	# Copy additional scripts
	mkdir -p $(DESTDIR)/usr/bin
	cp bin/iComposer-pre $(DESTDIR)/usr/bin/
	cp bin/icomposer-ntp $(DESTDIR)/usr/bin/
	cp bin/icomposer-plugins $(DESTDIR)/usr/bin/

clean:
	@echo "Nothing to clean."
