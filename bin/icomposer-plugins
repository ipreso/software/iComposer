#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====

###############################################################################
# Variables

# Get the hostname, without the ".novalocal" added by OpenStack
HOST=$( hostname -f | sed -r 's/^(.*ipreso\.com).*$/\1/g' )

CERT=/etc/napafilia/ssl/${HOST}.pem
COMPOSER_CONF=/opt/iComposer/var/www/application/config.ini
PLUGINS_SERVER="stretch.packages.ipreso.com"
PLUGINS_PROTOCOL="https"

###############################################################################
# Functions

function failure
{
    echo "ERROR: $1"
    exit 1
}

function initVariables () {

    COMMONNAME=$( openssl x509 -noout -subject \
                        -nameopt multiline \
                        -in ${CERT} | \
                    grep -i commonname | \
                    awk '{print $3}' )
}

function getPluginsList
{
    # Check certificates
    if [ ! -f "${CERT}" ];
    then
        echo "ERROR: no valid certificate."
        exit 1
    fi

    LIST=$( wget -q -t1 -T5 -O- \
                --certificate="${CERT}" \
    ${PLUGINS_PROTOCOL}://${PLUGINS_SERVER}/icomposer/${COMMONNAME}.list )

    return $?
}

function installPlugins
{
    echo "${LIST}" | while IFS= read -r LINE ; do

        # Do not parse empty strings or commented lines
        if [ -z "${LINE}" -o "${LINE:0:1}" == "#" ] ; then
            continue
        fi
        apt-get install -y $LINE
    done
}

###############################################################################
# Main procedure

# Initialize variables
initVariables || failure "Unable to initialize variables."

# Get list of plugins to install
getPluginsList || failure "Unable to get Plugins list."

# Install listed plugins
installPlugins || failure "Unable to install plugins."

exit 0
